package com.sheep.hub;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sheep.framework.db.AfeiDb;
import com.sheep.framework.util.ThreadPoolManager;
import com.sheep.hub.adapter.MyPagerAdapter;
import com.sheep.hub.bean.HitchSheet;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.Preference;

public class Logo_BackUPActivity extends BaseActivity {

    private TextView tv_init;

    private ViewPager vp_guide;
    private ArrayList<View> views;

    private boolean isFirst;
    private boolean isGuideFinished;
    private boolean isCopyFinished;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        tv_init = (TextView) findViewById(R.id.tv_init);

        boolean isCopyHitch = Preference.getBoolean(Constant.K_COPY_HITCH, false);
        if (!isCopyHitch) {
            copyHitch();
        }
        
        if (isFirst = Preference.getBoolean(Constant.IS_LOGO_FIRST, true)) {
           // initViewPager();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    launch(MainActivity.class);
                    finish();
                }
            }, 1000);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    launch(MainActivity.class);
                    finish();
                }
            }, 1000);
        }
    }

    private void initViewPager() {
        vp_guide = (ViewPager) findViewById(R.id.vp_guide);
        vp_guide.setVisibility(View.VISIBLE);
        views = new ArrayList<View>();
        int[] drawableIds = new int[]{R.drawable.img_guide01, R.drawable.img_guide02, R.drawable.img_guide03, R.drawable.img_guide04};
        for (int i = 0; i < 4; i++) {
            View view = View.inflate(this, R.layout.view_logo_guide, null);
            ImageView iv_guide = (ImageView) view.findViewById(R.id.iv_guide);
            iv_guide.setImageResource(drawableIds[i]);
            if (3 == i) {
                Button btn_ok = (Button) view.findViewById(R.id.btn_ok);
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isGuideFinished = true;
                        if (isCopyFinished) {
                            handler.sendEmptyMessage(0);
                        } else {
                            Toast.makeText(Logo_BackUPActivity.this, R.string.tip_app_init_0, Toast.LENGTH_SHORT).show();
                            vp_guide.setVisibility(View.GONE);
                        }
                    }
                });
            }
            views.add(view);
        }
        vp_guide.setAdapter(new MyPagerAdapter(views));
        Preference.putBoolean(Constant.IS_LOGO_FIRST,false);
    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (!isFirst || (isFirst && isGuideFinished)) {
                        launch(MainActivity.class);
                        finish();
                    }
                    break;
                case 1:
                    int count = (Integer) msg.obj;
                    int modulo = count % 6;
                    if (0 == modulo) {
                        tv_init.setText(getString(R.string.tip_app_init_0));
                    } else if (1 == modulo) {
                        tv_init.setText(getString(R.string.tip_app_init_1));
                    } else if (2 == modulo) {
                        tv_init.setText(getString(R.string.tip_app_init_2));
                    } else if (3 == modulo) {
                        tv_init.setText(getString(R.string.tip_app_init_3));
                    } else if (4 == modulo) {
                        tv_init.setText(getString(R.string.tip_app_init_4));
                    } else if (5 == modulo) {
                        tv_init.setText(getString(R.string.tip_app_init_5));
                    }
                    //加载中...
                    break;
                default:
                    break;
            }

        }

    };

    private void copyHitch() {
        Runnable r = new Runnable() {

            @Override
            public void run() {
                InputStream is = getResources().openRawResource(R.raw.hitch);
                //InputStream is = am.open("hitch.xls");
                Workbook workbook;
                try {
                    workbook = Workbook.getWorkbook(is);
                    Sheet sheet = workbook.getSheet(0);
                    int columns = sheet.getColumns();
                    int rows = sheet.getRows();
                    AfeiDb afeiDb = HubApp.getInstance().getAfeiDb();
                    if (rows > 1) { //忽略掉标题
                        Runnable initTextRunnable = new Runnable() {
                            private int count = 0;

                            @Override
                            public void run() {
                                Message msg = handler.obtainMessage();
                                msg.obj = count++;
                                msg.what = 1;
                                handler.sendMessage(msg);
                                handler.postDelayed(this, 500);
                            }
                        };
                        handler.postDelayed(initTextRunnable, 500);

                        for (int i = 1; i < rows; i++) {
                            HitchSheet hs = new HitchSheet();
                            StringBuffer sb = new StringBuffer();
                            for (int j = 0; j < columns; j++) {
                                String contents = sheet.getCell(j, i).getContents();
                                contents = contents.replace("'", "");
                                sb.append(contents + " ");
                                if (0 == j) { //第一个 一级类型
                                    hs.setType(contents);
                                } else if (1 == j) { //故障码
                                    hs.setCode(contents);
                                } else if (2 == j) { //是否严重
                                    if (!TextUtils.isEmpty(contents)) {
                                        hs.setIsSerious(contents.toUpperCase());
                                    }
                                } else if (3 == j) { //中文定义 名称
                                    hs.setName(contents);
                                } else if (4 == j) { //英文定义 名称
                                    hs.setEnglishName(contents);
                                } else if (5 == j) { //范畴
                                    hs.setCategory(contents);
                                } else if (6 == j) { //背景知识
                                    hs.setDetail(contents);
                                }
                            }
                            afeiDb.save(hs);
                        }
                        handler.removeCallbacks(initTextRunnable);
                    }
                } catch (BiffException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Preference.putBoolean(Constant.K_COPY_HITCH, true);
                isCopyFinished = true;
                handler.sendEmptyMessage(0);
            }
        };

        ThreadPoolManager.getInstance().executeTask(r);
    }
}
