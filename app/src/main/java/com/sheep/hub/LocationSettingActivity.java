package com.sheep.hub;/*
  */

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.maps.model.LatLng;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.help.Inputtips;
import com.amap.api.services.help.Tip;
import com.sheep.framework.util.Density;
import com.sheep.framework.view.KeyOrHandSetEditText;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.hub.adapter.SimpleTextAdapter;
import com.sheep.hub.bean.LocalLocation;
import com.sheep.hub.bean.SearchHistory;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.dialog.DropPopupWindow;
import com.sheep.hub.dialog.RouteSearchPoiDialog;
import com.sheep.hub.util.LocationUtil;
import com.sheep.hub.util.PoiSearchHelper;

import java.util.ArrayList;
import java.util.List;

public class LocationSettingActivity extends BaseActivity implements TextWatcher, View.OnClickListener {

    private static final int MARKER_REQ = 0;
    public static final String IS_HOME_SETTING = "is_home_setting";

    private KeyOrHandSetEditText et_poi;
    private DropPopupWindow dropPopupWindow;
    private Inputtips inputTips;

    private boolean hasGetMyLocation;
    private PoiSearchHelper poiSearchHelper;
    private RouteSearchPoiDialog poiSearchDialog;
    private String dbRowName;

    private DropPopupWindow spinnerDropPopupWindow;
    private TextView tv_store;
    private ArrayList<LocalLocation> storeList;
    private Button btn_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_setting);
        boolean isHome = getIntent().getBooleanExtra(IS_HOME_SETTING, false);
        dbRowName = isHome ? Constant.ROW_HOME_NAME : Constant.ROW_COMPANY_NAME;
        initTitle(getString(R.string.set_location, isHome ? getString(R.string.family) : getString(R.string.company)));
        initView();

        hasGetMyLocation = (null == HubApp.getInstance().getMyLocation());

        LocationUtil.requestMyLocation(this, new LocationUtil.GetLocationSuccessListener() {
            @Override
            public void onSuccess(AMapLocation aMapLocation) {
                hasGetMyLocation = true;
            }
        });
    }

    private void initView() {
        final Button btn_next = (Button) findViewById(R.id.btn_next);
        btn_next.setText(null == HubApp.getInstance().getMyLocation() ? getString(R.string.china) : HubApp.getInstance().getMyLocation().getCity());
        btn_next.setVisibility(View.VISIBLE);
        et_poi = (KeyOrHandSetEditText) findViewById(R.id.et_poi);
        tv_store = (TextView) findViewById(R.id.tv_store);

        storeList = HubApp.getInstance().getAfeiDb().findAllByWhereStr(LocalLocation.class, "rowName = '" + Constant.ROW_STORE_NAME + "'");
        tv_store.setText(getString(R.string.store_location) + "(" + (null == storeList ? 0 : storeList.size()) + ")");

        btn_search = (Button) findViewById(R.id.btn_search);
        btn_search.setOnClickListener(this);
        findViewById(R.id.ll_current).setOnClickListener(this);
        findViewById(R.id.ll_marker).setOnClickListener(this);
        findViewById(R.id.ll_store).setOnClickListener(this);
        et_poi.addTextChangedListener(this);

        LocationUtil.requestMyLocation(this, new LocationUtil.GetLocationSuccessListener() {
            @Override
            public void onSuccess(AMapLocation aMapLocation) {
                btn_next.setText(aMapLocation.getCity());
            }
        });
    }


    private void searchPoiTips() {
        if (null == inputTips) {
            inputTips = new Inputtips(LocationSettingActivity.this,
                    new Inputtips.InputtipsListener() {

                        @Override
                        public void onGetInputtips(List<Tip> tipList, int rCode) {
                            if (rCode == 0) {
                                ArrayList<Tip> tempList = new ArrayList<Tip>();
                                tempList.addAll(tipList);
                                dropPopupWindow.setList(tempList);
                                if (!dropPopupWindow.isShowing()) {
                                    if(!LocationSettingActivity.this.isFinishing()) {
                                        dropPopupWindow.show(et_poi, -20, 0);
                                    }
                                }
                            }
                        }
                    });
        }
        try {
            inputTips.requestInputtips(et_poi.getText().toString().trim(), null == HubApp.getInstance().getMyLocation() ? "" : HubApp.getInstance().getMyLocation().getCityCode());// 第一个参数表示提示关键字，第二个参数默认代表全国，也可以为城市区号
        } catch (AMapException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_search:
                poiSearch();
                break;
            case R.id.ll_marker:
                launch(MarkerMapActivity.class, MARKER_REQ);
                break;
            case R.id.ll_current:
                if (!hasGetMyLocation) {
                    Toast.makeText(this, R.string.location_fail, Toast.LENGTH_SHORT).show();
                    return;
                }
                LocationUtil.getAddressByLatLonPoint(this, new LatLonPoint(HubApp.getInstance().getMyLocation().getLatitude(), HubApp.getInstance().getMyLocation().getLongitude()),
                        new LocationUtil.GetAddressSuccessListener() {

                            @Override
                            public void onSuccess(String address) {
                                et_poi.setHandText(address);
                                setResult(HubApp.getInstance().getMyLocation().getLatitude(), HubApp.getInstance().getMyLocation().getLongitude(), address);
                            }

                            @Override
                            public void onFail() {
                                et_poi.setHandText(getString(R.string.unknown_road));
                                setResult(HubApp.getInstance().getMyLocation().getLatitude(), HubApp.getInstance().getMyLocation().getLongitude(), getString(R.string.unknown_road));
                            }
                        });
                break;
            case R.id.ll_store:
                if (null == storeList || storeList.size() <= 0) {
                    Toast.makeText(this, R.string.has_not_store_data, Toast.LENGTH_SHORT).show();
                } else {
                    if (null == spinnerDropPopupWindow) {
                        int size = storeList.size();
                        int listHeight = Density.of(this, 48) * (size > 8 ? 8 : size)+7;
                        spinnerDropPopupWindow = new DropPopupWindow(LocationSettingActivity.this, v.getWidth(), listHeight,false);
                        SimpleTextAdapter<LocalLocation> localLocationSimpleTextAdapter = new SimpleTextAdapter<LocalLocation>(LocationSettingActivity.this, new SimpleTextAdapter.OnItemClickListener() {

                            @Override
                            public void OnItemClick(int position) {
                                LocalLocation localLocation = (LocalLocation) spinnerDropPopupWindow.getAdapter().getItem(position);
                                HubApp.getInstance().getAfeiDb().deleteById(localLocation.getClass(), localLocation.get_id());
                                setResult(localLocation.getLatitude(), localLocation.getLongitude(), localLocation.getName());
                                spinnerDropPopupWindow.dismiss();
                            }
                        }) {
                            @Override
                            public void setTextView(TextView textView, LocalLocation localLocation) {
                                textView.setText(localLocation.getName());
                            }
                        };
                        localLocationSimpleTextAdapter.setList(storeList);
                        spinnerDropPopupWindow.setAdapter(localLocationSimpleTextAdapter);
                    }
                    spinnerDropPopupWindow.show(v, 0, 0);
                }
                break;
            default:
        }
    }

    private void setResult(double lat, double lon, String name) {
        LocalLocation localLocation = new LocalLocation();
        localLocation.set_id("" + System.currentTimeMillis());
        localLocation.setRowName(dbRowName);
        localLocation.setLatitude(lat);
        localLocation.setLongitude(lon);
        localLocation.setName(LocationUtil.getSimpleAddress(name));
        HubApp.getInstance().getAfeiDb().deleteByWhereStr(LocalLocation.class, "rowName = '" + dbRowName + "'");
        HubApp.getInstance().getAfeiDb().save(localLocation);
        Intent result = new Intent();
        result.putExtra("data", localLocation);
        result.putExtra("title",""+localLocation.getName());
        setResult(RESULT_OK, result);
        Toast.makeText(this, R.string.setting_success, Toast.LENGTH_SHORT).show();
        finish();
    }

    private void setResultWithoutSave(LocalLocation localLocation) {
        Intent result = new Intent();
        result.putExtra("data", localLocation);
        result.putExtra("title",""+localLocation.getName());
        setResult(RESULT_OK, result);
        Toast.makeText(this, R.string.setting_success, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode && MARKER_REQ == requestCode) {
            LatLng latLng = data.getParcelableExtra("data");
            et_poi.setText(data.getStringExtra("title"));
            setResult(latLng.latitude, latLng.longitude, et_poi.getText().toString());
        }
    }

    private void poiSearch() {
        if (null == poiSearchHelper) {
            poiSearchHelper = new PoiSearchHelper(this, new PoiSearchHelper.PoiQuerySuccessListener() {
                @Override
                public void onSuccess(ArrayList<PoiItem> poiItemList) {
                    if (null == poiSearchDialog || !poiSearchDialog.isShowing()) {
                        poiSearchDialog = new RouteSearchPoiDialog(LocationSettingActivity.this);
                        poiSearchDialog.setTitle(R.string.choose_poi);
                        poiSearchDialog.setOnListClickListener(new RouteSearchPoiDialog.OnListItemClick() {
                            @Override
                            public void onListItemClick(
                                    RouteSearchPoiDialog dialog, PoiItem endPoiItem) {
                                SearchHistory history = new SearchHistory();
                                history.set_id("" + System.currentTimeMillis());
                                history.setKey(et_poi.getText().toString().trim());
                                if (!HubApp.getInstance().getAfeiDb().findAll(SearchHistory.class).contains(history)) {
                                    HubApp.getInstance().getAfeiDb().save(history);
                                }
                                setResult(endPoiItem.getLatLonPoint().getLatitude(), endPoiItem.getLatLonPoint().getLongitude(),
                                        TextUtils.isEmpty(endPoiItem.getSnippet()) ? (TextUtils.isEmpty(endPoiItem.getTitle()) ? et_poi.getText().toString() : endPoiItem.getTitle()) : endPoiItem.getSnippet());
                            }
                        });
                        poiSearchDialog.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2() {
                            @Override
                            public void onPullDownToRefresh(PullToRefreshBase refreshView) {

                            }

                            @Override
                            public void onPullUpToRefresh(PullToRefreshBase refreshView) {
                                poiSearchHelper.queryNext();
                            }
                        });
                    }
                    if (!poiSearchDialog.isShowing()) {
                        poiSearchDialog.show();
                    }
                    poiSearchDialog.onRefreshComplete();
                    poiSearchDialog.addList(poiItemList);
                    if (poiItemList.size() != Constant.PAGE_SIZE) {
                        poiSearchDialog.setMode(PullToRefreshBase.Mode.DISABLED);
                    }
                }

                @Override
                public void onFail() {

                }
            });
        }
        poiSearchHelper.setPoiKey(et_poi.getText().toString().trim());
        poiSearchHelper.query(true);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (null == dropPopupWindow) {
            dropPopupWindow = new DropPopupWindow(LocationSettingActivity.this, et_poi.getWidth() + 30, Density.of(LocationSettingActivity.this, 300));
            dropPopupWindow.setAdapter(new SimpleTextAdapter<Tip>(LocationSettingActivity.this, new SimpleTextAdapter.OnItemClickListener() {

                @Override
                public void OnItemClick(int position) {
                    et_poi.setHandText(((SimpleTextAdapter<Tip>) dropPopupWindow.getAdapter()).getItem(position).getName());
                    et_poi.setSelection(et_poi.getText().length());
                    dropPopupWindow.dismiss();
                    btn_search.performClick();
                }
            }) {
                @Override
                public void setTextView(TextView textView, Tip tip) {
                    textView.setText(tip.getName());
                }
            });
        }
        if (!TextUtils.isEmpty(s.toString().trim()) && !et_poi.isHandSet()) {
            searchPoiTips();
        } else if (dropPopupWindow.isShowing()) {
            dropPopupWindow.dismiss();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
