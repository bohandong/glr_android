package com.sheep.hub;/*
 */

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.amap.api.maps.AMap;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.services.core.LatLonPoint;
import com.sheep.hub.util.LocationUtil;

public class MarkerMapActivity extends BaseMapActivity implements AMap.OnMapClickListener, View.OnClickListener {

    private Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maker_map);
        initTitle(R.string.marker);
        initView(savedInstanceState);
    }

    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        Button btn_next = (Button) findViewById(R.id.btn_next);
        btn_next.setText(R.string.positive);
        btn_next.setVisibility(View.VISIBLE);

        aMap.setOnMapClickListener(this);
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (null == marker) {
            marker = aMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                            .decodeResource(getResources(), R.drawable.icon_marker))));
            marker.setDraggable(true);
        }
        marker.setPosition(latLng);
    }

    @Override
    public void onClick(View v) {
        if (null == marker || null == marker.getPosition()) {
            Toast.makeText(this, R.string.no_marker, Toast.LENGTH_SHORT).show();
        } else {
            LocationUtil.getAddressByLatLonPoint(this, new LatLonPoint(marker.getPosition().latitude, marker.getPosition().longitude), new LocationUtil.GetAddressSuccessListener() {
                @Override
                public void onSuccess(String address) {
                    Intent data = new Intent();
                    data.putExtra("data", marker.getPosition());
                    data.putExtra("title", address);
                    setResult(RESULT_OK, data);
                    finish();
                }

                @Override
                public void onFail() {
                    Intent data = new Intent();
                    data.putExtra("data", marker.getPosition());
                    data.putExtra("title", getString(R.string.unknown_road));
                    setResult(RESULT_OK, data);
                    finish();
                }
            });

        }
    }
}
