package com.sheep.hub;/*
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.maps.model.LatLng;
import com.sheep.framework.db.utils.DateTimeUtil;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.activity.CarHitchActivity;
import com.sheep.hub.activity.DriveGradeActivity;
import com.sheep.hub.bean.LocalLocation;
import com.sheep.hub.bean.Weather;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.LocationUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MineActivity extends BaseActivity implements View.OnClickListener {
    private static final int HOME_SETTING_REQ = 0;
    private static final int COMPANY_SETTING_REQ = 1;
    private TextView tv_home_setting;
    private TextView tv_company_setting;
    private TextView tv_home;
    private TextView tv_company;
    private long dateId;
    private Handler delayHandler;
    private FrameLayout fl_no_weather;
    private LinearLayout ll_weather;
    private ArrayList<LocalLocation> companyList;
    private ArrayList<LocalLocation> houseList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine);

        initTitle(R.string.mine);
        initView();
    }

    private void initView() {
        tv_home = (TextView) findViewById(R.id.tv_home);
        tv_company = (TextView) findViewById(R.id.tv_company);
        tv_home_setting = (TextView) findViewById(R.id.tv_home_setting);
        tv_company_setting = (TextView) findViewById(R.id.tv_company_setting);

        houseList = HubApp.getInstance().getAfeiDb().findAllByWhereStr(LocalLocation.class, " rowName= '" + Constant.ROW_HOME_NAME + "'");
        if (null != houseList && houseList.size() > 0) {
            setSimpleAddress(houseList.get(0).getName(), tv_home);
            tv_home_setting.setText(R.string.click_to_modify);
        }

        companyList = HubApp.getInstance().getAfeiDb().findAllByWhereStr(LocalLocation.class, " rowName= '" + Constant.ROW_COMPANY_NAME + "'");
        if (null != companyList & companyList.size() > 0) {
            setSimpleAddress(companyList.get(0).getName(), tv_company);
            tv_company_setting.setText(R.string.click_to_modify);
        }

        tv_home.setMovementMethod(ScrollingMovementMethod.getInstance());
        tv_company.setMovementMethod(ScrollingMovementMethod.getInstance());


        fl_no_weather = (FrameLayout) findViewById(R.id.fl_no_weather);
        ll_weather = (LinearLayout) findViewById(R.id.ll_weather);

        String area = (null == HubApp.getInstance().getMyLocation()) ? "" : HubApp.getInstance().getMyLocation().getDistrict();
        final TextView tv_lbs = ((TextView) findViewById(R.id.tv_lbs));
        if (TextUtils.isEmpty(area)) {
            LocationUtil.requestMyLocation(this, new LocationUtil.GetLocationSuccessListener() {
                @Override
                public void onSuccess(AMapLocation aMapLocation) {
                    HubApp.getInstance().setMyLocation(aMapLocation);
                    tv_lbs.setText(aMapLocation.getDistrict());
                }
            });
        } else {
            tv_lbs.setText(area);
        }

        tv_home_setting.setOnClickListener(this);
        tv_company_setting.setOnClickListener(this);
        findViewById(R.id.ll_store).setOnClickListener(this);
        findViewById(R.id.ll_score).setOnClickListener(this);
        findViewById(R.id.ll_hitch).setOnClickListener(this);
        findViewById(R.id.iv_refresh).setOnClickListener(this);
        findViewById(R.id.tv_my_home).setOnClickListener(this);
        findViewById(R.id.tv_my_company).setOnClickListener(this);
        setTime();
        new GetWeather().execute(null == HubApp.getInstance().getMyLocation() ? "南京" : HubApp.getInstance().getMyLocation().getCity());
    }


    private void setTime() {
        dateId = System.currentTimeMillis();
        delayHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    dateId += 1000;
                    ((TextView) findViewById(R.id.tv_date)).setText(DateTimeUtil.geChineseDate(dateId));
                }
            }
        };
        ((TextView) findViewById(R.id.tv_date)).setText(DateTimeUtil.geChineseDate(dateId));
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                delayHandler.sendEmptyMessage(0);
                delayHandler.postDelayed(this, 1000);
            }
        }, 1000);
    }

    private void setSimpleAddress(String address, TextView textView) {
        String desc;
        if (TextUtils.isEmpty(address)) {
            desc = getString(R.string.unknown_road);
        } else {
            String[] spit = address.split(" ");
            desc = spit[spit.length - 1];
        }
        textView.setText(getString(R.string.brackets, LocationUtil.getSimpleAddress(desc)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_home_setting:
                Intent intent = new Intent(this, LocationSettingActivity.class);
                intent.putExtra(LocationSettingActivity.IS_HOME_SETTING, true);
                launch(intent, HOME_SETTING_REQ);
                break;
            case R.id.tv_company_setting:
                Intent intent2 = new Intent(this, LocationSettingActivity.class);
                intent2.putExtra(LocationSettingActivity.IS_HOME_SETTING, false);
                launch(intent2, COMPANY_SETTING_REQ);
                break;
            case R.id.tv_my_home:
                if (houseList.size() > 0) {
                    Intent intent3 = new Intent(this, ShowLocationOnMapActivity.class);
                    intent3.putExtra(ShowLocationOnMapActivity.TITLE, getString(R.string.home));
                    LocalLocation home = houseList.get(0);
                    intent3.putExtra(ShowLocationOnMapActivity.LATLNG, new LatLng(home.getLatitude(), home.getLongitude()));
                    intent3.putExtra(ShowLocationOnMapActivity.DESC, home.getName());
                    launch(intent3);
                }
                break;
            case R.id.tv_my_company:
                if (companyList.size() > 0) {
                    Intent intent4 = new Intent(this, ShowLocationOnMapActivity.class);
                    intent4.putExtra(ShowLocationOnMapActivity.TITLE, getString(R.string.company));
                    LocalLocation company = companyList.get(0);
                    intent4.putExtra(ShowLocationOnMapActivity.LATLNG, new LatLng(company.getLatitude(), company.getLongitude()));
                    intent4.putExtra(ShowLocationOnMapActivity.DESC, company.getName());
                    launch(intent4);
                }
                break;
            case R.id.ll_store:
                ArrayList<String> storeList = new ArrayList<String>();
                ArrayList<LocalLocation> localLocations = HubApp.getInstance().getAfeiDb().findAllByWhereStr(LocalLocation.class, "rowName = '" + Constant.ROW_STORE_NAME + "'");
                for (LocalLocation localLocation : localLocations) {
                    storeList.add(localLocation.getName());
                }
                Intent intent3 = new Intent(this, CommonListActivity.class);
                intent3.putExtra(CommonListActivity.TITLE, getString(R.string.store_location));
                intent3.putExtra(CommonListActivity.TYPE, CommonListActivity.STORE_ACTIVITY_TYPE);
                intent3.putStringArrayListExtra(CommonListActivity.LIST, storeList);
                intent3.putExtra(CommonListActivity.EXTRA_LIST, localLocations);
                launch(intent3);
                break;
            case R.id.ll_score:
                launch(DriveGradeActivity.class);
                break;
            case R.id.ll_hitch:
                launch(CarHitchActivity.class);
                break;
            case R.id.iv_refresh:
                new GetWeather().execute(null == HubApp.getInstance().getMyLocation() ? "南京" : HubApp.getInstance().getMyLocation().getCity());
                break;
            default:
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case HOME_SETTING_REQ:
                    LocalLocation home = (LocalLocation) data.getSerializableExtra("data");
                    if (null == houseList) {
                        houseList = new ArrayList<LocalLocation>();
                    }
                    houseList.clear();
                    houseList.add(home);
                    tv_home.setText(getString(R.string.brackets, LocationUtil.getSimpleAddress(home.getName())));
                    tv_home_setting.setText(R.string.click_to_modify);
                    break;
                case COMPANY_SETTING_REQ:
                    LocalLocation company = (LocalLocation) data.getSerializableExtra("data");
                    if (null == companyList) {
                        companyList = new ArrayList<LocalLocation>();
                    }
                    companyList.clear();
                    companyList.add(company);
                    tv_company.setText(getString(R.string.brackets, LocationUtil.getSimpleAddress(company.getName())));
                    tv_company_setting.setText(R.string.click_to_modify);
                    break;
                default:
            }
        }
    }

    class GetWeather extends AsyncTask<String, Void, Object[]> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = CommonUtils.showProgressDialog(MineActivity.this, R.string.get_weather_ing);
        }

        @Override
        protected Object[] doInBackground(String... params) {
            Object[] objects = new Object[2];
            dateId = System.currentTimeMillis();
            try {
                String path = "http://api.map.baidu.com/telematics/v3/weather?location=" + URLEncoder.encode((null == HubApp.getInstance().getMyLocation()) ? "南京" : HubApp.getInstance().getMyLocation().getCity(), "UTF-8") + "&output=json&ak=G7gDLZimN3yo7dsqdhq2ccCh&mcode=96:D6:77:5B:DD:DD:CE:A9:19:54:FB:9C:F8:86:8C:BB:91:42:09:91;com.sheep.hub";
                URL url = new URL(path);
                URLConnection connection = url.openConnection();
                connection.connect();
                dateId = connection.getDate();
                String json = CommonUtils.readInStream(connection.getInputStream());
                JSONObject object = new JSONObject(json).getJSONArray("results").getJSONObject(0).getJSONArray("weather_data").getJSONObject(0);
                Weather weather = new Weather();
                String tempDate = object.getString("date");
                if (tempDate.length() > 4 && tempDate.endsWith("℃)")) {
                    tempDate = tempDate.substring(tempDate.length() - 4, tempDate.length() - 1);
                } else {
                    tempDate = getString(R.string.unknown);
                }
                weather.setDate(tempDate);
                weather.setWeather(object.getString("weather"));
                weather.setTemperature(object.getString("temperature"));
                weather.setWind(object.getString("wind"));
                objects[1] = weather;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            objects[0] = DateTimeUtil.geChineseDate(dateId);
            return objects;
        }

        @Override
        protected void onPostExecute(Object[] o) {
            dialog.dismiss();
            ((TextView) findViewById(R.id.tv_date)).setText((String) (o[0]));
            if (null != o[1]) {
                fl_no_weather.setVisibility(View.GONE);
                ll_weather.setVisibility(View.VISIBLE);
                Weather bean = (Weather) o[1];
                ((TextView) findViewById(R.id.tv_temperature)).setText(bean.getDate());
                ((TextView) findViewById(R.id.tv_cloud)).setText(bean.getWeather() + "   " + bean.getTemperature());
                ((TextView) findViewById(R.id.tv_wind)).setText(bean.getWind());
            } else {
                fl_no_weather.setVisibility(View.VISIBLE);
                ll_weather.setVisibility(View.GONE);
            }
        }
    }

}
