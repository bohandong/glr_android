package com.sheep.hub;/*
 */

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.TextView;

import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.model.AMapNaviGuide;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshListView;
import com.sheep.hub.adapter.SimpleTextAdapter;

import java.util.ArrayList;

public class RouteDescListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        initTitle(R.string.route_detail);
        initView();
    }

    private void initView() {
//        Button btn_next = (Button) findViewById(R.id.btn_next);
//        btn_next.setVisibility(View.VISIBLE);
//        btn_next.setText(R.string.begin_navigation);
//        btn_next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                launch(NaviActivity.class);
//            }
//        });
        PullToRefreshListView plv_list = (PullToRefreshListView) findViewById(android.R.id.list);
        plv_list.setMode(PullToRefreshBase.Mode.DISABLED);
        SimpleTextAdapter<AMapNaviGuide> adapter = new SimpleTextAdapter<AMapNaviGuide>(this, true) {
            @Override
            public void setTextView(TextView textView, AMapNaviGuide aMapNaviGuide) {
                StringBuffer buffer = new StringBuffer();
                buffer.append("" + ((Integer) textView.getTag() + 1) + ": 直行");
                buffer.append(aMapNaviGuide.getLength() + "米");
                switch (aMapNaviGuide.getIconType()) {
                    case 15:
                        buffer.append("到达目的地");
                        break;
                    case 13:
                        buffer.append("到达服务区");
                        break;
                    case 14:
                        buffer.append("到达收费站");
                        break;
                    case 16:
                        buffer.append("到达隧道");
                        break;
                    case 10:
                        buffer.append("到达途经点");
                        break;
                    case 19:
                        buffer.append("通过人行横道");
                        break;
                    case 1:
                        buffer.append("车");
                        break;
                    case 23:
                        buffer.append("到道路斜对面");
                        break;
                    case 11:
                        buffer.append("进入环岛");
                        break;
                    case 17:
                        buffer.append("靠左");
                        break;
                    case 18:
                        buffer.append("靠右");
                        break;
                    case 2:
                        buffer.append("左转");
                        break;
                    case 8:
                        buffer.append("左转掉头");
                        break;
                    case 6:
                        buffer.append("左后方");
                        break;
                    case 4:
                        buffer.append("左前方");
                        break;
                    case 12:
                        buffer.append("驶出环岛");
                        break;
                    case 20:
                        buffer.append("通过过街天桥");
                        break;
                    case 3:
                        buffer.append("右转");
                        break;
                    case 7:
                        buffer.append("右后方");
                        break;
                    case 5:
                        buffer.append("右前方");
                        break;
                    case 22:
                        buffer.append("通过广场");
                        break;
                    case 9:
                        buffer.append("直行");
                        break;
                    case 21:
                        buffer.append("通过地下通道");
                        break;
                }
                if (!TextUtils.isEmpty(aMapNaviGuide.getName())&&15!=aMapNaviGuide.getIconType()) {
                    buffer.append("进入" + aMapNaviGuide.getName());
                }
                textView.setText(buffer.toString());
            }
        };
        adapter.setList((ArrayList<AMapNaviGuide>) AMapNavi.getInstance(this).getNaviGuideList());
        plv_list.setAdapter(adapter);
    }

}
