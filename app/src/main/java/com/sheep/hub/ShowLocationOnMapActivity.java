package com.sheep.hub;/*
 */

import android.os.Bundle;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;

public class ShowLocationOnMapActivity extends BaseMapActivity  implements AMap.OnMapLoadedListener{

    public static final String TITLE="title";
    public static final String LATLNG="latlng";
    public static final String DESC="desc";
    private LatLng location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maker_map);
        initTitle("" + getIntent().getStringExtra(TITLE));
        initView(savedInstanceState);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        MarkerOptions options=new MarkerOptions();
        location= getIntent().getParcelableExtra(LATLNG);
        options.title(getIntent().getStringExtra(DESC));
        options.position(location);
        aMap.addMarker(options);
        aMap.setOnMapLoadedListener(this);
    }

    @Override
    public void onMapLoaded() {
        aMap.moveCamera(CameraUpdateFactory.changeLatLng(location));
    }
}
