package com.sheep.hub.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sheep.hub.R;
import com.sheep.hub.bluetooth.BluetoothManager;
import com.sheep.hub.bluetooth.IBluetoothListener;


public class BluetoothDiscoveringActivity extends BaseActivity {

	private ProgressBar pb_scanning;
	private ListView lv_paired_devices;
	private ListView lv_new_devices;
	private TextView tv_title_paired_device;
	private TextView tv_title_new_device;
	private LinearLayout ll_scaning;
	private ImageView iv_gap;
	
	
	private List<BluetoothTermailDevice> pairedDevices;
	private List<BluetoothTermailDevice> newDevices;
	
	// Return Intent extra
	public static String EXTRA_DEVICE_ADDRESS = "device_address";

	private DiscoveringDeviceAdapter newDevAdapter;
	private DiscoveringDeviceAdapter pairedDevAdapter;
	
	@Override
	protected void loadView() {
		setContentView(R.layout.activity_discoveringdevice);
	}

	@Override
	protected void findView() {
		pb_scanning = (ProgressBar) findViewById(R.id.pb_scanning);
		lv_paired_devices = (ListView) findViewById(R.id.lv_paired_devices);
		lv_new_devices = (ListView) findViewById(R.id.lv_new_devices);
		
		
		ll_scaning = (LinearLayout) findViewById(R.id.ll_scaning);
		tv_title_paired_device = (TextView)findViewById(R.id.tv_title_paired_device);
		tv_title_new_device = (TextView)findViewById(R.id.tv_title_new_device);
		iv_gap = (ImageView)findViewById(R.id.iv_gap);
	}

	@Override
	protected void setListener() {
		lv_paired_devices.setOnItemClickListener(bluetoothDeviClickListener);
		lv_new_devices.setOnItemClickListener(bluetoothDeviClickListener);
	}

	@Override
	protected void doLogic() {
		pairedDevices = new ArrayList<BluetoothTermailDevice>();
		newDevices = new ArrayList<BluetoothTermailDevice>();
		
		BluetoothManager manager = BluetoothManager.getInstance();
		manager.addListener(new IBluetoothListener() {
			@Override
			public void onStateChange(int state,int what) {
				switch (state) {
				case BluetoothManager.STATE_DISCOVERED:
					ll_scaning.setVisibility(View.GONE);
					if(newDevices.size()<=0){
						iv_gap.setVisibility(View.GONE);
						lv_new_devices.setVisibility(View.GONE);
						tv_title_new_device.setVisibility(View.GONE);
					}
					
					if(pairedDevices.size()<=0){
						iv_gap.setVisibility(View.GONE);
						lv_paired_devices.setVisibility(View.GONE);
						tv_title_paired_device.setVisibility(View.GONE);
					}
					break;

				default:
					break;
				}
			}
			
			@Override
			public void onScanningDevice(String name, String address,int bondState) {
				if (bondState != BluetoothDevice.BOND_BONDED) {
					newDevices.add(new BluetoothTermailDevice(name, address));
					if(null == newDevAdapter){
						newDevAdapter = new DiscoveringDeviceAdapter(newDevices);
						lv_new_devices.setAdapter(newDevAdapter);
					}else{
						newDevAdapter.setDevices(newDevices);
						newDevAdapter.notifyDataSetChanged();
					}
				}else{
					pairedDevices.add(new BluetoothTermailDevice(name, address));
					if(null == pairedDevAdapter){
						pairedDevAdapter = new DiscoveringDeviceAdapter(pairedDevices);
						lv_paired_devices.setAdapter(pairedDevAdapter);
					}else{
						pairedDevAdapter.setDevices(pairedDevices);
						pairedDevAdapter.notifyDataSetChanged();
					}
				}
			}
		});
		
		manager.enableBluetooth();
	}
	
	private OnItemClickListener bluetoothDeviClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			BluetoothTermailDevice dev = (BluetoothTermailDevice) parent.getItemAtPosition(position);
			// Create the result Intent and include the MAC address
			Intent intent = new Intent();
			intent.putExtra(EXTRA_DEVICE_ADDRESS, dev.getAddress());

			// Set result and finish this Activity
			setResult(Activity.RESULT_OK, intent);
			finish();
		}
		
	};
	
	private class DiscoveringDeviceAdapter extends BaseAdapter{

		private List<BluetoothTermailDevice> devices;
		
		public DiscoveringDeviceAdapter(List<BluetoothTermailDevice> devices){
			this.devices = devices;
		}
		
		public void setDevices(List<BluetoothTermailDevice> devices){
			this.devices = devices;
		}
		
		@Override
		public int getCount() {
			return null != devices? devices.size():0;
		}

		@Override
		public Object getItem(int position) {
			return devices.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = View.inflate(BluetoothDiscoveringActivity.this, R.layout.item_discovering_device, null);
			TextView tv_device_name = (TextView) convertView.findViewById(R.id.tv_device_name);
			TextView tv_device_address = (TextView) convertView.findViewById(R.id.tv_device_address);
				
			BluetoothTermailDevice bluetoothTermailDevice = devices.get(position);
			tv_device_name.setText(bluetoothTermailDevice.getName());
			tv_device_address.setText(bluetoothTermailDevice.getAddress());
			
			return convertView;
		}
	}
	
	private class BluetoothTermailDevice{
		private String name;
		private String address;
		public BluetoothTermailDevice(String name, String address) {
			this.name = name;
			this.address = address;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
	}

}
