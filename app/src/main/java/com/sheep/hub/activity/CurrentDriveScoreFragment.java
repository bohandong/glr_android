/*
 * Copyright (C), 2014-2015, 联创车盟汽车服务有限公司
 * FileName: CurrentDriveScoreFragment.java
 * Author:   shufei
 * Date:     2015年1月12日 下午6:29:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.sheep.hub.activity;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sheep.framework.db.AfeiDb;
import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;
import com.sheep.hub.bean.DriveScore;
import com.sheep.hub.bluetooth.BluetoothInstruct;
import com.sheep.hub.bluetooth.BluetoothManager;
import com.sheep.hub.bluetooth.IBluetoothListener;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class CurrentDriveScoreFragment extends Fragment implements OnClickListener {

	private TextView tv_drive_starttime;
	private TextView tv_drive_endtime;
	private TextView tv_score;
	private TextView tv_emergencyaddspeedcount;
	private TextView tv_emergencysubspeedcount;
	private TextView tv_emptyrunningtime;
	private TextView tv_overspeedtime;
	private TextView tv_avgcostoil;
	private LinearLayout rl_detail;
	private View v_score_bgcolor;
	
	
	private BluetoothManager manager;
	private AfeiDb afeiDb;
	
	private ProgressDialog pd;
	private long lastTime = 0 ;
	private int clickCount = 0;
	
    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	afeiDb = HubApp.getInstance().getAfeiDb();
    	manager = BluetoothManager.getInstance();
        View view = inflater.inflate(R.layout.fragment_drivecurrent, null, false);
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_title.setText(getActivity().getString(R.string.title_current_drive_score_detail));
        
        ImageView iv_back = (ImageView) view.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        
        v_score_bgcolor = view.findViewById(R.id.v_score_bgcolor);
        
        
        tv_drive_starttime = (TextView) view.findViewById(R.id.tv_drive_starttime);
        tv_drive_endtime = (TextView) view.findViewById(R.id.tv_drive_endtime);
        tv_score = (TextView) view.findViewById(R.id.tv_score);
        
        tv_emergencyaddspeedcount = (TextView) view.findViewById(R.id.tv_emergencyaddspeedcount);
        tv_emergencysubspeedcount = (TextView) view.findViewById(R.id.tv_emergencysubspeedcount);
        tv_emptyrunningtime = (TextView) view.findViewById(R.id.tv_emptyrunningtime);
        tv_overspeedtime = (TextView) view.findViewById(R.id.tv_overspeedtime);
        tv_avgcostoil = (TextView) view.findViewById(R.id.tv_avgcostoil);
        
        rl_detail = (LinearLayout) view.findViewById(R.id.rl_detail);
        
        view.findViewById(R.id.rl_score).setOnClickListener(this);
        
        
        manager.addListener(new IBluetoothListener() {
			@Override
			public void onStateChange(int state, int what) {
				switch (state) {
				case BluetoothManager.STATE_DRIVE_SCORE_SUCCESS:
					Message obtainMessage = handler.obtainMessage();
					obtainMessage.what = BluetoothManager.STATE_DRIVE_SCORE_SUCCESS;
					obtainMessage.obj = what;
                    //L.e("--->CurrentDriveScoreFragment  ... 获取到驾驶评分数据..what:"+what);
                    handler.sendMessageDelayed(obtainMessage,1000);
					break;
				case BluetoothManager.STATE_DRIVE_SCORE_FAILURE:
					handler.sendEmptyMessage(BluetoothManager.STATE_DRIVE_SCORE_FAILURE);
					break;
				default:
					break;
				}
			}
			@Override
			public void onScanningDevice(String name, String address, int bondState) {
				
			}
		});
        
        //初始化时也需要读取上次的驾驶评分
        ArrayList<DriveScore> driveScores = afeiDb.findAllByWhereStrWithOrderAndLimitStr(DriveScore.class, "", " time desc ", 1+"");
        if(null != driveScores && driveScores.size()>0){
        	readDriveScoreFromDb(Integer.valueOf(driveScores.get(0).getSequence()));
        }else{
        	readDriveScoreFromDb(1);
        }
        
        return view;
    }
    
    private Handler handler = new Handler(){
    	public void handleMessage(Message msg) {
    		if(pd != null && pd.isShowing()){
    			pd.dismiss();
    		}
    		switch (msg.what) {
			case BluetoothManager.STATE_DRIVE_SCORE_SUCCESS:
				CommonUtils.showShortToast(HubApp.getInstance(), getString(R.string.tip_drive_score_success));
				int sequence  =  (Integer)msg.obj;
                //L.e("--->CurrentDriveScoreFragment  ... 获取到驾驶评分数据..准备显示sequence:"+sequence);
				readDriveScoreFromDb(sequence);
				break;
			case BluetoothManager.STATE_DRIVE_SCORE_FAILURE:
				CommonUtils.showShortToast(HubApp.getInstance(), getString(R.string.tip_drive_score_failure));
				break;

			default:
				break;
			}
    	};
    };
	
    
    private void readDriveScoreFromDb(int sequence){
    	
    	ArrayList<DriveScore> driveScores = afeiDb.findAllByWhereStr(DriveScore.class, " sequence = '"+sequence+"'");
    	if(null != driveScores && driveScores.size()>0){
    		rl_detail.setVisibility(View.VISIBLE);
    		tv_drive_endtime.setVisibility(View.VISIBLE);
    		DriveScore ds = driveScores.get(0);
    		Resources resources = HubApp.getInstance().getResources();
    		String strStartTime = resources.getString(R.string.drive_starttime);
    		tv_drive_starttime.setText(String.format(strStartTime, ds.getStartTime()));
    		String strEndTime = resources.getString(R.string.drive_endtime);
    		tv_drive_endtime.setText(String.format(strEndTime, ds.getEndTime()));
    		setDriveScore(Integer.valueOf(ds.getDriveScore())); 
    		tv_emergencyaddspeedcount.setText(String.format(resources.getString(R.string.emergencyaddspeedcount), ds.getEmerAddSpeedTime()));
    		tv_emergencysubspeedcount.setText(String.format(resources.getString(R.string.emergencysubspeedcount), ds.getEmerSubSpeedTime()));
    		tv_emptyrunningtime.setText(String.format(resources.getString(R.string.emptyrunningtime), ds.getEmptyRunningTime()) + " min ");
    		tv_overspeedtime.setText(String.format(resources.getString(R.string.overspeedtime), ds.getOverSpeedTime()) + " min ");
    		//hub 发过来的单位是  0.01L/h
    		tv_avgcostoil.setText(String.format(resources.getString(R.string.avgcostoil), Integer.valueOf(ds.getAvgCostOil())*100)+" L/h");
    	}else{
    		rl_detail.setVisibility(View.GONE);
    		setDriveScore(100); //默认100
    		tv_drive_endtime.setVisibility(View.GONE);
    		tv_drive_starttime.setText("您还未进行驾驶评分");
    	}
    }
    
    
    public void setDriveScore(int score){
    	if(score > 80){
    		v_score_bgcolor.setBackgroundResource(R.drawable.shape_circle_blue1);
    	}else if(score >= 60){
    		v_score_bgcolor.setBackgroundResource(R.drawable.shape_circle_orange1);
    	}else{
    		v_score_bgcolor.setBackgroundResource(R.drawable.shape_circle_red1);
    	}
    	tv_score.setText(score+""); 
    }
    
    /* (non-Javadoc)
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                getActivity().finish();
                break;
            case R.id.rl_score:
            	//判断是否点击过频繁了，过于频繁则不响应了
            	if(checkClickFrequency()){
            		CommonUtils.showLongToast(getActivity(), getString(R.string.tip_click_too_much));
            		return;
            	}
            	
            	if(manager.isBluetoothConn()){
    				BluetoothInstruct.getDriveScore(); //获取驾驶评分
    				if(null == pd){
    					pd = new ProgressDialog(getActivity());
    					pd.setMessage(getString(R.string.tip_drive_score_waitting));
    					pd.setCancelable(false);
    					pd.show();
    				}else{
    					if(pd.isShowing()){
    						pd.dismiss();
    						pd.show();
    					}else{
    						pd.show();
    					}
    				}
    				handler.postDelayed(new Runnable() {
						
						@Override
						public void run() {
							if(null!=pd&&pd.isShowing()){
								pd.dismiss();
							}
						}
					}, 5000);
    				//CommonUtils.showShortToast(getActivity(), getString(R.string.tip_drive_score_waitting));
    			}else{
    				
    				CommonUtils.showShortToast(getActivity(), getString(R.string.tip_bluetooth_disconnect));
    			}
            	break;
            default:
                break;
        }
    }

   
    //两次点击 小于3秒中，不给响应...
	private boolean checkClickFrequency() {
		boolean retValue = false;
		if(clickCount!=0){
			long disGapTime = System.currentTimeMillis() - lastTime;
			if(disGapTime < 3*1000){
				retValue = true;    //小于3秒了，过于频繁了..
			}
		}
		lastTime = System.currentTimeMillis();
		clickCount++;
		return retValue;
	}
}
