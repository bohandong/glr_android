package com.sheep.hub.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;

import com.baidu.navisdk.remote.BNRemoteConstants.AssistantType;
import com.baidu.navisdk.remote.BNRemoteVistor;
import com.baidu.navisdk.remote.aidl.BNEventListener;
import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.HubApp;
import com.sheep.hub.bluetooth.BluetoothInstruct;
import com.sheep.hub.bluetooth.BluetoothManager;
import com.sheep.hub.bluetooth.IBluetoothListener;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.ManeuverValueTool;

import java.util.List;

public class NaviDataCollectorEnginer {
    private static Activity sactivity;

    private static final String TAG = "NaviDataCEnginer";

    private static boolean isConn = false;

    private static HubApp app;

    private static Handler handler;
    private static BluetoothManager manager;

    private static boolean isStartUp = false;

   // private boolean is

    private static NaviDataCollectorEnginer naviDataCollectorEnginer= null;

    //  TODO 这里存在一个问题，，若是多实例的话，，那会产生多次的   单例呢，看看是否能正常的connect到service上去。...
    private NaviDataCollectorEnginer(Activity activity) {
        sactivity = activity;
        this.app = HubApp.getInstance();
        this.handler = new Handler();
        manager = BluetoothManager.getInstance();
        isStartUp = false;
        init();
    }

    public static NaviDataCollectorEnginer getInstance(Activity activity){
        sactivity = activity;
        if(null == naviDataCollectorEnginer){
            naviDataCollectorEnginer = new NaviDataCollectorEnginer(sactivity);
        }else{
           if(!isConn){
               init();
           }
        }
        return naviDataCollectorEnginer;
    }

    private static void init() {
        BNRemoteVistor.getInstance().setOnConnectListener(onConnectListener);
        BNRemoteVistor.getInstance().connectToBNService(sactivity);
        isConn = true;
        //L.e("---------->>初始化 。。。 setOnConnectListener(onConnectListener) ");
    }

    public void destroy() {
        if(isConn){
            BNRemoteVistor.getInstance().disconnectToBNService(sactivity);
            L.e("--------------destroy  disconnectToBNService(activity)");
            isConn = false;
            sactivity = null;
        }
    }


    public static BNRemoteVistor.OnConnectListener onConnectListener = new BNRemoteVistor.OnConnectListener() {
        @Override
        public void onConnectFail(final String reason) {  //连接失败，原因："+reason
            //L.e("---------->>初始化 。。。 onConnectFail "+reason);
        }

        @Override
        public void onConnectSuccess() {  //连接成功
            try {
                BNRemoteVistor.getInstance().setBNEventListener(mBNEventListener);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            L.e("INDEX","百度连接成功");
        }

        @Override
        public void onDisconnect() {
            L.e("INDEX","百度 disconnect");
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(isRunningApp(sactivity, Constant.NAVI_PACKAGENAME)) {
                        init();
                    }
                    else
                    {
                        handler.postDelayed(this,1000);
                    }
                }
            }, 500);
        }
    };

    private static  BNEventListener.Stub mBNEventListener = new BNEventListener.Stub() {

        /**
         * 辅助诱导图标更新回调
         *
         * @param assistantType 辅助诱导类型
         * @param limitedSpeed 当类型是SpeedCamera和IntervalCamera的时候，会带有限速的值
         * @param distance 诱导距离(以米位单位),当距离为0时，表明这个诱导丢失消失
         */
        @Override
        public void onAssistantChanged(int assistantType, final int limitedSpeed, final int distance) throws RemoteException {
            startUploadDate();
            Log.e(TAG, "onRoadCameraChanged: assistantType = " + assistantType + " ,limitedSpeed = " + limitedSpeed + " ,distance = " + distance);
            String strTemp = null;
            if(AssistantType.IntervalCamera==assistantType||
                    AssistantType.SpeedCamera==assistantType||
                    AssistantType.TrafficLightCamera==assistantType||
                    AssistantType.PeccanryCamera==assistantType||
                    AssistantType.BlindBend==assistantType||
                    AssistantType.Accident==assistantType||
                    AssistantType.Tunnel==assistantType){
                if(AssistantType.IntervalCamera==assistantType){ //区间测速摄像头
                    app.getFrequenceMsg().setLimitSpeed(limitedSpeed);
                    app.getFrequenceMsg().setLimitDistance(distance); //距限速路口距离
                    if(distance <= 0){
                        app.getFrequenceMsg().setLimitSpeed(0XFF);
                    }
                    strTemp = "IntervalCamera";
                }else if(AssistantType.SpeedCamera==assistantType){ //测速摄像头
                    app.getFrequenceMsg().setLimitSpeed(limitedSpeed);
                    app.getFrequenceMsg().setLimitDistance(distance);//距限速路口距离
                    if(distance <= 0){
                        app.getFrequenceMsg().setLimitSpeed(0XFF);
                    }
                    strTemp = "SpeedCamera";
                }else if(AssistantType.TrafficLightCamera==assistantType){ // 交通信号灯摄像头
                    app.getFrequenceMsg().setCameraDistance(distance);
                    strTemp = "TrafficLightCamera";
                }else if(AssistantType.PeccanryCamera==assistantType){ //违章摄像头
                    app.getFrequenceMsg().setCameraDistance(distance);
                    strTemp = "PeccanryCamera";
                }else if(AssistantType.Tunnel==assistantType){ //隧道
                    if(distance>0){
                        app.getFrequenceMsg().setTunnel(0x01); //前方有隧道
                    }else{
                        app.getFrequenceMsg().setTunnel(0x00);
                    }
                    strTemp = "Tunnel";
                }
                //add by lvmu 20150618 start
                else if( AssistantType.BlindBend == assistantType ) {
                    app.getFrequenceMsg().setCameraDistance(distance | 0x1000 );
                }
                else if( AssistantType.Accident == assistantType ) {
                    app.getFrequenceMsg().setCameraDistance(distance | 0x2000 );
                }
                //add by lvmu 20150618 end
            }else{
                //app.getFrequenceMsg().setTunnel(0x00);//不在隧道里
            }
            final String assistantStr = strTemp;
           // CommonUtils.writeLogDate("onAssistantChanged ---...onRoadCameraChanged: assistantType = " + assistantType + " ,limitedSpeed = " + limitedSpeed + " ,distance = " + distance);
            /*activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(activity, "assistantStr:"+assistantStr+" limitedSpeed:"+limitedSpeed+" distance:"+distance, Toast.LENGTH_SHORT).show();
                }
            });*/
            //L.e("INDEX","辅助诱导图标更新回调 "+"assistantStr:"+assistantStr+" limitedSpeed:"+limitedSpeed+" distance:"+distance);
        }

        /**
         * GPS速度变化，在实际应用中，在某些情况下，手机GPS速度与实际车速在相差4km/h左右
         *
         * @param speed gps速度，单位km/h
         * @param latitude 纬度，GCJ-02坐标
         * @param longitude 经度，GCJ-02坐标
         */
        @Override
        public void onGpsChanged(int speed, double latitude, double longitude) {
            //add by xuxw 20150618 start
            //if( app.isNaving() )
            //{
                startUploadDate();
            //}
            //else
            //{
            //    return;
            //}
            //add by xuxw 20150618 end
            //del by xuxw 20150618 start
            //startUploadDate();
            //del by xuxw 20150618 end
            //if(speed>2){
            //add by lvmu 20150714 start
            if( speed > 10 && speed < 20 )
            {
                speed += 1;
            }
            else if( speed > 15 && speed < 30 )
            {
                speed += 2;
            }
            else if( speed >= 30 && speed < 40 )
            {
                speed += 3;
            }
            else if( speed >= 40 && speed < 45 )
            {
                speed += 4;
            }
            else if( speed >= 45 && speed < 110 )
            {
                speed += 5;
            }
            else if( speed >= 110 )
            {
                speed += 6;
            }
            //add by lvmu 20150714 end

            speed = (int)((float)speed/1.6);
            app.getFrequenceMsg().setGpsSeepd(speed);//设置GSP的速度
                //Log.e(TAG, "onGpsSpeedChanged: speed = " + speed + " ,latitude = " + latitude + " ,longitude = " + longitude);
            //}
        }

        /**
         * 服务区更新回调
         *
         * @param serviceArea 服务区的名字
         * @param distance 服务区的距离，当distance为0或者serviceArea为空时，表明服务区消失
         */
        @Override
        public void onServiceAreaChanged(String serviceArea, int distance) {
            //Log.e(TAG, "onServiceAreaChanged: serviceArea = " + serviceArea + " ,distance = " + distance);
        }

        /**
         * 导航机动点更新
         *
         * @param maneuverName 下一个机动点名称，具体可以参考官网上，每一个机动点名称对应的图标
         * @param distance 距离下一个机动点距离（以米为单位）
         */
        @Override
        public void onManeuverChanged(final String maneuverName, final int distance) {
            startUploadDate();
            Integer maneuverType = ManeuverValueTool.maneuverMap.get(maneuverName);
            int intType = 0;
            if(null != maneuverType && maneuverType>0){
                app.getFrequenceMsg().setManeuverType(maneuverType);
                app.getFrequenceMsg().setManeuverDistance(distance);
                intType = maneuverType;
            }

            //L.e("INDEX","导航机动点更新 "+"--->onManeuverChanged----maneuverType:"+intType+" maneuverName:"+maneuverName+"distance:"+distance);
            //CommonUtils.writeLogDate("onManeuverChanged----maneuverType:"+intType+" distance:"+distance+ " maneuverName:"+maneuverName);
        }

        /**
         * 到达目的地的距离和时间更新
         *
         * @param remainDistance 到达目的地的剩余距离（以米为单位）
         * @param remainTime 到达目的地的剩余时间(以秒为单位)
         */
        @Override
        public void onRemainInfoChanged(final int remainDistance, final int remainTime) {
            startUploadDate();
            app.getFrequenceMsg().setDestinationDistance(remainDistance); //设置距离目的的距离
            //Log.e(TAG, "onRemainInfoChanged: remainDistance = " + remainDistance + " ,remainTime = " + remainTime);
            /*activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(activity, "remainDistance:"+remainDistance+" remainTime:"+remainTime, Toast.LENGTH_SHORT).show();
                }
            });*/
        }

        /**
         * 当前道路名更新
         *
         * @param currentRoadName 当前路名
         */
        @Override
        public void onCurrentRoadNameChanged(String currentRoadName) {
            //Log.e(TAG, "onCurrentRoadNameChanged: currentRoadName = " + currentRoadName);
        }

        /**
         * 下一道路名更新
         *
         * @param nextRoadName 下一个道路名
         */
        @Override
        public void onNextRoadNameChanged(String nextRoadName) {
            //Log.e(TAG, "onNextRoadNameChanged: nextRoadName = " + nextRoadName);
        }

        /**
         * 开始导航
         */
        @Override
        public void onNaviStart() {
            app.setIsNaving(true);
             //
            startUploadDate();

            //todo 开始计时 导航数据，，，每500ms发送一次数据...
            Log.e(TAG, "NaviStart");
        }


        private void startUploadDate(){
            app.setIsNaving(true);
            //L.e("----app.isNaving():"+app.isNaving()+"  isStartUp:"+isStartUp+" ---manager.isBluetoothConn():"+manager.isBluetoothConn());


            //checkBaiduRunning(); //只要开始导航，则检查百度导航是否被强制关了


            if(!isStartUp){
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(app.isNaving()&&manager.isBluetoothConn()){ //蓝牙连接了，发个数据，，没有连接就不发数据了
                            handler.postDelayed(this,500);
                            app.getFrequenceMsg().setExit(0xFF);
                            BluetoothInstruct.setFrequenceSendMsg(0x03, 0);
                            isStartUp = true;
                            Log.e("INDEX","发一条导航 ");
                        }else{
                            isStartUp = false;
                            Log.e("INDEX","导航终止 ");
                        }
                    }
                },500);
            }



        }

        /**
         * 结束导航
         */
        @Override
        public void onNaviEnd() {
            naviEnd();
            Log.e(TAG, "onNaviEnd");
        }

        /**
         * 电子狗模式开启
         */
        @Override
        public void onCruiseStart() {
            //add by lvmu 20150624 start
            startUploadDate();
            //add by lvmu 20150624 end

            Log.e(TAG, "onCruiseStart");
        }

        /**
         * 电子狗模式结束
         */
        @Override
        public void onCruiseEnd() {
            //add by lvmu 20150624 start
            naviEnd();
            //add by lvmu 20150624 end
            Log.e(TAG, "onCruiseEnd");
        }

        /**
         * 导航中偏航
         */
        @Override
        public void onRoutePlanYawing() {
            Log.e(TAG, "onRoutePlanYawing");
        }

        /**
         * 导航中偏航结束,重新算路成功
         */
        @Override
        public void onReRoutePlanComplete() {
            Log.e(TAG, "onReRoutePlanComplete");
        }

        /**
         * gps丢失
         */
        @Override
        public void onGPSLost() {
            app.getFrequenceMsg().setGpsSeepd(0xFFFF);
            //Log.e(TAG, "onGPSLost");
        }

        /**
         * gps正常
         */
        @Override
        public void onGPSNormal() {
            Log.e(TAG, "onGPSNormal");
        }

        /**
         * 扩展事件接口，现在暂时没有数据回调，用作以后扩展
         *
         * @param eventType
         * @param data
         */
        @Override
        public void onExtendEvent(int eventType, Bundle data) {
            Log.e(TAG, "onExtendEvent: eventType = " + eventType + " ,data = " + data.toString());
        }

    };

    private static void naviEnd() {
        /*
        app.setIsNaving(false);
        //add by lvmu 20150624 start
        app.getFrequenceMsg().setExit(0x01); //退出导航...
        app.getFrequenceMsg().setManeuverType(0X04);
        app.getFrequenceMsg().setManeuverDistance(0X000F);
        app.getFrequenceMsg().setLimitSpeed(0XFF);
        app.getFrequenceMsg().setLimitDistance(0XFFFF);
        app.getFrequenceMsg().setTunnel(0XFF);
        app.getFrequenceMsg().setCameraDistance(0xFFFF);
        app.getFrequenceMsg().setDestinationDistance(0XFFFFFFFF);
        //frequenceMsg.setExit(0XFF);
        //frequenceMsg.setUnReadMsgOrTel(0XFF);
        //del by lvmu 20150714 start
        //app.getFrequenceMsg().setGpsSeepd(0XFFFF);
        //del by lvmu 20150714 end
        //add by lvmu 20150624 end
        handler.postDelayed(new Runnable() { //退出导航时，，等一小会儿 也发一次数据
            @Override
            public void run() {
                if(manager.isBluetoothConn()){
                    BluetoothInstruct.setFrequenceSendMsg();
                    //L.e("------>onNaviEnd  结束导航  --manager.isBluetoothConn() true");
                }else{
                    //L.e("------>onNaviEnd  结束导航  --manager.isBluetoothConn() false");
                }
            }
        }, 500);
*/


        //manager.addListener(bluetoothListener);
    }

    private static IBluetoothListener bluetoothListener = new IBluetoothListener() {

        @Override
        public void onStateChange(int state, int what) {
            switch (state) {
                case BluetoothManager.STATE_FEQURENCYSENDMSG_SUCCESS: //收
                    app.getFrequenceMsg().setExit(0xFF);
                    //L.e("-------->收到 常时送信 正常的应答--------");
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onScanningDevice(String name, String address,
                                     int bondState) {
        }
    };

    private static void checkBaiduRunning() {
        isStartUp = true;

        /*
        if(!isStartUp){
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(app.isNaving()&&manager.isBluetoothConn()){ //蓝牙连接了，发个数据，，没有连接就不发数据了
                        handler.postDelayed(this,2000);//两秒检查一下，
                        if(!isRunningApp(sactivity, Constant.NAVI_PACKAGENAME)){
                           naviEnd(); //导航结束了
                           //L.e("检查下 checkBaiduRunning  导航结束了 ");
                        }
                        isStartUp = true;
                        //L.e("检查下 checkBaiduRunning ");
                    }else{
                        isStartUp = false;
                        //L.e("不在导航了 checkBaiduRunning... ");
                    }
                }
            },500);

        }
        */
    }


    public static boolean isRunningApp(Context context, String packageName) {
        boolean isAppRunning = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        //List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(100);
        List<ActivityManager.RunningAppProcessInfo> list = am.getRunningAppProcesses();
        //for (ActivityManager.RunningTaskInfo info : list) {
        for (ActivityManager.RunningAppProcessInfo info : list) {
            //if (info.topActivity.getPackageName().equals(packageName) && info.baseActivity.getPackageName().equals(packageName)) {
            //if ( info.baseActivity.getPackageName().equals(packageName)) {
            if ( info.processName.equals(packageName)) {
                    isAppRunning = true;
                    // find it, break
                    break;
                }
        }
        //L.e("------------>isRunningApp::"+isAppRunning);
        return isAppRunning;
    }

}
