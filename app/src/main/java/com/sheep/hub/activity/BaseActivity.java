/*
 * Copyright (C), 2014-2015, 联创车盟汽车服务有限公司
 * FileName: BaseActivity.java
 * Author:   shufei
 * Date:     2015年1月4日 上午9:47:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.sheep.hub.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sheep.hub.HubApp;
import com.sheep.hub.R;




/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */

public abstract class BaseActivity extends FragmentActivity implements OnClickListener {

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    protected boolean isDefalutPageAnim = true;
    private TextView tv_title;
    protected Button btn_right;
    protected HubApp app;

    PowerManager.WakeLock mWakeLock;
    
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        app = HubApp.getInstance();
        app.addActivity(this);
        //add by lvmu 20150716 start
        PowerManager pm =(PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK|PowerManager.ON_AFTER_RELEASE, "GLR-HUD");
        mWakeLock.acquire();
        //add by lvmu 20150716 end
        init();
    }
    
    /**
     * 固定几种走，三板斧套路
     * 功能描述: <br>
     * 〈功能详细描述〉
     *
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    private void init() {
        loadView();
        initTop();
        findView();
        setListener();
        doLogic();
    }
    
    
    /**
     * 功能描述: <br>
     * 〈功能详细描述〉
     * 初始化标题部门，find View的过程
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    private void initTop() {
        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        btn_right = (Button) findViewById(R.id.btn_right);
        
        if(null != iv_back){
            iv_back.setOnClickListener(this);
        }
    }
    

    /**
     * 加载布局，一般放setContentView之类的
     * 功能描述: <br>
     * 〈功能详细描述〉
     *
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    protected abstract void  loadView() ;
    /**
     * 查找 view过程
     * 功能描述: <br>
     * 〈功能详细描述〉
     *
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    protected abstract void  findView() ;
    /**
     * 设置相应的监听器
     * 功能描述: <br>
     * 〈功能详细描述〉
     *
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    protected abstract void  setListener() ;
    
    /**
     * 处理 流程逻辑的代码
     * 功能描述: <br>
     * 〈功能详细描述〉
     *
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    protected abstract void  doLogic() ;
    
    /* (non-Javadoc)
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;

            default:
                break;
        }
    }
    
    /* (non-Javadoc)
     * @see android.app.Activity#setTitle(java.lang.CharSequence)
     */
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        tv_title.setText(title);
    }
    
    /* (non-Javadoc)
     * @see android.app.Activity#setTitle(int)
     */
    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);
        tv_title.setText(titleId);
    }
    /**
     * 不携带数据的intent式开启startActivity
     * @param cls 需要开启的activity
     */
    public void startAct(Class<?> cls){
        Intent intent = new Intent(this,cls);
        startActivity(intent);
        changePageAnim();  //页面切换效果
    }
    
  //页面切换效果
    public void changePageAnim() {
        if(isDefalutPageAnim){
            //页面切换效果,第一个参数是进来的动画，第二个参数是出去的动画
            overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
        }
    }
    
}
