package com.sheep.hub.activity;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.sheep.framework.db.AfeiDb;
import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.framework.view.pulltorefresh.PullToRefreshAnimatedExpandListView;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase.Mode;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase.OnRefreshListener2;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;
import com.sheep.hub.adapter.HistoryScoreGroupAdapter;
import com.sheep.hub.bean.DSChildItem;
import com.sheep.hub.bean.DSGroupItem;
import com.sheep.hub.bean.DriveScore;
import com.sheep.hub.view.AnimatedExpandableListView;

public class HistoryDriveScoreActivity extends BaseActivity {

	private AnimatedExpandableListView aelv_historyscore;
    private ArrayList<DSGroupItem> gis ;
    private HistoryScoreGroupAdapter adapter;
    private PullToRefreshAnimatedExpandListView ptraxl_score;
    
    private ArrayList<DriveScore> displayDriveScores ;
    private int currentPage = 1; //从第1页开始，每页10条数据
    private int perPageCount = 2000; //每页的数据
    
    private Mode reFreshMode ;
    
    private TextView tv_title;
    private ImageView iv_back;
    
	@Override
	protected void loadView() {
		setContentView(R.layout.fragment_historyscore);
	}

	@Override
	protected void findView() {
		tv_title = (TextView) findViewById(R.id.tv_title);
        
        
        iv_back = (ImageView) findViewById(R.id.iv_back);
        
        ptraxl_score = (PullToRefreshAnimatedExpandListView)findViewById(R.id.aelv_historyscore);
	}

	@Override
	protected void setListener() {
		iv_back.setOnClickListener(this);
	}

	@Override
	protected void doLogic() {
		tv_title.setText("驾驶记录");
		if(null == gis){
    		gis = new ArrayList<DSGroupItem>();
    	}else{
    		gis.clear();
    	}

    	if(null == displayDriveScores){
    		displayDriveScores = new ArrayList<DriveScore>(); 
    	}else{
    		displayDriveScores.clear();
    	}
    	
         ptraxl_score.setMode(Mode.DISABLED);
         aelv_historyscore = ptraxl_score.getRefreshableView();
        
         ptraxl_score.setOnRefreshListener(new OnRefreshListener2() {

 			@Override
 			public void onPullDownToRefresh(PullToRefreshBase refreshView) {
 				//L.e("--------> ptraxl_score ...onRefresh----onPullDownRefreshComplete");
 				reFreshMode = Mode.PULL_FROM_START;
 				currentPage = 1;
 				new DriveScoreReader().execute(currentPage); //每次都从第1页开始加载
 			}

 			@Override
 			public void onPullUpToRefresh(PullToRefreshBase refreshView) {
 				//L.e("--------> ptraxl_score ...onRefresh -----onPullUpRefreshComplete");
 				reFreshMode = Mode.PULL_FROM_END;
 				new DriveScoreReader().execute(++currentPage); //每次都从第1页开始加载
 			}
 		});
         
         //initData();
         new DriveScoreReader().execute(1);
         adapter = new HistoryScoreGroupAdapter(this, gis);
         aelv_historyscore.setAdapter(adapter);
         
         aelv_historyscore.setGroupIndicator(null);
         
         aelv_historyscore.setOnGroupClickListener(new OnGroupClickListener() {
             
             @Override
             public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                 
                 if(aelv_historyscore.isGroupExpanded(groupPosition)){
                     aelv_historyscore.collapseGroupWithAnimation(groupPosition);
                 }else{
                     aelv_historyscore.expandGroupWithAnimation(groupPosition);
                 }
                 
                 int groupCount = adapter.getGroupCount();
                 for(int i=0;i<groupCount;i++){
                     if (aelv_historyscore.isGroupExpanded(i)&&i!=groupPosition) {
                         aelv_historyscore.collapseGroupWithAnimation(i);
                     }
                 }
                 
                 return true;
             }
         });
	}
	
	
	class DriveScoreReader extends AsyncTask<Integer, Void, ArrayList<DriveScore>> {

		private ProgressDialog dialog;
		
		
		@Override
		protected ArrayList<DriveScore> doInBackground(Integer... params) {
			AfeiDb afeiDb = HubApp.getInstance().getAfeiDb();
			int page = params[0];
			int count = afeiDb.calculateCountByWhereStr(DriveScore.class, "");
			int sumPage = (count+perPageCount-1)/perPageCount;
			//L.e("-->>sum count:"+count +" sumPage::"+sumPage+" page::"+page);
			if(page>sumPage){
				currentPage --;
				return null;
			}else{
				ArrayList<DriveScore> driveScores = afeiDb.findAllByWhereStrWithOrderAndLimitStr(DriveScore.class, "", "startTime desc", perPageCount*(page-1)+","+perPageCount);
				return driveScores;
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = CommonUtils.showProgressDialog(HistoryDriveScoreActivity.this, R.string.tip_load_data_ing);
		}

		@Override
		protected void onPostExecute(ArrayList<DriveScore> result) {
			super.onPostExecute(result);
			dialog.dismiss();
			
			if(Mode.PULL_FROM_END==reFreshMode){
				if(null != result && result.size()>0){
					displayDriveScores.addAll(result);
				}
				ptraxl_score.onPullUpRefreshComplete();
			}else if(Mode.PULL_FROM_START==reFreshMode){
				displayDriveScores.clear();
				if(null != result && result.size()>0){
					displayDriveScores.addAll(result);
				}
				ptraxl_score.onPullDownRefreshComplete();
			}else{
				if(null != result && result.size()>0){
					displayDriveScores.addAll(result);
				}
			}
			ds2DsGroup();
			//L.e("-->>displayDriveScores::size:"+displayDriveScores.size() +" currentPage:"+currentPage);
		}
	}
	
	private void ds2DsGroup() {
		if(null != displayDriveScores && displayDriveScores.size()>0){
			gis.clear(); //先清除之类的数据
			for(DriveScore ds:displayDriveScores){
				DSGroupItem dsg = new DSGroupItem();
				ArrayList<DSChildItem> cis = new ArrayList<DSChildItem>();
	            dsg.setDriveScore(ds.getDriveScore());
	            dsg.setStartTime(ds.getStartTime());
	            
	            DSChildItem cis1 = new DSChildItem();
	            cis1.setKey("驾驶结束时间：");
	            cis1.setValue(ds.getEndTime());
	            
	            DSChildItem cis2 = new DSChildItem();
	            cis2.setKey("急加速次数：");
	            cis2.setValue(ds.getEmerAddSpeedTime());
	            
	            DSChildItem cis3 = new DSChildItem();
	            cis3.setKey("急减速次数：");
	            cis3.setValue(ds.getEmerSubSpeedTime());
	            
	            DSChildItem cis4 = new DSChildItem();
	            cis4.setKey("引擎空转时间：");
	            cis4.setValue(second2Min(ds.getEmptyRunningTime()));
	            
	            DSChildItem cis5 = new DSChildItem();
	            cis5.setKey("超速时间：");
	            cis5.setValue(second2Min(ds.getOverSpeedTime()));
	            
	            DSChildItem cis6 = new DSChildItem();
	            cis6.setKey("驾驶时间：");
	            cis6.setValue(betweeSes(ds.getEndTime(), ds.getStartTime()));

				//add by lvmu 20150731 start
				DSChildItem cis7 = new DSChildItem();
				cis7.setKey("行驶里程：");
				if( ds.getDrivedistance()!=null )
				cis7.setValue(getOilCost(ds.getDrivedistance()) + "km");

				DSChildItem cis8 = new DSChildItem();
				cis8.setKey("本次油耗：");
				if( ds.getCostoil()!=null )
				cis8.setValue(getOilCost(ds.getCostoil())+"L");

				DSChildItem cis9 = new DSChildItem();
				cis9.setKey("平均油耗：");
				if( ds.getAvgCostOil()!=null )
				cis9.setValue(getOilCost(ds.getAvgCostOil())+"L/100km");

				//add by lvmu 20150731 end
	            
	            cis.add(cis1);
	            cis.add(cis2);
	            cis.add(cis3);
	            cis.add(cis4);
	            cis.add(cis5);
	            cis.add(cis6);
				//add by lvmu 20150731 start
				if( !( ds.getDrivedistance() == null || ds.getDrivedistance().equals("0")) ) {
					cis.add(cis7);
				}
				if( !( ds.getCostoil() == null || ds.getCostoil().equals("0")) ) {
					cis.add(cis8);
				}
				if( !( ds.getAvgCostOil() == null || ds.getAvgCostOil().equals("0")) ) {
					cis.add(cis9);
				}
				//add by lvmu 20150731 end
	            dsg.setCis(cis);
	            gis.add(dsg);
			}
		}
		
		adapter.setData(gis);
	}

	private String betweeSes(String dt1,String dt2){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date d1 = sdf.parse(dt1);
			Date d2 = sdf.parse(dt2);
			long betweeMiss = d1.getTime()-d2.getTime();
			return second2Min(betweeMiss/1000+"");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}

    private String second2Min(String strsecond){
        String retValue = null;
        Integer intSecond = Integer.valueOf(strsecond);
        DecimalFormat df = new DecimalFormat("###,###.##");
        if(intSecond<60){
            retValue = intSecond + "s";
        }else if(intSecond>60&&intSecond<3600){ //1小时之内
            if(intSecond%60 == 0){
                retValue = intSecond / 60 +"min";
            }else{
                retValue = intSecond / 60 +"min " + intSecond%60 +"s";
            }
        }else{
            if(intSecond%3600 == 0){
                retValue = intSecond / 3600 +"h";
            }else if(intSecond%60 == 0){
                retValue = intSecond / 3600 +"h "+ ((intSecond % 3600) / 60) +"min";
            }else {
                retValue = intSecond / 3600 +"h "+ ((intSecond % 3600) / 60) +"min " + intSecond%60 +"s";
            }
        }
        return retValue;
    }


    private String getOilCost(String oilcost){
        float temp = Float.valueOf(oilcost)/100.00f;
        DecimalFormat df = new DecimalFormat("###,###.##");
        return df.format(temp);
    }

}
