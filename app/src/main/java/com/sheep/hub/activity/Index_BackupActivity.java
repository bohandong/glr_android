package com.sheep.hub.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.sheep.framework.log.L;
import com.sheep.framework.ui.ArrayListAdapter;
import com.sheep.framework.util.Density;
import com.sheep.hub.R;
import com.sheep.hub.bean.Menu;


public class Index_BackupActivity extends BaseActivity {

	private TextView tv_title;
	private GridView gv_menu;
	
	private int screenWidth;
	private int screenHeight;
	private int gridViewHeight;
	private int gridViewWidth;

	@Override
	protected void loadView() {
		setContentView(R.layout.activity_index_backup);
	}

	@Override
	protected void findView() {
		tv_title = (TextView) findViewById(R.id.tv_title);
		gv_menu = (GridView) findViewById(R.id.gv_menu);
		
	}

	@Override
	protected void setListener() {
		gv_menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
			}
		});
	}

	@Override
	protected void doLogic() {
		WindowManager windowManger = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		screenWidth = windowManger.getDefaultDisplay().getWidth();
		screenHeight = windowManger.getDefaultDisplay().getHeight();
		
		Rect rect = new Rect();
		getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
		int width = rect.width();
		int height = rect.height();
		
		//L.e("------->width:"+width+" height:"+height+" rect.top:"+rect.top);
		
		MenuAdapter adapter = new MenuAdapter(this);
		ArrayList<Menu> menuData = getMenuData();
		adapter.setList(menuData);
		gv_menu.setAdapter(adapter);
		
	}
	
	private ArrayList<Menu> getMenuData(){
		ArrayList<Menu> menus = new ArrayList<Menu>();
		
		Menu menu1 = new Menu();
		menu1.setName("百度导航");
		
		Menu menu2 = new Menu();
		menu2.setName("百度地图");
		
		Menu menu3 = new Menu();
		menu3.setName("亮度调节");
		
		Menu menu4 = new Menu();
		menu4.setName("连接HUB");
		
		Menu menu5 = new Menu();
		menu5.setName("故障检测");
		
		Menu menu6 = new Menu();
		menu6.setName("驾驶记录");
		
		Menu menu7 = new Menu();
		menu7.setName("OBD模块更换");
		
		Menu menu8 = new Menu();
		menu8.setName("关于");
		
		menus.add(menu1);
		menus.add(menu2);
		menus.add(menu3);
		menus.add(menu4);
		menus.add(menu5);
		menus.add(menu6);
		menus.add(menu7);
		menus.add(menu8);
		return menus;
	}
	
	
	class MenuAdapter extends ArrayListAdapter<Menu>{
		
		private LayoutInflater inflater;

		public MenuAdapter(Activity context) {
			super(context);
			inflater = LayoutInflater.from(context);
		}

		//一页显示完，不存在复用，直接拿来用即可。
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = inflater.inflate(R.layout.item_index, parent, false);
			
			AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT);
			//L.e("1111111 lp.width:::"+lp.width+"  lp.height::"+lp.height);
			int ofw = Density.of(mContext, 15);
			int ofh = Density.of(mContext, 100);
			lp.width = (screenWidth-ofw)/2;
			lp.height = (screenHeight-ofh)/4;
			//L.e("lp.width:::"+lp.width+"  lp.height::"+lp.height);
			view.setLayoutParams(lp);
			TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
			ImageView iv_icon = (ImageView) view.findViewById(R.id.iv_icon);
			
			Menu menu = getItem(position);
			tv_name.setText(menu.getName());
			//iv_icon
			
			return view;
		}
		
	}

	
}
