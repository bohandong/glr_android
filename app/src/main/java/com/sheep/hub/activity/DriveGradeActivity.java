/*
 */
package com.sheep.hub.activity;

import android.support.v4.app.FragmentTabHost;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.sheep.hub.R;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class DriveGradeActivity extends BaseActivity {

    private RadioGroup rg_tabchange;
    private FragmentTabHost tabHost;

    /* (non-Javadoc)
     * @see com.sheep.hub.activity.BaseActivity#loadView()
     */
    @Override
    protected void loadView() {
        setContentView(R.layout.activity_drivegrade);
    }

    /* (non-Javadoc)
     * @see com.sheep.hub.activity.BaseActivity#findView()
     */
    @Override
    protected void findView() {
        tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        rg_tabchange = (RadioGroup) findViewById(R.id.rg_tabchange);
    }

    /* (non-Javadoc)
     * @see com.sheep.hub.activity.BaseActivity#setListener()
     */
    @Override
    protected void setListener() {
        rg_tabchange.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkRb = (RadioButton) rg_tabchange.findViewById(checkedId);
                if(R.id.rb_current == checkRb.getId()){
                    tabHost.setCurrentTab(0);
                }else if(R.id.rb_history == checkRb.getId()){
                    tabHost.setCurrentTab(1);
                }
                checkRb.setChecked(true);
            }
        });
    }

    /* (non-Javadoc)
     * @see com.sheep.hub.activity.BaseActivity#doLogic()
     */
    @Override
    protected void doLogic() {
       
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        tabHost.getTabWidget().setVisibility(View.GONE);
        tabHost.addTab(tabHost.newTabSpec("current").setIndicator("current"), CurrentDriveScoreFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("history").setIndicator("history"),HistoryDriveScoreFragment.class,null);
        
        tabHost.setCurrentTab(0);
        RadioButton defaultCheckRb = (RadioButton) rg_tabchange.findViewById(R.id.rb_current);
        defaultCheckRb.setChecked(true);
        //tabHost.addTab(arg0, arg1, arg2);
        
    }

}
