/*
 * Copyright (C), 2014-2015, 联创车盟汽车服务有限公司
 * FileName: CarHitchActivity.java
 * Author:   shufei
 * Date:     2015年1月15日 下午3:28:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.sheep.hub.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sheep.framework.db.AfeiDb;
import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;
import com.sheep.hub.adapter.CarHitchGroupAdapter;
import com.sheep.hub.bean.Hitch;
import com.sheep.hub.bean.HitchChildItem;
import com.sheep.hub.bean.HitchGroupItem;
import com.sheep.hub.bluetooth.BluetoothInstruct;
import com.sheep.hub.bluetooth.BluetoothManager;
import com.sheep.hub.bluetooth.IBluetoothListener;
import com.sheep.hub.util.Preference;
import com.sheep.hub.view.AnimatedExpandableListView;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *  爱车故障
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class CarHitchActivity extends BaseActivity {

    private static final int STATA_OVRE_TIME = 1;
    private static final int STATA_NORMAL = 2;
    private TextView tv_score;
    private RelativeLayout rl_score;
    private View v_scorebg;
    private TextView tv_detection_time;
    private AnimatedExpandableListView aelv_hitch;
    private ArrayList<HitchGroupItem> hgis;
    private CarHitchGroupAdapter adapter;
	private BluetoothManager manager;

	private AfeiDb afeiDb;
	//private ProgressDialog pd;

    private long lastNoHitchTime = 0 ;
    private int clickCount = 0;
    private TextView tv_check;

    private Map<Integer,Runnable> runMap = new HashMap<Integer,Runnable>();
    private int checkCount ;
    private long lastTime; //

    private int failCheckCount;
    /* (non-Javadoc)
     * @see com.sheep.hub.activity.BaseActivity#loadView()
     */
    @Override
    protected void loadView() {
        setContentView(R.layout.activity_carhitch);
    }

    /* (non-Javadoc)
     * @see com.sheep.hub.activity.BaseActivity#findView()
     */
    @Override
    protected void findView() {
        tv_score = (TextView) findViewById(R.id.tv_score);
        rl_score = (RelativeLayout) findViewById(R.id.rl_score);
        v_scorebg = findViewById(R.id.v_scorebg);
        tv_detection_time = (TextView) findViewById(R.id.tv_detection_time);
        
        aelv_hitch = (AnimatedExpandableListView) findViewById(R.id.aelv_hitch);

        tv_check = (TextView) findViewById(R.id.tv_check);
    }

    /* (non-Javadoc)
     * @see com.sheep.hub.activity.BaseActivity#setListener()
     */
    @Override
    protected void setListener() {
        rl_score.setOnClickListener(this);
        //全部展开-不处理
        aelv_hitch.setOnGroupClickListener(new OnGroupClickListener() {
            
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
            	return true;
            }
        });
    }
    
    private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case BluetoothManager.STATE_IMMEDIA_EXPERIENCE_SUCCESS:
                //setWaitCheck();
                cancelDelayRunnable();
				//dismissPd();
				//CommonUtils.showLongToast(HubApp.getInstance(),getString(R.string.tip_immedia_experience_success));
				break;
			case BluetoothManager.STATE_IMMEDIA_EXPERIENCE_FAILURE:
                //Preference.putString("lasttime",CommonUtils.date2StringTime(new Date()));
                //String srcDetectionTime = getString(R.string.last_detection_time);
                //tv_detection_time.setText(String.format(srcDetectionTime, Preference.getString("lasttime")));
                cancelDelayRunnable();
                setWaitCheck();
				//dismissPd();
				CommonUtils.showShortToast(HubApp.getInstance(),"目前不在怠速状态，为了行车安全请不要体检");
				break;
			case BluetoothManager.STATE_IMMEDIA_EXPERIENCE_NOTCONT_OBD:
				//dismissPd();
                setWaitCheck();
                cancelDelayRunnable();
				CommonUtils.showShortToast(HubApp.getInstance(),getString(R.string.tip_immedia_experience_notconn_obd));
				break;	
			case BluetoothManager.STATE_HITCH_SUCCESS:
                Preference.putString("lasttime",CommonUtils.date2StringTime(new Date()));
                cancelDelayRunnable();
                int sequence = (Integer)msg.obj;
                //从数据库中读取数据
                //L.e("检测完成，收到故障码，准备查库显示中...sequence:" + sequence);
                readHitchFromDb(sequence);
                setWaitCheck();
                break;
           case BluetoothManager.STATE_HITCH_SUCCESS_NOHITCH:
                Preference.putString("lasttime",CommonUtils.date2StringTime(new Date()));
                afeiDb.deleteByWhereStr(Hitch.class, "");
                cancelDelayRunnable();
                initDateView(STATA_NORMAL);
                setWaitCheck();
                break;
			default:
				break;
			}
			
		};
	};

    private void cancelDelayRunnable() {
        Runnable runnable = runMap.get(checkCount);
        handler.removeCallbacks(runnable);
    }



    /*private void dismissPd(){
		if(null != pd && pd.isShowing()){
			pd.dismiss();
		}
	}*/

    private void setWaitCheck(){
        tv_check.setText("点击检测");
        tv_check.setTextColor(getResources().getColor(R.color.white));
    }

    private void setChecking(){
        tv_check.setText("正在体检");
        tv_check.setTextColor(getResources().getColor(R.color.black));
    }

    /* (non-Javadoc)
     * @see com.sheep.hub.activity.BaseActivity#doLogic()
     */
   /* private long failureLastTime = 0;
    private long successLastTime = 0;
    private long hitchSuccessLastTime = 0;
    private long noNoticeLastTime = 0;*/

    private IBluetoothListener listeners = new IBluetoothListener() {

        @Override
        public void onStateChange(int state, int what) {
            switch (state) {
                case BluetoothManager.STATE_IMMEDIA_EXPERIENCE_SUCCESS:
                    handler.sendEmptyMessageDelayed(BluetoothManager.STATE_IMMEDIA_EXPERIENCE_SUCCESS, 1000);
                    break;
                case BluetoothManager.STATE_IMMEDIA_EXPERIENCE_FAILURE:
                    handler.sendEmptyMessageDelayed(BluetoothManager.STATE_IMMEDIA_EXPERIENCE_FAILURE, 1000);
                        //L.e("STATE_IMMEDIA_EXPERIENCE_FAILURE:::failureLastTime:" + failureLastTime + "System.currentTimeMillis():" + System.currentTimeMillis());
                        //handler.sendEmptyMessage(BluetoothManager.STATE_IMMEDIA_EXPERIENCE_FAILURE);
                    break;
                case BluetoothManager.STATE_IMMEDIA_EXPERIENCE_NOTCONT_OBD:
                    handler.sendEmptyMessageDelayed(BluetoothManager.STATE_IMMEDIA_EXPERIENCE_NOTCONT_OBD, 1000);
                        //handler.sendEmptyMessage(BluetoothManager.STATE_IMMEDIA_EXPERIENCE_NOTCONT_OBD);
                    break;
                case BluetoothManager.STATE_HITCH_SUCCESS:
                    //L.e("--->检测收到一个故障码：：----what::" + what);
                    Message obtainMessage = handler.obtainMessage();
                    obtainMessage.what = BluetoothManager.STATE_HITCH_SUCCESS;
                    obtainMessage.obj = what;
                    handler.sendMessageDelayed(obtainMessage, 1000);
                    break;
                case BluetoothManager.STATE_HITCH_SUCCESS_NOHITCH:
                    //L.e("-->没有故障...STATE_HITCH_SUCCESS_NOHITCH.::" + (System.currentTimeMillis() - lastNoHitchTime));
                    Message obtainMessage2 = handler.obtainMessage();
                    obtainMessage2.what = BluetoothManager.STATE_HITCH_SUCCESS_NOHITCH;
                    obtainMessage2.obj = what;
                    handler.sendMessageDelayed(obtainMessage2, 1000);
                        //L.e("-->没有故障22...STATE_HITCH_SUCCESS_NOHITCH.");
                        //L.e("-->没有故障11...STATE_HITCH_SUCCESS_NOHITCH.");
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onScanningDevice(String name, String address, int bondState) {

        }
    };

    @Override
    protected void doLogic() {
    	afeiDb = app.getAfeiDb();
    	hgis = new ArrayList<HitchGroupItem>();
        setWaitCheck();
    	manager = BluetoothManager.getInstance();

        manager.addListener(listeners);
    	
    	setTitle("故障检测");
    	/*initData();
         */
    	adapter = new CarHitchGroupAdapter(this,hgis);
    	aelv_hitch.setAdapter(adapter);
    	
    	ArrayList<Hitch> hitch = afeiDb.findAllByWhereStrWithOrderAndLimitStr(Hitch.class, "", " time desc ", 1+"");
    	if(null != hitch && hitch.size()>0){
    		String sequence = hitch.get(0).getSequence();
    		readHitchFromDb(Integer.valueOf(sequence));
    	}else{
    		//没有数据时，显示体验100分，上次检测时间，填写无
    		setDriveScore(100);

            //Preference.putString("lasttime","");
            String lasttime = Preference.getString("lasttime");
            if(TextUtils.isEmpty(lasttime)){
                tv_detection_time.setText("您还未进行体检，立即体验下吧!");
            }else{
                String srcDetectionTime = getString(R.string.last_detection_time);
                tv_detection_time.setText(String.format(srcDetectionTime, lasttime));
            }

    	}
    	
    	
       
		
        adapter.setOnChildViewClickListener(new CarHitchGroupAdapter.OnChildViewClickListener() {
			
			@Override
			public void onChildClick(View expandView,ImageView arrowView, boolean isExpand,
					int childPosition, int groupPosition) {
				if(isExpand){
					expandView.setVisibility(View.VISIBLE);
					arrowView.setImageResource(R.drawable.icon_arrow_open);
				}else{
					expandView.setVisibility(View.GONE);
					arrowView.setImageResource(R.drawable.icon_arrow_close);
				}
			}
		});
        
        
    }
    
    public void setDriveScore(int score){
    	if(score > 80){
    		v_scorebg.setBackgroundResource(R.drawable.shape_circle_blue1);
    	}else if(score >= 60){
    		v_scorebg.setBackgroundResource(R.drawable.shape_circle_orange1);
    	}else{
    		v_scorebg.setBackgroundResource(R.drawable.shape_circle_red1);
    	}
    	tv_score.setText(score+""); 
    }

    /**
     * 无故障码是调用的
     */
    private void initDateView(int type) {
        String srcDetectionTime = getString(R.string.last_detection_time);
        tv_detection_time.setText(String.format(srcDetectionTime, Preference.getString("lasttime")));

        if(type == STATA_NORMAL){
            failCheckCount=0;
            setDriveScore(100);
            hgis.clear(); //先清除之前的数据
            if(null != adapter){
                //L.e("adapter is not null hgis...:" + hgis.toString());
                adapter.setDate(hgis);
            }else{
                adapter = new CarHitchGroupAdapter(this, hgis);
                aelv_hitch.setAdapter(adapter);
            }
        }

        String msg = "当前车况良好!";
        if(STATA_OVRE_TIME == type){
            failCheckCount++;
            if(failCheckCount<=3){
                setChecking();
                BluetoothInstruct.immediaExperience(); //立即体验
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        initDateView(STATA_OVRE_TIME);
                        setWaitCheck();
                    }
                };
                runMap.put(++checkCount,r);
                handler.postDelayed(r, 10000);
                msg="";
            }else {
                msg = "体检超时，请重试!";
            }
        }
        if(!TextUtils.isEmpty(msg)) {
            CommonUtils.showShortToast(this, msg);
        }
       /* AlertDialog.Builder builder = new AlertDialog.Builder(tv_detection_time.getContext());
        builder.setMessage(msg);
        builder.setPositiveButton("确定",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();*/
    }

    private void readHitchFromDb(int what) {
    	
    	ArrayList<Hitch> hitchs = afeiDb.findAllByWhereStr(Hitch.class, " sequence = '"+what+"' ");
    	//L.e("故障码的数据是：："+hitchs.toString());
    	if(null == hitchs||hitchs.size()==0){
    		//L.e("故障码的数据为空!!!");
    		return ;
    	}
    	
    	List<Hitch> normalHitch = new ArrayList<Hitch>();
    	List<Hitch> seriousHitch = new ArrayList<Hitch>();
    	
    	
    	int seriousHitchCount = 0;
    	int normalHitchCount = 0;
    	for(Hitch h : hitchs){
    		if("Y".equals(h.getIsSerious())){
    			seriousHitch.add(h);
    			seriousHitchCount ++;
    		}else{
    			normalHitch.add(h);
    			normalHitchCount ++;
    		}
    	}
    	
    	hgis.clear(); //先清除之前的数据
        fillHgis(seriousHitch,"严重故障");
        fillHgis(normalHitch,"一般故障");

    	if(null != adapter){
    		//L.e("adapter is not null hgis .readHitchFromDb..:" + hgis.toString());
    		adapter.setDate(hgis);
    	}else{
    		adapter = new CarHitchGroupAdapter(this, hgis);
            aelv_hitch.setAdapter(adapter);
    	}
    	
    	 aelv_hitch.setGroupIndicator(null);
     	
         //默认，将其全部展开
     	int groupCount = aelv_hitch.getExpandableListAdapter().getGroupCount();
         for(int i=0;i<groupCount;i++){
             aelv_hitch.expandGroup(i);
         }
    	
    	//处理故障数分及体验时间
    	String time = hitchs.get(0).getTime();
    	String srcDetectionTime = getString(R.string.last_detection_time);
    	tv_detection_time.setText(String.format(srcDetectionTime, time));
    	
    	int score = 100;  //初始值 是100分
    	if(seriousHitchCount > 4){
    		score = 20; //超过4的严重问题，扣了只剩下20分
    	}else{
    		score = score - seriousHitchCount*20;
    	}
    	
    	if(normalHitchCount > 10){
    		score = score - 50;
    	}else{
    		score = score - 5*normalHitchCount;
    	}
    	
    	score = (score <= 0) ? 0:score;
    	//int score = 100 - hitchs.size(); //故障数不超过1天...
    	setDriveScore(score);
    	
	}
    
    private void fillHgis(List<Hitch> typeHitchs,String type){
    	if(null != typeHitchs &&typeHitchs.size()>0){
    		HitchGroupItem hi = new HitchGroupItem();
    		hi.setHitchNum(typeHitchs.size()+"");
    		hi.setHitchType(type);
    		if("严重故障".equals(type)){
    			hi.setResouceColorId(R.color.text_red_1);
    		}else if("一般故障".equals(type)){
    			hi.setResouceColorId(R.color.text_red_2);
    		}
    		ArrayList<HitchChildItem> hcis = new ArrayList<HitchChildItem>();
    		for(int i=0;i<typeHitchs.size();i++){
				HitchChildItem hci = new HitchChildItem();
				Hitch hitch = typeHitchs.get(i);
				if(TextUtils.isEmpty(hitch.getName())){
					hci.setHitchName("未知故障 "+ hitch.getCode());
				}else{
					hci.setHitchName(hitch.getName());
				}
                hci.setHitchCode(hitch.getCode());
				hci.setHitchCarType(hitch.getCategory());
				hci.setHitchDetail(hitch.getDetail());
				if("严重故障".equals(type)){
					hci.setHitchIconId(R.drawable.icon_serioushitch);
	    		}else if("一般故障".equals(type)){
	    			hci.setHitchIconId(R.drawable.icon_normalhitch);
	    		}
				hcis.add(hci);
    		}
    		hi.setHcis(hcis);
    		hgis.add(hi);
    	}
    }
    
    @Override 
    public void onClick(View v) {
    	super.onClick(v);
    	switch (v.getId()) {
		case R.id.rl_score:
            /*if(checkClickFrequency()){
                CommonUtils.showLongToast(this, getString(R.string.tip_click_too_much));
                return;
            }*/
			if(manager.isBluetoothConn()){
                synchronized (CarHitchActivity.class){
                    if("点击检测".equals(tv_check.getText().toString())){
                        setChecking();
                        BluetoothInstruct.immediaExperience(); //立即体验
                        Runnable r = new Runnable() {
                            @Override
                            public void run() {
                                initDateView(STATA_OVRE_TIME);
                                setWaitCheck();
                            }
                        };
                        runMap.put(++checkCount,r);
                        handler.postDelayed(r, 10000);
                    }else{
                        CommonUtils.showShortToast(this,"正在体检中，请稍候再试");
                    }
                }


				/*if(null == pd){
					pd = new ProgressDialog(CarHitchActivity.this);
					pd.setMessage(getString(R.string.tip_immedia_experience_waitting));
					pd.show();
				}else{
					if(!pd.isShowing()){
						pd.show();
					}
				}*/
				/*handler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						if(null!=pd&&pd.isShowing()){
							pd.dismiss();
						}
					}
				}, 5000);*/
				//CommonUtils.showShortToast(CarHitchActivity.this, getString(R.string.tip_immedia_experience_waitting));
			}else{
				CommonUtils.showShortToast(CarHitchActivity.this, getString(R.string.tip_bluetooth_disconnect));
			}
			break;

		default:
			break;
		}
    }

    //两次点击 小于3秒中，不给响应...
    private boolean checkClickFrequency() {
        boolean retValue = false;
        if(clickCount!=0){
            long disGapTime = System.currentTimeMillis() - lastTime;
            if(disGapTime < 2*1000){
                retValue = true;    //小于3秒了，过于频繁了..
            }
        }
        lastTime = System.currentTimeMillis();
        clickCount++;
        return retValue;
    }

    @Override
    protected void onDestroy() {
        manager.removeListener(listeners);

        super.onDestroy();
    }

    /**
     * 功能描述: <br>
     * 〈功能详细描述〉
     *
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    private void initData() {
        HitchGroupItem hi1 = new HitchGroupItem();
        hi1.setHitchNum("2");
        hi1.setHitchType("严重故障");
        hi1.setResouceColorId(R.color.text_red_1);
        
        HitchGroupItem hi2 = new HitchGroupItem();
        hi2.setHitchNum("2");
        hi2.setHitchType("一般故障");
        hi2.setResouceColorId(R.color.text_red_2);
        
        
        ArrayList<HitchChildItem> hcis1 = new ArrayList<HitchChildItem>();
        for(int i=0;i<3;i++){
            HitchChildItem hci1 = new HitchChildItem();
            hci1.setHitchName("混合动力电池温度过高"+i);
            hci1.setHitchCarType("适用于所有车辆制造商"+i);
            hci1.setHitchDetail("安全带传感器是根据在坐垫中安装的压力传感器的原理来检查司机或是乘客是否将安全带系好。");
            hci1.setHitchIconId(R.drawable.icon_normalhitch);
            hcis1.add(hci1);
            hi1.setHcis(hcis1);
        }
        
        ArrayList<HitchChildItem> hcis2 = new ArrayList<HitchChildItem>();
        for(int i=0;i<2;i++){
            HitchChildItem hci1 = new HitchChildItem();
            hci1.setHitchName("发动机转速过低"+i);
            hci1.setHitchCarType("适用于所有车辆制造商"+i);
            hci1.setHitchDetail("发动机转速过低，建议检测下，看看问题。垫中安装的压力传感器的原理来检查司机或是乘客是否将安全带系好。");
            hci1.setHitchIconId(R.drawable.icon_serioushitch);
            hcis2.add(hci1);
            hi2.setHcis(hcis2);
        }
        
      
        hgis.add(hi1);
        hgis.add(hi2);
    }

}
