/*
  */
package com.sheep.hub.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sheep.hub.CommonListActivity;
import com.sheep.hub.OfflineMapListActivity;
import com.sheep.hub.R;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.Preference;
import com.sheep.hub.util.TTSController;
import com.sheep.hub.view.MySwitchBtn;
import com.sheep.hub.view.MySwitchBtn.OnSwitchListener;

import java.util.ArrayList;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class NavigationActivity extends BaseActivity {

    private static final int VOICE_REQ = 0;
    private static final int THEME_REQ = 1;

    private RelativeLayout rl_map_manager;
    private RelativeLayout rl_map_colorselector;
    private RadioGroup rg_daynight_mode;
    private RadioGroup rg_font;
    private MySwitchBtn msb_street;
    private MySwitchBtn msb_voice;
    private RelativeLayout rl_voicetype_selector;
    private RadioGroup rg_voicerate;

    private int mOpen = R.drawable.witchoff_check_bg_green;
    private int mClose = R.drawable.witchoff_check_bg_white;
    private int mCircle = R.drawable.witchoff_check_handler;
    private Button btn_reset;

    private TextView tv_voice;
    private ArrayList<String> voiceTypeList;

    private TextView tv_theme;
    private ArrayList<String> themeList;

    @Override
    protected void loadView() {
        setContentView(R.layout.activity_navigation_setting);
    }

    @Override
    protected void findView() {
        rl_map_manager = (RelativeLayout) findViewById(R.id.rl_map_manager);
        rl_map_colorselector = (RelativeLayout) findViewById(R.id.rl_map_colorselector);
        rg_daynight_mode = (RadioGroup) findViewById(R.id.rg_daynight_mode);
        rg_font = (RadioGroup) findViewById(R.id.rg_font);
        msb_street = (MySwitchBtn) findViewById(R.id.msb_street);
        msb_voice = (MySwitchBtn) findViewById(R.id.msb_voice);
        rl_voicetype_selector = (RelativeLayout) findViewById(R.id.rl_voicetype_selector);
        rg_voicerate = (RadioGroup) findViewById(R.id.rg_voicerate);
        btn_reset = (Button) findViewById(R.id.btn_reset);

        tv_voice = (TextView) findViewById(R.id.tv_voice);
        voiceTypeList = new ArrayList<String>();
        for (String temp : getResources().getStringArray(R.array.navi_voice_key))
            voiceTypeList.add(temp);
        tv_voice.setText(voiceTypeList.get(Preference.getInt(Constant.PRE_VOICE_TYPE, 0)));

        tv_theme = (TextView) findViewById(R.id.tv_theme);
        themeList = new ArrayList<String>();
        for (String temp : getResources().getStringArray(R.array.navi_theme))
            themeList.add(temp);
        tv_theme.setText(themeList.get(Preference.getInt(Constant.PRE_NAVI_THEME)));
    }

    @Override
    protected void setListener() {
        rl_map_manager.setOnClickListener(this);
        rl_map_colorselector.setOnClickListener(this);
        rl_voicetype_selector.setOnClickListener(this);
        rg_daynight_mode.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_auto:
                        Preference.putString(Constant.K_DAYNIGHT_MODE, Constant.V_AUTO);
                        // TODO 改变昼夜模式-自动
                        break;
                    case R.id.rb_day:
                        Preference.putString(Constant.K_DAYNIGHT_MODE, Constant.V_DAY);
                        // TODO 改变昼夜模式-白天
                        break;
                    case R.id.rb_night:
                        Preference.putString(Constant.K_DAYNIGHT_MODE, Constant.V_NIGHT);
                        // TODO 改变昼夜模式-黑夜
                        break;

                    default:
                        break;
                }
            }
        });
        rg_font.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_middle:
                        Preference.putString(Constant.K_FONT, Constant.V_MIDDLE);
                        // TODO 改变字体-中
                        break;
                    case R.id.rb_big:
                        Preference.putString(Constant.K_FONT, Constant.V_BIG);
                        // TODO 改变字体-大
                        break;
                    case R.id.rb_small:
                        Preference.putString(Constant.K_FONT, Constant.V_SMALL);
                        // TODO 改变字体-小
                        break;

                    default:
                        break;
                }
            }
        });

        rg_voicerate.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_normal:
                        Preference.putString(Constant.K_VOICE_RATE, Constant.V_NORMAL);
                        TTSController.getInstance().setVoiceFrequency(Constant.V_NORMAL);
                        // TODO 改变语音频率-一般
                        break;
                    case R.id.rb_frequent:
                        Preference.putString(Constant.K_VOICE_RATE, Constant.V_FREQUENT);
                        TTSController.getInstance().setVoiceFrequency(Constant.V_FREQUENT);
                        // TODO 改变语音频率-频繁
                        break;

                    default:
                        break;
                }
            }
        });

        msb_street.setOnSwitchListener(new OnSwitchListener() {

            @Override
            public void onSwitched(boolean isSwitchOn) {
                //TODO 街区详图，开关处理
                Preference.putBoolean(Constant.PRE_LAYER, isSwitchOn);
            }
        });
        msb_voice.setOnSwitchListener(new OnSwitchListener() {

            @Override
            public void onSwitched(boolean isSwitchOn) {
                //TODO 语音，开关处理
                Preference.putBoolean(Constant.PRE_SOUNDS, isSwitchOn);
                TTSController.getInstance().setVolume(isSwitchOn);
            }
        });

        btn_reset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_map_manager:
                startAct(OfflineMapListActivity.class);
                break;
            case R.id.btn_reset:
                resetSetting();
                break;
            case R.id.rl_map_colorselector:
                Intent intent = new Intent(this, CommonListActivity.class);
                intent.putExtra(CommonListActivity.LIST, themeList);
                intent.putExtra(CommonListActivity.TITLE, getString(R.string.navigation_theme));
                startActivityForResult(intent, THEME_REQ);
                break;
            case R.id.rl_voicetype_selector:
                Intent voiceIntent = new Intent(this, CommonListActivity.class);
                voiceIntent.putExtra(CommonListActivity.LIST, voiceTypeList);
                voiceIntent.putExtra(CommonListActivity.TITLE, getString(R.string.voice_type));
                startActivityForResult(voiceIntent, VOICE_REQ);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case VOICE_REQ:
                    int voicePosition = data.getIntExtra("position", 0);
                    tv_voice.setText(voiceTypeList.get(voicePosition));
                    Preference.putInt(Constant.PRE_VOICE_TYPE, voicePosition);
                    TTSController.getInstance().setVoiceMan(voicePosition);
                    break;
                case THEME_REQ:
                    int themePosition = data.getIntExtra("position", 0);
                    tv_theme.setText(themeList.get(themePosition));
                    Preference.putInt(Constant.PRE_NAVI_THEME, themePosition);
                default:
            }
        }
    }

    private void resetSetting() {
        //TODO 地图配色
        Preference.putInt(Constant.PRE_NAVI_THEME, 0);
        tv_theme.setText(themeList.get(0));
        //TODO 尽夜模式
        RadioButton rbDayNightModeChecked = (RadioButton) rg_daynight_mode.findViewById(R.id.rb_auto);
        rbDayNightModeChecked.setChecked(true);
        //TODO 字体大小 恢复 
        RadioButton rbFontChecked = (RadioButton) rg_font.findViewById(R.id.rb_middle);
        rbFontChecked.setChecked(true);
        //TODO 语音提示频率 
        RadioButton rbVoiceRateChecked = (RadioButton) rg_voicerate.findViewById(R.id.rb_normal);
        rbVoiceRateChecked.setChecked(true);

        //TODO 语音类型
        Preference.putInt(Constant.PRE_VOICE_TYPE, 0);
        tv_voice.setText(voiceTypeList.get(0));
        TTSController.getInstance().setVoiceMan(0);

        //选区详图
        msb_street.setSwitchState(false);
        msb_street.invalidate();
        //语音
        msb_voice.setSwitchState(true);
        msb_voice.invalidate();
    }

    @Override
    protected void doLogic() {
        setTitle(R.string.title_navigation_setting);

        msb_street.setImageResource(mOpen, mClose, mCircle);
        msb_voice.setImageResource(mOpen, mClose, mCircle);
        //获取尽夜模式
        String daynightMode = Preference.getString(Constant.K_DAYNIGHT_MODE, Constant.V_AUTO);
        int daynightModeCheckedId = R.id.rb_auto;
        if (Constant.V_AUTO.equals(daynightMode)) {
            daynightModeCheckedId = R.id.rb_auto;
        } else if (Constant.V_DAY.equals(daynightMode)) {
            daynightModeCheckedId = R.id.rb_day;
        } else if (Constant.V_NIGHT.equals(daynightMode)) {
            daynightModeCheckedId = R.id.rb_night;
        }
        RadioButton rbDayNightModeChecked = (RadioButton) rg_daynight_mode.findViewById(daynightModeCheckedId);
        rbDayNightModeChecked.setChecked(true);

        //获取字体大小 
        String fontValue = Preference.getString(Constant.K_FONT, Constant.V_MIDDLE);
        int fontCheckedId = R.id.rb_middle;
        if (Constant.V_MIDDLE.equals(fontValue)) {
            fontCheckedId = R.id.rb_middle;
        } else if (Constant.V_BIG.equals(fontValue)) {
            fontCheckedId = R.id.rb_big;
        } else if (Constant.V_SMALL.equals(fontValue)) {
            fontCheckedId = R.id.rb_small;
        }
        RadioButton rbFontChecked = (RadioButton) rg_font.findViewById(fontCheckedId);
        rbFontChecked.setChecked(true);

        //获取语音提示频率
        String voiceRateValue = Preference.getString(Constant.K_VOICE_RATE, Constant.V_NORMAL);
        int voiceRateCheckId = R.id.rb_normal;
        if (Constant.V_NORMAL.equals(voiceRateValue)) {
            voiceRateCheckId = R.id.rb_normal;
        } else if (Constant.V_FREQUENT.equals(voiceRateValue)) {
            voiceRateCheckId = R.id.rb_frequent;
        }
        RadioButton rbVoiceRateChecked = (RadioButton) rg_voicerate.findViewById(voiceRateCheckId);
        rbVoiceRateChecked.setChecked(true);

        boolean streetMapSwitch = Preference.getBoolean(Constant.PRE_LAYER, false);

        msb_street.setSwitchState(streetMapSwitch);
        msb_voice.setSwitchState(Preference.getBoolean(Constant.PRE_SOUNDS, true));

    }

}
