package com.sheep.hub.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.sheep.hub.bluetooth.BluetoothInstruct;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class NotiService extends NotificationListenerService {
    Context context;
    private static final String TAG = "GLR NotiService";

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        String pack = sbn.getPackageName();
        Bundle extras = sbn.getNotification().extras;
        String title = extras.getString("android.title");
        String extrastring = extras.toString();
        String text = extras.getCharSequence("android.text").toString();
        //String bigtext = extras.getCharSequence("android.bigText").toString();

        if(pack.equals("com.google.android.apps.maps")) {
            Log.i(TAG,"----------GOOGLE NAVIGATION----------");
            Log.i(TAG, text);

            String[] parts = text.split(" ");
            int maneuvertype = 0x04;
            int distance = 0;
            String distanceString;

            if (text.contains("ft -")) {
                distanceString = text.substring(0, text.indexOf("ft -")-1);
                distance = Integer.parseInt(distanceString);
                Log.i(TAG, "Distance = " + distance + " ft");

                distance = (int)(distance*0.9144);                  //Convert unit from feet to meter
            }
            else if (text.contains("mi -")) {
                float f;
                distanceString = text.substring(0, text.indexOf("mi -")-1);
                f = Float.parseFloat(distanceString);
                Log.i(TAG, "Distance = " + f + " mi");

                distance = (int)(f*1609.34);                        //Convert unit from mile to meter
            }

            if (text.contains("Continue") || text.contains("Head")) {
                maneuvertype = 0x04;
            }
            else if (text.contains("U-turn")) {
                maneuvertype = 0x08;
            }
            else if (text.contains("Turn left") || text.contains("turn left")) {
                maneuvertype = 0x02;
            }
            else if (text.contains("Turn right") || text.contains("turn right")) {
                maneuvertype = 0x06;
            }
            else if (text.contains("exit")) {
                maneuvertype = 0x05;
            }

            BluetoothInstruct.setFrequenceSendMsg(maneuvertype, distance);
        }
        else {
            Log.i(TAG, "----------"+pack+"----------");
            Log.i(TAG, "Title:\t"+title);
            Log.i(TAG, "Extra:\t"+extrastring);
            //Log.i(TAG, "Text:\t"+text);
            //Log.i(TAG, "NotiService: Bigtext:\t"+bigtext);
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        String packname = sbn.getPackageName();
        Log.i(TAG, "Notification Removed: "+packname);
    }
}