/*
  */
package com.sheep.hub.activity;

import java.util.ArrayList;
import java.util.Random;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.sheep.framework.db.AfeiDb;
import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.framework.view.pulltorefresh.PullToRefreshAnimatedExpandListView;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase.Mode;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase.OnRefreshListener2;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;
import com.sheep.hub.adapter.HistoryScoreGroupAdapter;
import com.sheep.hub.bean.DSChildItem;
import com.sheep.hub.bean.DSGroupItem;
import com.sheep.hub.bean.DriveScore;
import com.sheep.hub.view.AnimatedExpandableListView;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class HistoryDriveScoreFragment extends Fragment implements OnClickListener {

    private AnimatedExpandableListView aelv_historyscore;
    private ArrayList<DSGroupItem> gis ;
    private HistoryScoreGroupAdapter adapter;
    private PullToRefreshAnimatedExpandListView ptraxl_score;
    
    private ArrayList<DriveScore> displayDriveScores ;
    private int currentPage = 1; //从第1页开始，每页10条数据
    private int perPageCount = 10; //每页的数据
    
    private Mode reFreshMode ;
    
    
    
    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	if(null == gis){
    		gis = new ArrayList<DSGroupItem>();
    	}else{
    		gis.clear();
    	}
    	
    	if(null == displayDriveScores){
    		displayDriveScores = new ArrayList<DriveScore>(); 
    	}else{
    		displayDriveScores.clear();
    	}
    	
        View view = inflater.inflate(R.layout.fragment_historyscore, null, false);
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_title.setText(getActivity().getString(R.string.title_history_drive_score_recode));
        
        ImageView iv_back = (ImageView) view.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        
        
        
        ptraxl_score = (PullToRefreshAnimatedExpandListView)view.findViewById(R.id.aelv_historyscore);
        ptraxl_score.setMode(Mode.BOTH);
        aelv_historyscore = ptraxl_score.getRefreshableView();
       
        ptraxl_score.setOnRefreshListener(new OnRefreshListener2() {

			@Override
			public void onPullDownToRefresh(PullToRefreshBase refreshView) {
				//L.e("--------> ptraxl_score ...onRefresh----onPullDownRefreshComplete");
				reFreshMode = Mode.PULL_FROM_START;
				currentPage = 1;
				new DriveScoreReader().execute(currentPage); //每次都从第1页开始加载
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase refreshView) {
				//L.e("--------> ptraxl_score ...onRefresh -----onPullUpRefreshComplete");
				reFreshMode = Mode.PULL_FROM_END;
				new DriveScoreReader().execute(++currentPage); //每次都从第1页开始加载
			}
		});
        
        //initData();
        new DriveScoreReader().execute(1);
        adapter = new HistoryScoreGroupAdapter(getActivity(), gis);
        aelv_historyscore.setAdapter(adapter);
        
        aelv_historyscore.setGroupIndicator(null);
        
        aelv_historyscore.setOnGroupClickListener(new OnGroupClickListener() {
            
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                
                if(aelv_historyscore.isGroupExpanded(groupPosition)){
                    aelv_historyscore.collapseGroupWithAnimation(groupPosition);
                }else{
                    aelv_historyscore.expandGroupWithAnimation(groupPosition);
                }
                
                int groupCount = adapter.getGroupCount();
                for(int i=0;i<groupCount;i++){
                    if (aelv_historyscore.isGroupExpanded(i)&&i!=groupPosition) {
                        aelv_historyscore.collapseGroupWithAnimation(i);
                    }
                }
                
                return true;
            }
        });
        
        return view;
    }


	private void ds2DsGroup() {
		if(null != displayDriveScores && displayDriveScores.size()>0){
			gis.clear(); //先清除之类的数据
			for(DriveScore ds:displayDriveScores){
				DSGroupItem dsg = new DSGroupItem();
				ArrayList<DSChildItem> cis = new ArrayList<DSChildItem>();
	            dsg.setDriveScore(ds.getDriveScore());
	            dsg.setStartTime(ds.getStartTime());
	            
	            DSChildItem cis1 = new DSChildItem();
	            cis1.setKey("驾驶结束时间：");
	            cis1.setValue(ds.getEndTime());
	            
	            DSChildItem cis2 = new DSChildItem();
	            cis2.setKey("急加速次数：");
	            cis2.setValue(ds.getEmerAddSpeedTime());
	            
	            DSChildItem cis3 = new DSChildItem();
	            cis3.setKey("急减速次数：");
	            cis3.setValue(ds.getEmerSubSpeedTime());
	            
	            DSChildItem cis4 = new DSChildItem();
	            cis4.setKey("引擎空转时间：");
	            cis4.setValue(ds.getEmptyRunningTime()+"min");
	            
	            DSChildItem cis5 = new DSChildItem();
	            cis5.setKey("超速时间：");
	            cis5.setValue(ds.getOverSpeedTime()+"min");
	            
	            DSChildItem cis6 = new DSChildItem();
	            cis6.setKey("平均油耗：");
	            cis6.setValue(Integer.valueOf(ds.getAvgCostOil())*100+"L/H");
	            
	            cis.add(cis1);
	            cis.add(cis2);
	            cis.add(cis3);
	            cis.add(cis4);
	            cis.add(cis5);
	            cis.add(cis6);
	            dsg.setCis(cis);
	            gis.add(dsg);
			}
		}
		
		adapter.setData(gis);
	}
	
	class DriveScoreReader extends AsyncTask<Integer, Void, ArrayList<DriveScore>> {

		private ProgressDialog dialog;
		
		
		@Override
		protected ArrayList<DriveScore> doInBackground(Integer... params) {
			AfeiDb afeiDb = HubApp.getInstance().getAfeiDb();
			int page = params[0];
			int count = afeiDb.calculateCountByWhereStr(DriveScore.class, "");
			int sumPage = (count+perPageCount-1)/perPageCount;
			//L.e("-->>sum count:"+count +" sumPage::"+sumPage+" page::"+page);
			if(page>sumPage){
				currentPage --;
				return null;
			}else{
				ArrayList<DriveScore> driveScores = afeiDb.findAllByWhereStrWithOrderAndLimitStr(DriveScore.class, "", "time desc", perPageCount*(page-1)+","+perPageCount);
				return driveScores;
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = CommonUtils.showProgressDialog(getActivity(), R.string.tip_load_data_ing);
		}

		@Override
		protected void onPostExecute(ArrayList<DriveScore> result) {
			super.onPostExecute(result);
			dialog.dismiss();
			
			if(Mode.PULL_FROM_END==reFreshMode){
				if(null != result && result.size()>0){
					displayDriveScores.addAll(result);
				}
				ptraxl_score.onPullUpRefreshComplete();
			}else if(Mode.PULL_FROM_START==reFreshMode){
				displayDriveScores.clear();
				if(null != result && result.size()>0){
					displayDriveScores.addAll(result);
				}
				ptraxl_score.onPullDownRefreshComplete();
			}else{
				if(null != result && result.size()>0){
					displayDriveScores.addAll(result);
				}
			}
			ds2DsGroup();
			//L.e("-->>displayDriveScores::size:"+displayDriveScores.size() +" currentPage:"+currentPage);
		}
	}

	

    /* (non-Javadoc)
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                getActivity().finish();
                break;

            default:
                break;
        }
    }
   
}
