/*
 */
package com.sheep.hub.activity;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.AboutActivity;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;
import com.sheep.hub.bluetooth.BluetoothInstruct;
import com.sheep.hub.bluetooth.BluetoothManager;
import com.sheep.hub.bluetooth.IBluetoothListener;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.dialog.LogoutDialog;
import com.sheep.hub.dialog.ObdSetDialog;
import com.sheep.hub.util.Preference;
import com.sheep.hub.util.ScreenBrightUtil;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 * 
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class SettingActivity extends BaseActivity {

	private RelativeLayout rl_position;
	private RelativeLayout rl_obd_setting;
	private RelativeLayout rl_update_set;
	private RelativeLayout rl_about_set;
	private RelativeLayout rl_poweroff_set;
	private SeekBar sb_screenbright;
	private HubApp hubApp;
	private BluetoothManager manager;


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sheep.hub.BaseActivity#loadView()
	 */
	@Override
	protected void loadView() {
		setContentView(R.layout.activity_setting);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sheep.hub.BaseActivity#findView()
	 */
	@Override
	protected void findView() {
		rl_position = (RelativeLayout) findViewById(R.id.rl_position);
		rl_obd_setting = (RelativeLayout) findViewById(R.id.rl_obd_setting);
		rl_update_set = (RelativeLayout) findViewById(R.id.rl_update_set);
		rl_about_set = (RelativeLayout) findViewById(R.id.rl_about_set);
		rl_poweroff_set = (RelativeLayout) findViewById(R.id.rl_poweroff_set);

		sb_screenbright = (SeekBar) findViewById(R.id.sb_screenbright);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sheep.hub.BaseActivity#setListener()
	 */
	@Override
	protected void setListener() {
		rl_position.setOnClickListener(this);
		rl_obd_setting.setOnClickListener(this);
		rl_update_set.setOnClickListener(this);
		rl_about_set.setOnClickListener(this);
		rl_poweroff_set.setOnClickListener(this);
		sb_screenbright.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
					private int curentBright;

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						setScreenBrightness(curentBright);
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						if (fromUser) {
							// 不需要设置手机的亮度
							curentBright = progress;
						}
					}
				});
	}

	/**
	 * 保存当前的屏幕亮度值，并使之生效
	 */
	private void setScreenBrightness(int curentBright) {
		// 准备发送蓝牙命令
		String bluetoothPwd = hubApp.getBluetoothPwd();

		if (!manager.isBluetoothConn()) {
			CommonUtils.showShortToast(SettingActivity.this,
					getString(R.string.tip_bluetooth_disconnect));
		} else {
			Preference.putInt(Constant.K_BRIGHT, curentBright);
			BluetoothInstruct.sendSetMessage(bluetoothPwd, curentBright);
			//L.e("----->> BluetoothInstruct.sendSetMessage --- 屏幕亮度值:"+curentBright);
			
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sheep.hub.activity.BaseActivity#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.rl_position:
			startAct(NavigationActivity.class);
			break;
		case R.id.rl_obd_setting:
			if (!manager.isBluetoothConn()) {
				CommonUtils.showShortToast(SettingActivity.this,
						getString(R.string.tip_bluetooth_disconnect));
			}else{
				showObdSetDialog();
			}
			break;
		case R.id.rl_update_set:
            Toast.makeText(this,R.string.can_not_assist,Toast.LENGTH_SHORT).show();
            break;
		case R.id.rl_about_set:
			startAct(AboutActivity.class);
			break;
		case R.id.rl_poweroff_set:
			showLogoutDialog();
			break;

		default:
			break;
		}
	}

	/**
	 * 功能描述: <br>
	 * 〈功能详细描述〉
	 *
	 * @see [相关类/方法](可选)
	 * @since [产品/模块版本](可选)
	 */
	private void showLogoutDialog() {
		LogoutDialog dialog = new LogoutDialog(this, R.style.DialogNoTitle);
        dialog.setOnLogoutListener(new LogoutDialog.LogoutListener() {
            @Override
            public void onLogoutOk() {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onLogoutCancel() {

            }
        });
		dialog.show();
	}

	/**
	 * 功能描述: <br>
	 * 〈功能详细描述〉
	 *
	 * @see [相关类/方法](可选)
	 * @since [产品/模块版本](可选)
	 */
	private void showObdSetDialog() {
		ObdSetDialog dialog = new ObdSetDialog(this, R.style.DialogNoTitle);
		ObdSetDialog.BluetoothPwdSureListener listener = new ObdSetDialog.BluetoothPwdSureListener() {

			@Override
			public void onBluetoothPwdSure(String pwd) {
				if (!TextUtils.isEmpty(pwd)) {
					if(4 == pwd.length()){
						CommonUtils.showLongToast(SettingActivity.this, "pwd:"+ pwd);
						hubApp.setBluetoothPwd(pwd);
					}else{
						CommonUtils.showLongToast(SettingActivity.this, getString(R.string.tip_bluetooth_pwd_length_error));
					}
				}
			}
		};
		dialog.setOnBluetoothPwdSureListener(listener);
		dialog.show();
	}

	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_SUCCESS:
				//CommonUtils.showLongToast(SettingActivity.this,getString(R.string.tip_set_message_command_success));
				break;
			case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_FAILURE:
				//CommonUtils.showLongToast(SettingActivity.this,getString(R.string.tip_set_message_command_failure));
				break;

			default:
				break;
			}
			
		};
	};
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sheep.hub.BaseActivity#doLogic()
	 */
	@Override
	protected void doLogic() {
		//isReceiveSetMessageCommand = false;
		hubApp = HubApp.getInstance();
		setTitle(R.string.title_setting);

		manager = BluetoothManager.getInstance();

		manager.addListener(new IBluetoothListener() {

			@Override
			public void onStateChange(int state,int what) {
				switch (state) {
				case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_SUCCESS:
					handler.sendEmptyMessage(BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_SUCCESS);
					break;
				case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_FAILURE:
					handler.sendEmptyMessage(BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_FAILURE);
					break;
				default:
					break;
				}
			}

			@Override
			public void onScanningDevice(String name, String address,
					int bondState) {

			}
		});

		/*int screenBrightness = ScreenBrightUtil.getScreenBrightness(this);
		int progress = (int) ((screenBrightness / 255.0f) * 100);*/
		int progress = Preference.getInt(Constant.K_BRIGHT);
		sb_screenbright.setProgress(progress);
	}

}
