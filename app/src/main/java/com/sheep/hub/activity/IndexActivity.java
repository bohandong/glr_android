package com.sheep.hub.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.CallLog;
import android.provider.Telephony;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sheep.framework.db.AfeiDb;
import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.AboutActivity;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;
import com.sheep.hub.bean.DriveScore;
import com.sheep.hub.bean.FrequenceMsg;
import com.sheep.hub.bluetooth.BluetoothInstruct;
import com.sheep.hub.bluetooth.BluetoothManager;
import com.sheep.hub.bluetooth.IBluetoothListener;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.dialog.BrightDialog;
import com.sheep.hub.dialog.BrightDialog.OnBrightChangeListener;
import com.sheep.hub.dialog.ObdSetDialog;
import com.sheep.hub.util.MmsTelTool;
import com.sheep.hub.util.Preference;

public class IndexActivity extends BaseActivity {

    private RelativeLayout rl_navi;
    private RelativeLayout rl_bright;
    private RelativeLayout rl_bluetoothconn;
    private RelativeLayout rl_checkhitch;
    private RelativeLayout rl_driverecode;
    private RelativeLayout rl_obd_set;
    private RelativeLayout rl_about;


    private HubApp hubApp;
    private BluetoothManager manager;
    private TextView tv_name22;

    private int count;  //计数器
    private boolean isDiscovingFinish = false;


    private ProgressDialog pd;
    private static final int DISCOVING_FINISH = 1000;

    private boolean isObdsetting = false;

    private NaviDataCollectorEnginer naviDataCollectorEnginer;

    private boolean isVisiable = false;

    private boolean isPageFinish = false;

    private int initCount = 0;//起动计数

    private boolean isFoundDevice = false;

    private BroadcastReceiver baiduPackageAddedReceiver;

    //private boolean enableAutoConnection;
    private int autoConnectCount;

    //add by lvmu 20150610 start
    private boolean handConnect = true;
    private boolean handConnect1 = true;
    private boolean disconbyhand = false;
    private boolean atconnecting = false;
    //private boolean first_kidou = true;
    private boolean first_connect = false;
    private boolean ispaired_glrhud = false;
    private boolean first_kidou_bt_open = false;
    //add by lvmu 20150610 end

    private Runnable autoConnectRunnable = new Runnable() {
        @Override
        public void run() {
            //modify by lvmu 20150610 start
              L.e("autoConnectRunnable", "自动重连............."+autoConnectCount);
            autoConnectCount++;
            if (isFoundDevice == false) {
                //  if(autoConnectCount<=3) {
                handConnect1 = false;
                //try {
                //    manager.remove_glrhud();
                //} catch (Exception e) {
                //    e.printStackTrace();
                //}
                 //          manager.unregisterReceiver();
                 //          manager.registerBroadcast();
//            manager.removeListener(listeners);
                //           manager.addListener(listeners);
                //               manager.releaseResource();
                prepareConn();
                //  }
                // modify by lvmu 20150610 end
            }
        }
    };
    private Runnable dismissDialogRunnable = new Runnable() {
        @Override
        public void run() {
            if (null != pd && pd.isShowing()) {
                pd.dismiss();
            }
        }
    };
    private long handConnTime = 0;
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                //add by lvmu 20150623 start
                case BluetoothManager.STATE_FIRST_KIDOU_BT_OPEN:
                    //if( first_kidou == true )
                    //{
                    //    first_kidou = false;
                    //}
                    tv_name22.setText("连接中...");
                    break;
                //add by lvmu 20150623 end


                case BluetoothManager.STATE_DEVICE_DISCOVERING: //只有在正在扫描中，出现这个动画
                    //log lvmu
                    L.e("XX_STATE_DEVICE_DISCOVERING");
                    //add by lvmu 20150619 start
                    atconnecting = false;
                    //add by lvmu 20150619 end
                    if (!isDiscovingFinish && (null == pd || !pd.isShowing())) {
                        pd = new ProgressDialog(IndexActivity.this);
                        pd.setMessage("正在连接HUD中，请稍候...");
                        pd.setCancelable(true);
                        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                removeCallbacks(dismissDialogRunnable);
                            }
                        });
                        //delete by lvmu 20150610 start
                        //pd.show();
                        //postDelayed(dismissDialogRunnable, 15000);
                        //delete by lvmu 20150610 end
                        //add by lvmu 20150610 start
                        //if( handConnect1 == true ) {
                        //    pd.show();
                        //    postDelayed(dismissDialogRunnable, 15000);
                        //}
                        //add by lvmu 20150610 end
                    }

                    break;
                case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_SUCCESS:
                    if (isObdsetting) {
                        CommonUtils.showShortToast(IndexActivity.this, getString(R.string.tip_set_message_obdset_success));
                    }
                    break;
                case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_FAILURE:
                    //CommonUtils.showLongToast(SettingActivity.this,getString(R.string.tip_set_message_command_failure));
                    break;
                case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_OBDCONING:
                    CommonUtils.showShortToast(IndexActivity.this, "OBD正在连接中，请断开OBD重试!");
                    break;
                case BluetoothManager.STATE_CONNECTED:
                    //log lvmu


                    //enableAutoConnection = true;
                    //autoConnectCount=3;
                    try {
                        ispaired_glrhud = manager.ispaired_glrhud();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //if( isFoundDevice == true && ispaired_glrhud )
                    if( ispaired_glrhud )
                    {
                        for(int i=0;i<autoConnectCount;i++) {
                            handler.removeCallbacks(autoConnectRunnable);
                        }
                        first_connect = true;
                        CommonUtils.showShortToast(IndexActivity.this, getString(R.string.tip_bluetooth_connsuccess));
                        tv_name22.setText("断开连接");
                        L.e("XX_STATE_CONNECTED");
                        //add by lvmu 20150610 start
                        handConnect = false;
                        //add by lvmu 20150610 end
                        if (null != pd && pd.isShowing()) {
                            pd.dismiss();
                        }

                        try {
                            Thread.sleep(500); //尝试 休眠100毫秒
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        queryUnCallAndSms();
                        //first_kidou = false;
                    }
                    else if( atconnecting == false && disconbyhand == false ) {
                        handler.postDelayed(autoConnectRunnable, 3000);
                        atconnecting = true;
                    }
                    break;
                case BluetoothManager.STATE_NOTFOUND:
                    //log lvmu
                    L.e("XX_STATE_NOTFOUND");
                    //try {
                    //        manager.remove_glrhud();
                    //    } catch (Exception e) {
                    //    e.printStackTrace();
                    //}
                    //if(IndexActivity.this.is)
                    //modify by lvmu 20150610 start
                    //if (isVisiable ) {
                    //    CommonUtils.showShortToast(IndexActivity.this, "未找到匹配的设备");
                    //}
                    //add by lvmu 20150611 start
                    //if( atconnecting == false && first_kidou == false )
                     if( atconnecting == false ) {
                        handler.postDelayed(autoConnectRunnable, 3000);
                        atconnecting = true;
//                        L.e( "AUTOCONN_STATE_NOTFOUND" );
                    }
                    //add by lvmu 20150611 end
                    //modify by lvmu 20150610 end
                    if (null != pd && pd.isShowing()) {
                        pd.dismiss();
                    }
                    break;
                //add by lvmu 20150620 start
                case BluetoothManager.STATE_BLUETOOTH_SHUT_DOWN_HAND:
                    L.e("XX_STATE_BLUETOOTH_SHUT_DOWN_HAND");
                    if( !manager.isBluetoothEnable()) {
                        disconbyhand = true;
                        manager.releaseResource();
                        tv_name22.setText("连接HUD");
                        handConnect = true;
                    }
                    else{
                        //if( atconnecting == false && disconbyhand == false && first_kidou == false ) {
                        if( atconnecting == false && disconbyhand == false ){
                            handler.postDelayed(autoConnectRunnable, 3000);
                            atconnecting = true;
                        }
                        else if( disconbyhand == true )
                        {
                            tv_name22.setText("连接HUD");
                        }
                        handConnect = true;
                    }
                    break;
                //add by lvmu 20150620 end
                case BluetoothManager.STATE_BLUETOOTH_DISCONNECT:
                    //log lvmu
                    L.e("INDEX","XX_STATE_BLUETOOTH_DISCONNECT");
                    //if (enableAutoConnection) {
                        //autoConnectCount=0;
                        //add by lvmu 20150611 start
                        //if( atconnecting == false && disconbyhand == false && first_kidou == false ) {
                        if( atconnecting == false && disconbyhand == false ) {
                            tv_name22.setText("连接中...");
                            handler.postDelayed(autoConnectRunnable, 3000);
                            L.e("INDEX","AUTOCONN_STATE_BLUETOOTH_DISCONNECT");
                            atconnecting = true;
                        }
                        else if( disconbyhand == true )
                        {
                            tv_name22.setText("连接HUD");
                        }

                        //add by lvmu 20150611 end
                   // }

                    manager.releaseResource();
                    //add by lvmu 20150611 start
                    //disconbyhand = false;
                    handConnect = true;
                    //add by lvmu 20150611 end
                    break;
                //add by lvmu 20150619 start
                case BluetoothManager.STATE_BLUETOOTH_PAIRE_FAILURE:
                    //if( atconnecting == false && first_kidou == false ) {
                    if( atconnecting == false ) {
                        tv_name22.setText("连接中...");
                        handler.postDelayed(autoConnectRunnable, 3000);
                        L.e("AUTOCONN_STATE_BLUETOOTH_PAIRE_FAILURE");
                        atconnecting = true;
                    }
                    break;
                //add by lvmu 20150619 end

                case BluetoothManager.STATE_DISCOVERED: //查找结束....
                    //log lvmu
                    L.e("INDEX","XX_STATE_DISCOVERED");
                    //delete by lvmu 20150610 start
                    //if (null != pd && pd.isShowing() && !isFoundDevice) {
                    //    pd.dismiss();
                    //    CommonUtils.showShortToast(IndexActivity.this, "蓝牙未扫描到匹配的设备");
                    //}
                    // delete by lvmu 20150610 end
                    //add by lvmu 20150610 start
                    //if( !isFoundDevice && atconnecting == false && first_kidou == false ) {
                    if( !isFoundDevice && atconnecting == false ) {
                        //add by lvmu 20150611 start
                            tv_name22.setText("连接中...");
                            handler.postDelayed(autoConnectRunnable, 3000);
                            L.e("AUTOCONN_STATE_DISCOVERED");
                            atconnecting = true;
                        //add by lvmu 20150611 end
                    }
                    if( null != pd && pd.isShowing() ) {
                        pd.dismiss();
                    }
                    //first_kidou = false;
                    //add by lvmu 20150610 end

                    //delete by lvmu 20150610 start
                    //if (enableAutoConnection&&autoConnectCount<3) {
                    //    handler.postDelayed(autoConnectRunnable, 3000);
                    //}
                    //L.e("--->IndexActivity...:STATE_DISCOVERED");
                    // delete by lvmu 20150610 end
                    break;
                default:
                    break;
            }

        }

        ;
    };

    private void queryUnCallAndSms() {
        app.setUnReadSmsNum(MmsTelTool.getUnreadSmsCount(IndexActivity.this) + MmsTelTool.getUnreadMmsCount(IndexActivity.this));
        int missedCalls = MmsTelTool.getMissedCalls(IndexActivity.this);
        app.setUnCallNum(missedCalls);
        prepareMsgCallChangMsg();
    }


    @Override
    protected void onResume() {
        super.onResume();
       /* if(null != naviDataCollectorEnginer && initCount>0){
            naviDataCollectorEnginer.destroy();
            naviDataCollectorEnginer = null;
        }*/
        naviDataCollectorEnginer = NaviDataCollectorEnginer.getInstance(this);
        initCount++;
        isVisiable = true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        isVisiable = false;
    }

    @Override
    protected void loadView() {
        setContentView(R.layout.activity_index);
    }

    @Override
    protected void findView() {
        rl_navi = (RelativeLayout) findViewById(R.id.rl_navi);
        rl_bright = (RelativeLayout) findViewById(R.id.rl_bright);
        rl_bluetoothconn = (RelativeLayout) findViewById(R.id.rl_bluetoothconn);
        rl_checkhitch = (RelativeLayout) findViewById(R.id.rl_checkhitch);
        rl_driverecode = (RelativeLayout) findViewById(R.id.rl_driverecode);
        rl_obd_set = (RelativeLayout) findViewById(R.id.rl_obd_set);
        rl_about = (RelativeLayout) findViewById(R.id.rl_about);

        tv_name22 = (TextView) findViewById(R.id.tv_name22);
        isPageFinish = false;
    }

    @Override
    protected void setListener() {
        rl_navi.setOnClickListener(this);
        rl_bright.setOnClickListener(this);
        rl_bluetoothconn.setOnClickListener(this);
        rl_checkhitch.setOnClickListener(this);
        rl_driverecode.setOnClickListener(this);
        rl_obd_set.setOnClickListener(this);
        rl_about.setOnClickListener(this);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        isPageFinish = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_navi: //------
                checkNavi();
                break;

            case R.id.rl_bright: //调试亮度
                if (!manager.isBluetoothConn()) {
                    CommonUtils.showShortToast(IndexActivity.this, getString(R.string.tip_bluetooth_disconnect));
                } else {
                    showBrightDialog();
                }

                break;
            case R.id.rl_bluetoothconn: //连接

                //add by lvmu 20150610 start
                if( handConnect == true )
                {
                    if( disconbyhand == false ) {

                        if (System.currentTimeMillis() - handConnTime > 3000) {
                            CommonUtils.showLongToast(this, "蓝牙正在连接中，请稍后");
                            handConnTime = System.currentTimeMillis();
                        }

                        //CommonUtils.showLongToast(this, "蓝牙正在连接中，请稍后");
                        //tv_name22.setText("连接中");
                    }
                    else
                    {
                        handConnect1 = true;
                        disconbyhand = false;
                        //try {
                        //    manager.remove_glrhud();
                        //} catch (Exception e) {
                        //    e.printStackTrace();
                        //}
                        prepareConn();
                    }
                    break;
                }
                else
                {
                    showDisconDialog();
                }

                //add by lvmu 20150610 end
                /*
                count++;
                if (!isDiscovingFinish & count > 1) {
                    if (!manager.isBluetoothEnable()) {
                        if (System.currentTimeMillis() - disconnectTime < 2000) {
                            CommonUtils.showLongToast(this, "请不要频繁的连接和断开蓝牙");
                        } else {
                            handConnect = true;
                            handConnect1 = true;
                            prepareConn();
                        }
                    } else {
                        CommonUtils.showLongToast(this, getString(R.string.tip_prepare_bluetooth_conn));
                    }

                } else {
                    if (!manager.isBluetoothConn()) { //准备去连接
                        enableAutoConnection = true;
                        if (System.currentTimeMillis() - disconnectTime < 2000) {
                            CommonUtils.showLongToast(this, "请不要频繁的连接和断开蓝牙");
                        } else {
                            prepareConn();
                        }
                    } else {
                        //add by lvmu 20150610 start
                        enableAutoConnection = false;
                        //add by lvmu 20150610 end
                        showDisconDialog();
                    }
                }
                */
                break;
            case R.id.rl_checkhitch:
                startAct(CarHitchActivity.class);
                break;
            case R.id.rl_driverecode:
                //add by lvmu 20150726 start
                AfeiDb afeiDb = HubApp.getInstance().getAfeiDb();
                int driveScoreNum = afeiDb.calculateCountByWhereStr(DriveScore.class, "");
                if( driveScoreNum == 0 )
                {
                    CommonUtils.showShortToast(IndexActivity.this, "您还没有驾驶记录");
                    return;
                }
                else {
                    //add by lvmu 20150726 end
                    startAct(HistoryDriveScoreActivity.class);
                }
                break;
            case R.id.rl_obd_set:
                if (!manager.isBluetoothConn()) {
                    CommonUtils.showShortToast(IndexActivity.this, getString(R.string.tip_bluetooth_disconnect));
                } else {
                    showObdSetDialog();
                }
                break;
            case R.id.rl_about:
                startAct(AboutActivity.class);
                //checkBaiduRunning();
                break;

            default:
                break;
        }

    }

    private void checkBaiduRunning() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (app.isNaving()) { //蓝牙连接了，发个数据，，没有连接就不发数据了
                    handler.postDelayed(this, 2000);//两秒检查一下，
                    if (!NaviDataCollectorEnginer.isRunningApp(IndexActivity.this, Constant.NAVI_PACKAGENAME)) {
                        //L.e("IndexActivity 检查下 checkBaiduRunning  导航结束了 ");
                    }
                    //L.e("IndexActivity 检查下 checkBaiduRunning ");
                } else {
                    //L.e("IndexActivity 不在导航了 checkBaiduRunning... ");
                }
            }
        }, 500);


    }


    private void showDisconDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("是否确定断开蓝牙连接?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //disconnectTime = System.currentTimeMillis();
                //enableAutoConnection = false;
                manager.releaseResource();
                tv_name22.setText("连接HUD");
                dialog.dismiss();
                //add by lvmu 20150611 start
                disconbyhand = true;
                handConnect = true;
                //add by lvmu 20150611 end


            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    private void showDownloadDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //add by lvmu 20150726 start
        //AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
        //builder2.setMessage("请至应用商店下载最新版本百度导航");
        //add by lvmu 20150726 end
        builder.setMessage("立即下载百度导航?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    Uri uri = Uri.parse("market://details?id=" + Constant.NAVI_PACKAGENAME);//id为包名
                    Intent it = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(it);
                    dialog.dismiss();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        //add by lvmu 20150726 start
        if (Build.MANUFACTURER.equals("samsung")) {
            // dialog.dismiss();
            CommonUtils.showShortToast(IndexActivity.this, "请至应用商店下载最新版本百度导航");
        }
        //add by lvmu 20150726 end
        else
        {
            builder.show();
        }

    }


    /**
     * 1、检查百度导航是否存在，不存在解压安装
     * 2、存在了，直接打开此应用...
     * 3、数据的处理...
     */
    private void checkNavi() {
        int packageExists = CommonUtils.isPackageExists(this, Constant.NAVI_PACKAGENAME);
        if (2 != packageExists) {
            if (null == baiduPackageAddedReceiver) {
                baiduPackageAddedReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (Constant.NAVI_PACKAGENAME.equals(intent.getDataString().substring(8))) {
                            if (null != naviDataCollectorEnginer) {
                                naviDataCollectorEnginer.destroy();
                            }
                            naviDataCollectorEnginer = NaviDataCollectorEnginer.getInstance(IndexActivity.this); //有百度导航时，才开始确认数据
                        }
                    }
                };
                IntentFilter filter = new IntentFilter();
                filter.addAction(Intent.ACTION_PACKAGE_ADDED);
                filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
                filter.addDataScheme("package");
                registerReceiver(baiduPackageAddedReceiver, filter);
            }
            if (0 == packageExists) {
                showDownloadDialog();
            } else if (1 == packageExists) {
                showUpdateDownloadDialog();
            }
        } else {
            if (null != naviDataCollectorEnginer) {
                naviDataCollectorEnginer.destroy();
            }
            naviDataCollectorEnginer = NaviDataCollectorEnginer.getInstance(this); //有百度导航时，才开始确认数据
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            ComponentName cn = new ComponentName("com.baidu.navi", "com.baidu.navi.NaviActivity");
            intent.setComponent(cn);
            startActivity(intent);
        }

    }

    private void showUpdateDownloadDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("百度导航版本过低，部分功能可能存在问题。是否更新？");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Uri uri = Uri.parse("market://details?id=" + Constant.NAVI_PACKAGENAME);//id为包名
                Intent it = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(it);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    private void prepareConn() {
        isDiscovingFinish = false;
        if (!manager.isSupportBluetooth()) {
            CommonUtils.showShortToast(this, "您的手机暂时不支持蓝牙功能");
        } else {
            if (manager.isSupportBluetooth() && !manager.isBluetoothEnable()) {
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE); //请求蓝牙设备打开
                intent.setAction(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE); //设置蓝牙设置可以被三方扫描到
                intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 120); //bluetooth 可见时间 2分钟
                startActivityForResult(intent, 100);
                //L.e("--->开始了...startActivityForResult(intent,100)");
            } else { //蓝牙已经开始了，则直接....打就是了
                //add by lvmu 20150611 start
                tv_name22.setText("连接中...");
                //add by lvmu 20150611 end
                isDiscovingFinish = false;
                manager.enableBluetooth();
                //L.e("--->开始了...manager.enableBluetooth()");
                if (null == pd) {
                    pd = new ProgressDialog(IndexActivity.this);
                    pd.setMessage("正在连接HUD中，请稍候...");
                    pd.setCancelable(true);
                    pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(dismissDialogRunnable);
                        }
                    });
                }
                handler.removeCallbacks(dismissDialogRunnable);
                //delete by lvmu 20150610 start
                //handler.postDelayed(dismissDialogRunnable, 15000);
                //pd.show();
                //delete by lvmu 20150610 end
                //add by lvmu 20150610 start
                //if( handConnect1 == true ) {
                //    handler.postDelayed(dismissDialogRunnable, 15000);
                //    pd.show();
                //}
                //add by lvmu 20150610 end

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (100 == requestCode) {
            switch (resultCode) {
                case Activity.RESULT_OK: //蓝牙开启了，则直接扫描呢
                    //delete by lvmu 20150618 start
                    //manager.enableBluetooth();
                    //delete by lvmu 20150618 end
                    //add by lvmu 20150618 start
                    tv_name22.setText("连接中...");
                    //add by lvmu 20150618 end
                    break;
                case Activity.RESULT_CANCELED:
                    CommonUtils.showShortToast(this, "蓝牙不开启，无法连接到HUD");
                    //add by lvmu 20150618 start
                    disconbyhand = true;
                    //add by lvmu 20150618 end

                    break;
            }
        }
    }

    private IBluetoothListener listeners = new IBluetoothListener() {

        @Override
        public void onStateChange(int state, int what) {
            switch (state) {
                //add by lvmu 20150623 start
                case BluetoothManager.STATE_FIRST_KIDOU_BT_OPEN:
                    handler.sendEmptyMessage(BluetoothManager.STATE_FIRST_KIDOU_BT_OPEN);
                    break;
                //add by lvmu 20150623 end
                case BluetoothManager.STATE_DEVICE_DISCOVERING: //正在扫描中...，请稍候...
                    handler.sendEmptyMessage(BluetoothManager.STATE_DEVICE_DISCOVERING);
                    break;
                case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_SUCCESS:
                    handler.sendEmptyMessage(BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_SUCCESS);
                    break;
                case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_FAILURE:
                    handler.sendEmptyMessage(BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_FAILURE);
                    break;
                case BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_OBDCONING:
                    handler.sendEmptyMessage(BluetoothManager.STATE_BLUETOOTH_SETMESSAGE_OBDCONING);
                    break;
                //add by lvmu 20150620 start
                case BluetoothManager.STATE_BLUETOOTH_SHUT_DOWN_HAND:
                    handler.sendEmptyMessage(BluetoothManager.STATE_BLUETOOTH_SHUT_DOWN_HAND);
                    break;
                //add by lvmu 20150620 end
                case BluetoothManager.STATE_BLUETOOTH_OFF:
                case BluetoothManager.STATE_BLUETOOTH_DISCONNECT:
                    handler.sendEmptyMessage(BluetoothManager.STATE_BLUETOOTH_DISCONNECT);
                    isDiscovingFinish = true;
                    isFoundDevice = false;
                    L.e( "XXXXX_FOUNDDEVICE_FALSE_DISCON" );
                    break;
                case BluetoothManager.STATE_NOTFOUND:
                    isDiscovingFinish = true;
                    isFoundDevice = false;
                    L.e( "XXXXX_FOUNDDEVICE_FALSE" );
                    handler.sendEmptyMessage(BluetoothManager.STATE_NOTFOUND);
                    break;
                case BluetoothManager.STATE_DISCOVERED:
                    isDiscovingFinish = true;
                    handler.sendEmptyMessage(BluetoothManager.STATE_DISCOVERED);
                    break;
                //add by lvmu 20150619 start
                case BluetoothManager.STATE_BLUETOOTH_PAIRE_FAILURE:
                    handler.sendEmptyMessage(BluetoothManager.STATE_BLUETOOTH_PAIRE_FAILURE);
                //add by lvmu 20150619 end
                case BluetoothManager.STATE_CONNECTED:
                    isDiscovingFinish = true;
                    handler.sendEmptyMessage(BluetoothManager.STATE_CONNECTED);
                    break;
                case BluetoothManager.STATE_FEQURENCYSENDMSG_FAILURE:
                case BluetoothManager.STATE_FEQURENCYSENDMSG_SUCCESS:
                    app.getFrequenceMsg().setExit(0xFF);
                    if (isFinishing() && isPageFinish) {//若页面关了，
                        manager.releaseResource();
                    }
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onScanningDevice(String name, String address,
                                     int bondState) {
            L.e("----onScanningDevice----啊啊...--->name:" + name);
            //modify by lvmu 20150611 start
           // if (BluetoothManager.BLUETOOTH_NAME.equals(name) && !isDiscovingFinish) {
            if (BluetoothManager.BLUETOOTH_NAME.equals(name) ) {
                //if( !(manager.pairBluetoothDevice(address))){
                //    if( atconnecting == false ) {
                //        handler.postDelayed(autoConnectRunnable, 10000);
                //        L.e("AUTOCONN_CREATEBOND_FAULT");
                //        atconnecting = true;
                //    }
                //}
                manager.pairBluetoothDevice(address);
                isFoundDevice = true;
            }
            //modify by lvmu 20150611 end

        }
    };

    @Override
    protected void doLogic() {
        naviDataCollectorEnginer = NaviDataCollectorEnginer.getInstance(this);
        ; //有百度导航时，才开始确认数据
        hubApp = HubApp.getInstance();
        manager = BluetoothManager.getInstance();
        count = 0;
        isFoundDevice = false;

        manager.addListener(listeners);

        registerObserver();
        //add by lvmu 20150611 start

         //   try {
         //       manager.remove_glrhud();
         //   } catch (Exception e) {
         //       e.printStackTrace();
         //   }
        //add by lvmu 20150611 end
            prepareConn();
            atconnecting = true;

        //add by lvmu 20150611 start
        //获取电话通讯服务
        TelephonyManager tpm = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        //创建一个监听对象，监听电话状态改变事件
        tpm.listen(new MyPhoneStateListener(),
                PhoneStateListener.LISTEN_CALL_STATE);
        //add by lvmu 20150611 end

        //ncl.
    }

    private BroadcastReceiver callReceiver;

    private ContentObserver newMmsContentObserver = new ContentObserver(new Handler()) {
        public void onChange(boolean selfChange) {
            app.setUnReadSmsNum(MmsTelTool.getUnreadSmsCount(IndexActivity.this) + MmsTelTool.getUnreadMmsCount(IndexActivity.this));
            prepareMsgCallChangMsg();
        }
    };


    private ContentObserver missedCallObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            //L.e("---> selfChange::"+selfChange +"  未接电话  ");
            int missedCalls = MmsTelTool.getMissedCalls(IndexActivity.this);
            //L.e("---> selfChange::"+selfChange +"  未接电话  个数是："+missedCalls);
            app.setUnCallNum(missedCalls);
            prepareMsgCallChangMsg();
        }
    };

    private void registerObserver() {
        unregisterObserver();
        getContentResolver().registerContentObserver(Uri.parse("content://sms"), true, newMmsContentObserver);
        getContentResolver().registerContentObserver(Telephony.MmsSms.CONTENT_URI, true, newMmsContentObserver);
        getContentResolver().registerContentObserver(CallLog.Calls.CONTENT_URI, true, missedCallObserver);

        final IntentFilter filter = new IntentFilter();
        filter.addAction("com.android.phone.NotificationMgr.MissedCall_intent");
        registerReceiver(callReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action != null && "com.android.phone.NotificationMgr.MissedCall_intent".equals(action)) {
                    app.setUnCallNum(intent.getExtras().getInt("MissedCallNumber"));
                    prepareMsgCallChangMsg();
                }
            }
        }, filter);
    }

    //立马获取到的相关的 未读短信彩信及未接电话的个数...，处理相关的未读短信及未接电话的
    private void prepareMsgCallChangMsg() {
        FrequenceMsg frequenceMsg = app.getFrequenceMsg();
        int unReadSmsNum = app.getUnReadSmsNum();
        int unCallNum = app.getUnCallNum();
        if (unReadSmsNum == 0 && unCallNum == 0) { //无未读短信及未接电话
            frequenceMsg.setUnReadMsgOrTel(0x00);
        } else if (unReadSmsNum == 0 && unCallNum > 0) { //有未接电话
            frequenceMsg.setUnReadMsgOrTel(0x01);
        } else if (unReadSmsNum > 0 && unCallNum == 0) { //有未读短信
            frequenceMsg.setUnReadMsgOrTel(0x02);
        } else if (unReadSmsNum > 0 && unCallNum > 0) { //有未读短信及有未接电话
            frequenceMsg.setUnReadMsgOrTel(0x03);
        }
        //L.e("---> UnReadMsgOrTel::" + frequenceMsg.getUnReadMsgOrTel() + " unReadSmsNum:" + unReadSmsNum + " unCallNum:" + unCallNum);
        //若没有在导航中，，有数据变化了，应该是立即发过去
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //if (!app.isNaving() && manager.isBluetoothConn()) { //若不在导航中，有未接短信或未接电话来了，蓝牙连接中是，应该可以发命令
                if (manager.isBluetoothConn()) {
                    BluetoothInstruct.setFrequenceSendMsg(0X14, 0);
                }
            }
        }, 500);
    }

    private void unregisterObserver() {
        try {
            if (newMmsContentObserver != null) {
                getContentResolver().unregisterContentObserver(newMmsContentObserver);
            }
            if (newMmsContentObserver != null) {
                getContentResolver().unregisterContentObserver(newMmsContentObserver);
            }
        } catch (Exception e) {
        }
        if (null != callReceiver) {
            unregisterReceiver(callReceiver);
        }
    }


    private void showBrightDialog() {
        BrightDialog dialog = new BrightDialog(this);

        dialog.setOnBrightChangeListener(new OnBrightChangeListener() {

            @Override
            public void onBrightChange(int bright) {
                //L.e("-------->>bright:" + bright);
                setScreenBrightness(bright);
            }
        });

        int lastBright = Preference.getInt(Constant.K_BRIGHT, 0);
        if (0 != lastBright) {
            dialog.setBright(lastBright);
        }
        dialog.show();
    }


    /**
     * 保存当前的屏幕亮度值，并使之生效
     */
    private void setScreenBrightness(int curentBright) {
        // 准备发送蓝牙命令
        String bluetoothPwd = hubApp.getBluetoothPwd();
        if (!manager.isBluetoothConn()) {
            CommonUtils.showShortToast(this, getString(R.string.tip_bluetooth_disconnect));
        } else {
            Preference.putInt(Constant.K_BRIGHT, curentBright);
            BluetoothInstruct.sendSetMessage(null, curentBright); //设置亮度，并不改蓝牙密码
            isObdsetting = false;  //进行的是亮度设置，而不是obd设置
            //L.e("----->> BluetoothInstruct.sendSetMessage --- 屏幕亮度值:" + curentBright);
        }

    }

    /**
     * 功能描述: <br>
     * 〈功能详细描述〉
     * 密码修改，并没有马上发指令到hub上，先是记录下来，下次更新亮度时使用
     *
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    private void showObdSetDialog() {
        ObdSetDialog dialog = new ObdSetDialog(this, R.style.DialogNoTitle);
        ObdSetDialog.BluetoothPwdSureListener listener = new ObdSetDialog.BluetoothPwdSureListener() {

            @Override
            public void onBluetoothPwdSure(String pwd) {
                if (!TextUtils.isEmpty(pwd)) {
                    if (4 == pwd.length()) {
                        //CommonUtils.showLongToast(IndexActivity.this, "pwd:"+ pwd);
                        hubApp.setBluetoothPwd(pwd);
                        int bright = Preference.getInt(Constant.K_BRIGHT, 0);
                       /* if(bright==0){
                            bright = 80;
                        }*/
                        bright = 0XFFFF;
                        BluetoothInstruct.sendSetMessage(pwd, bright);
                        Preference.putInt(Constant.K_BRIGHT, bright);
                        isObdsetting = true; //obd进行了设置
                    } else {
                        CommonUtils.showLongToast(IndexActivity.this, getString(R.string.tip_bluetooth_pwd_length_error));
                    }
                }
            }
        };
        dialog.setOnBluetoothPwdSureListener(listener);
        dialog.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        //add by lvmu 20150716 start
        mWakeLock.release();
        //add by lvmu 20150716 end
        if (null != baiduPackageAddedReceiver) {
            unregisterReceiver(baiduPackageAddedReceiver);
        }
        app.getFrequenceMsg().setExit(0x02); //app退出时发0x02
        isPageFinish = true;
        //todo 应该立即发送数据 ...
        if (manager.isBluetoothConn()) {
            BluetoothInstruct.setFrequenceSendMsg(0x04, 0);  //退出应用了，个消息过去
        }
        L.e("----->  退出onDestory 执行了........");
        if (null != naviDataCollectorEnginer) {
            naviDataCollectorEnginer.destroy();
            naviDataCollectorEnginer = null;
        }
        unregisterObserver();
        manager.releaseResource();
        //new Thread(new Runnable() {
        //    @Override
        //    public void run() {
        //        try {
        //            Thread.sleep(1000);
        //        } catch (InterruptedException e) {
        //            e.printStackTrace();
        //        }
        //        L.e("----->  退出onDestory 执行了.  Thread manager.releaseResource();.......");
        //        manager.releaseResource();
        //    }
        //}).start();
        //add by lvmu 20150611 start
        //Intent intent = new Intent(Intent.ACTION_MAIN);
        //intent.addCategory(Intent.CATEGORY_HOME);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //this.startActivity(intent);
        System.exit(0);
        //add by lvmu 20150611 end
    }


    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("您确认需要退出应用吗");
        builder.setPositiveButton("退出", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callSuperBackPressed();
                //add by lvmu 20150611 start
                disconbyhand = true;
               // manager.releaseResource();
                //add by lvmu 20150611 end
                finish();
            }
        });
        builder.setNegativeButton("后台", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
            }
        });
        builder.show();
    }

    private void callSuperBackPressed() {
        super.onBackPressed();
    }

    //add by lvmu 20150617 start
    class MyPhoneStateListener extends PhoneStateListener {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            switch(state) {
                case TelephonyManager.CALL_STATE_IDLE: //空闲
                    break;
                case TelephonyManager.CALL_STATE_RINGING: //来电
                    app.setUnCallNum( 1 );
                    prepareMsgCallChangMsg();
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK: //摘机（正在通话中）
                    break;
            }
        }
        //add by lvmu 20150617 end
    }



}

