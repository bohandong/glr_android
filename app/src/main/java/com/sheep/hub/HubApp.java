package com.sheep.hub;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import com.amap.api.location.AMapLocation;
import com.amap.api.navi.AMapNavi;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.sheep.framework.db.AfeiDb;
import com.sheep.framework.exception.CrashHandler;
import com.sheep.framework.log.L;
import com.sheep.hub.bean.FrequenceMsg;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.Preference;
import com.sheep.hub.util.TTSController;

import java.util.LinkedList;
import java.util.List;

public class HubApp extends Application {

    private static HubApp instance;

    private AfeiDb afeiDb;

    private List<Activity> activitys = new LinkedList<Activity>();

    private List<Activity> activityList = new LinkedList<Activity>();

    private boolean openCrashHandler = true;

    private AMapLocation myLocation;

    private String pairBluetoothAddress; //蓝牙连接成功后，直接记住蓝牙地址
    private String bluetoothPwd;  //蓝牙密码

    private int unReadSmsNum;
    private int unCallNum;
    private boolean isNaving;

    private FrequenceMsg frequenceMsg;

    public void onCreate() {
        super.onCreate();
        instance = this;
        afeiDb = AfeiDb.create(this, Constant.DB_NAME, true);

        // 全局捕获异常错误信息存至SD卡
        if (openCrashHandler) {
            CrashHandler crashHandler = CrashHandler.getInstance();
            crashHandler.init(getApplicationContext());
        }

        //L.e("------->> application.....onCreate ...");

        // TODO 处理 加密key
        /*if (!Preference.getBoolean("isSaveKey", false)) {
            String secretKey2 = MD5Util.md5(Constant.SECRET_KEY1);
			Preference.putString(Constant.SECRET_KEY2, secretKey2);
			Preference.putBoolean("isSaveKey", true);
		}*/

        // 初始化 异步加载图片
        //initImageLoader(getApplicationContext());
        //TTSController ttsManager = TTSController.getInstance();// 初始化语音模块
        //ttsManager.init();
        //AMapNavi.getInstance(this).setAMapNaviListener(ttsManager);// 设置语音模块播报
    }

    private void initImageLoader(Context context) {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.RGB_565)// 防止内存溢出的，图片太多就这这个。还有其他设置
                .cacheOnDisc(true).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context)
                .threadPoolSize(3)
                        // default
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .denyCacheImageMultipleSizesInMemory()
                        // .memoryCache(new LruMemoryCache((int) (6 * 1024 * 1024)))
                .memoryCache(new WeakMemoryCache())
                .memoryCacheSize((int) (2 * 1024 * 1024))
                .memoryCacheSizePercentage(13)
                        // default
                        // .discCache(new UnlimitedDiscCache(cacheDir))
                        // default
                .discCacheSize(50 * 1024 * 1024).discCacheFileCount(100)
                .discCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .defaultDisplayImageOptions(defaultOptions).writeDebugLogs() // Remove
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    public static HubApp getInstance() {
        return instance;
    }

    // 添加Activity到容器中
    public void addActivity(Activity activity) {
        if (activitys != null && activitys.size() > 0) {
            if (!activitys.contains(activity)) {
                activitys.add(activity);
            }
        } else {
            activitys.add(activity);
        }
    }

    public List<Activity> getActivitys() {
        return activitys;
    }

    public void exit() {
        if (activitys != null && activitys.size() > 0) {
            for (Activity activity : activitys) {
                activity.finish();
            }
        }
        System.exit(0);
    }

    public void finishAll() {
        if (activitys != null && activitys.size() > 0) {
            for (Activity activity : activitys) {
                activity.finish();
            }
        }
    }

    public AfeiDb getAfeiDb() {
        if (null == afeiDb) {
            afeiDb = AfeiDb.create(this, Constant.DB_NAME, true);
        }
        return afeiDb;
    }

    public String getPairBluetoothAddress() {
        if (null != pairBluetoothAddress) {
            return pairBluetoothAddress;
        } else {
            return Preference.getString(Constant.K_BLUETOOTH_ADRESS);
        }
    }

    public void setPairBluetoothAddress(String pairBluetoothAddress) {
        this.pairBluetoothAddress = pairBluetoothAddress;
        Preference.putString(Constant.K_BLUETOOTH_ADRESS, pairBluetoothAddress);
    }


    public String getBluetoothPwd() {
        if (null != bluetoothPwd) {
            return bluetoothPwd;
        } else {
            return Preference.getString(Constant.K_BLUETOOTH_PWD);
        }
    }

    public void setBluetoothPwd(String bluetoothPwd) {
        this.bluetoothPwd = bluetoothPwd;
        Preference.putString(Constant.K_BLUETOOTH_PWD, bluetoothPwd);
    }

    public int getSequence() {
        return Preference.getInt(Constant.K_SEQUENCE, 1);
    }

    public void generateSequence() {
        int sequence = Preference.getInt(Constant.K_SEQUENCE, 1);
        if (sequence < 65535) { //65535  对应 0xffff，则自增
            sequence++;
        } else {
            sequence = 1;
        }
        Preference.putInt(Constant.K_SEQUENCE, sequence);
    }


    // 添加Activity到容器中
    public void addActivityToList(Activity activity) {
        if (activityList != null && activityList.size() > 0) {
            if (!activityList.contains(activity)) {
                activityList.add(activity);
            }
        } else {
            activityList.add(activity);
        }
    }

    public List<Activity> getActivityLists() {
        return activityList;
    }

    public void finishAllActivityLists() {
        if (activityList != null && activityList.size() > 0) {
            for (Activity activity : activityList) {
                activity.finish();
            }
        }
    }

    public FrequenceMsg getFrequenceMsg(){
        if(null == frequenceMsg){
            frequenceMsg = new FrequenceMsg();
            frequenceMsg.setCommand(0x02); //命令字
        }
        return frequenceMsg;
    }

    public void releaseFrequenceMsg(){
        frequenceMsg = null;
    }



    public AMapLocation getMyLocation() {
        return myLocation;
    }

    public void setMyLocation(AMapLocation myLocation) {
        this.myLocation = myLocation;
    }

    public int getUnReadSmsNum() {
        return unReadSmsNum;
    }

    public void setUnReadSmsNum(int unReadSmsNum) {
        this.unReadSmsNum = unReadSmsNum;
    }

    public int getUnCallNum() {
        return unCallNum;
    }

    public void setUnCallNum(int unCallNum) {
        this.unCallNum = unCallNum;
    }

    public boolean isNaving() {
        return isNaving;
    }

    public void setIsNaving(boolean isNaving) {
        this.isNaving = isNaving;
    }



}
