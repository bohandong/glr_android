package com.sheep.hub.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.sheep.framework.log.L;
import com.sheep.framework.util.StringUtils;
import com.sheep.hub.HubApp;

/****
 */
public class Preference {

	private static SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(HubApp.getInstance());

	public static void putString(String key, String value) {
		Editor editor = pref.edit();
		editor.putString(key, value);
		//editor.commit();
		apply(editor);
	}
	
	public static void putDouble(String key, double value) {
		Editor editor = pref.edit();
		editor.putString(key, String.valueOf(value));
		//editor.commit();
		apply(editor);
	}

	public static void putInt(String key, int value) {
		Editor editor = pref.edit();
		editor.putInt(key, value);
		//editor.commit();
		apply(editor);
	}

	public static void putBoolean(String key, boolean value) {
		Editor editor = pref.edit();
		editor.putBoolean(key, value);
		//editor.commit();
		apply(editor);
	}

	public static int getInt(String key) {
		return pref.getInt(key, 0);
	}

	public static int getInt(String key, int default_value) {
		return pref.getInt(key, default_value);
	}

	public static String getString(String key) {
		return pref.getString(key, "");
	}

	public static String getString(String key, String default_value) {
		return pref.getString(key, default_value);
	}
	
	public static double getDouble(String key) {
		if(StringUtils.isBlank(pref.getString(key, ""))){
			return 0;
		}else{
			return Double.parseDouble(pref.getString(key, ""));
		}
	}

	public static boolean getBoolean(String key, boolean defValue) {
		return pref.getBoolean(key, defValue);
	}

	public static void apply(Editor editor){
		try {
			Method method = editor.getClass().getDeclaredMethod("apply", null);
			method.invoke(editor, null);
			//L.e("---->采用了apply方法");
		} catch (NoSuchMethodException e) {
			editor.commit(); //低版本的，直接用commit进行提交 
			//L.e("---->采用了commit方法");
		} catch (IllegalAccessException e) {
			editor.commit(); //低版本的，直接用commit进行提交 
		} catch (IllegalArgumentException e) {
			editor.commit(); //低版本的，直接用commit进行提交 
		} catch (InvocationTargetException e) {
			editor.commit(); //低版本的，直接用commit进行提交 
		}
	}
}
