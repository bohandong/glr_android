package com.sheep.hub.util;

import com.sheep.hub.bean.Interest;

import java.util.HashMap;
import java.util.Map;

/**
 * 机动点-值对应表
 * Created by chensf on 2015/4/14.
 */
public class ManeuverValueTool {

    public static final Map<String,Integer> maneuverMap = new HashMap<String,Integer>();

    static{
        maneuverMap.put("turn_left_back",0X01);  //左后方转弯
        maneuverMap.put("turn_left",0X02); //左转
        maneuverMap.put("turn_left_front",0X03); //左前方转弯
        maneuverMap.put("turn_front",0X04); //直行
        maneuverMap.put("turn_right_front",0X05); //右前方转弯
        maneuverMap.put("turn_right",0X06); //右转
        maneuverMap.put("turn_right_back",0X07); //右后方转弯
        maneuverMap.put("turn_back",0X08); //掉头
        maneuverMap.put("turn_left_side",0X09); //普通/JCT/SAPA二分歧 靠左
        maneuverMap.put("turn_front_2branch_left",0X09); //普通/JCT/SAPA二分歧 靠左
        maneuverMap.put("turn_right_side",0X0A); //普通/JCT/SAPA二分歧 靠右
        maneuverMap.put("turn_front_2branch_right",0X0A); //普通/JCT/SAPA二分歧 靠右
        maneuverMap.put("turn_left_side_main",0X0B); //左侧走本线,IC二分歧左侧直行走IC,普通/JCT/SAPA二分歧左侧 直行,八方向靠左直行
        maneuverMap.put("turn_branch_left_straight",0X0C);  //靠最左走本线 ,普通/JCT/SAPA三分歧左侧 直行 ,IC三分歧左侧直行,八方向靠最左侧直行
        maneuverMap.put("turn_right_3branch_left",0X0C); //八方向右转+随后靠最左
        maneuverMap.put("turn_right_side_main",0X0D); //右侧走本线,IC二分歧右侧直行走IC,普通/JCT/SAPA二分歧右侧 直行,八方向靠右直行
        maneuverMap.put("turn_branch_right_straight",0X0E); //靠最右走本线,普通/JCT/SAPA三分歧右侧 直行,IC三分歧右侧直行,八方向靠最右侧直行
        maneuverMap.put("turn_left_3branch_right",0X0E); //八方向左转+随后靠最右
        maneuverMap.put("turn_branch_center",0X0F); //中间走本线,普通三分歧/JCT/SAPA 靠中间,普通/JCT/SAPA三分歧中央 直行,IC三分歧中央走IC,IC三分歧中间直行,八方向沿中间直行
        maneuverMap.put("turn_front_3branch_middle",0X0F);
        maneuverMap.put("turn_left_side_ic",0X10);  //IC二分歧左侧走IC
        maneuverMap.put("turn_right_side_ic",0X11); //IC二分歧右侧走IC
        maneuverMap.put("turn_branch_left",0X12); //普通三分歧/JCT/SAPA 靠最左,IC三分歧左侧走IC
        maneuverMap.put("turn_front_3branch_left",0X12); //
        maneuverMap.put("turn_branch_right",0X13); //普通三分歧/JCT/SAPA 靠最右,IC三分歧右侧走IC
        maneuverMap.put("turn_front_3branch_right",0X13);
        maneuverMap.put("turn_dest",0X14); //目的地
        maneuverMap.put("turn_left_2branch_left",0X15); //八方向左转+随后靠左
        maneuverMap.put("turn_lf_2branch_left",0X15); //八方向左前方靠左侧
        maneuverMap.put("turn_left_2branch_right",0X16); //八方向左转+随后靠右
        maneuverMap.put("turn_lf_2branch_right",0X16); //八方向左前方靠右侧
        maneuverMap.put("turn_left_3branch_left",0X17); //八方向左转+随后靠最左
        maneuverMap.put("turn_left_3branch_middle",0X18); //八方向左转+随后沿中间
        maneuverMap.put("turn_right_2branch_left",0X19); //八方向右转+随后靠左
        maneuverMap.put("turn_rf_2branch_left",0X19); //八方向右前方靠左侧
        maneuverMap.put("turn_right_2branch_right",0X1A); //八方向右转+随后靠右
        maneuverMap.put("turn_rf_2branch_right",0X1A); //八方向右前方靠右侧
        maneuverMap.put("turn_right_3branch_middle",0X1B); //八方向右转+随后沿中间
        maneuverMap.put("turn_right_3branch_right",0X1C); //八方向右转+随后靠最右
        maneuverMap.put("turn_ring",0X1D); //环岛
        maneuverMap.put("turn_ring_out",0X1E); //环岛出口
        maneuverMap.put("turn_tollgate",0X1F); //收费站
        maneuverMap.put("turn_tollgate_s",0X1F); //收费站
    }

}
