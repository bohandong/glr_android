package com.sheep.hub.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;

import com.sheep.framework.log.L;

/**
 * Created by chensf on 2015/4/12.
 */
public class MmsTelTool {

    /**
     * 获取普通  未读短信的个数
     * @param context
     * @return
     */
    public static final int getUnreadSmsCount(Context context){
        int result = 0;
        Cursor csr = context.getContentResolver().query(Uri.parse("content://sms"), null,
                "type = 1 and read = 0", null, null);
        if (csr != null) {
            result = csr.getCount();
            csr.close();
        }
        return result;
    }


    /**
     * 获取彩信  未读彩信的个数
     * @param context
     * @return
     */
    public static final int getUnreadMmsCount(Context context){
        int result = 0;
        Cursor csr = context.getContentResolver().query(Uri.parse("content://mms/inbox"),
                null, "read = 0", null, null);
        if (csr != null) {
            result = csr.getCount();
            csr.close();
        }
        return result;
    }

    /**
     * 获取 未接电话的个数
     * @param context
     * @return
     */
    public static final int getMissedCalls(Context context) {
        int result = 0;
        Cursor csr = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, new String[]{CallLog.Calls.NUMBER,
                CallLog.Calls.TYPE, CallLog.Calls.NEW}, "type=? and new=?", new String[]{CallLog.Calls.MISSED_TYPE+"","1"}, CallLog.Calls.DEFAULT_SORT_ORDER);
        //L.e("-->>getMissedCalls  crs 执行完");
        if (csr != null) {
            result = csr.getCount();
            //L.e("-->>getMissedCalls  crs 执行完，不为空:"+result);
        }
       /* while (csr.moveToNext()){
            String aNew = csr.getString(csr.getColumnIndex("new"));
            String aType = csr.getString(csr.getColumnIndex("type"));
            String aNumber = csr.getString(csr.getColumnIndex("number"));
            L.e("-->>aNumber  "+aNumber+" aType:"+aType+" aNew:"+aNew);
        }*/

        csr.close();
        return result;
    }

}
