package com.sheep.hub.util;/*
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChineseNumber2DigitUtil {

    public static double distance2Digit(String msg) {
        double number = 0;
        Pattern pattern = Pattern.compile("[\\d零一两二三四五六七八九十个百千万亿]+[点]?[\\d零一两二三四五六七八九十]*(千米|米|公里)");
        Matcher matcher = pattern.matcher(msg);
        if (matcher.find()) {
            String result = matcher.group();
            boolean isKm = -1 != result.indexOf("公里") || -1 != result.indexOf("千米");
            String textString = result.replace("米", "").replace("公里", "").replace("公里","");
            int index = -1;
            if (-1 != (index = textString.indexOf("点"))) {
                String startString = textString.substring(0, index);
                String endString = textString.substring(index + 1, textString.length());
                long otherNumber = chnNum2Decimal(endString);
                number = chnNum2Digit(startString);
                number = number + otherNumber * Math.pow(0.1, endString.length());
            } else {
                number = chnNum2Digit(textString);
            }
            number = number * (isKm ? 1000 : 1);
        }
        return number;
    }


    private static Long chnNum2Decimal(String chnNum) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < chnNum.length(); i++) {
            char bit = chnNum.charAt(i);
            switch (bit) {
                case '零':
                    buffer.append(0);
                    break;
                case '一':
                    buffer.append(1);
                    break;
                case '二':
                    buffer.append(2);
                    break;
                case '三':
                    buffer.append(3);
                    break;
                case '四':
                    buffer.append(4);
                    break;
                case '五':
                    buffer.append(5);
                    break;
                case '六':
                    buffer.append(6);
                    break;
                case '七':
                    buffer.append(7);
                    break;
                case '八':
                    buffer.append(8);
                    break;
                case '九':
                    buffer.append(9);
                    break;
            }
        }
        return Long.parseLong(buffer.toString());
    }

    private static long chnNum2Digit(String chnNum) {
        java.util.Map<String, Integer> unitMap = new java.util.HashMap<String, Integer>();
        unitMap.put("个", 1);// 仅在数据存储时使用
        unitMap.put("十", 10);
        unitMap.put("百", 100);
        unitMap.put("千", 1000);
        unitMap.put("万", 10000);
        unitMap.put("亿", 100000000);

        java.util.Map<String, Integer> numMap = new java.util.HashMap<String, Integer>();
        numMap.put("零", 0);
        numMap.put("一", 1);
        numMap.put("两",2);
        numMap.put("二", 2);
        numMap.put("三", 3);
        numMap.put("四", 4);
        numMap.put("五", 5);
        numMap.put("六", 6);
        numMap.put("七", 7);
        numMap.put("八", 8);
        numMap.put("九", 9);

        // 数据存储结构：
        // 单位 数量
        // "个" num
        // "十" num
        // "百" num
        // "千" num
        // "万" num
        // "亿" num
        java.util.Map<String, Long> dataMap = new java.util.LinkedHashMap<String, Long>();

        // 保存"亿"或"万"之前存在的多位数
        java.util.List<Long> multiNumList = new java.util.ArrayList<Long>();

        long tempNum = 0;
        for (int i = 0; i < chnNum.length(); i++) {
            char bit = chnNum.charAt(i);
            // 因为"亿"或"万"存在多位情况，所以需要进行判断
            boolean isExist = false;
            // 存在"亿"或"万"情况(不计算当前索引位)
            if ((chnNum.indexOf('亿', i) != -1 || chnNum.indexOf('万', i) != -1) && chnNum.charAt(i) != '亿'
                    && chnNum.charAt(i) != '万') {
                isExist = true;
            }

            // 数字
            if (numMap.containsKey(bit + "")) {
                if (i != chnNum.length() - 1) {
                    tempNum = tempNum + numMap.get(bit + "");
                }
                // 最末位数字情况
                else {
                    dataMap.put("个", Long.valueOf(numMap.get(bit + "") + ""));
                    tempNum = 0;
                }
            } else if (bit == '亿') {
                // 存在"万亿"情况，取出"万"位值*10000,0000后重新put到map
                if (i - 1 >= 0 && chnNum.charAt(i - 1) == '万') {
                    Long dataValue = dataMap.get("万");
                    if (dataValue != null && dataValue > 0) {
                        dataMap.put("万", dataValue * unitMap.get(bit + ""));
                    }
                    continue;
                }

                // 最后一位数进list等待处理
                if (tempNum != 0) {
                    multiNumList.add(tempNum);
                }
                // 处理"亿"之前的多位数
                long sum = 0;
                for (Long num : multiNumList) {
                    sum += num;
                }
                multiNumList.clear();
                dataMap.put("亿", sum);
                tempNum = 0;
            } else if (bit == '万') {
                // 最后一位数进list等待处理
                if (tempNum != 0) {
                    multiNumList.add(tempNum);
                }
                // 处理"万"之前的多位数
                long sum = 0;
                for (Long num : multiNumList) {
                    sum += num;
                }
                multiNumList.clear();
                dataMap.put("万", sum);
                tempNum = 0;
            } else if (bit == '千' && tempNum > 0) {
                // 存在"亿"或"万"情况，临时变量值*"千"单位值进list等待处理
                if (isExist) {
                    multiNumList.add(tempNum * unitMap.get(bit + ""));
                    tempNum = 0;
                }
                // 不存在"亿"或"万"情况，临时变量值put到map
                else {
                    dataMap.put("千", tempNum);
                    tempNum = 0;
                }
            } else if (bit == '百' && tempNum > 0) {
                // 存在"亿"或"万"情况，临时变量值*"百"单位值进list等待处理
                if (isExist) {
                    multiNumList.add(tempNum * unitMap.get(bit + ""));
                    tempNum = 0;
                }
                // 不存在"亿"或"万"情况，临时变量值put到map
                else {
                    dataMap.put("百", tempNum);
                    tempNum = 0;
                }
            } else if (bit == '十') {
                // 存在"亿"或"万"情况，临时变量值*"十"单位值进list等待处理
                if (isExist) {
                    if (tempNum != 0) {
                        multiNumList.add(tempNum * unitMap.get(bit + ""));
                        tempNum = 0;
                    }
                    // 将"十"转换成"一十"
                    else {
                        tempNum = 1 * unitMap.get(bit + "");
                    }
                }
                // 不存在"亿"或"万"情况，临时变量值put到map
                else {
                    if (tempNum != 0) {
                        dataMap.put("十", tempNum);
                    }
                    // 将"十"转换成"一十"
                    else {
                        dataMap.put("十", 1l);
                    }
                    tempNum = 0;
                }
            }
        }

        long sum = 0;
        java.util.Set<String> keys = dataMap.keySet();
        for (String key : keys) {
            Integer unitValue = unitMap.get(key);
            Long dataValue = dataMap.get(key);
            sum += unitValue * dataValue;
        }
        return sum;
    }
}
