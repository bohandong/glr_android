package com.sheep.hub.util;/*
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeAddress;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;

public class LocationUtil {
    public static final void requestMyLocation(Context context) {
        requestMyLocation(context, null);
    }

    public static final void requestMyLocation(Context context, final GetLocationSuccessListener listener) {
        LocationManagerProxy.getInstance(context).requestLocationData(LocationProviderProxy.AMapNetwork, -1, 15, new AMapLocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onLocationChanged(Location location) {
            }

            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                HubApp.getInstance().setMyLocation(aMapLocation);
                if (null != listener) {
                    listener.onSuccess(aMapLocation);
                }
            }
        });
    }

    public interface GetLocationSuccessListener {
        void onSuccess(AMapLocation aMapLocation);
    }

    public interface GetAddressSuccessListener {
        void onSuccess(String address);

        void onFail();
    }

    public static void getAddressByLatLonPoint(final Context context, LatLonPoint latLonPoint, final GetAddressSuccessListener listener) {
        final ProgressDialog dialog = CommonUtils.showProgressDialog(context, R.string.search_ing);
        RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 200,
                GeocodeSearch.AMAP);// 第一个参数表示一个Latlng，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
        GeocodeSearch geocodeSearch = new GeocodeSearch(context);
        geocodeSearch.setOnGeocodeSearchListener(new GeocodeSearch.OnGeocodeSearchListener() {
            @Override
            public void onRegeocodeSearched(RegeocodeResult result, int rCode) {
                dialog.dismiss();
                if (rCode == 0) {
                    if (result != null && result.getRegeocodeAddress() != null
                            && result.getRegeocodeAddress().getFormatAddress() != null) {
                        if (null != listener) {
                            RegeocodeAddress address = result.getRegeocodeAddress();
                            if (TextUtils.isEmpty(result.getRegeocodeAddress().getFormatAddress())) {
                                if (null != listener) {
                                    listener.onFail();
                                } else {
                                    Toast.makeText(context, R.string.no_result, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                listener.onSuccess(result.getRegeocodeAddress().getFormatAddress() + context.getString(R.string.nearby));
                            }
                        }
                    } else {
                        if (null != listener) {
                            listener.onFail();
                        } else {
                            Toast.makeText(context, R.string.no_result, Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    if (null != listener) {
                        listener.onFail();
                    } else {
                        Toast.makeText(context, R.string.error_network, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {
                dialog.dismiss();
            }
        });
        geocodeSearch.getFromLocationAsyn(query);// 设置同步逆地理编码请求
    }

    public static String getSimpleAddress(String detailAddress, String defaultAddress) {
        if (TextUtils.isEmpty(detailAddress)) {
            return defaultAddress;
        }
        String temp = detailAddress.trim().replaceAll(" ", "");
        int index = temp.lastIndexOf("市");
        if (-1 != index && index < (temp.length() - 1)) {
            temp = temp.substring(index + 1);
        }
        if (TextUtils.isEmpty(temp)) {
            temp = TextUtils.isEmpty(detailAddress) ? defaultAddress : detailAddress;
        }
        return temp;
    }

    public static String getSimpleAddress(String detailAddress) {
        return getSimpleAddress(detailAddress, HubApp.getInstance().getString(R.string.unknown_road));
    }
}
