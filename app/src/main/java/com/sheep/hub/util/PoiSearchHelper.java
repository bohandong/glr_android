package com.sheep.hub.util;/*
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.amap.api.maps.AMap;
import com.amap.api.maps.overlay.PoiOverlay;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.poisearch.PoiItemDetail;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;
import com.sheep.hub.constant.Constant;

import java.util.ArrayList;
import java.util.List;

public class PoiSearchHelper implements PoiSearch.OnPoiSearchListener {

    private Context mContext;
    private ProgressDialog progressDialog;
    private int currentPage;

    private PoiSearch.Query query;
    private PoiSearch poiSearch;// POI搜索
    private String poiKey;

    private boolean isNearBy;

    private PoiQuerySuccessListener poiQuerySuccessListener;

    public PoiSearchHelper(Context mContext, String poiKey) {
        this.mContext = mContext;
        this.poiKey = poiKey;
    }

    public PoiSearchHelper(Context mContext) {
        this.mContext = mContext;
    }

    public PoiSearchHelper(Context mContext, String poiKey, PoiQuerySuccessListener poiQuerySuccessListener) {
        this.mContext = mContext;
        this.poiKey = poiKey;
        this.poiQuerySuccessListener = poiQuerySuccessListener;
    }

    public PoiSearchHelper(Context mContext, PoiQuerySuccessListener poiQuerySuccessListener) {
        this.mContext = mContext;
        this.poiQuerySuccessListener = poiQuerySuccessListener;
    }

    public void setPoiQuerySuccessListener(PoiQuerySuccessListener poiQuerySuccessListener) {
        this.poiQuerySuccessListener = poiQuerySuccessListener;
    }

    public void setPoiKey(String poiKey) {
        if (!poiKey.equals(this.poiKey)) {
            this.poiKey = poiKey;
            currentPage = 0;
        }
    }

    public void query() {
        query(0, true);
    }

    public void query(boolean isShowDialog) {
        query(0, isShowDialog);
    }

    public void queryNext() {
        query(++currentPage, false);
    }

    private void query(int currentPage, boolean isShowDialog) {
        if (isShowDialog) {
            if(null==progressDialog){
                progressDialog= CommonUtils.showProgressDialog(mContext,R.string.search_ing);
            }else{
                progressDialog.show();
            }
        }
        this.currentPage = currentPage;
        query = new PoiSearch.Query(poiKey.trim(), "", null == HubApp.getInstance().getMyLocation() ? "南京" : HubApp.getInstance().getMyLocation().getCity());// 第一个参数表示搜索字符串，第二个参数表示poi搜索类型，第三个参数表示poi搜索区域（空字符串代表全国）
        query.setPageSize(Constant.PAGE_SIZE);
        query.setPageNum(currentPage);

        poiSearch = new PoiSearch(mContext, query);
        poiSearch.setOnPoiSearchListener(this);
        if (null != HubApp.getInstance().getMyLocation() && isNearBy) {
            poiSearch.setBound(new PoiSearch.SearchBound(new LatLonPoint(HubApp.getInstance().getMyLocation().getLatitude(), HubApp.getInstance().getMyLocation().getLongitude()), 200000, true));
        }
        poiSearch.searchPOIAsyn();
    }


    private void dismissProgressDialog() {
        if (null != progressDialog && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void setNearBy(boolean isNearBy) {
        this.isNearBy = isNearBy;
    }

    @Override
    public void onPoiSearched(PoiResult result, int rCode) {
        dismissProgressDialog();
        if (rCode == 0) {
            if (result != null && result.getQuery() != null) {
                if (result.getQuery().equals(query)) {
                    if (null != poiQuerySuccessListener) {
                        poiQuerySuccessListener.onSuccess(result.getPois());
                    } else if (currentPage == 0) {
                        Toast.makeText(mContext, R.string.no_result, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else {
            if (currentPage == 0) {
                if (rCode == 27) {
                    Toast.makeText(mContext, R.string.error_network, Toast.LENGTH_SHORT).show();
                } else if (rCode == 32) {
                    Toast.makeText(mContext, R.string.error_key, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.error_other)
                            + rCode, Toast.LENGTH_SHORT).show();
                }
            }
            currentPage--;
            if (null != poiQuerySuccessListener) {
                poiQuerySuccessListener.onFail();
            }
        }
    }

    @Override
    public void onPoiItemDetailSearched(PoiItemDetail poiItemDetail, int i) {

    }

    public void markToMap(AMap aMap, List<PoiItem> poiItemList) {
        PoiOverlay poiOverlay = new PoiOverlay(aMap, poiItemList);
        poiOverlay.removeFromMap();
        poiOverlay.addToMap();
        poiOverlay.zoomToSpan();
    }

    public interface PoiQuerySuccessListener {
        public void onSuccess(ArrayList<PoiItem> poiItemList);

        public void onFail();

    }
}
