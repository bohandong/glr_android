package com.sheep.hub;/*
 */

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps.model.LatLng;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshListView;
import com.sheep.hub.adapter.SimpleTextAdapter;
import com.sheep.hub.bean.LocalLocation;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.LocationUtil;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class CommonListActivity extends BaseCalculateRouteActivity implements AdapterView.OnItemClickListener {
    public static final int DEFAULT_TYPE = 0;
    public static final int STORE_ACTIVITY_TYPE = 1;
    public static final String TYPE = "type";
    public static final String TITLE = "title";
    public static final String LIST = "list";
    public static final String EXTRA_LIST = "extra_list";

    public ArrayList<String> list;
    private PullToRefreshListView plv_list;
    private SimpleTextAdapter<String> adapter;

    private int type = DEFAULT_TYPE;//type为1时表示收藏地址

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        String title = getIntent().getStringExtra(TITLE);
        type = getIntent().getIntExtra(TYPE, DEFAULT_TYPE);
        initTitle(TextUtils.isEmpty(title) ? getString(R.string.choose) : title);
        list = getIntent().getStringArrayListExtra(LIST);
        initView();
    }

    private void initView() {
        if (STORE_ACTIVITY_TYPE == type) {
            Button button = (Button) findViewById(R.id.btn_next);
            button.setVisibility(View.VISIBLE);
            button.setText(R.string.create);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    launch(MarkerMapActivity.class, STORE_ACTIVITY_TYPE);
                }
            });
        }


        plv_list = (PullToRefreshListView) findViewById(android.R.id.list);
        plv_list.setMode(PullToRefreshBase.Mode.DISABLED);
        plv_list.getRefreshableView().setFooterDividersEnabled(false);
        adapter = new SimpleTextAdapter<String>(this) {
            @Override
            public void setTextView(TextView textView, String s) {
                textView.setText(s);
            }
        };
        adapter.setList(list);
        plv_list.setAdapter(adapter);

        TextView emptyView = (TextView) LayoutInflater.from(this).inflate(R.layout.view_empy, plv_list.getRefreshableView(), false);
        switch (type) {
            case DEFAULT_TYPE:
                emptyView.setText(R.string.no_data);
                break;
            case STORE_ACTIVITY_TYPE:
                emptyView.setText(R.string.has_not_store_data);
                break;
        }
        plv_list.setEmptyView(emptyView);

        plv_list.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (type) {
            case DEFAULT_TYPE:
                Intent data = new Intent();
                data.putExtra("data", (String) parent.getAdapter().getItem(position));
                data.putExtra("position", position - 1);
                setResult(RESULT_OK, data);
                finish();
                break;
            case STORE_ACTIVITY_TYPE:
                LocalLocation localLocation = ((ArrayList<LocalLocation>) getIntent().getSerializableExtra(EXTRA_LIST)).get(position - 1);
                startRouteSearch(new PoiItem("" + System.currentTimeMillis(), new LatLonPoint(localLocation.getLatitude(), localLocation.getLongitude()), localLocation.getName(), localLocation.getName()));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case STORE_ACTIVITY_TYPE:
                    LatLng latLng = data.getParcelableExtra("data");
                    String address = data.getStringExtra("title");
                    LocalLocation localLocation = new LocalLocation();
                    localLocation.set_id("" + System.currentTimeMillis());
                    localLocation.setRowName(Constant.ROW_STORE_NAME);
                    localLocation.setLatitude(latLng.latitude);
                    localLocation.setLongitude(latLng.longitude);
                    localLocation.setName(LocationUtil.getSimpleAddress(address));
                    HubApp.getInstance().getAfeiDb().save(localLocation);
                    list.add(address);
                    ArrayList<LocalLocation> localLocationArrayList = (ArrayList<LocalLocation>) getIntent().getSerializableExtra(EXTRA_LIST);
                    localLocationArrayList.add(localLocation);
                    getIntent().putExtra(EXTRA_LIST, localLocationArrayList);
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    }

}
