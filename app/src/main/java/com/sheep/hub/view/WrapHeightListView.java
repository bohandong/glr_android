package com.sheep.hub.view;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 高度为wap_content的自适合高度的ListView
 * @author chensf
 *
 */
public class WrapHeightListView extends ListView {

	public WrapHeightListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	public WrapHeightListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public WrapHeightListView(Context context) {
		super(context);
	}

	@Override     
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {     
        int expandSpec = MeasureSpec.makeMeasureSpec(     
                Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);     
        super.onMeasure(widthMeasureSpec, expandSpec);     
    } 
	
}
