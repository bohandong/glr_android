/*
 * Copyright (C), 2014-2015, 联创车盟汽车服务有限公司
 * FileName: DrawableLeftCenterRadioButton.java
 * Author:   shufei
 * Date:     2015年1月12日 下午7:38:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.sheep.hub.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * 可以让drawableleft 居中的RadioButton
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class DrawableLeftCenterRadioButton extends RadioButton {

    /**
     * @param context
     * @param attrs
     */
    public DrawableLeftCenterRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    public DrawableLeftCenterRadioButton(Context context) {
        super(context);
    }
    

    /* (non-Javadoc)
     * @see android.widget.CompoundButton#onDraw(android.graphics.Canvas)
     */
    @Override
    protected void onDraw(Canvas canvas) {
        Drawable[] drawables = getCompoundDrawables();
        if(null != drawables){
            Drawable drawableLeft = drawables[0];
            if(null != drawableLeft){
                float textWidth = getPaint().measureText(getText().toString());
                int drawablePadding = getCompoundDrawablePadding();
                int drawableWidth = drawableLeft.getIntrinsicWidth();
                float bodyWidth = textWidth + drawablePadding + drawableWidth;
                canvas.translate((getWidth()-bodyWidth)/2, 0);
                
            }
        }
        super.onDraw(canvas);
    }
}
