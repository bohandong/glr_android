package com.sheep.hub.adapter;/*
  */

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.sheep.framework.ui.ArrayListAdapter;
import com.sheep.hub.R;


public abstract class SimpleTextAdapter<T> extends ArrayListAdapter<T> {
    private OnItemClickListener onItemClickListener;
    private boolean isWrapHeight;

    public SimpleTextAdapter(Activity context, OnItemClickListener onItemClickListener) {
        super(context);
        this.onItemClickListener = onItemClickListener;
    }

    public SimpleTextAdapter(Activity context, OnItemClickListener onItemClickListener, boolean isWrapHeight) {
        super(context);
        this.onItemClickListener = onItemClickListener;
        this.isWrapHeight = isWrapHeight;
    }

    public SimpleTextAdapter(Activity context) {
        super(context);
    }

    public SimpleTextAdapter(Activity context, boolean isWrapHeight) {
        super(context);
        this.isWrapHeight = isWrapHeight;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        TextView textView;
        if (null == convertView) {
            if (isWrapHeight) {
                textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_text, null);
                textView.setSingleLine(false);
            } else {
                textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_text, parent, false);
            }
        } else {
            textView = (TextView) convertView;
        }
        T bean = mList.get(position);
        textView.setTag(position);
        setTextView(textView, bean);
        if (null != onItemClickListener) {
            textView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClick(position);
                }
            });
        }
        return textView;
    }

    public abstract void setTextView(TextView textView, T t);

    public interface OnItemClickListener {
        void OnItemClick(int position);
    }
}
