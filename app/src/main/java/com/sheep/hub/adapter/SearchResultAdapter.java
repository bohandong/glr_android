package com.sheep.hub.adapter;/*
  */

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.amap.api.services.core.PoiItem;
import com.sheep.framework.ui.ArrayListAdapter;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;
import com.sheep.hub.bean.SearchHistory;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

public class SearchResultAdapter extends ArrayListAdapter<PoiItem> {

    private boolean hasGetLocation;

    public SearchResultAdapter(Activity context) {
        super(context);
        hasGetLocation = null != HubApp.getInstance().getMyLocation();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (null == convertView) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_search_result, parent, false);
            holder = new ViewHolder();
            holder.tv_title = (TextView) view.findViewById(R.id.tv_title);
            holder.tv_desc = (TextView) view.findViewById(R.id.tv_desc);
            holder.tv_distance = (TextView) view.findViewById(R.id.tv_distance);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) convertView.getTag();
        }
        PoiItem poiItem = mList.get(position);
        holder.tv_title.setText(poiItem.getTitle());
        holder.tv_desc.setText(TextUtils.isEmpty(poiItem.getSnippet()) ? mContext.getString(R.string.no_desc) : poiItem.getSnippet());
        if (hasGetLocation) {
          float distance = AMapUtils.calculateLineDistance(new LatLng(HubApp.getInstance().getMyLocation().getLatitude(), HubApp.getInstance().getMyLocation().getLongitude())
                    , new LatLng(poiItem.getLatLonPoint().getLatitude(), poiItem.getLatLonPoint().getLongitude()));
            holder.tv_distance.setText("" + new DecimalFormat("#.#").format(distance/1000)+"km");
        } else {
            holder.tv_distance.setText(R.string.unknown_distance);
        }
        return view;
    }

    class ViewHolder {
        TextView tv_title;
        TextView tv_desc;
        TextView tv_distance;
    }
}
