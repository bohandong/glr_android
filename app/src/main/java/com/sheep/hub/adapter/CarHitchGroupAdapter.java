package com.sheep.hub.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sheep.framework.log.L;
import com.sheep.hub.R;
import com.sheep.hub.bean.HitchChildItem;
import com.sheep.hub.bean.HitchGroupItem;
import com.sheep.hub.view.AnimatedExpandableListView.AnimatedExpandableListAdapter;

public class CarHitchGroupAdapter extends AnimatedExpandableListAdapter {

	private Context context;
	private ArrayList<HitchGroupItem> hgis; 
	private LayoutInflater inflater ; 
	private OnChildViewClickListener onChildClickListener;
	
	public CarHitchGroupAdapter(Context context,ArrayList<HitchGroupItem> hgis){
		this.context = context;
		this.hgis = hgis;
		inflater = LayoutInflater.from(context);
	}
	
	public CarHitchGroupAdapter(Context context){
		this.context = context;
		inflater = LayoutInflater.from(context);
	}
	
	public void setOnChildViewClickListener(OnChildViewClickListener onChildClickListener){
		this.onChildClickListener = onChildClickListener;
	}
	
	public void setDate(ArrayList<HitchGroupItem> hgis){
		this.hgis = hgis;
		notifyDataSetChanged();
	}
	
	@Override
	public int getGroupCount() {
		return ((null == hgis)||(hgis.size() <=0))? 0 : hgis.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return hgis.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return hgis.get(groupPosition).getHcis().get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}
	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		GroupViewHolder holder = null;
		if(null != convertView){
			holder = (GroupViewHolder) convertView.getTag();
		}else{
			convertView = inflater.inflate(R.layout.item_carhitch, null, false);
			holder = new GroupViewHolder();
			holder.tv = (TextView) convertView.findViewById(R.id.tv_hitchname);
			convertView.setTag(holder);
		}
		HitchGroupItem group = (HitchGroupItem) getGroup(groupPosition);
		holder.tv.setText(group.getHitchType()+" ("+group.getHitchNum()+") ");
		if("严重故障".equals(group.getHitchType())){
			holder.tv.setTextColor(context.getResources().getColor(R.color.text_dark_red_1));
		}else{
			holder.tv.setTextColor(context.getResources().getColor(R.color.text_dark_yellow_1));
		}
		return convertView;
	}

	@Override
	public View getRealChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		ChildViewHolder holder = null;
		if(null != convertView){
			holder = (ChildViewHolder) convertView.getTag();
		}else{
			convertView = inflater.inflate(R.layout.item_carhitch_child, parent, false);
			holder = new ChildViewHolder();
			holder.rl_hitchtitle = (RelativeLayout) convertView.findViewById(R.id.rl_hitchtitle);
			holder.iv_hitchicon = (ImageView) convertView.findViewById(R.id.iv_hitchicon);
			holder.tv_hitchname = (TextView) convertView.findViewById(R.id.tv_hitchname);
			holder.iv_arrow = (ImageView) convertView.findViewById(R.id.iv_arrow);
			holder.rl_hitchdetail = (LinearLayout) convertView.findViewById(R.id.rl_hitchdetail);
			holder.tv_hitchname_detail = (TextView) convertView.findViewById(R.id.tv_hitchname_detail);
			holder.tv_hitchtype = (TextView) convertView.findViewById(R.id.tv_hitchtype);
			holder.tv_hitchdetail = (TextView) convertView.findViewById(R.id.tv_hitchdetail);
			convertView.setTag(holder);
		}
		HitchGroupItem hgi = (HitchGroupItem)getGroup(groupPosition);
		final HitchChildItem hci = hgi.getHcis().get(childPosition); 
		
		holder.iv_hitchicon.setImageResource(hci.getHitchIconId());
		holder.tv_hitchname.setText(hci.getHitchName());
		//L.e("----->>hci.getHitchName():"+hci.getHitchName()+" hci.getHitchCarType():"+hci.getHitchCarType());
        String hitchCode = "<font color=#000000>故障码值：</font>"+hci.getHitchCode();
        holder.tv_hitchname_detail.setText(Html.fromHtml(hitchCode)); //要是故障码值
        String hitchCatagory = "<font color=#000000>故障范畴：</font>"+hci.getHitchCarType();
		holder.tv_hitchtype.setText(Html.fromHtml(hitchCatagory)); //要是故障范畴
        String hitchDetail = hci.getHitchDetail();
        if(TextUtils.isEmpty(hitchDetail)){
            hitchDetail = "无";
        }
		String detail = "<font color=#000000>背景知识：</font>"+hitchDetail;
		holder.tv_hitchdetail.setText(Html.fromHtml(detail));
		final ImageView arrowView =  holder.iv_arrow;
		final LinearLayout expandView =  holder.rl_hitchdetail;
		if(null != onChildClickListener){
			
			holder.rl_hitchtitle.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					hci.setExpand(!hci.isExpand());
					onChildClickListener.onChildClick(expandView, arrowView, hci.isExpand(), childPosition, groupPosition);
					
				}
			});
		}
		
		return convertView;
	} 
	
	public interface OnChildViewClickListener{
		void onChildClick(View expandView,ImageView arrowView,boolean isExpand,int childPosition,int groupPosition);
	}
   
	@Override
	public int getRealChildrenCount(int groupPosition) {
		return hgis.get(groupPosition).getHcis().size();
	}

	class GroupViewHolder{
		TextView tv;
	}
	
	class ChildViewHolder{
		RelativeLayout rl_hitchtitle;
		LinearLayout rl_hitchdetail;
		ImageView iv_hitchicon;
		TextView tv_hitchname;
		ImageView iv_arrow;
		TextView tv_hitchname_detail;
		TextView tv_hitchtype;
		TextView tv_hitchdetail;
	}
}
