/*
  */
package com.sheep.hub.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sheep.framework.ui.ArrayListAdapter;
import com.sheep.hub.R;
import com.sheep.hub.bean.DriveScore;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class HistoryScoreAdapter extends ArrayListAdapter<DriveScore> {

    /**
     * @param context
     * @param listView
     */
    public HistoryScoreAdapter(Activity context, ListView listView) {
        super(context, listView);
    }
    
    public HistoryScoreAdapter(Activity context) {
        super(context);
    }

    /* (non-Javadoc)
     * @see com.sheep.framework.ui.ArrayListAdapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.item_history_score, null,false);
        // TODO Auto-generated method stub
        return view;
    }

}
