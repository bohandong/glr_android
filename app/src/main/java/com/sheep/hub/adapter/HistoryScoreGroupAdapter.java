/*
 * Copyright (C), 2014-2015, 联创车盟汽车服务有限公司
 * FileName: HistoryScoreGroupAdapter.java
 * Author:   shufei
 * Date:     2015年1月15日 上午10:24:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.sheep.hub.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.utils.L;
import com.sheep.hub.R;
import com.sheep.hub.bean.DSChildItem;
import com.sheep.hub.bean.DSGroupItem;
import com.sheep.hub.view.AnimatedExpandableListView.AnimatedExpandableListAdapter;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class HistoryScoreGroupAdapter extends AnimatedExpandableListAdapter {

    private ArrayList<DSGroupItem> gis ;
    
    private Context context;
    
    private LayoutInflater inflater;
    
    public HistoryScoreGroupAdapter(Context context,ArrayList<DSGroupItem> gis){
        this.context = context;
        this.gis = gis;
        this.inflater = LayoutInflater.from(context);
    }
    
    public void setData(ArrayList<DSGroupItem> gis){
        this.gis = gis;
        notifyDataSetChanged();
    }
    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#getGroupCount()
     */
    @Override
    public int getGroupCount() {
        return ((null == gis)||(gis.size() <=0))? 0 : gis.size();
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#getGroup(int)
     */
    @Override
    public Object getGroup(int groupPosition) {
        return gis.get(groupPosition);
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#getChild(int, int)
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return gis.get(groupPosition).getCis().get(childPosition);
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#getGroupId(int)
     */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#getChildId(int, int)
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#hasStableIds()
     */
    @Override
    public boolean hasStableIds() {
        return false;
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#getGroupView(int, boolean, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHoldler holder = null;
        if(null == convertView){
            convertView = inflater.inflate(R.layout.item_history_score, parent, false);
            holder = new GroupViewHoldler();
            holder.iv_arrow = (ImageView) convertView.findViewById(R.id.iv_arrow);
            holder.tv_drive_score = (TextView) convertView.findViewById(R.id.tv_drive_score);
            holder.tv_starttime = (TextView) convertView.findViewById(R.id.tv_starttime);
            convertView.setTag(holder);
        }else{
            holder = (GroupViewHoldler) convertView.getTag();
        }
        if(isExpanded){
            holder.iv_arrow.setImageResource(R.drawable.icon_arrow_open);
        }else{
            holder.iv_arrow.setImageResource(R.drawable.icon_arrow_close);
        }
        DSGroupItem dsGroupItem = gis.get(groupPosition);
        holder.tv_starttime.setText(dsGroupItem.getStartTime());
        
        String strDriveScore = context.getResources().getString(R.string.history_score_recode);
        holder.tv_drive_score.setText(String.format(strDriveScore, dsGroupItem.getDriveScore()));
        
        return convertView;
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#isChildSelectable(int, int)
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    /* (non-Javadoc)
     * @see com.sheep.hub.view.AnimatedExpandableListView.AnimatedExpandableListAdapter#getRealChildView(int, int, boolean, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
            ViewGroup parent) {
        ChildViewHolder holder = null;
        if(null == convertView){
            convertView = inflater.inflate(R.layout.item_history_score_child, null, false);
            holder = new ChildViewHolder();
            holder.tv_span = (TextView) convertView.findViewById(R.id.tv_span);
            holder.tv_value = (TextView) convertView.findViewById(R.id.tv_value);
            convertView.setTag(holder);
        }else{
            holder = (ChildViewHolder) convertView.getTag();
        }
        DSChildItem dsChildItem = gis.get(groupPosition).getCis().get(childPosition);
        holder.tv_span.setText(dsChildItem.getKey());
        holder.tv_value.setText(dsChildItem.getValue());
        
        return convertView;
    }

    /* (non-Javadoc)
     * @see com.sheep.hub.view.AnimatedExpandableListView.AnimatedExpandableListAdapter#getRealChildrenCount(int)
     */
    @Override
    public int getRealChildrenCount(int groupPosition) {
        int size = gis.get(groupPosition).getCis().size();
        return size;
    }

    static class GroupViewHoldler{
        TextView tv_starttime;
        ImageView iv_arrow;
        TextView tv_drive_score;
    }
    
    static class ChildViewHolder{
        TextView tv_span;
        TextView tv_value;
    }
}
