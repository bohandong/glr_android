package com.sheep.hub.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.sheep.framework.ui.ArrayListAdapter;
import com.sheep.hub.R;

public class RouteSearchAdapter extends ArrayListAdapter<PoiItem> {


    public RouteSearchAdapter(Activity context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (null == convertView) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_poi, parent, false);
            holder = new ViewHolder();
            holder.tv_name = (TextView) view.findViewById(R.id.tv_name);
            holder.tv_address = (TextView) view.findViewById(R.id.tv_address);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        PoiItem bean = mList.get(position);
        holder.tv_name.setText(bean.getTitle());
        String address;
        if (!TextUtils.isEmpty(bean.getSnippet())) {
            address = bean.getSnippet();
        } else {
            address = mContext.getString(R.string.china);
        }
        holder.tv_address.setText(mContext.getString(R.string.address) + ":" + address);
        return view;
    }

    static class ViewHolder {
        TextView tv_name;
        TextView tv_address;
    }
}
