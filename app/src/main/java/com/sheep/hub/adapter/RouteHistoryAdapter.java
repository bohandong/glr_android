package com.sheep.hub.adapter;/*
  */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sheep.framework.ui.ArrayListAdapter;
import com.sheep.hub.R;
import com.sheep.hub.bean.RouteHistory;

public class RouteHistoryAdapter extends ArrayListAdapter<RouteHistory> {
    public RouteHistoryAdapter(Activity context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (null == convertView) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_route_history, parent, false);
            holder = new ViewHolder();
            holder.tv_route = (TextView) view.findViewById(R.id.tv_route);
            holder.tv_date = (TextView) view.findViewById(R.id.tv_date);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        RouteHistory history = mList.get(position);
        holder.tv_route.setText(history.getStartTitle() + "→" + history.getDestinationTitle());
        holder.tv_date.setText(history.getDate());
        return view;
    }

    static class ViewHolder {
        TextView tv_date;
        TextView tv_route;
    }
}
