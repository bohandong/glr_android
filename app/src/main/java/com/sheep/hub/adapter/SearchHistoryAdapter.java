package com.sheep.hub.adapter;/*
  */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.sheep.framework.ui.ArrayListAdapter;
import com.sheep.hub.R;
import com.sheep.hub.bean.SearchHistory;

import java.util.ArrayList;

public class SearchHistoryAdapter extends ArrayListAdapter<SearchHistory> {

    public SearchHistoryAdapter(Activity context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (null == convertView) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_search_history, parent, false);
            holder = new ViewHolder();
            holder.tv_text = (TextView) view.findViewById(R.id.tv_text);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tv_text.setText(mList.get(position).getKey());
        return view;
    }

    class ViewHolder {
        TextView tv_text;
    }
}
