package com.sheep.hub.adapter;/*
  */

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.maps.offlinemap.OfflineMapCity;
import com.amap.api.maps.offlinemap.OfflineMapProvince;
import com.amap.api.maps.offlinemap.OfflineMapStatus;
import com.sheep.hub.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OffLineAdapter extends BaseExpandableListAdapter {

    private Activity mContext;
    private ArrayList<OfflineMapProvince> provinceList;
    private HashMap<Object, List<OfflineMapCity>> cityMap;
    private boolean[] isOpenItem;

    private DownloadButtonListener downloadButtonListener;

    public OffLineAdapter(Activity context) {
        this.mContext = context;
    }

    @Override
    public int getGroupCount() {
        return provinceList.size();
    }

    public void setList(ArrayList<OfflineMapProvince> groupList) {
        this.provinceList = groupList;
        isOpenItem = new boolean[groupList.size()];
    }

    public void setCityMap(HashMap<Object, List<OfflineMapCity>> cityMap) {
        this.cityMap = cityMap;
    }

    /**
     * 获取一级标签内容
     */
    @Override
    public Object getGroup(int groupPosition) {
        return provinceList.get(groupPosition).getProvinceName();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return cityMap.get(groupPosition).size();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return cityMap.get(groupPosition).get(childPosition).getCity();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        GroupViewHolder holder;
        View view;
        if (convertView == null) {
            holder = new GroupViewHolder();
            view = LayoutInflater.from(mContext).inflate(R.layout.item_offline_group, parent, false);
            holder.tv_name = (TextView) view.findViewById(R.id.tv_name);
            holder.iv_icon = (ImageView) view.findViewById(R.id.iv_icon);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (GroupViewHolder) view.getTag();
        }
        holder.tv_name.setText(provinceList.get(groupPosition)
                .getProvinceName());
        if (isOpenItem[groupPosition]) {
            holder.iv_icon.setImageResource(R.drawable.icon_right);
        } else {
            holder.iv_icon.setImageResource(R.drawable.icon_below);
        }
        return view;
    }

    static class GroupViewHolder {
        TextView tv_name;
        ImageView iv_icon;
    }

    @Override
    public View getChildView(final int groupPosition,
                             final int childPosition, boolean isLastChild, View convertView,
                             ViewGroup parent) {
        ViewHolder holder;
        View view;
        if (null == convertView) {
            holder = new ViewHolder();
            view = LayoutInflater.from(mContext).inflate(R.layout.item_offline_child, parent, false);
            holder.tv_city = (TextView) view.findViewById(R.id.tv_city);
            holder.tv_size = (TextView) view.findViewById(R.id.tv_size);
            holder.btn_download = (Button) view.findViewById(R.id.btn_download);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        OfflineMapCity bean = cityMap.get(groupPosition).get(childPosition);
        boolean isInstalled = (OfflineMapStatus.SUCCESS == bean.getState());
        holder.tv_city.setText(bean.getCity() + (isInstalled ? "(" + mContext.getString(R.string.has_installed) + ")" : ""));
        holder.tv_size.setText(mContext.getString(R.string.size) + ": " + new DecimalFormat("#.##").format(bean.getSize() / (1024 * 1024f)) + "MB");
        holder.btn_download.setEnabled(!isInstalled);
        holder.btn_download.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if ( null != downloadButtonListener) {
                    downloadButtonListener.onClickListener(v, groupPosition, childPosition);
                }
            }
        });
        return view;
    }

    class ViewHolder {
        TextView tv_city;
        TextView tv_size;
        Button btn_download;
    }

    public void setDownloadButtonListener(DownloadButtonListener downloadButtonListener) {
        this.downloadButtonListener = downloadButtonListener;
    }

    public interface DownloadButtonListener {
        void onClickListener(View v, final int groupPosition, final int childPosition);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setIsOpenItem(int currentOpenItem, boolean isOpen) {
        isOpenItem[currentOpenItem] = isOpen;
        notifyDataSetChanged();
    }
}
