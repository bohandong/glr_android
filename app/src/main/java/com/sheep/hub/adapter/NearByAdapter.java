package com.sheep.hub.adapter;/*
  */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sheep.framework.ui.ArrayListAdapter;
import com.sheep.hub.R;
import com.sheep.hub.bean.Interest;

public class NearByAdapter extends ArrayListAdapter<Interest> {
    public NearByAdapter(Activity context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (null == convertView) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_nearby, parent, false);
            holder = new ViewHolder();
            holder.tv_text = (TextView) view.findViewById(R.id.tv_text);
            holder.iv_icon = (ImageView) view.findViewById(R.id.iv_icon);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) convertView.getTag();
        }
        Interest bean = mList.get(position);
        holder.tv_text.setText(bean.getNameId());
        holder.iv_icon.setImageResource(bean.getDrawableId());
        return view;
    }


    static class ViewHolder {
        TextView tv_text;
        ImageView iv_icon;
    }
}