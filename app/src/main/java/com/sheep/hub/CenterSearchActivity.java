package com.sheep.hub;/*
 */

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.navi.model.NaviLatLng;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.help.Inputtips;
import com.sheep.framework.util.Density;
import com.sheep.framework.view.KeyOrHandSetEditText;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshListView;
import com.sheep.hub.adapter.SearchHistoryAdapter;
import com.sheep.hub.adapter.SimpleTextAdapter;
import com.sheep.hub.bean.SearchHistory;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.dialog.DropPopupWindow;
import com.sheep.hub.dialog.RouteSearchPoiDialog;
import com.sheep.hub.util.PoiSearchHelper;

import java.util.ArrayList;

public class CenterSearchActivity extends BaseCalculateRouteActivity implements TextWatcher, View.OnClickListener, AdapterView.OnItemClickListener, TextView.OnEditorActionListener {
    private KeyOrHandSetEditText et_poi;
    private PullToRefreshListView lv_list;

    private DropPopupWindow dropPopupWindow;
    private SearchHistoryAdapter adapter;

    private Button btn_search;

    private ArrayList<SearchHistory> searchHistoryList;
    private TextView footerView;
    private PoiSearchHelper poiSearchHelper;
    private RouteSearchPoiDialog poiSearchDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initTitle(R.string.add_center_location);
        initView();
    }

    private void initView() {

        if (null != HubApp.getInstance().getMyLocation()) {
            Button btn_next = (Button) findViewById(R.id.btn_next);
            btn_next.setText(HubApp.getInstance().getMyLocation().getCity());
            btn_next.setVisibility(View.VISIBLE);
        }

        et_poi = (KeyOrHandSetEditText) findViewById(R.id.et_poi);

        lv_list = (PullToRefreshListView) findViewById(android.R.id.list);
        adapter = new SearchHistoryAdapter(this);
        lv_list.setMode(PullToRefreshBase.Mode.DISABLED);
        lv_list.setAdapter(adapter);
        lv_list.setEmptyView(LayoutInflater.from(this).inflate(R.layout.view_empy, lv_list.getRefreshableView(), false));
        lv_list.getRefreshableView().setSelector(new ColorDrawable(0));

        footerView = (TextView) LayoutInflater.from(this).inflate(R.layout.footer_clear_history, lv_list.getRefreshableView(), false);
        footerView.setText(R.string.clear_search_history);
        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HubApp.getInstance().getAfeiDb().deleteByWhereStr(SearchHistory.class, "");
                adapter.getList().clear();
                adapter.notifyDataSetChanged();
            }
        });
        lv_list.getRefreshableView().addFooterView(footerView);
        btn_search = (Button) findViewById(R.id.btn_search);
        btn_search.setOnClickListener(this);
        et_poi.addTextChangedListener(this);
        lv_list.setOnItemClickListener(this);
        et_poi.setOnEditorActionListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        searchHistoryList = HubApp.getInstance().getAfeiDb().findAll(SearchHistory.class);
        adapter.setList(searchHistoryList);

        if (searchHistoryList.size() > 0 && lv_list.getRefreshableView().getFooterViewsCount() <= 0) {
            lv_list.getRefreshableView().addFooterView(footerView);
        }
    }

    @Override
    public void onClick(View v) {
        String key = et_poi.getText().toString().trim();
        if (TextUtils.isEmpty(key)) {
            Toast.makeText(CenterSearchActivity.this, R.string.please_input_key_search, Toast.LENGTH_SHORT).show();
            return;
        }
        SearchHistory history = new SearchHistory();
        history.set_id("" + System.currentTimeMillis());
        history.setKey(key);
        if (!adapter.getList().contains(history)) {
            HubApp.getInstance().getAfeiDb().save(history);
        }
        poiDialogSearch();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            btn_search.performClick();
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        et_poi.setHandText(adapter.getItem(position - 1).getKey());
        et_poi.setSelection(et_poi.getText().toString().length());
        poiDialogSearch();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (null == dropPopupWindow) {
            dropPopupWindow = new DropPopupWindow(CenterSearchActivity.this, et_poi.getWidth() + 30, Density.of(CenterSearchActivity.this, 300));
            dropPopupWindow.setAdapter(new SimpleTextAdapter<PoiItem>(CenterSearchActivity.this, new SimpleTextAdapter.OnItemClickListener() {

                @Override
                public void OnItemClick(int position) {
                    PoiItem bean = (PoiItem) dropPopupWindow.getAdapter().getItem(position);
                    et_poi.clearFocus();
                    et_poi.setHandText(TextUtils.isEmpty(bean.getSnippet()) ? bean.getTitle() : bean.getSnippet());
                    dropPopupWindow.dismiss();
                    ArrayList<NaviLatLng> centerList =
                            (ArrayList<NaviLatLng>) getIntent().getSerializableExtra(NaviRouteActivity.CENTER_POITEM_LIST);
                    centerList.add(new NaviLatLng(bean.getLatLonPoint().getLatitude(),
                            bean.getLatLonPoint().getLongitude()));
                    startRouteSearch((ArrayList<NaviLatLng>) getIntent().getSerializableExtra(NaviRouteActivity.START_POITEM_LIST), centerList,
                            (ArrayList<NaviLatLng>) getIntent().getSerializableExtra(NaviRouteActivity.END_POITEM_LIST));
                }
            }) {
                @Override
                public void setTextView(TextView textView, PoiItem poiItem) {
                    textView.setText(poiItem.getTitle() + (TextUtils.isEmpty(poiItem.getSnippet()) ? "" : poiItem.getSnippet()));
                }
            });
            dropPopupWindow.setPullMode(PullToRefreshBase.Mode.PULL_FROM_END, new PullToRefreshBase.OnRefreshListener2<ListView>() {
                @Override
                public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

                }

                @Override
                public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                    poiSearchHelper.queryNext();
                }
            });
        }
        if (!TextUtils.isEmpty(s.toString().trim()) && !et_poi.isHandSet()) {
            poiAsyncSearch();
        } else if (dropPopupWindow.isShowing()) {
            dropPopupWindow.dismiss();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void poiDialogSearch() {
        if (null == poiSearchHelper) {
            poiSearchHelper = new PoiSearchHelper(this, new PoiSearchHelper.PoiQuerySuccessListener() {
                @Override
                public void onSuccess(ArrayList<PoiItem> poiItemList) {
                    if (null == poiSearchDialog || !poiSearchDialog.isShowing()) {
                        poiSearchDialog = new RouteSearchPoiDialog(CenterSearchActivity.this);
                        poiSearchDialog.setTitle(R.string.center_poi);
                        poiSearchDialog.setOnListClickListener(new RouteSearchPoiDialog.OnListItemClick() {
                            @Override
                            public void onListItemClick(
                                    RouteSearchPoiDialog dialog, PoiItem endPoiItem) {
                                et_poi.setHandText((TextUtils.isEmpty(endPoiItem.getSnippet()) ? endPoiItem.getTitle() : endPoiItem.getSnippet()));
                                ArrayList<NaviLatLng> centerList =
                                        (ArrayList<NaviLatLng>) getIntent().getSerializableExtra(NaviRouteActivity.CENTER_POITEM_LIST);
                                centerList.add(new NaviLatLng(endPoiItem.getLatLonPoint().getLatitude(),
                                        endPoiItem.getLatLonPoint().getLongitude()));
                                startRouteSearch((ArrayList<NaviLatLng>) getIntent().getSerializableExtra(NaviRouteActivity.START_POITEM_LIST), centerList,
                                        (ArrayList<NaviLatLng>) getIntent().getSerializableExtra(NaviRouteActivity.END_POITEM_LIST));
                            }
                        });
                        poiSearchDialog.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2() {
                            @Override
                            public void onPullDownToRefresh(PullToRefreshBase refreshView) {

                            }

                            @Override
                            public void onPullUpToRefresh(PullToRefreshBase refreshView) {
                                poiSearchHelper.queryNext();
                            }
                        });
                    }
                    if (!poiSearchDialog.isShowing()) {
                        poiSearchDialog.show();
                    }
                    poiSearchDialog.onRefreshComplete();
                    poiSearchDialog.addList(poiItemList);
                    if (poiItemList.size() != Constant.PAGE_SIZE) {
                        poiSearchDialog.setMode(PullToRefreshBase.Mode.DISABLED);
                    }

                }

                @Override
                public void onFail() {

                }
            });
        }
        poiSearchHelper.setPoiKey(et_poi.getText().toString().trim());
        poiSearchHelper.query(true);
    }

    private void poiAsyncSearch() {
        if (null == poiSearchHelper) {
            poiSearchHelper = new PoiSearchHelper(this, new PoiSearchHelper.PoiQuerySuccessListener() {
                @Override
                public void onSuccess(ArrayList<PoiItem> poiItemList) {
                    if (!dropPopupWindow.isShowing()) {
                        dropPopupWindow.show(et_poi, -20, 0);
                    }
                    dropPopupWindow.onRefreshComplete();
                    dropPopupWindow.addList(poiItemList);
                    if (poiItemList.size() < Constant.PAGE_SIZE) {
                        dropPopupWindow.getListView().setMode(PullToRefreshBase.Mode.DISABLED);
                    }

                }

                @Override
                public void onFail() {
                    dropPopupWindow.onRefreshComplete();
                }
            });
        }
        poiSearchHelper.setPoiKey(et_poi.getText().toString().trim());
        poiSearchHelper.query(false);
    }
}
