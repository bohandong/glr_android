package com.sheep.hub;/*
 */

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.sheep.framework.util.Density;
import com.sheep.framework.view.KeyOrHandSetEditText;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshListView;
import com.sheep.hub.adapter.RouteHistoryAdapter;
import com.sheep.hub.adapter.SimpleTextAdapter;
import com.sheep.hub.bean.LocalLocation;
import com.sheep.hub.bean.RouteHistory;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.dialog.DropPopupWindow;
import com.sheep.hub.dialog.RouteSearchPoiDialog;
import com.sheep.hub.util.LocationUtil;
import com.sheep.hub.util.PoiSearchHelper;

import java.util.ArrayList;

public class NaviSearchActivity extends BaseCalculateRouteActivity implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemClickListener {

    private static final int HOME_SETTING_REQ = 0;
    private static final int COMPANY_SETTING_REQ = 1;

    private KeyOrHandSetEditText et_from;
    private KeyOrHandSetEditText et_to;
    private DropPopupWindow dropPopupWindow;

    private PullToRefreshListView plv_list;
    private RouteHistoryAdapter routeHistoryAdapter;

    private PoiSearchHelper startPoiAsyncSearchHelper;
    private PoiSearchHelper destinationAsyncPoiSearchHelper;
    private PoiItem startPoiItem;
    private PoiItem destinationPoiItem;
    private RouteSearchPoiDialog poiSearchDialog;

    private PoiSearchHelper startPoiDialogSearchHelper;
    private PoiSearchHelper destinationDialogPoiSearchHelper;

    private LocalLocation homeLocation;
    private LocalLocation companyLocation;
    private TextView tv_home_setting;
    private TextView tv_company_setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navi_search);
        initTitle(R.string.navigation);
        initView();
    }

    private void initView() {
        et_from = (KeyOrHandSetEditText) findViewById(R.id.et_from);
        et_to = (KeyOrHandSetEditText) findViewById(R.id.et_to);

        plv_list = (PullToRefreshListView) findViewById(android.R.id.list);
        plv_list.setMode(PullToRefreshBase.Mode.DISABLED);
        routeHistoryAdapter = new RouteHistoryAdapter(this);
        routeHistoryAdapter.setList(HubApp.getInstance().getAfeiDb().findAll(RouteHistory.class));
        plv_list.setAdapter(routeHistoryAdapter);
        TextView emptyView = (TextView) View.inflate(this, R.layout.view_empy, null);
        emptyView.setText(R.string.no_history_route);
        plv_list.setEmptyView(emptyView);
        View footerView = LayoutInflater.from(this).inflate(R.layout.footer_clear_history, plv_list.getRefreshableView(), false);
        plv_list.getRefreshableView().addFooterView(footerView);
        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HubApp.getInstance().getAfeiDb().deleteByWhereStr(RouteHistory.class, "");
                routeHistoryAdapter.getList().clear();
                routeHistoryAdapter.notifyDataSetChanged();
            }
        });

        tv_home_setting = (TextView) findViewById(R.id.tv_home_setting);
        tv_company_setting = (TextView) findViewById(R.id.tv_company_setting);
        ArrayList<LocalLocation> homeList = HubApp.getInstance().getAfeiDb().findAllByWhereStr(LocalLocation.class, "rowName= '" + Constant.ROW_HOME_NAME + "'");
        if (null != homeList && homeList.size() > 0) {
            homeLocation = homeList.get(0);
            tv_home_setting.setText(LocationUtil.getSimpleAddress(homeLocation.getName()));
        }
        ArrayList<LocalLocation> companyList = HubApp.getInstance().getAfeiDb().findAllByWhereStr(LocalLocation.class, "rowName= '" + Constant.ROW_COMPANY_NAME + "'");
        if (null != companyList && companyList.size() > 0) {
            companyLocation = companyList.get(0);
            tv_company_setting.setText(LocationUtil.getSimpleAddress(companyLocation.getName()));
        }
        et_from.addTextChangedListener(new TextWatcherListener(et_from));
        et_to.addTextChangedListener(new TextWatcherListener(et_to));
        et_from.setOnFocusChangeListener(this);
        et_to.setOnFocusChangeListener(this);
        findViewById(R.id.btn_go).setOnClickListener(this);
        plv_list.setOnItemClickListener(this);

        findViewById(R.id.ll_company).setOnClickListener(this);
        findViewById(R.id.ll_home).setOnClickListener(this);

        LocationUtil.requestMyLocation(this);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (null != dropPopupWindow && dropPopupWindow.isShowing()) {
            dropPopupWindow.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_go:
                if (!TextUtils.isEmpty(et_from.getText().toString().trim())) {
                    if (null == HubApp.getInstance().getMyLocation() || 0 == HubApp.getInstance().getMyLocation().getLatitude() || 0 == HubApp.getInstance().getMyLocation().getLongitude()) {
                        Toast.makeText(this, R.string.location_fail, Toast.LENGTH_SHORT).show();
                        et_from.setHint("");
                        showKeyboard(et_from);
                        return;
                    }
                } else if (getString(R.string.my_location).equals(et_from.getHint()) && (null != HubApp.getInstance().getMyLocation() && 0 < HubApp.getInstance().getMyLocation().getLatitude() && 0 < HubApp.getInstance().getMyLocation().getLongitude())) {
                    String address;
                    String[] split;
                    if (null != HubApp.getInstance().getMyLocation().getExtras() && !TextUtils.isEmpty(HubApp.getInstance().getMyLocation().getExtras().getString("desc")) &&
                            2 == (split = HubApp.getInstance().getMyLocation().getExtras().getString("desc").split("市")).length) {
                        address = split[1];
                    } else {
                        address = HubApp.getInstance().getMyLocation().getDistrict();
                    }
                    startPoiItem = new PoiItem("" + System.currentTimeMillis(),
                            new LatLonPoint(HubApp.getInstance().getMyLocation().getLatitude(),
                                    HubApp.getInstance().getMyLocation().getLongitude()), address, address);
                } else {
                    showKeyboard(et_from);
                    Toast.makeText(this, R.string.has_no_destination, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(et_to.getText().toString().trim())) {
                    showKeyboard(et_to);
                    Toast.makeText(this, R.string.has_no_destination, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (null == startPoiItem) {
                    poiDialogSearch(true);
                } else if (null == destinationPoiItem) {
                    poiDialogSearch(false);
                } else if (startPoiItem.getLatLonPoint().getLatitude() == destinationPoiItem.getLatLonPoint().getLatitude() && startPoiItem.getLatLonPoint().getLongitude() == destinationPoiItem.getLatLonPoint().getLongitude()) {
                    Toast.makeText(this, R.string.same_start_destination, Toast.LENGTH_SHORT).show();
                } else {
                    startRouteSearch(startPoiItem, destinationPoiItem);
                }
                break;
            case R.id.ll_home:
                if (null != homeLocation) {
                    destinationPoiItem = new PoiItem("" + System.currentTimeMillis(), new LatLonPoint(homeLocation.getLatitude(), homeLocation.getLongitude()), getString(R.string.home), homeLocation.getName());
                    et_to.setHandText(LocationUtil.getSimpleAddress(homeLocation.getName()));
                } else {
                    Intent intent = new Intent(this, LocationSettingActivity.class);
                    intent.putExtra(LocationSettingActivity.IS_HOME_SETTING, true);
                    launch(intent, HOME_SETTING_REQ);
                }
                break;
            case R.id.ll_company:
                if (null != companyLocation) {
                    destinationPoiItem = new PoiItem("" + System.currentTimeMillis(), new LatLonPoint(companyLocation.getLatitude(), companyLocation.getLongitude()), getString(R.string.home), companyLocation.getName());
                    et_to.setHandText(LocationUtil.getSimpleAddress(companyLocation.getName()));
                } else {
                    Intent intent = new Intent(this, LocationSettingActivity.class);
                    intent.putExtra(LocationSettingActivity.IS_HOME_SETTING, false);
                    launch(intent, COMPANY_SETTING_REQ);
                }
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == plv_list.getRefreshableView()) {
            RouteHistory history = routeHistoryAdapter.getItem(position - 1);
            et_from.setHandText(TextUtils.isEmpty(history.getStartSnippet()) ? history.getStartTitle() : history.getStartSnippet());
            et_to.setHandText(TextUtils.isEmpty(history.getDestinationSnippet()) ? history.getDestinationTitle() : history.getDestinationSnippet());

            startPoiItem = new PoiItem("" + System.currentTimeMillis(),
                    new LatLonPoint(history.getStartLat(),
                            history.getStartLong()), history.getStartTitle(), history.getStartSnippet());
            destinationPoiItem = new PoiItem("" + System.currentTimeMillis(),
                    new LatLonPoint(history.getDestinationLat(),
                            history.getDestinationLong()), history.getDestinationTitle(), history.getDestinationSnippet());
            startRouteSearch(startPoiItem, destinationPoiItem);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case HOME_SETTING_REQ:
                    homeLocation = (LocalLocation) data.getSerializableExtra("data");
                    tv_home_setting.setText(LocationUtil.getSimpleAddress(homeLocation.getName()));
                    et_to.setHandText(LocationUtil.getSimpleAddress(homeLocation.getName()));
                    destinationPoiItem = new PoiItem("" + System.currentTimeMillis(), new LatLonPoint(homeLocation.getLatitude(), homeLocation.getLongitude()), getString(R.string.home), homeLocation.getName());
                    break;
                case COMPANY_SETTING_REQ:
                    companyLocation = (LocalLocation) data.getSerializableExtra("data");
                    tv_company_setting.setText(LocationUtil.getSimpleAddress(companyLocation.getName()));
                    et_to.setHandText(LocationUtil.getSimpleAddress(homeLocation.getName()));
                    destinationPoiItem = new PoiItem("" + System.currentTimeMillis(), new LatLonPoint(companyLocation.getLatitude(), companyLocation.getLongitude()), getString(R.string.home), companyLocation.getName());
                    break;
                case IS_BEGIN_NAVI_REQ:
                    finish();
                    break;
                default:
            }
        }
    }

    private void showKeyboard(final EditText editText) {
        editText.postDelayed(new Runnable() {
            @Override
            public void run() {
                editText.requestFocus();
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).showSoftInput(editText, 0);
            }
        }, 200);
    }

    private void poiDialogSearch(final boolean isStart) {
        PoiSearchHelper tempHelper;
        if (isStart) {
            tempHelper = startPoiDialogSearchHelper;
        } else {
            tempHelper = destinationDialogPoiSearchHelper;
        }
        if (null == tempHelper) {
            tempHelper = new PoiSearchHelper(this, new PoiSearchHelper.PoiQuerySuccessListener() {
                @Override
                public void onSuccess(ArrayList<PoiItem> poiItemList) {
                    if (null == poiSearchDialog || !poiSearchDialog.isShowing()) {
                        poiSearchDialog = new RouteSearchPoiDialog(NaviSearchActivity.this);
                        poiSearchDialog.setTitle(isStart ? R.string.start_poi : R.string.destination_poi);
                        poiSearchDialog.setOnListClickListener(new RouteSearchPoiDialog.OnListItemClick() {
                            @Override
                            public void onListItemClick(
                                    RouteSearchPoiDialog dialog, PoiItem endPoiItem) {
                                if (isStart) {
                                    startPoiItem = endPoiItem;
                                    et_from.setHandText((TextUtils.isEmpty(endPoiItem.getSnippet()) ? endPoiItem.getTitle() : endPoiItem.getSnippet()));
                                    poiDialogSearch(false);
                                } else {
                                    destinationPoiItem = endPoiItem;
                                    et_to.setHandText((TextUtils.isEmpty(endPoiItem.getSnippet()) ? endPoiItem.getTitle() : endPoiItem.getSnippet()));
                                    startRouteSearch(startPoiItem, destinationPoiItem);
                                }
                            }
                        });
                        poiSearchDialog.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2() {
                            @Override
                            public void onPullDownToRefresh(PullToRefreshBase refreshView) {

                            }

                            @Override
                            public void onPullUpToRefresh(PullToRefreshBase refreshView) {
                                if (isStart) {
                                    startPoiDialogSearchHelper.queryNext();
                                } else {
                                    destinationDialogPoiSearchHelper.queryNext();
                                }
                            }
                        });
                    }
                    if (!poiSearchDialog.isShowing()) {
                        poiSearchDialog.show();
                    }
                    poiSearchDialog.onRefreshComplete();
                    poiSearchDialog.addList(poiItemList);
                    if (poiItemList.size() != Constant.PAGE_SIZE) {
                        poiSearchDialog.setMode(PullToRefreshBase.Mode.DISABLED);
                    }

                }

                @Override
                public void onFail() {

                }
            });
            if (isStart) {
                startPoiDialogSearchHelper = tempHelper;
            } else {
                destinationDialogPoiSearchHelper = tempHelper;
            }
        }
        tempHelper.setPoiKey(isStart ? et_from.getText().toString().trim() : et_to.getText().toString().trim());
        tempHelper.query(true);
    }

    private void poiAsyncSearch(final boolean isStart) {
        PoiSearchHelper tempHelper;
        if (isStart) {
            tempHelper = startPoiAsyncSearchHelper;
        } else {
            tempHelper = destinationAsyncPoiSearchHelper;
        }
        if (null == tempHelper) {
            tempHelper = new PoiSearchHelper(this, new PoiSearchHelper.PoiQuerySuccessListener() {
                @Override
                public void onSuccess(ArrayList<PoiItem> poiItemList) {
                    if (!dropPopupWindow.isShowing()) {
                        dropPopupWindow.show(isStart ? et_from : et_to, -20, 0);
                    } else {
                        dropPopupWindow.show(isStart ? et_from : et_to, -20, 0);
                    }
                    dropPopupWindow.onRefreshComplete();
                    dropPopupWindow.addList(poiItemList);
                    if (poiItemList.size() < Constant.PAGE_SIZE) {
                        dropPopupWindow.getListView().setMode(PullToRefreshBase.Mode.DISABLED);
                    }

                }

                @Override
                public void onFail() {
                    dropPopupWindow.onRefreshComplete();
                }
            });
            if (isStart) {
                startPoiAsyncSearchHelper = tempHelper;
            } else {
                destinationAsyncPoiSearchHelper = tempHelper;
            }
        }
        tempHelper.setPoiKey(isStart ? et_from.getText().toString().trim() : et_to.getText().toString().trim());
        tempHelper.query(false);
    }

//    private void routeSearch() {
//        showProgressDialog();
//        ArrayList<NaviLatLng> start = new ArrayList<NaviLatLng>();
//        start.add(new NaviLatLng(startPoiItem.getLatLonPoint().getLatitude(),
//                startPoiItem.getLatLonPoint().getLongitude()));
//        ArrayList<NaviLatLng> end = new ArrayList<NaviLatLng>();
//        end.add(new NaviLatLng(destinationPoiItem.getLatLonPoint().getLatitude(),
//                destinationPoiItem.getLatLonPoint().getLongitude()));
//        if (AMapNavi.getInstance(this).calculateDriveRoute(start, end, null, AMapNavi.DrivingShortDistance)) {
//        } else {
//            Toast.makeText(NaviSearchActivity.this, R.string.navigation_fail, Toast.LENGTH_SHORT).show();
//        }
//    }


    class TextWatcherListener implements android.text.TextWatcher {
        private EditText editText;

        public TextWatcherListener(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            dropPopupWindow = new DropPopupWindow(NaviSearchActivity.this, et_from.getWidth() + 30, Density.of(NaviSearchActivity.this, 300));
            dropPopupWindow.setAdapter(new SimpleTextAdapter<PoiItem>(NaviSearchActivity.this, new SimpleTextAdapter.OnItemClickListener() {

                @Override
                public void OnItemClick(int position) {
                    PoiItem bean = (PoiItem) dropPopupWindow.getAdapter().getItem(position);
                    if (R.id.et_from == editText.getId()) {
                        et_from.clearFocus();
                        et_from.setHandText(TextUtils.isEmpty(bean.getSnippet()) ? bean.getTitle() : bean.getSnippet());
                        startPoiItem = bean;
                        if (null == destinationPoiItem) {
                            showKeyboard(et_to);
                        } else {
                            et_from.setSelection(et_to.getText().length());
                            et_from.clearFocus();
                        }
                    } else {
                        et_to.clearFocus();
                        et_to.setHandText((TextUtils.isEmpty(bean.getSnippet()) ? bean.getTitle() : bean.getSnippet()));
                        destinationPoiItem = bean;
                        if (null == startPoiItem) {
                            if (getString(R.string.my_location).equals(et_from.getHint()) &&
                                    (null != HubApp.getInstance().getMyLocation() &&
                                            0 < HubApp.getInstance().getMyLocation().getLatitude() && 0 < HubApp.getInstance().getMyLocation().getLongitude())) {
                            } else {
                                showKeyboard(et_from);
                            }
                        } else {
                            et_to.setSelection(et_to.getText().length());
                            et_to.clearFocus();
                        }
                    }
                    dropPopupWindow.dismiss();
                }
            }) {
                @Override
                public void setTextView(TextView textView, PoiItem poiItem) {
                    textView.setText(poiItem.getTitle() + (TextUtils.isEmpty(poiItem.getSnippet()) ? "" : poiItem.getSnippet()));
                }
            });
            dropPopupWindow.setPullMode(PullToRefreshBase.Mode.PULL_FROM_END, new PullToRefreshBase.OnRefreshListener2<ListView>() {
                @Override
                public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

                }

                @Override
                public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                    if (R.id.et_from == editText.getId()) {
                        startPoiAsyncSearchHelper.queryNext();
                    } else {
                        destinationAsyncPoiSearchHelper.queryNext();
                    }
                }
            });
            if (!TextUtils.isEmpty(s.toString().trim()) && !((KeyOrHandSetEditText) editText).isHandSet()) {
                if (R.id.et_from == editText.getId()) {
                    startPoiItem = null;
                    poiAsyncSearch(true);
                } else if (R.id.et_to == editText.getId()) {
                    destinationPoiItem = null;
                    poiAsyncSearch(false);
                }
            } else if (dropPopupWindow.isShowing()) {
                dropPopupWindow.dismiss();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
