package com.sheep.hub.constant;


import com.sheep.hub.R;
import com.sheep.hub.bean.Interest;

import java.util.ArrayList;

/**
 * @author chensf5
 */
public class Constant {

    // 数据库名称
    public static final String DB_NAME = "Hub.db";
    //是否是第一次进入App
    public static final String IS_LOGO_FIRST ="is_logo_first";
    public static final String IS_HOME_FIRST ="is_home_first";
    
    public static final String NAVI_PACKAGENAME = "com.baidu.navi"; //百度导航的包名
    public static final String NAVI_ACTIVITY = "com.baidu.navi.NaviActivity"; //百度导航的类名

    //昼夜模式
    public static final String K_DAYNIGHT_MODE = "k_daynight_mode";
    public static final String V_DAY = "v_day";
    public static final String V_NIGHT = "v_night";
    public static final String V_AUTO = "v_auto";

    //字体大小
    public static final String K_FONT = "k_font";
    public static final String V_SMALL = "v_small";
    public static final String V_MIDDLE = "v_middle";
    public static final String V_BIG = "v_big";

    //语音提示频率
    public static final String K_VOICE_RATE = "k_voice_rate";
    public static final String V_NORMAL = "50";
    public static final String V_FREQUENT = "75";
    
    //蓝牙相关常量
    public static final String K_BLUETOOTH_ADRESS = "k_bluetooth_address";
    public static final String K_SEQUENCE = "sequence"; //sequence号
    public static final String K_BLUETOOTH_PWD = "k_bluetooth_pwd"; //蓝牙密码
    
    public static final String K_COPY_HITCH = "k_copy_hitch"; //是否拷贝了故障码
    
    public static final String K_BRIGHT = "K_bright"; //亮度

    //语音
//    public static final String K_VOICE_SWITCH = "K_voice_switch";
    public static final String ROW_HOME_NAME = "home";
    public static final String ROW_COMPANY_NAME = "company";
    public static final String ROW_STORE_NAME="store";

    public static final int PAGE_SIZE = 20;
    public static final int HAS_STORED = 0;
    public static final int HAS_NOT_STORED = 1;

    public static final String PRE_TRAFFIC = "traffic_open";
    //街区详情
    public static final String PRE_LAYER = "layer_open";
    public static final String PRE_SOUNDS = "sounds_open";

    public static ArrayList<Interest> NEARBY_INTEREST_LIST;

    public static ArrayList<Interest> getNEARBY_INTEREST_LIST() {
        if (null == NEARBY_INTEREST_LIST) {
            NEARBY_INTEREST_LIST = new ArrayList<Interest>();
            NEARBY_INTEREST_LIST.add(new Interest(R.string.gas_station, R.drawable.icon_launcher));
            NEARBY_INTEREST_LIST.add(new Interest(R.string.parker, R.drawable.icon_launcher));
            NEARBY_INTEREST_LIST.add(new Interest(R.string.food, R.drawable.icon_launcher));
            NEARBY_INTEREST_LIST.add(new Interest(R.string.hotel, R.drawable.icon_launcher));
            NEARBY_INTEREST_LIST.add(new Interest(R.string.scenery, R.drawable.icon_launcher));
            NEARBY_INTEREST_LIST.add(new Interest(R.string.market, R.drawable.icon_launcher));
            NEARBY_INTEREST_LIST.add(new Interest(R.string.car_service, R.drawable.icon_launcher));
            NEARBY_INTEREST_LIST.add(new Interest(R.string.life_service, R.drawable.icon_launcher));
        }
        return NEARBY_INTEREST_LIST;
    }

    public static final String PRE_VOICE_TYPE="voice_type";
    public static final String PRE_NAVI_THEME="navigation_theme";
}
