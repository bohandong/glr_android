package com.sheep.hub;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;

import com.amap.api.maps.AMapException;
import com.amap.api.maps.MapView;
import com.amap.api.maps.offlinemap.OfflineMapCity;
import com.amap.api.maps.offlinemap.OfflineMapManager;
import com.amap.api.maps.offlinemap.OfflineMapManager.OfflineMapDownloadListener;
import com.amap.api.maps.offlinemap.OfflineMapProvince;
import com.amap.api.maps.offlinemap.OfflineMapStatus;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.adapter.OffLineAdapter;
import com.sheep.hub.dialog.ConfirmDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OfflineMapListActivity extends BaseActivity implements
        OfflineMapDownloadListener, OffLineAdapter.DownloadButtonListener {

    private OfflineMapManager offlineMapManager;
    private ArrayList<OfflineMapProvince> provinceList;
    private HashMap<Object, List<OfflineMapCity>> cityMap;
    private int groupPosition = -1;
    private int childPosition = -1;
    private boolean isStart = false;// 判断是否开始下载,true表示开始下载，false表示下载失败

    private OffLineAdapter adapter;
    private MapView mapView;

    private ProgressDialog progressDialog;
    private ExpandableListView elv_list;

    private boolean isMobileDownload;
    private boolean isMobileMode;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_map);
        initTitle(R.string.offline_map);
        initView();
    }

    private void initView() {
        mapView = new MapView(this);
        offlineMapManager = new OfflineMapManager(this, this);
        elv_list = (ExpandableListView) findViewById(android.R.id.list);
        elv_list.setGroupIndicator(null);

        initData();

        adapter = new OffLineAdapter(this);
        adapter.setList(provinceList);
        adapter.setDownloadButtonListener(this);
        adapter.setCityMap(cityMap);
        elv_list.setAdapter(adapter);

        elv_list.setOnGroupCollapseListener(new OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                adapter.setIsOpenItem(groupPosition, false);
            }
        });

        elv_list.setOnGroupExpandListener(new OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                adapter.setIsOpenItem(groupPosition, true);
            }
        });
    }

    private void download(View v, int groupPosition, final int childPosition, String city) {
        try {
            // 下载全国概要图、直辖市、港澳离线地图数据
            if (groupPosition == 0 || groupPosition == 1 || groupPosition == 2) {
                isStart = offlineMapManager.downloadByProvinceName(cityMap
                        .get(groupPosition).get(childPosition)
                        .getCity());
            }
            // 下载各省的离线地图数据
            else {
                // 下载各省列表中的省份离线地图数据
                if (childPosition == 0) {
                    isStart = offlineMapManager
                            .downloadByProvinceName(provinceList.get(
                                    groupPosition).getProvinceName());
                }
                // 下载各省列表中的城市离线地图数据
                else if (childPosition > 0) {
                    isStart = offlineMapManager.downloadByCityName(cityMap
                            .get(groupPosition).get(childPosition)
                            .getCity());
                }
            }
        } catch (AMapException e) {
            e.printStackTrace();
            Toast.makeText(OfflineMapListActivity.this, R.string.offline_download_fail, Toast.LENGTH_SHORT).show();
        }
        // 保存当前正在正在下载省份或者城市的position位置
        if (isStart) {
            OfflineMapListActivity.this.groupPosition = groupPosition;
            OfflineMapListActivity.this.childPosition = childPosition;
            if (null == progressDialog) {
                progressDialog = CommonUtils.showHorizontalProgressDialog(this, getString(R.string.download_city_ing, city)+"("+getString(R.string.download_donot_quit)+")", getString(R.string.prompt));
                progressDialog.setMax(100);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        OfflineMapListActivity.this.groupPosition = -1;
                        OfflineMapListActivity.this.childPosition = -1;
                        offlineMapManager.stop();
                    }
                });
            }
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.download_city_ing, city)+"("+getString(R.string.download_donot_quit)+")");
            progressDialog.show();

        }

    }

    @Override
    public void onClickListener(final View v, final int groupPosition, final int childPosition) {
        final OfflineMapCity bean = cityMap.get(groupPosition).get(childPosition);
        if (-1 != this.groupPosition && -1 != this.childPosition) {
            Toast.makeText(this, getString(R.string.download_city_ing, cityMap.get(this.groupPosition).get(this.childPosition).getCity()), Toast.LENGTH_SHORT).show();
        }
        if (isMobileMode = !CommonUtils.isWifiMode(OfflineMapListActivity.this) || isMobileDownload) {
            ConfirmDialog dialog = new ConfirmDialog(OfflineMapListActivity.this);
            dialog.setTitle(getString(R.string.prompt));
            dialog.setMessage(getString(R.string.is_mobile_network_download));
            dialog.setListener(new ConfirmDialog.ConfirmListener() {
                @Override
                public void onNegativeListener() {

                }

                @Override
                public void onPositiveListener() {
                    if (isMobileMode) {
                        isMobileDownload = true;
                    }
                    download(v, groupPosition, childPosition, bean.getCity());
                }
            });
            dialog.show();
        } else {
            download(v, groupPosition, childPosition, bean.getCity());
        }
    }


    private void initData() {
        provinceList = offlineMapManager.getOfflineMapProvinceList();
        cityMap = new HashMap<Object, List<OfflineMapCity>>();

        List<OfflineMapProvince> bigCityList = new ArrayList<OfflineMapProvince>();// 以省格式保存直辖市、港澳、全国概要图
        List<OfflineMapCity> cityList = new ArrayList<OfflineMapCity>();// 以市格式保存直辖市、港澳、全国概要图
        List<OfflineMapCity> gangaoList = new ArrayList<OfflineMapCity>();// 保存港澳城市
        List<OfflineMapCity> gaiyaotuList = new ArrayList<OfflineMapCity>();// 保存概要图
        for (int i = 0; i < provinceList.size(); i++) {
            OfflineMapProvince offlineMapProvince = provinceList.get(i);
            List<OfflineMapCity> city = new ArrayList<OfflineMapCity>();
            OfflineMapCity aMapCity = getCity(offlineMapProvince);
            if (offlineMapProvince.getCityList().size() != 1) {
                city.add(aMapCity);
                city.addAll(offlineMapProvince.getCityList());
            } else {
                cityList.add(aMapCity);
                bigCityList.add(offlineMapProvince);
            }
            cityMap.put(i + 3, city);
        }
        OfflineMapProvince title = new OfflineMapProvince();

        title.setProvinceName("全国概要图");
        provinceList.add(0, title);
        title = new OfflineMapProvince();
        title.setProvinceName("直辖市");
        provinceList.add(1, title);
        title = new OfflineMapProvince();
        title.setProvinceName("港澳");
        provinceList.add(2, title);
        provinceList.removeAll(bigCityList);

        for (OfflineMapProvince aMapProvince : bigCityList) {
            if (aMapProvince.getProvinceName().contains("香港")
                    || aMapProvince.getProvinceName().contains("澳门")) {
                gangaoList.add(getCity(aMapProvince));
            } else if (aMapProvince.getProvinceName().contains("全国概要图")) {
                gaiyaotuList.add(getCity(aMapProvince));
            }
        }
        try {
            cityList.remove(4);// 从List集合体中删除香港
            cityList.remove(4);// 从List集合体中删除澳门
            cityList.remove(4);// 从List集合体中删除全国概要图
        } catch (Throwable e) {
            e.printStackTrace();
        }
        cityMap.put(0, gaiyaotuList);// 在HashMap中第0位置添加全国概要图
        cityMap.put(1, cityList);// 在HashMap中第1位置添加直辖市
        cityMap.put(2, gangaoList);// 在HashMap中第2位置添加港澳
    }

    private OfflineMapCity getCity(OfflineMapProvince aMapProvince) {
        OfflineMapCity aMapCity = new OfflineMapCity();
        aMapCity.setCity(aMapProvince.getProvinceName());
        aMapCity.setSize(aMapProvince.getSize());
        aMapCity.setCompleteCode(aMapProvince.getcompleteCode());
        aMapCity.setState(aMapProvince.getState());
        aMapCity.setUrl(aMapProvince.getUrl());
        return aMapCity;
    }

    @Override
    public void onDownload(int status, int completeCode, String downName) {
        switch (status) {
            case OfflineMapStatus.SUCCESS:
                if (null != progressDialog && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                changeOfflineMapTitle(OfflineMapStatus.SUCCESS);
                elv_list.expandGroup(this.groupPosition);
                this.childPosition = -1;
                this.groupPosition = -1;
                break;
            case OfflineMapStatus.LOADING:
                progressDialog.setProgress(completeCode);
                changeOfflineMapTitle(-2);
                break;
            case OfflineMapStatus.UNZIP:
                progressDialog.setMessage(getString(R.string.unzip_city_ing, downName));
                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }
                progressDialog.setProgress(completeCode);
                changeOfflineMapTitle(OfflineMapStatus.UNZIP);
                break;
            case OfflineMapStatus.WAITING:
                break;
            case OfflineMapStatus.PAUSE:
                break;
            case OfflineMapStatus.STOP:
                progressDialog.dismiss();
                break;
            case OfflineMapStatus.ERROR:
                progressDialog.dismiss();
                Toast.makeText(this, R.string.offline_download_fail, Toast.LENGTH_SHORT).show();
                changeOfflineMapTitle(OfflineMapStatus.LOADING);
                break;
            default:
                break;
        }
    }

    private void changeOfflineMapTitle(int status) {
        if (groupPosition == 0 || groupPosition == 1 || groupPosition == 2) {
            cityMap.get(groupPosition).get(childPosition).setState(status);// -2表示正在下载离线地图数据
        } else {
            if (childPosition == 0) {
                for (int i = 0; i < cityMap.get(groupPosition).size(); i++) {
                    cityMap.get(groupPosition).get(i).setState(status);// -2表示正在下载离线地图数据
                }
            } else {
                cityMap.get(groupPosition).get(childPosition).setState(status);// -2表示正在下载离线地图数据
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }
    }
}
