package com.sheep.hub;/*
 */

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.help.Inputtips;
import com.amap.api.services.help.Tip;
import com.sheep.framework.util.Density;
import com.sheep.framework.view.KeyOrHandSetEditText;
import com.sheep.hub.adapter.NearByAdapter;
import com.sheep.hub.adapter.SimpleTextAdapter;
import com.sheep.hub.bean.Interest;
import com.sheep.hub.bean.SearchHistory;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.dialog.DropPopupWindow;
import com.sheep.hub.util.LocationUtil;

import java.util.ArrayList;
import java.util.List;

public class NearbyActivity extends BaseActivity implements TextView.OnEditorActionListener, AdapterView.OnItemClickListener, TextWatcher, View.OnClickListener {

    private TextView tv_area;

    private KeyOrHandSetEditText et_poi;
    private DropPopupWindow dropPopupWindow;
    private Inputtips inputTips;

    private View btn_search;

    private ArrayList<SearchHistory> searchHistoryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby);
        initTitle(R.string.nearby);
        initView();
    }

    private void initView() {
        tv_area = (TextView) findViewById(R.id.tv_area);
        tv_area.setText(getAreaContent(null == HubApp.getInstance().getMyLocation() ? getString(R.string.china) : HubApp.getInstance().getMyLocation().getDistrict()));

        et_poi = (KeyOrHandSetEditText) findViewById(R.id.et_poi);

        searchHistoryList = HubApp.getInstance().getAfeiDb().findAll(SearchHistory.class);

        GridView gv_list = (GridView) findViewById(R.id.gv_list);
        gv_list.setSelector(new ColorDrawable(0));
        NearByAdapter adapter = new NearByAdapter(this);
        adapter.setList(Constant.getNEARBY_INTEREST_LIST());
        gv_list.setAdapter(adapter);

        LocationUtil.requestMyLocation(this, new LocationUtil.GetLocationSuccessListener() {
            @Override
            public void onSuccess(AMapLocation aMapLocation) {
                tv_area.setText(getAreaContent(HubApp.getInstance().getMyLocation().getDistrict()));
            }
        });

        et_poi.addTextChangedListener(this);
        gv_list.setOnItemClickListener(this);
        btn_search = findViewById(R.id.btn_search);
        btn_search.setOnClickListener(this);
    }

    private SpannableString getAreaContent(String area) {
        SpannableString sp = new SpannableString(getString(R.string.search_with_area, area));
        sp.setSpan(
                new ForegroundColorSpan(getResources().getColor(
                        R.color.title_bg)), 1, area.length() + 1,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
//        sp.setSpan(
//                new ForegroundColorSpan(getResources().getColor(
//                        android.R.color.black)), 0, 1,
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        sp.setSpan(
//                new ForegroundColorSpan(getResources().getColor(
//                        android.R.color.black)), area.length() + 1, area.length(),
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sp.setSpan(new RelativeSizeSpan(0.9f), 1, area.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sp;
    }

    @Override
    public void onClick(View v) {
        String key = et_poi.getText().toString().trim();
        if (TextUtils.isEmpty(key)) {
            Toast.makeText(this, R.string.please_input_key_search, Toast.LENGTH_SHORT).show();
            return;
        }
        SearchHistory history = new SearchHistory();
        history.set_id("" + System.currentTimeMillis());
        history.setKey(key.trim());
        if (null == searchHistoryList || !searchHistoryList.contains(history)) {
            HubApp.getInstance().getAfeiDb().save(history);
        }
        Intent intent = new Intent(this, SearchResultActivity.class);
        intent.putExtra(SearchResultActivity.POI_KEY, key);
        intent.putExtra(SearchResultActivity.IS_NEARBY,true);
        launch(intent);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            btn_search.performClick();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        et_poi.setHandText(getString(((Interest) parent.getAdapter().getItem(position)).getNameId()));
        et_poi.setSelection(et_poi.getText().length());
        btn_search.performClick();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (null == dropPopupWindow) {
            dropPopupWindow = new DropPopupWindow(this, et_poi.getWidth() + 30, Density.of(this, 300));
            dropPopupWindow.setAdapter(new SimpleTextAdapter<Tip>(this, new SimpleTextAdapter.OnItemClickListener() {

                @Override
                public void OnItemClick(int position) {
                    et_poi.setHandText(((SimpleTextAdapter<Tip>) dropPopupWindow.getAdapter()).getItem(position).getName());
                    dropPopupWindow.dismiss();
                }
            }) {
                @Override
                public void setTextView(TextView textView, Tip tip) {
                    textView.setText(tip.getName());
                }
            });
        }
        if (!TextUtils.isEmpty(s.toString().trim()) && !et_poi.isHandSet()) {
            searchPoiTips();
        } else if (dropPopupWindow.isShowing()) {
            dropPopupWindow.dismiss();
        }
    }

    private void searchPoiTips() {
        if (null == inputTips) {
            inputTips = new Inputtips(this,
                    new Inputtips.InputtipsListener() {

                        @Override
                        public void onGetInputtips(List<Tip> tipList, int rCode) {
                            if (rCode == 0) {
                                ArrayList<Tip> tempList = new ArrayList<Tip>();
                                tempList.addAll(tipList);
                                dropPopupWindow.setList(tempList);
                                if (!dropPopupWindow.isShowing()) {
                                    dropPopupWindow.show(et_poi, -20, 0);
                                }
                            }
                        }
                    });
        }
        try {
            inputTips.requestInputtips(et_poi.getText().toString().trim(), null == HubApp.getInstance().getMyLocation() ? "" : HubApp.getInstance().getMyLocation().getCityCode());
        } catch (AMapException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

