package com.sheep.hub;/*
 */

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.LatLng;
import com.amap.api.navi.model.NaviLatLng;

public class BaseActivity extends Activity {
    protected void initTitle() {
        initTitle(-1);
    }

    protected void initTitle(int title) { 
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        if (null != tv_title && 0 < title) {
            setTitle(title);
            tv_title.setText(getTitle());
        }
        ImageButton ib_back = (ImageButton) findViewById(R.id.ib_back);
        if (null != ib_back) {
            ib_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    protected void initTitle(String title) {
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        if (null != tv_title) {
            setTitle(title);
            tv_title.setText(getTitle());
        }
        ImageButton ib_back = (ImageButton) findViewById(R.id.ib_back);
        if (null != ib_back) {
            ib_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    protected final void launch(Class<? extends Activity> activity) {
        launch(new Intent(BaseActivity.this, activity));
    }

    protected final void launch(Intent intent) {
        startActivity(intent);
    }

    protected final void launch(Class<? extends Activity> activity, int requestCode) {
        launch(new Intent(BaseActivity.this, activity), requestCode);
    }

    protected final void launch(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

}
