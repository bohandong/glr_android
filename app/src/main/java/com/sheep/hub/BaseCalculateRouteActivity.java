package com.sheep.hub;/*
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.AMapNaviListener;
import com.amap.api.navi.model.AMapNaviInfo;
import com.amap.api.navi.model.AMapNaviLocation;
import com.amap.api.navi.model.NaviLatLng;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.sheep.framework.db.utils.DateTimeUtil;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.bean.RouteHistory;
import com.sheep.hub.util.LocationUtil;

import java.util.ArrayList;

public class BaseCalculateRouteActivity extends BaseActivity {

    protected static final  int IS_BEGIN_NAVI_REQ=99;
    private PoiItem saveStartPoitem;
    private PoiItem saveDestinationPoitem;
    private ProgressDialog progressDialog;
    private AMapNaviListener aMapNaviListener;
    private ArrayList<NaviLatLng> startList;
    private ArrayList<NaviLatLng> destinationList;
    private ArrayList<NaviLatLng> centerList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected final void startRouteSearch(final PoiItem destination) {
        final Handler handler = new Handler();
        if (null == HubApp.getInstance().getMyLocation()) {
            progressDialog = CommonUtils.showProgressDialog(this, R.string.location_ing);
            Runnable locationRunnable = new Runnable() {
                @Override
                public void run() {
                    LocationUtil.requestMyLocation(BaseCalculateRouteActivity.this, new LocationUtil.GetLocationSuccessListener() {

                        @Override
                        public void onSuccess(AMapLocation aMapLocation) {
                            progressDialog.setMessage(getString(R.string.search_ing));
                            routeSearch(destination);
                        }
                    });
                }
            };
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    handler.removeCallbacks(this);
                    progressDialog.dismiss();
                }
            }, 8000);
            handler.post(locationRunnable);
        } else {
//            progressDialog = CommonUtils.showProgressDialog(this, R.string.search_ing);
            routeSearch(destination);
        }
    }

    protected final void startRouteSearch(final PoiItem start, final PoiItem destination) {
        progressDialog = CommonUtils.showProgressDialog(this, R.string.search_ing);
        startList = new ArrayList<NaviLatLng>();
        startList.add(new NaviLatLng(start.getLatLonPoint().getLatitude(),
                start.getLatLonPoint().getLongitude()));
       destinationList = new ArrayList<NaviLatLng>();
        destinationList.add(new NaviLatLng(destination.getLatLonPoint().getLatitude(),
                destination.getLatLonPoint().getLongitude()));

        saveStartPoitem = start;
        saveDestinationPoitem = destination;
        if (AMapNavi.getInstance(this).calculateDriveRoute(startList, destinationList, null, AMapNavi.DrivingShortDistance)) {
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, R.string.navigation_fail, Toast.LENGTH_SHORT).show();
        }
    }

    protected final void startRouteSearch(ArrayList<NaviLatLng> starts, ArrayList<NaviLatLng> centers, ArrayList<NaviLatLng> destinations) {
        progressDialog = CommonUtils.showProgressDialog(this, R.string.search_ing);
        startList = starts;
        centerList = centers;
        destinationList = destinations;
        if (AMapNavi.getInstance(this).calculateDriveRoute(startList, destinationList, centerList, AMapNavi.DrivingShortDistance)) {
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, R.string.navigation_fail, Toast.LENGTH_SHORT).show();
        }
    }

    private void routeSearch(PoiItem destination) {
        progressDialog = CommonUtils.showProgressDialog(this, R.string.search_ing);
        startList = new ArrayList<NaviLatLng>();
        startList.add(new NaviLatLng(HubApp.getInstance().getMyLocation().getLatitude(),
                HubApp.getInstance().getMyLocation().getLongitude()));
        destinationList = new ArrayList<NaviLatLng>();
        saveStartPoitem = new PoiItem("" + System.currentTimeMillis(),
                new LatLonPoint(HubApp.getInstance().getMyLocation().getLatitude(),
                        HubApp.getInstance().getMyLocation().getLongitude()), LocationUtil.getSimpleAddress(HubApp.getInstance().getMyLocation().getAddress()), HubApp.getInstance().getMyLocation().getAddress());
        saveDestinationPoitem = destination;
        destinationList.add(new NaviLatLng(destination.getLatLonPoint().getLatitude(),
                destination.getLatLonPoint().getLongitude()));
        if (AMapNavi.getInstance(this).calculateDriveRoute(startList, destinationList, null, AMapNavi.DrivingShortDistance)) {
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, R.string.navigation_fail, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AMapNavi.getInstance(this).setAMapNaviListener(getAMapNaviListener());
    }

    @Override
    protected void onPause() {
        super.onPause();
        AMapNavi.getInstance(this)
                .removeAMapNaviListener(getAMapNaviListener());
    }

    private AMapNaviListener getAMapNaviListener() {
        if (aMapNaviListener == null) {

            aMapNaviListener = new AMapNaviListener() {

                @Override
                public void onTrafficStatusUpdate() {
                }

                @Override
                public void onStartNavi(int arg0) {
                }

                @Override
                public void onReCalculateRouteForYaw() {
                }

                @Override
                public void onReCalculateRouteForTrafficJam() {
                }

                @Override
                public void onLocationChange(AMapNaviLocation location) {
                }

                @Override
                public void onInitNaviSuccess() {
                }

                @Override
                public void onInitNaviFailure() {
                }

                @Override
                public void onGetNavigationText(int arg0, String arg1) {
                }

                @Override
                public void onEndEmulatorNavi() {
                }

                @Override
                public void onCalculateRouteSuccess() {
                    if (null != progressDialog && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Intent intent = new Intent(BaseCalculateRouteActivity.this, NaviRouteActivity.class);
                    intent.putExtra(NaviRouteActivity.START_POITEM_LIST, startList);
                    intent.putExtra(NaviRouteActivity.CENTER_POITEM_LIST, centerList);
                    intent.putExtra(NaviRouteActivity.END_POITEM_LIST, destinationList);
                    launch(intent, IS_BEGIN_NAVI_REQ);
                    saveRoute();
                    setResult(RESULT_OK);
                }

                @Override
                public void onCalculateRouteFailure(int arg0) {
                    if (null != progressDialog && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Toast.makeText(BaseCalculateRouteActivity.this, R.string.route_calculate_fail, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onArrivedWayPoint(int arg0) {

                }

                @Override
                public void onArriveDestination() {

                }

                @Override
                public void onGpsOpenStatus(boolean arg0) {
                }

                @Override
                public void onNaviInfoUpdated(AMapNaviInfo arg0) {
                }
            };
        }
        return aMapNaviListener;
    }

    private void saveRoute() {
        RouteHistory history = new RouteHistory();
        history.setDate(DateTimeUtil.geChineseDate(System.currentTimeMillis()));
        history.setStartPoitem(saveStartPoitem);
        history.setDestinationPoitem(saveDestinationPoitem);
        if (!HubApp.getInstance().getAfeiDb().findAll(RouteHistory.class).contains(history)) {
            HubApp.getInstance().getAfeiDb().save(history);
        }
    }
}
