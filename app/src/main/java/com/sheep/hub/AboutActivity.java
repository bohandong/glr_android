package com.sheep.hub;/*
 */

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.widget.TextView;

import com.sheep.framework.util.CommonUtils;

import java.util.Date;

public class AboutActivity extends BaseActivity {
     
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        initTitle(getString(R.string.about));
        TextView tv_version = (TextView) findViewById(R.id.tv_version);
        tv_version.setText(getString(R.string.version_information, getVersion()));
        
        ColorStateList redColors = ColorStateList.valueOf(0xffff0000);
        SpannableStringBuilder spanBuilder = new SpannableStringBuilder("这是一个测试");
        //style 为0 即是正常的，有Typeface.BOLD Typeface.ITALIC等
        //size  为0 即采用原始的正常的 size大小 
        spanBuilder.setSpan(new TextAppearanceSpan(null, 0, 60, redColors, null), 0, 3, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        
     /*   TextView tv_test = (TextView) findViewById(R.id.tv_test);
        tv_test.setText(spanBuilder);*/
        //Spanned spanBuilder = Html.fromHtml("<font size=80 color=#ff0000>这是一</font>测试");
    }

    private String getVersion() {
        String version = "";
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = "V " + info.versionName ;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }
}
