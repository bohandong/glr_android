package com.sheep.hub;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.activity.IndexActivity;
import com.sheep.hub.adapter.MyPagerAdapter;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.Preference;
import com.sheep.hub.view.CirclePageIndicator;

public class LogoActivity extends BaseActivity {

    private ViewPager vp_guide;
    private ArrayList<View> views;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        if (Preference.getBoolean(Constant.IS_LOGO_FIRST, true)) {
            initViewPager();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //launch(MainActivity.class);
                    /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date dataLineTime = null;
                    try {
                        dataLineTime = sdf.parse("2015-05-25");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(new Date().after(dataLineTime)){
                        CommonUtils.check();
                        int temp = 0;
                        int s = 6/temp;
                        //CommonUtils.showShortToast(LogoActivity.this,"应用程序已过期");
                    }else{
                        launch(IndexActivity.class);
                        finish();
                    }*/

                		 launch(IndexActivity.class);
                         finish();


                }
            }, 1000);
        }

    }

    private void initViewPager() {
        vp_guide = (ViewPager) findViewById(R.id.vp_guide);
        vp_guide.setVisibility(View.VISIBLE);
        views = new ArrayList<View>();
        int[] drawableIds = new int[]{R.drawable.img_guide01, R.drawable.img_guide02, R.drawable.img_guide03, R.drawable.img_guide04};
        for (int i = 0; i < 4; i++) {
            View view = View.inflate(this, R.layout.view_logo_guide, null);
            ImageView iv_guide = (ImageView) view.findViewById(R.id.iv_guide);
            iv_guide.setImageResource(drawableIds[i]);
            if (3 == i) {
                Button btn_ok = (Button) view.findViewById(R.id.btn_ok);
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //launch(MainActivity.class);
                        Preference.putBoolean(Constant.IS_LOGO_FIRST, false);
                    	launch(IndexActivity.class);
                        finish();
                    }
                });
            }
            views.add(view);
        }
        vp_guide.setAdapter(new MyPagerAdapter(views));
       /* CirclePageIndicator mIndicator = (CirclePageIndicator) findViewById(R.id.cpi_indicator);
        mIndicator.setViewPager(vp_guide);*/

    }

}
