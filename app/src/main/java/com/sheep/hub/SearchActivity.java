package com.sheep.hub;/*
 */

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.services.core.AMapException;
import com.amap.api.services.help.Inputtips;
import com.amap.api.services.help.Tip;
import com.sheep.framework.util.Density;
import com.sheep.framework.view.KeyOrHandSetEditText;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshListView;
import com.sheep.hub.adapter.SearchHistoryAdapter;
import com.sheep.hub.adapter.SimpleTextAdapter;
import com.sheep.hub.bean.SearchHistory;
import com.sheep.hub.dialog.ConfirmDialog;
import com.sheep.hub.dialog.DropPopupWindow;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends BaseActivity implements TextWatcher, View.OnClickListener, AdapterView.OnItemClickListener, TextView.OnEditorActionListener {
    private KeyOrHandSetEditText et_poi;
    private PullToRefreshListView lv_list;

    private DropPopupWindow dropPopupWindow;
    private Inputtips inputTips;
    private SearchHistoryAdapter adapter;

    private Button btn_search;

    private ArrayList<SearchHistory> searchHistoryList;
    private TextView footerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initTitle(R.string.search);
        initView();
    }

    private void initView() {

        if (null != HubApp.getInstance().getMyLocation()) {
            Button btn_next = (Button) findViewById(R.id.btn_next);
            btn_next.setText(HubApp.getInstance().getMyLocation().getCity());
            btn_next.setVisibility(View.VISIBLE);
        }

        et_poi = (KeyOrHandSetEditText) findViewById(R.id.et_poi);

        lv_list = (PullToRefreshListView) findViewById(android.R.id.list);
        adapter = new SearchHistoryAdapter(this);
        lv_list.setMode(PullToRefreshBase.Mode.DISABLED);
        lv_list.setAdapter(adapter);
        lv_list.setEmptyView(LayoutInflater.from(this).inflate(R.layout.view_empy, lv_list.getRefreshableView(), false));
        lv_list.getRefreshableView().setSelector(new ColorDrawable(0));

        footerView = (TextView) LayoutInflater.from(this).inflate(R.layout.footer_clear_history, lv_list.getRefreshableView(), false);
        footerView.setText(R.string.clear_search_history);
        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              ConfirmDialog dialog=  new ConfirmDialog(SearchActivity.this);
                dialog.setTitle(R.string.prompt);
                dialog.setMessage(getString(R.string.clear_all_search_history));
                dialog.setListener(new ConfirmDialog.ConfirmListener() {
                    @Override
                    public void onNegativeListener() {

                    }

                    @Override
                    public void onPositiveListener() {
                        HubApp.getInstance().getAfeiDb().deleteByWhereStr(SearchHistory.class, "");
                        adapter.getList().clear();
                        adapter.notifyDataSetChanged();
                    }
                });
                dialog.show();
            }
        });
        lv_list.getRefreshableView().addFooterView(footerView);
        btn_search = (Button) findViewById(R.id.btn_search);
        btn_search.setOnClickListener(this);
        et_poi.addTextChangedListener(this);
        lv_list.setOnItemClickListener(this);
        et_poi.setOnEditorActionListener(this);
    }


    private void searchPoiTips() {
        if (null == inputTips) {
            inputTips = new Inputtips(SearchActivity.this,
                    new Inputtips.InputtipsListener() {

                        @Override
                        public void onGetInputtips(List<Tip> tipList, int rCode) {
                            if (rCode == 0) {// 正确返回
                                ArrayList<Tip> tempList = new ArrayList<Tip>();
                                tempList.addAll(tipList);
                                dropPopupWindow.setList(tempList);
                                if (!dropPopupWindow.isShowing()) {
                                    dropPopupWindow.show(et_poi, -20, 0);
                                }
                            }
                        }
                    });
        }
        try {
            inputTips.requestInputtips(et_poi.getText().toString().trim(), null == HubApp.getInstance().getMyLocation() ? "" : HubApp.getInstance().getMyLocation().getCityCode());// 第一个参数表示提示关键字，第二个参数默认代表全国，也可以为城市区号
        } catch (AMapException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        searchHistoryList = HubApp.getInstance().getAfeiDb().findAll(SearchHistory.class);
        adapter.setList(searchHistoryList);

        if (searchHistoryList.size() > 0&&lv_list.getRefreshableView().getFooterViewsCount()<=0) {
            lv_list.getRefreshableView().addFooterView(footerView);
        }
    }

    @Override
    public void onClick(View v) {
        String key = et_poi.getText().toString().trim();
        if (TextUtils.isEmpty(key)) {
            Toast.makeText(SearchActivity.this, R.string.please_input_key_search, Toast.LENGTH_SHORT).show();
            return;
        }
        SearchHistory history = new SearchHistory();
        history.set_id("" + System.currentTimeMillis());
        history.setKey(key);
        if (!adapter.getList().contains(history)) {
            HubApp.getInstance().getAfeiDb().save(history);
        }
        Intent intent = new Intent(this, SearchResultActivity.class);
        intent.putExtra(SearchResultActivity.POI_KEY, key);
        launch(intent);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            btn_search.performClick();
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        et_poi.setHandText(adapter.getItem(position - 1).getKey());
        et_poi.setSelection(et_poi.getText().toString().length());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (null == dropPopupWindow) {
            dropPopupWindow = new DropPopupWindow(SearchActivity.this, et_poi.getWidth() + 30, Density.of(SearchActivity.this, 300));
            dropPopupWindow.setAdapter(new SimpleTextAdapter<Tip>(SearchActivity.this, new SimpleTextAdapter.OnItemClickListener() {

                @Override
                public void OnItemClick(int position) {
                    et_poi.setHandText(((SimpleTextAdapter<Tip>) dropPopupWindow.getAdapter()).getItem(position).getName());
                    dropPopupWindow.dismiss();
                }
            }) {
                @Override
                public void setTextView(TextView textView, Tip tip) {
                    textView.setText(tip.getName());
                }
            });
        }
        if (!TextUtils.isEmpty(s.toString().trim()) && !et_poi.isHandSet()) {
            searchPoiTips();
        } else if (dropPopupWindow.isShowing()) {
            dropPopupWindow.dismiss();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
