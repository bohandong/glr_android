/*
 */
package com.sheep.hub.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.sheep.hub.R;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 * 
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ObdSetDialog extends Dialog implements android.view.View.OnClickListener {

    private EditText et_bluetoothpwd;

    private BluetoothPwdSureListener listener;
    
    /**
     * @param context
     * @param theme
     */
    public ObdSetDialog(Context context, int theme) {
        super(context, theme);
    }

    public ObdSetDialog(Context context) {
        super(context);
    }

    /*
     * (non-Javadoc)
     * @see android.app.Dialog#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_obd_bluetoothsetting);
        
        et_bluetoothpwd = (EditText) findViewById(R.id.et_bluetoothpwd);
        
        findViewById(R.id.btn_ok).setOnClickListener(this);
    }
    
    /* (non-Javadoc)
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_ok){
            dismiss();
            if(null != listener){
                String pwd = et_bluetoothpwd.getEditableText().toString();
                listener.onBluetoothPwdSure(pwd);
            }
        }
    }
    
    
    public void setOnBluetoothPwdSureListener(BluetoothPwdSureListener listener) {
        this.listener = listener;
    }

    public interface BluetoothPwdSureListener{
        public void onBluetoothPwdSure(String pwd);
    }
    
    
}
