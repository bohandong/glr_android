/*
 */
package com.sheep.hub.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.sheep.framework.util.Density;
import com.sheep.hub.R;

import org.w3c.dom.Text;

public class NaviTipDialog extends Dialog implements View.OnClickListener {

    private OnButtonListener listener;

    private TextView tv_time;
    private TextView tv_traffic;
    private TextView tv_length;
    private TextView tv_cost;

    private String routeLength;
    private String routeCostTime;
    private String routeTrafficDesc;
    private String routeCostMoney;

    public NaviTipDialog(Context context, int theme) {
        super(context, theme);
    }

    public NaviTipDialog(Context context) {
        super(context, R.style.fluctuate_dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_navi_tip);

        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_traffic = (TextView) findViewById(R.id.tv_traffic);
        tv_length = (TextView) findViewById(R.id.tv_length);
        tv_cost = (TextView) findViewById(R.id.tv_cost);

        if (!TextUtils.isEmpty(routeCostTime)) {
            tv_time.setText(getContext().getString(R.string.route_length_desc, routeLength));
        }
        if (!TextUtils.isEmpty(routeLength)) {
            tv_length.setText(routeLength + "km");
        }
        if (!TextUtils.isEmpty(routeTrafficDesc)) {
            tv_traffic.setText(getContext().getString(R.string.route_traffic_red_green_desc, routeTrafficDesc));
        }
        if (!TextUtils.isEmpty(routeCostMoney)) {
            tv_cost.setText(getContext().getString(R.string.route_cost_desc, routeLength));
        }

        findViewById(R.id.ll_ok).setOnClickListener(this);
        findViewById(R.id.ll_cancel).setOnClickListener(this);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void onClick(View v) {
        dismiss();
        if (v.getId() == R.id.ll_ok) {
            if (null != listener) {
                listener.onNavigationistener();
            }
        } else if (v.getId() == R.id.ll_cancel) {
            if (null != listener) {
                listener.onCancelListener();
            }
        }
    }


    public void setListener(OnButtonListener listener) {
        this.listener = listener;
    }

    public interface OnButtonListener {
        public void onNavigationistener();

        public void onCancelListener();
    }


    public void setRouteLength(String routeLength) {
        this.routeLength = routeLength;
        if (null != tv_length) {
            tv_length.setText(routeLength + "km");
        }
    }

    public void setRouteCostTime(String routeCostTime) {
        this.routeCostTime = routeCostTime;
        if (null != tv_time) {
            tv_time.setText(getContext().getString(R.string.route_length_desc, routeLength));
        }
    }

    public void setRouteTrafficDesc(String routeTrafficDesc) {
        this.routeTrafficDesc = routeTrafficDesc;
        if (null != tv_traffic) {
            tv_traffic.setText(getContext().getString(R.string.route_traffic_red_green_desc, routeTrafficDesc));
        }
    }

    public void setRouteCostMoney(String routeCostMoney) {
        this.routeCostMoney = routeCostMoney;
        if (null != tv_cost) {
            tv_cost.setText(getContext().getString(R.string.route_cost_desc, routeLength));
        }
    }

    @Override
    public void show() {
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = (int) (Density.getSceenWidth(getContext()) - Density.of(getContext(), 48));
        window.setGravity(Gravity.BOTTOM);
        window.setAttributes(lp);
        super.show();
    }
}
