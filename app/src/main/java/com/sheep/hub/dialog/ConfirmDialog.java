/*
 */
package com.sheep.hub.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.sheep.hub.R;

import org.w3c.dom.Text;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ConfirmDialog extends Dialog implements View.OnClickListener {

    private ConfirmListener listener;

    private TextView tv_title;
    private TextView tv_message;
    private CharSequence mTitle;
    private CharSequence mMessage;

    /**
     * @param context
     * @param theme
     */
    public ConfirmDialog(Context context, int theme) {
        super(context, theme);
    }

    public ConfirmDialog(Context context) {
        super(context,R.style.fluctuate_dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_confirm);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_message = (TextView) findViewById(R.id.tv_message);

        if (!TextUtils.isEmpty(mTitle)) {
            tv_title.setText(mTitle);
        }
        if (!TextUtils.isEmpty(mMessage)) {
            tv_message.setText(mMessage);
        }

        findViewById(R.id.btn_ok).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void setTitle(CharSequence title) {
        this.mTitle = title;
        if (null != tv_title) {
            tv_title.setText(mTitle);
        }
    }

    public void setMessage(CharSequence mMessage) {
        this.mMessage = mMessage;
        if (null != tv_message) {
            tv_message.setText(mMessage);
        }
    }

    @Override
    public void onClick(View v) {
        dismiss();
        if (v.getId() == R.id.btn_ok) {
            if (null != listener) {
                listener.onPositiveListener();
            }
        } else if (v.getId() == R.id.btn_cancel) {
            if (null != listener) {
                listener.onNegativeListener();
            }
        }
    }


    public void setListener(ConfirmListener listener) {
        this.listener = listener;
    }

    public interface ConfirmListener {
        public void onNegativeListener();

        public void onPositiveListener();
    }

}
