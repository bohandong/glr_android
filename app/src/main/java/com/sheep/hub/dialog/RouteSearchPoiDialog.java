package com.sheep.hub.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshListView;
import com.sheep.hub.R;
import com.sheep.hub.adapter.RouteSearchAdapter;

import java.util.ArrayList;

public class RouteSearchPoiDialog extends Dialog implements
        OnItemClickListener, OnItemSelectedListener {

    private RouteSearchAdapter adapter;
    protected OnListItemClick mOnClickListener;
    private PullToRefreshBase.OnRefreshListener2 onRefreshListener;
    private PullToRefreshListView listView;

    private TextView tv_title;
    private CharSequence mTitle;

    public RouteSearchPoiDialog(Context context, int theme) {
        super(context, theme);
    }

    public RouteSearchPoiDialog(Context context) {
        this(context, R.style.fluctuate_dialog);
        adapter = new RouteSearchAdapter((Activity) context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_list);
        listView = (PullToRefreshListView) findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        listView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                dismiss();
                mOnClickListener.onListItemClick(RouteSearchPoiDialog.this,
                        adapter.getItem(position));
            }
        });
        listView.setOnRefreshListener(onRefreshListener);

        tv_title = (TextView) findViewById(R.id.tv_title);
        if (!TextUtils.isEmpty(mTitle)) {
            setTitle(mTitle);
        }

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                listView.onRefreshComplete();
            }
        });
    }

    public void addList(ArrayList<PoiItem> poiItems) {
        if (null == adapter.getList()) {
            adapter.setList(poiItems);
        } else {
            adapter.getList().addAll(poiItems);
        }
    }

    public void setMode(PullToRefreshBase.Mode mode) {
        listView.setMode(mode);
    }

    public void setList(ArrayList<PoiItem> poiItems) {
        adapter.setList(poiItems);
    }

    @Override
    public void onItemClick(AdapterView<?> view, View view1, int arg2, long arg3) {
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                               long arg3) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    public void setOnRefreshListener(PullToRefreshBase.OnRefreshListener2 onRefreshListener) {
        this.onRefreshListener = onRefreshListener;
    }

    public interface OnListItemClick {
        public void onListItemClick(RouteSearchPoiDialog dialog, PoiItem item);
    }

    public void setOnListClickListener(OnListItemClick l) {
        mOnClickListener = l;
    }

    public void onRefreshComplete() {
        listView.onRefreshComplete();
    }

    public void setPullMode(PullToRefreshBase.Mode mode) {
        listView.setMode(mode);
    }

    @Override
    public void setTitle(CharSequence title) {
        this.mTitle = title;
        if (null != tv_title) {
            tv_title.setText(mTitle);
        }
    }

    @Override
    public void setTitle(int titleId) {
        setTitle(getContext().getString(titleId));
    }
}
