package com.sheep.hub.dialog;/*
 */

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.sheep.framework.ui.ArrayListAdapter;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshListView;
import com.sheep.hub.R;

import java.util.ArrayList;

public class DropPopupWindow {
    private Context mContext;
    private PopupWindow popupWindow;
    private PullToRefreshListView plv_list;
    private ArrayListAdapter adapter;
    private boolean isClearList = true;

    public DropPopupWindow(Context context, int width, int height) {
        this.mContext = context;
        initPop(width, height);
    }

    public DropPopupWindow(Context context, int width, int height, boolean isClearList) {
        this.mContext = context;
        this.isClearList = isClearList;
        initPop(width, height);
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private void initPop(int width, int height) {
        plv_list = (PullToRefreshListView) View.inflate(mContext, R.layout.common_listview, null);
        plv_list.setMode(PullToRefreshBase.Mode.DISABLED);
        plv_list.getRefreshableView().setSelector(new ColorDrawable(0));
        plv_list.getRefreshableView().setCacheColorHint(Color.TRANSPARENT);
        plv_list.getRefreshableView().setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        plv_list.setFocusable(true);
        plv_list.setFocusableInTouchMode(true);
        popupWindow = new PopupWindow(plv_list, width, height, false);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.shape_drop_down_bg));
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);


        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if (null != adapter && isClearList) {
                    adapter.getList().clear();
                }
            }
        });
    }

    public void setPullMode(PullToRefreshBase.Mode mode) {
        plv_list.setMode(mode);
    }

    public void onRefreshComplete() {
        plv_list.onRefreshComplete();
    }

    public void setPullMode(PullToRefreshBase.Mode mode, PullToRefreshBase.OnRefreshListener2<ListView> onRefreshListener) {
        plv_list.setMode(mode);
        plv_list.setOnRefreshListener(onRefreshListener);
    }

    public void setOnItemClickListener(final AdapterView.OnItemClickListener onItemClickListener) {
        popupWindow.setFocusable(true);
        plv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onItemClickListener.onItemClick(parent, view, position, id);
                dismiss();
            }
        });
    }

    public void show(View anchor, int xOff, int yOff) {
        popupWindow.showAsDropDown(anchor, xOff, yOff);
    }

    public void dismiss() {
        popupWindow.dismiss();
    }

    public PopupWindow getPopupWindow() {
        return popupWindow;
    }


    public PullToRefreshListView getListView() {
        return plv_list;
    }

    public void setAdapter(ArrayListAdapter adapter) {
        this.adapter = adapter;
        plv_list.setAdapter(adapter);
    }

    public ArrayListAdapter getAdapter() {
        return adapter;
    }

    public void setList(ArrayList list) {
        if (null == list && popupWindow.isShowing()) {
            popupWindow.dismiss();
            return;
        }
        if (null != adapter) {
            adapter.setList(list);
        }
    }

    public void addList(ArrayList list) {
        if (null == list && popupWindow.isShowing()) {
            popupWindow.dismiss();
            return;
        }
        if (null != adapter) {
            if (null == adapter.getList()) {
                adapter.setList(list);
            } else {
                adapter.getList().addAll(list);
                adapter.notifyDataSetChanged();
            }
        }

    }

    public boolean isShowing() {
        return popupWindow.isShowing();
    }

}
