package com.sheep.hub.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.sheep.framework.util.Density;
import com.sheep.hub.R;

public class BrightDialog extends Dialog {
	
	private SeekBar sb_screenbright;
	private OnBrightChangeListener listener;

    private int initBright;

	public BrightDialog(Context context, int theme) {
		super(context, theme);
	}
	
	public BrightDialog(Context context) {
		super(context,R.style.fluctuate_dialog);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.dialog_bright);
		
		sb_screenbright = (SeekBar) findViewById(R.id.sb_screenbright);

        if (0!=initBright){
            sb_screenbright.setProgress(initBright);
        }
		
		sb_screenbright.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				if(null != listener){
					listener.onBrightChange(seekBar.getProgress()); //获取当前的亮度
				}
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if(fromUser){
                    if(progress<3){
                        seekBar.setProgress(2);
                    }else  if(progress>97){
                        seekBar.setProgress(98);
                    }
                }
			}
		});
		
	}

    public void setBright(int bright){
        this.initBright = bright;
    }
	
	
	/**
	 * 修改dialog的宽度大小
	 */
	@Override
	public void show() {
		Window window = getWindow();
		LayoutParams lp = window.getAttributes();
		lp.width = Density.getSceenWidth(getContext()) - Density.of(getContext(), 32);
		window.setAttributes(lp);
		
		super.show();
	}

	public void setOnBrightChangeListener(OnBrightChangeListener listener){
		this.listener = listener; 
	}
	
	public interface OnBrightChangeListener{
		public void onBrightChange(int bright);
	}

}
