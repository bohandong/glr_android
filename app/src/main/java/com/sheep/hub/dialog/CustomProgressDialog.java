package com.sheep.hub.dialog;/*
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sheep.hub.R;

public class CustomProgressDialog extends ProgressDialog {

    private ProgressBar pb_bar;
    private int mProgressStyle = STYLE_SPINNER;

    private TextView tv_message;
    private CharSequence mMessage;

    private TextView tv_percent;
    private TextView tv_num;
    private TextView tv_title;

    private int mMax;
    private int mProgressVal;
    private boolean mHasStarted;
    private CharSequence mTitle;

    public CustomProgressDialog(Context context) {
        super(context,R.style.fluctuate_dialog);
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (STYLE_HORIZONTAL == mProgressStyle) {
            setContentView(R.layout.dialog_horizontal_loading);
        } else {
            setContentView(R.layout.dialog_loading);
        }
        pb_bar = (ProgressBar) findViewById(R.id.pb_bar);
        tv_percent = (TextView) findViewById(R.id.tv_percent);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_title = (TextView) findViewById(R.id.tv_title);

        tv_message = (TextView) findViewById(R.id.tv_message);
        if (mMessage != null) {
            setMessage(mMessage);
        }
        if (0 > mMax) {
            setMax(mMax);
        }
        if (!TextUtils.isEmpty(mTitle)) {
            setTitle(mTitle);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mHasStarted = true;
    }
    @Override
    protected void onStop() {
        super.onStop();
        mHasStarted = false;
    }

    @Override
    public void setMessage(CharSequence message) {
        mMessage = message;
        if (tv_message != null) {
            tv_message.setText(message);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (STYLE_HORIZONTAL == mProgressStyle) {
            this.mTitle = title;
            if (null != pb_bar) {
                tv_title.setText(title);
            }
        }
    }

    @Override
    public void setMax(int max) {
        mMax = max;
        if (pb_bar != null) {
            pb_bar.setMax(max);
            tv_num.setText("" + max);
        }
    }

    @Override
    public void setProgress(int value) {
        mProgressVal = value;
        if (mHasStarted) {
            pb_bar.setProgress(value);
            tv_percent.setText("" + value + "/" + mMax);
        }
    }

    @Override
    public void setProgressStyle(int style) {
        mProgressStyle = style;
    }

    public int getProgress() {
        if (pb_bar != null) {
            return pb_bar.getProgress();
        }
        return mProgressVal;
    }
}
