/*
 */
package com.sheep.hub.bean;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class DriveScore extends BaseBean {

	private String _id; 
	private String sequence; //哪一次命令的数据
	private String time;  //数据获取的时间
	
    private String startTime;
    private String endTime;
    private String driveScore; //驾驶得分
    private String emerAddSpeedTime; //急加速次数
    private String emerSubSpeedTime; //急减速次数
    private String emptyRunningTime; //引擎空转时间
    private String overSpeedTime; //超速时间
    private String avgCostOil; //平均油耗

    //add by lvmu 20150731 start
    private String drivedistance; //行驶里程
    private String costoil; //本次油耗
    public String getDrivedistance() {
        return drivedistance;
    }
    public void setDrivedistance(String drivedistance) {
        this.drivedistance = drivedistance;
    }
    public String getCostoil() {

        return costoil;
    }
    public void setCostoil(String costoil) {

        this.costoil = costoil;
    }

    //add by lvmu 20150731 end
    
    public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    public String getDriveScore() {
        return driveScore;
    }
    public void setDriveScore(String driveScore) {
        this.driveScore = driveScore;
    }
    public String getEmerAddSpeedTime() {
        return emerAddSpeedTime;
    }
    public void setEmerAddSpeedTime(String emerAddSpeedTime) {
        this.emerAddSpeedTime = emerAddSpeedTime;
    }
    public String getEmerSubSpeedTime() {
        return emerSubSpeedTime;
    }
    public void setEmerSubSpeedTime(String emerSubSpeedTime) {
        this.emerSubSpeedTime = emerSubSpeedTime;
    }
    public String getEmptyRunningTime() {
        return emptyRunningTime;
    }
    public void setEmptyRunningTime(String emptyRunningTime) {
        this.emptyRunningTime = emptyRunningTime;
    }
    public String getOverSpeedTime() {
        return overSpeedTime;
    }
    public void setOverSpeedTime(String overSpeedTime) {
        this.overSpeedTime = overSpeedTime;
    }
    public String getAvgCostOil() {
        return avgCostOil;
    }
    public void setAvgCostOil(String avgCostOil) {
        this.avgCostOil = avgCostOil;
    }
	@Override
	public String toString() {
		return "DriveScore [_id=" + _id + ", sequence=" + sequence + ", time="
				+ time + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", driveScore=" + driveScore + ", emerAddSpeedTime="
				+ emerAddSpeedTime + ", emerSubSpeedTime=" + emerSubSpeedTime
				+ ", emptyRunningTime=" + emptyRunningTime + ", overSpeedTime="
				//+ overSpeedTime + ", avgCostOil=" + avgCostOil + "]";
                + overSpeedTime + ", avgCostOil=" + avgCostOil + ", drive_distance="+ drivedistance
                +",cost_oil=" + costoil + "]";
	}
    
    
}
