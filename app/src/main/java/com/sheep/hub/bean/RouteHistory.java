package com.sheep.hub.bean;/*
  */

import com.amap.api.services.core.PoiItem;
import com.sheep.framework.db.annotation.Table;

@Table(name = "T_ROUTE_HISTORY")
public class RouteHistory extends BaseBean {

    private String _id;

    private double startLong;
    private double startLat;
    private String startTitle;
    private String startSnippet;

    private double destinationLong;
    private double destinationLat;
    private String destinationTitle;
    private String destinationSnippet;

    private String date;

    public void setStartPoitem(PoiItem startPoitem) {
        this.startLat = startPoitem.getLatLonPoint().getLatitude();
        this.startLong = startPoitem.getLatLonPoint().getLongitude();
        this.startTitle = startPoitem.getTitle();
        this.startSnippet = startPoitem.getSnippet();
    }

    public void setDestinationPoitem(PoiItem destinationPoitem) {
        this.destinationLat = destinationPoitem.getLatLonPoint().getLatitude();
        this.destinationLong = destinationPoitem.getLatLonPoint().getLongitude();
        this.destinationTitle = destinationPoitem.getTitle();
        this.destinationSnippet = destinationPoitem.getSnippet();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public double getStartLong() {
        return startLong;
    }

    public void setStartLong(double startLong) {
        this.startLong = startLong;
    }

    public double getStartLat() {
        return startLat;
    }

    public void setStartLat(double startLat) {
        this.startLat = startLat;
    }

    public String getStartTitle() {
        return startTitle;
    }

    public void setStartTitle(String startTitle) {
        this.startTitle = startTitle;
    }

    public String getStartSnippet() {
        return startSnippet;
    }

    public void setStartSnippet(String startSnippet) {
        this.startSnippet = startSnippet;
    }

    public double getDestinationLong() {
        return destinationLong;
    }

    public void setDestinationLong(double destinationLong) {
        this.destinationLong = destinationLong;
    }

    public double getDestinationLat() {
        return destinationLat;
    }

    public void setDestinationLat(double destinationLat) {
        this.destinationLat = destinationLat;
    }

    public String getDestinationTitle() {
        return destinationTitle;
    }

    public void setDestinationTitle(String destinationTitle) {
        this.destinationTitle = destinationTitle;
    }

    public String getDestinationSnippet() {
        return destinationSnippet;
    }

    public void setDestinationSnippet(String destinationSnippet) {
        this.destinationSnippet = destinationSnippet;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RouteHistory history = (RouteHistory) o;

        if (Double.compare(history.destinationLat, destinationLat) != 0) return false;
        if (Double.compare(history.destinationLong, destinationLong) != 0) return false;
        if (Double.compare(history.startLat, startLat) != 0) return false;
        if (Double.compare(history.startLong, startLong) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(startLong);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(startLat);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(destinationLong);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(destinationLat);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
