/*
 */
package com.sheep.hub.bean;

import java.util.ArrayList;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class HitchGroupItem extends BaseBean {
    
    private String hitchType; //故障类型
    private String hitchNum; //故障数量
    private int resouceColorId; //行文字颜色
    
    private ArrayList<HitchChildItem> hcis;

    public String getHitchType() {
        return hitchType;
    }

    public void setHitchType(String hitchType) {
        this.hitchType = hitchType;
    }

    public String getHitchNum() {
        return hitchNum;
    }

    public void setHitchNum(String hitchNum) {
        this.hitchNum = hitchNum;
    }

    public int getResouceColorId() {
        return resouceColorId;
    }

    public void setResouceColorId(int resouceColorId) {
        this.resouceColorId = resouceColorId;
    }

    public ArrayList<HitchChildItem> getHcis() {
        return hcis;
    }

    public void setHcis(ArrayList<HitchChildItem> hcis) {
        this.hcis = hcis;
    }

    @Override
    public String toString() {
        return "HitchGroupItem [hitchType=" + hitchType + ", hitchNum=" + hitchNum + ", resouceColorId="
                + resouceColorId + ", hcis=" + hcis + "]";
    }
    
}
