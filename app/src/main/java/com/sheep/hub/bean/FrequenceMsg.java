package com.sheep.hub.bean;

/**
 * Created by chensf on 2015/4/12.
 * 常时送信的命令
 */
public class FrequenceMsg extends BaseBean {

    private int command; //命令字

    private int maneuverType; //机动点类型

    private int maneuverDistance; //距机动点距离

    private int limitSpeed;  //限速值

    private int limitDistance; //距限速路口距离

    private int tunnel ; //进出隧道

    private int cameraDistance; //距监控摄像距离

    private int destinationDistance; //距目的地距离

    private int exit;  //导航结束或app退出

    private int unReadMsgOrTel ; //未读短信或未接电话

    private int gpsSeepd;  //gps的速度

    /**
     * 初始化的数据，不是0，而是0xff
     */
    public FrequenceMsg(){
        this.maneuverType = 0X04;
        this.maneuverDistance = 0X000F;
        this.limitSpeed = 0XFF;
        this.limitDistance = 0XFFFF;
        this.tunnel = 0XFF;
        this.cameraDistance = 0xFFFF;
        this.destinationDistance = 0XFFFFFFFF;
        this.gpsSeepd = 0XFFFF;
    }

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }


    public int getManeuverType() {
        return maneuverType;
    }

    public void setManeuverType(int maneuverType) {
        this.maneuverType = maneuverType;
    }

    public int getManeuverDistance() {
        return maneuverDistance;
    }

    public void setManeuverDistance(int maneuverDistance) {
        this.maneuverDistance = maneuverDistance;
    }

    public int getLimitSpeed() {
        return limitSpeed;
    }

    public void setLimitSpeed(int limitSpeed) {
        this.limitSpeed = limitSpeed;
    }

    public int getLimitDistance() {
        return limitDistance;
    }

    public void setLimitDistance(int limitDistance) {
        this.limitDistance = limitDistance;
    }

    public int getTunnel() {
        return tunnel;
    }

    public void setTunnel(int tunnel) {
        this.tunnel = tunnel;
    }

    public int getCameraDistance() {
        return cameraDistance;
    }

    public void setCameraDistance(int cameraDistance) {
        this.cameraDistance = cameraDistance;
    }

    public int getDestinationDistance() {
        return destinationDistance;
    }

    public void setDestinationDistance(int destinationDistance) {
        this.destinationDistance = destinationDistance;
    }

    public int getExit() {
        return exit;
    }

    public void setExit(int exit) {
        this.exit = exit;
    }

    public int getUnReadMsgOrTel() {
        return unReadMsgOrTel;
    }

    public void setUnReadMsgOrTel(int unReadMsgOrTel) {
        this.unReadMsgOrTel = unReadMsgOrTel;
    }

    public int getGpsSeepd() {
        return gpsSeepd;
    }

    public void setGpsSeepd(int gpsSeepd) {
        this.gpsSeepd = gpsSeepd;
    }

    @Override
    public String toString() {
        return "FrequenceMsg{" +
                "command=" + command +
                ", maneuverType=" + maneuverType +
                ", maneuverDistance=" + maneuverDistance +
                ", limitSpeed=" + limitSpeed +
                ", limitDistance=" + limitDistance +
                ", tunnel=" + tunnel +
                ", cameraDistance=" + cameraDistance +
                ", destinationDistance=" + destinationDistance +
                ", exit=" + exit +
                ", unReadMsgOrTel=" + unReadMsgOrTel +
                ", gpsSeepd=" + gpsSeepd +
                '}';
    }


}
