/*
 */
package com.sheep.hub.bean;

import java.util.ArrayList;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *  
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class DSGroupItem extends BaseBean {

    private String startTime; //开始时间
    
    private String driveScore; //驾驶得分
    
    private ArrayList<DSChildItem> cis;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDriveScore() {
        return driveScore;
    }

    public void setDriveScore(String driveScore) {
        this.driveScore = driveScore;
    }

    public ArrayList<DSChildItem> getCis() {
        return cis;
    }

    public void setCis(ArrayList<DSChildItem> cis) {
        this.cis = cis;
    }

    @Override
    public String toString() {
        return "DSGroupItem [startTime=" + startTime + ", driveScore=" + driveScore + "]";
    }
    
}
