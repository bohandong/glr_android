package com.sheep.hub.bean;

public class Menu extends BaseBean{

	private String name; //菜单名称
	
	private int iconId; //资源id
	
	private String colorId; //背景颜色

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	@Override
	public String toString() {
		return "Menu [name=" + name + ", iconId=" + iconId + "]";
	}
	
	
	
}
