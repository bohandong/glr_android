package com.sheep.hub.bean;

/**
 * 故障码  从hub发到app中，存储在数据库的
 * @author chensf
 *
 */
public class Hitch {
	private String _id;
	private String sequence;  //命令序列号
	private String type; //一级类型  --对应一级类型
	private String code; //码值
	private String isSerious; //是否严重故障
	private String name; //故障名字
	private String englishName; //英文
	private String category; //范畴
	private String detail; //背景知识
	
	
	private String time; //体验时间
	
	
	
	private String ref1; //留着 备份字段1 在改不了数据库，要新增字段时，顶上用着
	private String ref2; //留着 备份字段2
	private String ref3; //留着 备份字段3
	private String ref4; //留着 备份字段4
	private String ref5; //留着 备份字段5
	
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getRef1() {
		return ref1;
	}
	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}
	public String getRef2() {
		return ref2;
	}
	public void setRef2(String ref2) {
		this.ref2 = ref2;
	}
	public String getRef3() {
		return ref3;
	}
	public void setRef3(String ref3) {
		this.ref3 = ref3;
	}
	public String getRef4() {
		return ref4;
	}
	public void setRef4(String ref4) {
		this.ref4 = ref4;
	}
	public String getRef5() {
		return ref5;
	}
	public void setRef5(String ref5) {
		this.ref5 = ref5;
	}
	
	public String getIsSerious() {
		return isSerious;
	}
	public void setIsSerious(String isSerious) {
		this.isSerious = isSerious;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	@Override
	public String toString() {
		return "Hitch [_id=" + _id + ", sequence=" + sequence + ", type="
				+ type + ", code=" + code + ", isSerious=" + isSerious
				+ ", name=" + name + ", englishName=" + englishName
				+ ", category=" + category + ", detail=" + detail + ", time="
				+ time + ", ref1=" + ref1 + ", ref2=" + ref2 + ", ref3=" + ref3
				+ ", ref4=" + ref4 + ", ref5=" + ref5 + "]";
	}
	
}
