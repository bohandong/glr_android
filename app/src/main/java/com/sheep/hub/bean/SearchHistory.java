package com.sheep.hub.bean;/*
  */

import com.sheep.framework.db.annotation.Table;

@Table(name = "T_SEARCH_HISTORY")
public class SearchHistory extends BaseBean {
    private String _id; //自增主键
    private String key;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchHistory history = (SearchHistory) o;

        if (key != null ? !key.equals(history.key) : history.key != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return key != null ? key.hashCode() : 0;
    }
}
