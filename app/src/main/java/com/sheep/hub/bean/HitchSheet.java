package com.sheep.hub.bean;

/**
 * 故障码表，从excel中，拷贝至数据库中
 * @author chensf
 *
 */
public class HitchSheet {
 
	private String _id; //表，主键
	private String type; //一级类型  --对应一级类型
	private String code; //码值
	private String isSerious; //是否严重故障
	private String name; //故障名字
	private String englishName; //英文
	private String category; //范畴
	private String detail; //背景知识
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getIsSerious() {
		return isSerious;
	}
	public void setIsSerious(String isSerious) {
		this.isSerious = isSerious;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	@Override
	public String toString() {
		return "HitchSheet [_id=" + _id + ", type=" + type + ", code=" + code
				+ ", isSerious=" + isSerious + ", name=" + name
				+ ", englishName=" + englishName + ", category=" + category
				+ ", detail=" + detail + "]";
	}
}
