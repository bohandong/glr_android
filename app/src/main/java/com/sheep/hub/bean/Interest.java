package com.sheep.hub.bean;/*
  */


public class Interest extends BaseBean {

    private int nameId;
    private int drawableId;

    public Interest() {

    }

    public Interest(int nameId, int drawableId) {
        this.nameId = nameId;
        this.drawableId = drawableId;
    }

    public int getNameId() {
        return nameId;
    }

    public void setNameId(int nameId) {
        this.nameId = nameId;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }
}
