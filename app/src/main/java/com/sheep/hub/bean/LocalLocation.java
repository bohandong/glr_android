package com.sheep.hub.bean;/*
 */

import com.sheep.framework.db.annotation.Table;

@Table(name = "T_LOCAL_LOCATION")
public class LocalLocation extends BaseBean {
    private String _id;
    private String name;
    private double latitude;
    private double longitude;
    private String rowName;

    private int isStored; //0代表收藏,1代表未收藏

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getIsStored() {
        return isStored;
    }

    public void setIsStored(int isStored) {
        this.isStored = isStored;
    }

    public String getRowName() {
        return rowName;
    }

    public void setRowName(String rowName) {
        this.rowName = rowName;
    }
}
