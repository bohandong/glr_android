/*
  */
package com.sheep.hub.bean;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author shufei
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class HitchChildItem extends BaseBean {

    private int hitchIconId;  //故障的icon id名称
    private String hitchCode; //故障码类型
    private String hitchName; //故障名称 
    private String hitchCarType; //故障的类型 比如，，，哪个所属xxx
    private String hitchDetail; //详细的故障描述
    private boolean expand;  //是否被展开了，默认都是关闭着
    
    
    
    
    public boolean isExpand() {
		return expand;
	}
	public void setExpand(boolean expand) {
		this.expand = expand;
	}
	public int getHitchIconId() {
        return hitchIconId;
    }
    public void setHitchIconId(int hitchIconId) {
        this.hitchIconId = hitchIconId;
    }
    public String getHitchName() {
        return hitchName;
    }
    public void setHitchName(String hitchName) {
        this.hitchName = hitchName;
    }
    public String getHitchCarType() {
        return hitchCarType;
    }
    public void setHitchCarType(String hitchCarType) {
        this.hitchCarType = hitchCarType;
    }
    public String getHitchDetail() {
        return hitchDetail;
    }
    public void setHitchDetail(String hitchDetail) {
        this.hitchDetail = hitchDetail;
    }
    public String getHitchCode() {
        return hitchCode;
    }

    public void setHitchCode(String hitchCode) {
        this.hitchCode = hitchCode;
    }

    @Override
    public String toString() {
        return "HitchChildItem [hitchIconId=" + hitchIconId + ", hitchName=" + hitchName + ", hitchCarType="
                + hitchCarType + ", hitchDetail=" + hitchDetail + "]";
    }
    
}
