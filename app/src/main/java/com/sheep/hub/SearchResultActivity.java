package com.sheep.hub;/*
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.sheep.framework.view.pulltorefresh.PullToRefreshBase;
import com.sheep.framework.view.pulltorefresh.PullToRefreshListView;
import com.sheep.hub.adapter.SearchResultAdapter;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.PoiSearchHelper;

import java.util.ArrayList;

public class SearchResultActivity extends BaseCalculateRouteActivity implements AdapterView.OnItemClickListener, PullToRefreshBase.OnRefreshListener2 {
    public static final String POI_KEY = "poi_key";
    public static final String IS_NEARBY="is_nearby";
    private String poiKey;
    private PullToRefreshListView plv_list;
    private PoiSearchHelper poiSearchHelper;
    private SearchResultAdapter searchResultAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        poiKey = getIntent().getStringExtra(POI_KEY);
        initTitle(poiKey);
        initView();
    }

    private void initView() {
        plv_list = (PullToRefreshListView) findViewById(android.R.id.list);
        plv_list.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        searchResultAdapter = new SearchResultAdapter(this);
        plv_list.setAdapter(searchResultAdapter);

        plv_list.setOnItemClickListener(this);
        plv_list.setOnRefreshListener(this);

        plv_list.setRefreshing();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Intent data = new Intent();
//        data.putExtra("data", searchResultAdapter.getItem(position));
//        setResult(RESULT_OK, data);
//        finish();
        startRouteSearch((PoiItem) parent.getAdapter().getItem(position));
    }

    @Override
    public void onPullDownToRefresh(PullToRefreshBase refreshView) {
        poiSearch(true);
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase refreshView) {
        poiSearch(false);
    }

    private void poiSearch(final boolean isFirstPage) {
        if (null == poiSearchHelper) {
            poiSearchHelper = new PoiSearchHelper(SearchResultActivity.this, poiKey, new PoiSearchHelper.PoiQuerySuccessListener() {
                @Override
                public void onSuccess(ArrayList<PoiItem> poiItemList) {
                    plv_list.onRefreshComplete();
                    searchResultAdapter.addList(poiItemList);
                    if (poiItemList.size() < Constant.PAGE_SIZE) {
                        plv_list.setMode(PullToRefreshBase.Mode.DISABLED);
                    }
                    if (null == plv_list.getEmptyView()) {
                        TextView empty = (TextView) View.inflate(SearchResultActivity.this, R.layout.view_empy, null);
                        empty.setText(R.string.no_search_result);
                        plv_list.setEmptyView(empty);
                    }
                }

                @Override
                public void onFail() {
                    plv_list.onRefreshComplete();
                    if (null == plv_list.getEmptyView()) {
                        TextView empty = (TextView) View.inflate(SearchResultActivity.this, R.layout.view_empy, null);
                        empty.setText(R.string.no_search_result);
                        plv_list.setEmptyView(empty);
                    }
                }
            });
            poiSearchHelper.setNearBy(getIntent().getBooleanExtra(IS_NEARBY,false));
        }
        if (isFirstPage) {
            poiSearchHelper.query(false);
        } else {
            poiSearchHelper.queryNext();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(RESULT_OK==resultCode&&IS_BEGIN_NAVI_REQ==requestCode){
            finish();
        }
    }
}
