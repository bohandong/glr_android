package com.sheep.hub;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.CallLog;
import android.provider.Telephony;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.LatLng;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.activity.SettingActivity;
import com.sheep.hub.bluetooth.BluetoothInstruct;
import com.sheep.hub.bluetooth.BluetoothManager;
import com.sheep.hub.bluetooth.IBluetoothListener;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.Preference;

public class MainActivity extends BaseMapActivity implements View.OnClickListener {
    private static final int NEARBY_REQ = 1;
    private static final int SETTING_REQ = 2;


    private ImageView iv_traffic;
    private ImageView iv_layer;
    private ImageView iv_location;
    private ImageView iv_sounds;

    private boolean isTrafficOpen;
    private boolean isLayerOpen;
    private boolean isSoundsOpen = true;

    private ImageView iv_icon;

    private long exitTime = 0;

    private int count;  //计数器

    private BluetoothManager manager;
    private boolean isDiscovingFinish = false;
    private Runnable locationPromptRunnable;
    private Handler locationHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = BluetoothManager.getInstance();
        initView(savedInstanceState);
        prepareConn();
        /*if (Preference.getBoolean(Constant.IS_HOME_FIRST, true)) {
            initGuide();
        }*/
        registerObserver();
        HubApp.getInstance().setUnReadSmsNum(getNewSmsCount() + getNewMmsCount());
        HubApp.getInstance().setUnCallNum(readMissCall());
        locationHandler =new Handler();
        locationHandler.postDelayed(locationPromptRunnable=new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, R.string.location_fail, Toast.LENGTH_SHORT).show();
            }
        }, 8000);
    }

    private void initGuide() {
        final ImageView iv_guide = (ImageView) findViewById(R.id.iv_guide);
        iv_guide.setVisibility(View.VISIBLE);
        Preference.putBoolean(Constant.IS_HOME_FIRST, false);
        iv_guide.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                AnimationSet animation = new AnimationSet(true);
                Animation alphaAnimation = new AlphaAnimation(1.0f, 0f);
                Animation scaleAnimation = new ScaleAnimation(1.0f, 0.1f, 1.0f, 0.1f,
                        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                        0.5f);
                animation.addAnimation(alphaAnimation);
                animation.addAnimation(scaleAnimation);
                animation.setDuration(200);
                animation.setFillAfter(true);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        iv_guide.clearAnimation();
                        iv_guide.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                iv_guide.startAnimation(animation);
                return true;
            }

        });
    }

    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);

        if (!CommonUtils.isSDCardAvailable()) {
            Toast.makeText(this, R.string.sd_not_available, Toast.LENGTH_SHORT).show();
            finish();
        }

        aMap.getUiSettings().setMyLocationButtonEnabled(false);

        iv_traffic = (ImageView) findViewById(R.id.iv_traffic);
        iv_layer = (ImageView) findViewById(R.id.iv_layer);
        iv_location = (ImageView) findViewById(R.id.iv_location);
        iv_sounds = (ImageView) findViewById(R.id.iv_sounds);

        iv_icon = (ImageView) findViewById(R.id.iv_icon);

        isTrafficOpen = Preference.getBoolean(Constant.PRE_TRAFFIC, false);
        isLayerOpen = Preference.getBoolean(Constant.PRE_LAYER, false);
        isSoundsOpen = Preference.getBoolean(Constant.PRE_SOUNDS, true);

        findViewById(R.id.ll_search).setOnClickListener(this);
        findViewById(R.id.ll_navigation).setOnClickListener(this);
        findViewById(R.id.ll_nearby).setOnClickListener(this);
        findViewById(R.id.ll_setting).setOnClickListener(this);
        findViewById(R.id.ll_mine).setOnClickListener(this);
        iv_traffic.setOnClickListener(this);
        iv_layer.setOnClickListener(this);
        iv_location.setOnClickListener(this);
        iv_sounds.setOnClickListener(this);
        iv_icon.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        aMap.setMapType(Preference.getBoolean(Constant.PRE_LAYER, false) ? AMap.MAP_TYPE_SATELLITE : AMap.MAP_TYPE_NORMAL);// 卫星地图模式
        aMap.setTrafficEnabled(Preference.getBoolean(Constant.PRE_TRAFFIC, false));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_navigation:
                launch(NaviSearchActivity.class);
                break;
            case R.id.ll_nearby:
                launch(NearbyActivity.class, NEARBY_REQ);
                break;
            case R.id.ll_setting:
                launch(SettingActivity.class, SETTING_REQ);
                break;
            case R.id.ll_mine:
                launch(MineActivity.class);
                break;
            case R.id.ll_search:
                launch(SearchActivity.class);
                break;
            case R.id.iv_traffic:
                Preference.putBoolean(Constant.PRE_TRAFFIC, isTrafficOpen = !isTrafficOpen);
                aMap.setTrafficEnabled(isTrafficOpen);// 显示实时交通状况
                break;
            case R.id.iv_layer:
                Preference.putBoolean(Constant.PRE_LAYER, isLayerOpen = !isLayerOpen);
                aMap.setMapType(isLayerOpen ? AMap.MAP_TYPE_SATELLITE : AMap.MAP_TYPE_NORMAL);// 卫星地图模式
                break;
            case R.id.iv_sounds:
                Preference.putBoolean(Constant.PRE_SOUNDS, isSoundsOpen = !isSoundsOpen);
                break;
            case R.id.iv_location:
                if (null != HubApp.getInstance().getMyLocation()) {
                    aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(HubApp.getInstance().getMyLocation().getLatitude(), HubApp.getInstance().getMyLocation().getLongitude())));
                } else {
                    Toast.makeText(this, R.string.location_fail, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.iv_icon:
                if (!isDiscovingFinish) {
                    CommonUtils.showLongToast(this, getString(R.string.tip_prepare_bluetooth_conn));
                } else {
                    if (!manager.isBluetoothConn()) { //准备去连接
                        prepareConn();
                    } else {
                        manager.releaseResource();
                        iv_icon.setImageResource(R.drawable.icon_bluetooth_discon);
                    }
                }
                //showBluetoothSearchDialog();
                //launch(BluetoothDiscoveringActivity.class);
                //launch(new Intent(MainActivity.this, BluetoothDiscoveringActivity.class), BLUETOOTH_REQ);
                break;
            default:
        }
    }

    private void changeIvIcon() {
        int picNum = count % 3;
        switch (picNum) {
            case 1:
                iv_icon.setImageResource(R.drawable.icon_bluetooth1);
                break;
            case 2:
                iv_icon.setImageResource(R.drawable.icon_bluetooth2);
                break;
            case 0:
                iv_icon.setImageResource(R.drawable.icon_bluetooth3);
                break;
            default:
                break;
        }
    }

    private void prepareConn() {
        isDiscovingFinish = false;
        count = 1;
        CommonUtils.showLongToast(this, getString(R.string.tip_prepare_bluetooth_conn));
        final Handler handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        changeIvIcon();
                        break;
                    case 2:
                        iv_icon.setImageResource(R.drawable.icon_bluetooth3);
                        break;
                    case 3:
                        iv_icon.setImageResource(R.drawable.icon_bluetooth_discon);
                        break;
                    default:
                        break;
                }

            }


        };

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                count++;
                Message om = handler.obtainMessage();
                if (isDiscovingFinish) {
                    if (manager.isBluetoothConn()) {
                        om.what = 2;
                    } else {
                        om.what = 3;
                    }
                } else {
                    if (!manager.isBluetoothConn()) {
                        om.what = 1; //正在连接
                        handler.postDelayed(this, 1000);
                    } else {
                        om.what = 2; //连接成功
                    }
                }


                handler.sendMessage(om);
            }
        }, 1000);


        manager.addListener(new IBluetoothListener() {

            @Override
            public void onStateChange(int state, int what) { //若通知 连接失败了，，则更新下状态
                if (state == BluetoothManager.STATE_BLUETOOTH_OFF || state == BluetoothManager.STATE_BLUETOOTH_DISCONNECT) {
                    Message om = handler.obtainMessage();
                    om.what = 3;
                    handler.sendMessage(om);
                    isDiscovingFinish = true;
                } else if (state == BluetoothManager.STATE_DISCOVERED) {
                    isDiscovingFinish = true;
                } else if(state == BluetoothManager.STATE_NOTFOUND){
                	 Message om = handler.obtainMessage();
                     om.what = 3;
                     handler.sendMessage(om);
                }
            }

            @Override
            public void onScanningDevice(String name, String address, int bondState) {
                if (BluetoothManager.BLUETOOTH_NAME.equals(name)) {
                    manager.pairBluetoothDevice(address);
                    //连接了呢
                }
            }
        });
        manager.enableBluetooth();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SETTING_REQ:
                    finish();
                    break;
                case NEARBY_REQ:
                    break;
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        manager.releaseResource();
        unregisterObserver();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                Toast.makeText(getApplicationContext(), R.string.more_click_to_exit, Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        locationHandler.removeCallbacks(locationPromptRunnable);
        super.onLocationChanged(aMapLocation);
    }

    private ContentObserver newMmsContentObserver = new ContentObserver(new Handler()) {
        public void onChange(boolean selfChange) {
            HubApp.getInstance().setUnReadSmsNum(getNewSmsCount() + getNewMmsCount());
            sendMsgCallChangMsg();
        }
    };
    private ContentObserver missedCallObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            HubApp.getInstance().setUnCallNum(getMissedCalls());
            sendMsgCallChangMsg();
        }
    };

    private BroadcastReceiver callReceiver;

    @SuppressLint("NewApi")
    private void registerObserver() {
        unregisterObserver();
        getContentResolver().registerContentObserver(Uri.parse("content://sms"), true,
                newMmsContentObserver);
        getContentResolver().registerContentObserver(Telephony.MmsSms.CONTENT_URI, true,
                newMmsContentObserver);
        getContentResolver().registerContentObserver(CallLog.Calls.CONTENT_URI, false, missedCallObserver);

        final IntentFilter filter = new IntentFilter();

        filter.addAction("com.android.phone.NotificationMgr.MissedCall_intent");

        //filter.addAction("com.android.phone.NotificationMgr.");

        registerReceiver(callReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action != null && "com.android.phone.NotificationMgr.MissedCall_intent".equals(action)) {
                    HubApp.getInstance().setUnCallNum(intent.getExtras().getInt("MissedCallNumber"));
                    sendMsgCallChangMsg();
                }
            }
        }, filter);
    }

    private int getNewSmsCount() {
        int result = 0;
        Cursor csr = getContentResolver().query(Uri.parse("content://sms"), null,
                "type = 1 and read = 0", null, null);
        if (csr != null) {
            result = csr.getCount();
            csr.close();
        }
        return result;
    }

    private int getNewMmsCount() {
        int result = 0;
        Cursor csr = getContentResolver().query(Uri.parse("content://mms/inbox"),
                null, "read = 0", null, null);
        if (csr != null) {
            result = csr.getCount();
            csr.close();
        }
        return result;
    }

    private int getMissedCalls() {
        int result = 0;
        Cursor csr = getContentResolver().query(CallLog.Calls.CONTENT_URI, new String[]{CallLog.Calls.NUMBER,
                CallLog.Calls.TYPE, CallLog.Calls.NEW}, "type=3", null, CallLog.Calls.DEFAULT_SORT_ORDER);
        if (csr != null) {
            result = csr.getCount();
            csr.close();
        }
        return result;
    }

    private synchronized void unregisterObserver() {
        try {
            if (newMmsContentObserver != null) {
                getContentResolver().unregisterContentObserver(newMmsContentObserver);
            }
            if (newMmsContentObserver != null) {
                getContentResolver().unregisterContentObserver(newMmsContentObserver);
            }
        } catch (Exception e) {
        }
        if (null != callReceiver) {
            unregisterReceiver(callReceiver);
        }
    }

    private int readMissCall() {
        int result = 0;
        Cursor cursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, new String[]{
                CallLog.Calls.TYPE
        }, " type=? and new=?", new String[]{
                CallLog.Calls.MISSED_TYPE + "", "1"
        }, "date desc");

        if (cursor != null) {
            result = cursor.getCount();
            cursor.close();
        }
        return result;
    }

    private void sendMsgCallChangMsg(){
        if(!HubApp.getInstance().isNaving()){
            BluetoothInstruct.sendNaviMessage("导航结束");
        }
    }
}
