package com.sheep.hub;/*
  */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.amap.api.maps.AMap;
import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.model.AMapNaviPath;
import com.amap.api.navi.model.NaviLatLng;
import com.amap.api.navi.view.RouteOverLay;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.dialog.ConfirmDialog;
import com.sheep.hub.util.TTSController;

import java.util.ArrayList;

public class NaviRouteActivity extends BaseMapActivity implements AMap.OnMapLoadedListener, View.OnClickListener {

    public static final String START_POITEM_LIST = "start_poitem_list";
    public static final String END_POITEM_LIST = "end_poitem_list";
    public static final String CENTER_POITEM_LIST = "center_poitem_list";
    private static final int GPS_REQ = 1;
    private static final int CENTER_REQ = 2;

    private AMapNavi mAmapNavi;
    private RouteOverLay mRouteOverLay;
    private boolean mIsMapLoaded;

    private ArrayList<NaviLatLng> startList;
    private ArrayList<NaviLatLng> destinationList;
    private ArrayList<NaviLatLng> centerList;
    private TextView tv_cost;
    private TextView tv_traffic;
    private TextView tv_time;
    private TextView tv_length;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        startList = (ArrayList<NaviLatLng>) getIntent().getSerializableExtra(START_POITEM_LIST);
        destinationList = (ArrayList<NaviLatLng>) getIntent().getSerializableExtra(END_POITEM_LIST);
        centerList = (ArrayList<NaviLatLng>) getIntent().getSerializableExtra(CENTER_POITEM_LIST);
        if (null == centerList) {
            centerList = new ArrayList<NaviLatLng>();
        }
        initTitle(R.string.route_display);
        initView(savedInstanceState);
    }

    public void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);

        Button button = (Button) findViewById(R.id.btn_next);
        button.setVisibility(View.VISIBLE);
        button.setText(R.string.add_center_location);
        button.setBackgroundResource(R.drawable.shape_transparent_bg);
        button.setOnClickListener(this);

        mRouteOverLay = new RouteOverLay(aMap, null);

        findViewById(R.id.btn_route).setOnClickListener(this);
        findViewById(R.id.btn_emulator).setOnClickListener(this);

        initBottomView();
    }

    private void initBottomView() {
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_traffic = (TextView) findViewById(R.id.tv_traffic);
        tv_length = (TextView) findViewById(R.id.tv_length);
        tv_cost = (TextView) findViewById(R.id.tv_cost);

        findViewById(R.id.ll_ok).setOnClickListener(this);
        findViewById(R.id.ll_cancel).setOnClickListener(this);
    }

    private void initNavi() {
        mAmapNavi = AMapNavi.getInstance(this);
        AMapNaviPath naviPath = mAmapNavi.getNaviPath();
        if (naviPath == null) {
            return;
        }
        mRouteOverLay.setRouteInfo(naviPath);
        mRouteOverLay.addToMap();
        double length = ((int) (naviPath.getAllLength() / (double) 1000 * 10))
                / (double) 10;

        tv_length.setText(length + "km");
        tv_time.setText(getString(R.string.route_length_desc, "" + ((naviPath.getAllTime() + 59) / 60)));
        tv_traffic.setText(getString(R.string.route_traffic_red_green_desc, "未知"));
        tv_cost.setText(getString(R.string.route_cost_desc, naviPath.getTollCost() + ""));
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    mRouteOverLay.zoomToSpan();
            }
        },500);
    }


    @Override
    public void onResume() {
        super.onResume();
        initNavi();
        TTSController.getInstance().startSpeaking();
    }

    @Override
    public void onPause() {
        super.onPause();
        TTSController.getInstance().stopSpeaking();
    }

    @Override
    public void onMapLoaded() {
        mIsMapLoaded = true;
        if (mRouteOverLay != null) {
            mRouteOverLay.zoomToSpan();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode && CENTER_REQ == requestCode) {
            finish();
        } else if (GPS_REQ == requestCode) {
            if (CommonUtils.isGPSOPen(this)) {
                launch(NaviActivity.class);
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_emulator:
                Intent intent = new Intent(this, NaviActivity.class);
                intent.putExtra(NaviActivity.IS_EMUALTOR, true);
                launch(intent);
                finish();
                break;
            case R.id.ll_ok:
                if (!CommonUtils.isGPSOPen(NaviRouteActivity.this)) {
                    ConfirmDialog dialog = new ConfirmDialog(NaviRouteActivity.this);
                    dialog.setTitle(getString(R.string.prompt));
                    dialog.setMessage(getString(R.string.open_gps_tip));
                    dialog.setListener(new ConfirmDialog.ConfirmListener() {
                        @Override
                        public void onNegativeListener() {

                        }

                        @Override
                        public void onPositiveListener() {
                            Intent gpsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            launch(gpsIntent, GPS_REQ);
                        }
                    });
                    dialog.show();
                } else {
                    launch(NaviActivity.class);
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case R.id.ll_cancel:
                finish();
                break;
            case R.id.btn_route:
                launch(RouteDescListActivity.class);
                break;
            case R.id.btn_next:
                Intent centerSearchIntent = new Intent(this, CenterSearchActivity.class);
                centerSearchIntent.putExtra(NaviRouteActivity.START_POITEM_LIST, startList);
                centerSearchIntent.putExtra(NaviRouteActivity.CENTER_POITEM_LIST, centerList);
                centerSearchIntent.putExtra(NaviRouteActivity.END_POITEM_LIST, destinationList);
                launch(centerSearchIntent, CENTER_REQ);
                break;
            default:
        }
    }
}
