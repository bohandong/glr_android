package com.sheep.hub;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.provider.Telephony;
import android.view.KeyEvent;

import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.AMapNaviListener;
import com.amap.api.navi.AMapNaviView;
import com.amap.api.navi.AMapNaviViewListener;
import com.amap.api.navi.AMapNaviViewOptions;
import com.amap.api.navi.model.AMapNaviInfo;
import com.amap.api.navi.model.AMapNaviLocation;
import com.sheep.hub.bluetooth.BluetoothInstruct;
import com.sheep.hub.constant.Constant;
import com.sheep.hub.util.Preference;
import com.sheep.hub.util.TTSController;

import java.util.Calendar;

/**
 * 实时导航界面
 */
public class NaviActivity extends BaseActivity implements
        AMapNaviViewListener {
    public static final String IS_EMUALTOR = "is_emulator";
    private AMapNaviView anv_navi;
    // 导航可以设置的参数
//    private boolean mDayNightFlag = Utils.DAY_MODE;// 默认为白天模式
//    private boolean mDeviationFlag = Utils.YES_MODE;// 默认进行偏航重算
//    private boolean mJamFlag = Utils.YES_MODE;// 默认进行拥堵重算
//    private boolean mTrafficFlag = Utils.OPEN_MODE;// 默认进行交通播报
//    private boolean mCameraFlag = Utils.OPEN_MODE;// 默认进行摄像头播报
//    private boolean mScreenFlag = Utils.YES_MODE;// 默认是屏幕常亮
    // 导航界面风格
//    private int mThemeStle;
    private AMapNaviListener mapNaviListener;
    private boolean isEmulator;
    private Handler handler;
    private Runnable emptyRannable;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navi);
        isEmulator = getIntent().getBooleanExtra(IS_EMUALTOR, false);
        //语音播报开始
        TTSController.getInstance().startSpeaking();
        if (isEmulator) {
            AMapNavi.getInstance(this).setEmulatorNaviSpeed(100);
            AMapNavi.getInstance(this).startNavi(AMapNavi.EmulatorNaviMode);
        } else {
            AMapNavi.getInstance(this).startNavi(AMapNavi.GPSNaviMode);
        }
        initView(savedInstanceState);

    }

    private void initView(Bundle savedInstanceState) {
        anv_navi = (AMapNaviView) findViewById(R.id.anv_navi);
        anv_navi.onCreate(savedInstanceState);
        anv_navi.setAMapNaviViewListener(this);
        setAmapNaviViewOptions();
    }

    /**
     * 设置导航的参数
     */
    private void setAmapNaviViewOptions() {
        if (anv_navi == null) {
            return;
        }
        AMapNaviViewOptions viewOptions = new AMapNaviViewOptions();
        viewOptions.setSettingMenuEnabled(false);// 设置导航setting可用

        // 设置导航是否为黑夜模式
        String dayNightMode = Preference.getString(Constant.K_DAYNIGHT_MODE, Constant.V_AUTO);
        boolean nightMode;
        if (dayNightMode.equals(Constant.V_NIGHT)) {
            nightMode = true;
        } else if (dayNightMode.equals(Constant.V_DAY)) {
            nightMode = false;
        } else {
            int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            if (hours > 8 && hours < 18) {
                nightMode = false;
            } else {
                nightMode = true;
            }

        }
        viewOptions.setNaviNight(nightMode);
        viewOptions.setReCalculateRouteForYaw(true);// 设置导偏航是否重算
//        viewOptions.setReCalculateRouteForTrafficJam(mJamFlag);// 设置交通拥挤是否重算
//        viewOptions.setTrafficInfoUpdateEnabled(mTrafficFlag);// 设置是否更新路况
        viewOptions.setCameraInfoUpdateEnabled(true);// 设置摄像头播报
        viewOptions.setScreenAlwaysBright(true);// 设置屏幕常亮情况

        int themePostion = Preference.getInt(Constant.PRE_NAVI_THEME);
        int themeCode;
        if (1 == themePostion) {
            themeCode = AMapNaviViewOptions.PINK_COLOR_TOPIC;
        } else if (2 == themePostion) {
            themeCode = AMapNaviViewOptions.WHITE_COLOR_TOPIC;
        } else {
            themeCode = AMapNaviViewOptions.BLUE_COLOR_TOPIC;
        }
        viewOptions.setNaviViewTopic(themeCode);// 设置导航界面主题样式

        anv_navi.setViewOptions(viewOptions);

    }

    private AMapNaviListener getAMapNaviListener() {
        if (mapNaviListener == null) {

            mapNaviListener = new AMapNaviListener() {

                @Override
                public void onTrafficStatusUpdate() {

                }

                @Override
                public void onStartNavi(int arg0) {
                    handler = new Handler();
                    handler.postDelayed(emptyRannable = new Runnable() {
                        @Override
                        public void run() {
                            BluetoothInstruct.sendNaviMessage("");
                            handler.postDelayed(this, 500);
                        }
                    }, 500);
                    HubApp.getInstance().setIsNaving(true);
                }

                @Override
                public void onReCalculateRouteForYaw() {
                    // 可以在频繁重算时进行设置,例如五次之后
                    // i++;
                    // if (i >= 5) {
                    // AMapNaviViewOptions viewOptions = new
                    // AMapNaviViewOptions();
                    // viewOptions.setReCalculateRouteForYaw(false);
                    // anv_navi.setViewOptions(viewOptions);
                    // }
                }

                @Override
                public void onReCalculateRouteForTrafficJam() {

                }

                @Override
                public void onLocationChange(AMapNaviLocation location) {

                }

                @Override
                public void onInitNaviSuccess() {
                }

                @Override
                public void onInitNaviFailure() {
                }

                @Override
                public void onGetNavigationText(int arg0, String arg1) {

                }

                @Override
                public void onEndEmulatorNavi() {
                    handler.removeCallbacks(emptyRannable);
                }

                @Override
                public void onCalculateRouteSuccess() {

                }

                @Override
                public void onCalculateRouteFailure(int arg0) {

                }

                @Override
                public void onArrivedWayPoint(int arg0) {

                }

                @Override
                public void onArriveDestination() {
                    handler.removeCallbacks(emptyRannable);
                    HubApp.getInstance().setIsNaving(false);
                }

                @Override
                public void onGpsOpenStatus(boolean arg0) {

                }

                @Override
                public void onNaviInfoUpdated(AMapNaviInfo arg0) {

                }
            };
        }
        return mapNaviListener;
    }

    @Override
    public void onNaviCancel() {
        onBackPressed();
        TTSController.getInstance().stopSpeaking();
    }

    /**
     * 点击设置按钮的事件
     */
    @Override
    public void onNaviSetting() {
//        Bundle bundle = new Bundle();
//        bundle.putInt(Utils.THEME, mThemeStle);
//        bundle.putBoolean(Utils.DAY_NIGHT_MODE, mDayNightFlag);
//        bundle.putBoolean(Utils.DEVIATION, mDeviationFlag);
//        bundle.putBoolean(Utils.JAM, mJamFlag);
//        bundle.putBoolean(Utils.TRAFFIC, mTrafficFlag);
//        bundle.putBoolean(Utils.CAMERA, mCameraFlag);
//        bundle.putBoolean(Utils.SCREEN, mScreenFlag);
//        Intent intent = new Intent(NaviActivity.this,
//                NaviSettingActivity.class);
//        intent.putExtras(bundle);
//        startActivity(intent);

    }

    @Override
    public void onNaviMapMode(int arg0) {

    }

    @Override
    public void onNaviTurnClick() {


    }

    @Override
    public void onNextRoadClick() {

    }

    @Override
    public void onScanViewButtonClick() {

    }

    private void processBundle(Bundle bundle) {

//        if (bundle != null) {
//            mDayNightFlag = bundle.getBoolean(Utils.DAY_NIGHT_MODE,
//                    mDayNightFlag);
//            mDeviationFlag = bundle.getBoolean(Utils.DEVIATION, mDeviationFlag);
//            mJamFlag = bundle.getBoolean(Utils.JAM, mJamFlag);
//            mTrafficFlag = bundle.getBoolean(Utils.TRAFFIC, mTrafficFlag);
//            mCameraFlag = bundle.getBoolean(Utils.CAMERA, mCameraFlag);
//            mScreenFlag = bundle.getBoolean(Utils.SCREEN, mScreenFlag);
//            mThemeStle = bundle.getInt(Utils.THEME);
//
//        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        anv_navi.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
//        Bundle bundle = getIntent().getExtras();
//        processBundle(bundle);
        setAmapNaviViewOptions();
        AMapNavi.getInstance(this).setAMapNaviListener(getAMapNaviListener());
        anv_navi.onResume();

    }

    @Override
    public void onPause() {
        anv_navi.onPause();
        super.onPause();
        AMapNavi.getInstance(this)
                .removeAMapNaviListener(getAMapNaviListener());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        anv_navi.onDestroy();
        TTSController.getInstance().stopSpeaking();
        handler.removeCallbacks(emptyRannable);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:
                Preference.putBoolean(Constant.PRE_SOUNDS, true);
                TTSController.getInstance().setVolume(true);
                break;
            default:
        }
        return super.onKeyDown(keyCode, event);
    }


}