package com.sheep.hub.bluetooth;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import android.text.TextUtils;
import android.util.Log;

import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.HubApp;
import com.sheep.hub.bean.FrequenceMsg;
import com.sheep.hub.util.ChineseNumber2DigitUtil;
import com.sheep.hub.util.TTSController;

/**
 * hub与app交互 命令指令集类
 *
 * @author chensf
 */
public class BluetoothInstruct {

    private static final byte[] CHECK_TIME = {0x01, 0x00}; //对应指令，高低取反的，实际数值是 0x00 0x01
    public static final int CHECK_TIME_COMMAND = 0x0001; //对应指令

    private static final byte[] SET_MESSAGE = {0x03, 0x00}; //设置送信 ，对应指令，高低取反的，实际数值是 0x00 0x01
    public static final int SET_MESSAGE_COMMAND = 0x0003; //对应指令

    private static final byte[] IMMEDIA_EXPERIENCE={0x04,0x00}; //立即体验
    public static final int IMMEDIA_EXPERIENCE_COMMAND = 0x0004; //立即体验 

    public static final byte[] DRIVE_SCORE = {0x05,0x00};  //驾驶评分 app发给hub，请求数据
    public static final int DRIVE_SCORE_COMMAND = 0x0051; //驾驶评分




    public static final int HITCH_COMMAND = 0x0050; //故障码命令


    public static final int SET_NAVI_COMMAND=0x0002;//导航指令
    public static final byte[] SET_NAVI = {0x02,0x00}; // 导航指令 对应指令

    private static final int CONTROLL_APP2HUB_REQ = 0x01; //app发给hub的命令控制位
    private static final int CONTROLL_HUB2APP_RES = 0x02; //hub应答app的命令控制位

    private static final int CONTROLL_HUB2APP_REQ = 0x03; //hub发送app的命令控制位
    private static final int CONTROLL_APP2HUB_RES = 0x04; //app应答hub的命令控制位

    private static final BluetoothManager manager = BluetoothManager.getInstance();


    public static final byte[] command = ByteUtils.createIntToByteArray(0x02, 2);
    public static final byte[] ffStr1 = ByteUtils.createIntToByteArray(0xFF, 1);
    public static final byte[] ffStr2 = ByteUtils.createIntToByteArray(0xFFFF, 2);
    public static final byte[] ffStr4 = ByteUtils.createIntToByteArray(0xFFFFFFFF, 4);


    private static Map<Integer,MsgFrame> frameMapForSended = new HashMap<Integer,MsgFrame>();

    /**
     * 将存起发过的帧数据清掉
     * @param sequence
     */
    public static void removeMsgFrameFromCache(int sequence){
        frameMapForSended.remove(sequence);
    }

    public static void repeatSendLastFrameDate(){
        int sequence = HubApp.getInstance().getSequence();
        MsgFrame msgFrame = frameMapForSended.get(sequence);
        //L.e("--->准备重新发--sequence:"+sequence);
        if(null != msgFrame){
            //L.e("--->重新发了次数据--sequence:"+sequence);
            manager.write(msgFrame, false);
        }
    }

    //对时操作
    public static void checkTime() {
    	
    	/*Calendar cd = Calendar.getInstance();
    	cd.setTimeInMillis(System.currentTimeMillis());
		cd.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		Calendar cd2 = Calendar.getInstance();
		cd2.set(Calendar.YEAR, cd.get(Calendar.YEAR));
		cd2.set(Calendar.MONTH, cd.get(Calendar.MONTH));
		cd.set(Calendar.DAY_OF_MONTH, cd.get(Calendar.DAY_OF_MONTH));
		cd2.set(Calendar.HOUR, cd.get(Calendar.HOUR));
		cd2.set(Calendar.MINUTE, cd.get(Calendar.MINUTE));
		cd2.set(Calendar.SECOND, cd.get(Calendar.SECOND));
		cd2.set(Calendar.MILLISECOND, cd.get(Calendar.MILLISECOND));*/
    	long standardTime = System.currentTimeMillis() - 7*60*60*1000;
    	
		int currentSecond = (int) (standardTime / 1000); //距离1970年的秒数，GMT时间
        byte[] sencondBytes = ByteUtils.createIntToByteArray(currentSecond, 4);
        sencondBytes = ByteUtils.reverseSort(sencondBytes); //高低位取反


        byte[] dataBytes = new byte[CHECK_TIME.length + sencondBytes.length];
        int position = 0;
        System.arraycopy(CHECK_TIME, 0, dataBytes, position, CHECK_TIME.length);
        position = position + CHECK_TIME.length;
        System.arraycopy(sencondBytes, 0, dataBytes, position, sencondBytes.length);


        int sequence = markSequence(CHECK_TIME_COMMAND);

        MsgFrame msgFrame = new MsgFrame(CONTROLL_APP2HUB_REQ, dataBytes, sequence);
        manager.write(msgFrame, false);
        frameMapForSended.put(sequence,msgFrame);
    }


    /**
     * 设置送信 命令
     *
     * @param bluetoothPwd
     * @param bright
     */
    public static void sendSetMessage(String bluetoothPwd, int bright) {
        byte[] pwdBytes = null;
        if (TextUtils.isEmpty(bluetoothPwd)) {
            byte[] dataBytes1 = ByteUtils.createIntToByteArray(255, 1);
            pwdBytes = new byte[]{dataBytes1[0], dataBytes1[0], dataBytes1[0], dataBytes1[0]};
        } else {
            pwdBytes = bluetoothPwd.getBytes();
        }
        //pwdBytes = ByteUtils.reverseSort(pwdBytes);

        byte[] brightBytes = ByteUtils.createIntToByteArray(bright, 2);
        brightBytes = ByteUtils.reverseSort(brightBytes);


        byte[] dataBytes = new byte[SET_MESSAGE.length + pwdBytes.length + brightBytes.length];
        int position = 0;
        System.arraycopy(SET_MESSAGE, 0, dataBytes, position, SET_MESSAGE.length);
        position = position + SET_MESSAGE.length;
        System.arraycopy(pwdBytes, 0, dataBytes, position, pwdBytes.length);
        position = position + pwdBytes.length;
        System.arraycopy(brightBytes, 0, dataBytes, position, brightBytes.length);


        int sequence = markSequence(SET_MESSAGE_COMMAND);
        MsgFrame msgFrame = new MsgFrame(CONTROLL_APP2HUB_REQ, dataBytes, sequence);
        manager.write(msgFrame, false);
        frameMapForSended.put(sequence,msgFrame);
    }

    /**
     * 立即体检命令
     */
    public static void immediaExperience(){
    	 int sequence = markSequence(IMMEDIA_EXPERIENCE_COMMAND);
         MsgFrame msgFrame = new MsgFrame(CONTROLL_APP2HUB_REQ, IMMEDIA_EXPERIENCE, sequence);
         manager.write(msgFrame, false);
        frameMapForSended.put(sequence,msgFrame);
    }

    /**
     * app给hub的 正常响应 应答，只包含一个0X00
     * @param sequence
     */
    public static void responseSuccessCode(int sequence){
    	 MsgFrame msgFrame = new MsgFrame(CONTROLL_APP2HUB_RES, new byte[]{0x00} ,sequence);
         manager.write(msgFrame, false);
    }

    /**
     * 获取驾驶评价的数据
     */
    public static void getDriveScore(){
    	int sequence = markSequence(DRIVE_SCORE_COMMAND);
    	MsgFrame msgFrame = new MsgFrame(CONTROLL_APP2HUB_REQ, DRIVE_SCORE, sequence);
    	manager.write(msgFrame, false);
    }


    /**
     * 设置常时 常时送信指令
     */
    public static void setFrequenceSendMsg(int maneuvertype, int distance){
        FrequenceMsg frequenceMsg = HubApp.getInstance().getFrequenceMsg();
        byte[] data = new byte[19]; //准备的数据
        int position = 0;
        System.arraycopy(SET_NAVI, 0, data, position, SET_NAVI.length);  //COMMAND ID 命令字
        position = position + SET_NAVI.length;






        /*
        boolean isNaving = HubApp.getInstance().isNaving();

        isNaving = false;

        if(!isNaving){ //若不在导航中，，相关的数据要清为0XFF了
            frequenceMsg.setManeuverType(0XFF);
            frequenceMsg.setManeuverDistance(0XFFFF);
            frequenceMsg.setLimitSpeed(0XFF);
            frequenceMsg.setLimitDistance(0XFFFF);
            frequenceMsg.setTunnel(0XFF);
            frequenceMsg.setCameraDistance(0xFFFF);
            frequenceMsg.setDestinationDistance(0XFFFFFFFF);
            //frequenceMsg.setExit(0XFF);
            //frequenceMsg.setUnReadMsgOrTel(0XFF);
            //frequenceMsg.setGpsSeepd(0XFFFF);
        }
        */


        frequenceMsg.setManeuverType(maneuvertype);
        frequenceMsg.setManeuverDistance(distance);
        frequenceMsg.setLimitSpeed(0XFF);
        frequenceMsg.setLimitDistance(0XFFFF);
        frequenceMsg.setTunnel(0XFF);
        frequenceMsg.setCameraDistance(0xFFFF);
        frequenceMsg.setDestinationDistance(0XFFFFFFFF);


        byte[] maneuverTypes = ByteUtils.createIntToByteArray(frequenceMsg.getManeuverType(), 1); //转机动点类型数据
        System.arraycopy(maneuverTypes, 0, data, position, maneuverTypes.length);  //机动点类型
        position = position + maneuverTypes.length;
        //L.e("-->maneuverTypes length:"+maneuverTypes.length+" position:"+position);

        byte[] maneuverDistances = ByteUtils.createIntToByteArray(frequenceMsg.getManeuverDistance(), 2);
        maneuverDistances = ByteUtils.reverseSort(maneuverDistances);
        System.arraycopy(maneuverDistances, 0, data, position, maneuverDistances.length);  //机动点距离
        position = position + maneuverDistances.length;
        //L.e("maneuverDistances----maneuverDistances:" + frequenceMsg.getManeuverDistance() + " maneuverDistances:" + ByteUtils.toHexString(maneuverDistances,0,maneuverDistances.length));
        //CommonUtils.writeLogDate("maneuverDistances----maneuverDistances:" + frequenceMsg.getManeuverDistance() + " maneuverDistances:" + ByteUtils.toHexString(maneuverDistances,0,maneuverDistances.length));

        byte[] limitSpeeds = ByteUtils.createIntToByteArray(frequenceMsg.getLimitSpeed(), 1);
        System.arraycopy(limitSpeeds, 0, data, position, limitSpeeds.length);  //限速值
        position = position + limitSpeeds.length;
        //CommonUtils.writeLogDate("setFrequenceSendMsg----frequenceMsg.getLimitSpeed():" + frequenceMsg.getLimitSpeed() + " limitSpeeds:" + ByteUtils.toHexString(limitSpeeds,0,limitSpeeds.length));
        //L.e("-->limitSpeeds length:"+limitSpeeds.length+" position:"+position);

        byte[] limitDistances = ByteUtils.createIntToByteArray(frequenceMsg.getLimitDistance(), 2);
        limitDistances = ByteUtils.reverseSort(limitDistances);
        System.arraycopy(limitDistances, 0, data, position, limitDistances.length);  //距限速路口距离
        position = position + limitDistances.length;
        //L.e("-->limitDistances length:"+limitDistances.length+" position:"+position);

        byte[] tunnels = ByteUtils.createIntToByteArray(frequenceMsg.getTunnel(), 1);
        System.arraycopy(tunnels,0,data,position,tunnels.length); //进出隧道
        position = position + tunnels.length;
        //L.e("-->tunnels length:"+tunnels.length+" position:"+position);

        byte[] cameraDistances = ByteUtils.createIntToByteArray(frequenceMsg.getCameraDistance(), 2);
        cameraDistances = ByteUtils.reverseSort(cameraDistances);
        System.arraycopy(cameraDistances,0,data,position,cameraDistances.length); //距监控摄像距离
        position = position + cameraDistances.length;
        //L.e("-->cameraDistances length:"+cameraDistances.length+" position:"+position);

        byte[] destinationDistances = ByteUtils.createIntToByteArray(frequenceMsg.getDestinationDistance(), 4);
        destinationDistances = ByteUtils.reverseSort(destinationDistances);
        System.arraycopy(destinationDistances,0,data,position,destinationDistances.length); //距目的地距离
        position = position + destinationDistances.length;
        //L.e("-->destinationDistances length:"+destinationDistances.length+" position:"+position);

        byte[] exits = ByteUtils.createIntToByteArray(frequenceMsg.getExit(), 1);
        System.arraycopy(exits,0,data,position,exits.length); //导航结束/app退出
        position = position + exits.length;
        //L.e("-->exits length:"+exits.length+" position:"+position);

        byte[] unReadMsgOrTels = ByteUtils.createIntToByteArray(frequenceMsg.getUnReadMsgOrTel(), 1);
        System.arraycopy(unReadMsgOrTels,0,data,position,unReadMsgOrTels.length); //未读短信及未接电话状态
        position = position + unReadMsgOrTels.length;
        //CommonUtils.writeLogDate(" 未读短信及未接电话状态是："+frequenceMsg.getUnReadMsgOrTel());
        //L.e("-->unReadMsgOrTels length:"+unReadMsgOrTels.length+" position:"+position);

        byte[] gpsSpeeds = ByteUtils.createIntToByteArray(frequenceMsg.getGpsSeepd(), 2);
        gpsSpeeds = ByteUtils.reverseSort(gpsSpeeds);
        System.arraycopy(gpsSpeeds,0,data,position,gpsSpeeds.length); //GPS速度
        //L.e("INDEX", "GPS速度导航" + frequenceMsg.getGpsSeepd());
        //L.e("INDEX", "GPS速度" + gpsSpeeds);

        int sequence = markSequence(SET_NAVI_COMMAND);
        MsgFrame msgFrame = new MsgFrame(CONTROLL_APP2HUB_REQ, data, sequence);
        byte[] commandMsg = msgFrame.getCommandMsg();
        CommonUtils.writeLogDate(" app常时送信发出的内容：" + ByteUtils.toHexString(commandMsg, 0, commandMsg.length));
        manager.write(msgFrame, false);
        //L.e("INDEX", "常时送信 ");
    }

    /**
     * 设置送信 命令
     *
     * @param msg TextUtils.isEmpty(msg)表示导航正在导航,且导航信息为变
     */
    public static void sendNaviMessage(String msg) {
        byte[] data = new byte[17];
        boolean isEmptyMsg;
        Log.e("msg:",msg);
        if ((isEmptyMsg = (TextUtils.isEmpty(msg)||msg.contains("导航准备就绪")))|| (msg.contains("结束")&&!msg.contains("导航结束"))) {
            if (isEmptyMsg) {
                data[15] = ByteUtils.createIntToByteArray(0x0, 1)[0];
            } else {
                data[15] = ByteUtils.createIntToByteArray(0x1, 1)[0];
            }
            data[2] = ffStr1[0];
            data[3] = ffStr2[1];
            data[4] = ffStr2[0];
            data[5] = ffStr1[0];
            data[6] = ffStr2[1];
            data[7] = ffStr2[0];
            data[8] = ffStr1[0];
            data[9] = ffStr2[1];
            data[10] = ffStr2[0];
            data[11] = ffStr4[3];
            data[12] = ffStr4[2];
            data[13] = ffStr4[1];
            data[14] = ffStr4[0];
        } else {
            data[15] = ByteUtils.createIntToByteArray(0, 1)[0];
            int distance = (int) ChineseNumber2DigitUtil.distance2Digit(msg);
            if (msg.contains("限速")) {
                data[5] = ByteUtils.createIntToByteArray((int) (ChineseNumber2DigitUtil.distance2Digit(msg.substring(msg.indexOf("限速")))/1000), 1)[0];
                byte[] speedDistance = ByteUtils.createIntToByteArray(distance, 2);
                data[6] = speedDistance[1];
                data[7] = speedDistance[0];
                data[2]=ffStr1[0];
                data[3] = ffStr2[1];
                data[4] = ffStr2[0];
            } else {
                int forward = 0xFF;
                if (msg.contains("掉头")) {
                    forward = 6;
                } else if (msg.contains("左转")) {
                    forward = 1;
                } else if (msg.contains("左前方")) {
                    forward = 2;
                } else if (msg.contains("直行")) {
                    forward = 3;
                } else if (msg.contains("右前方")) {
                    forward = 4;
                } else if (msg.contains("右转")) {
                    forward = 5;
                } else if (msg.contains("经过环岛左转")) {
                    forward = 7;
                } else if (msg.contains("经过环岛直行")) {
                    forward = 8;
                } else if (msg.contains("经过环岛右转")) {
                    forward = 9;
                }
                data[2] = ByteUtils.createIntToByteArray(forward, 1)[0];
//                data[2] = ByteUtils.createIntToByteArray(0xFF == forward ? 3 : forward, 1)[0];
                byte[] hexStr = ByteUtils.createIntToByteArray(distance, 2);
                if (0xFF != forward) {
                    data[3] = hexStr[1];
                    data[4] = hexStr[0];
                } else {
                    data[3] = ffStr2[1];
                    data[4] = ffStr2[0];
                }
                data[5] = ffStr1[0];
                data[6] = ffStr2[1];
                data[7] = ffStr2[0];
            }
            if (msg.contains("进入隧道")) {
                data[8] = ByteUtils.createIntToByteArray(0x01, 1)[0];
            } else {
                data[8] = ByteUtils.createIntToByteArray(0x00, 1)[0];
            }
            if (msg.contains("摄像")) {
                byte[] hexStr = ByteUtils.createIntToByteArray(distance, 2);
                data[9] = hexStr[1];
                data[10] = hexStr[0];
            } else {
                data[9] = ffStr2[1];
                data[10] = ffStr2[0];
            }
            if (msg.contains("目的地")) {
                byte[] hexStr = ByteUtils.createIntToByteArray(distance, 4);
                data[11] = hexStr[3];
                data[12] = hexStr[2];
                data[13] = hexStr[1];
                data[14] = hexStr[0];
            } else {
                data[11] = ffStr4[3];
                data[12] = ffStr4[2];
                data[13] = ffStr4[1];
                data[14] = ffStr4[0];
            }
        }
        data[0] = command[1];
        data[1] = command[0];
        int unreadState = 0x00;
        if ((HubApp.getInstance().getUnCallNum() > 0 && HubApp.getInstance().getUnReadSmsNum() <= 0)) {
            unreadState = 0x01;
        } else if ((HubApp.getInstance().getUnCallNum() <= 0 && HubApp.getInstance().getUnReadSmsNum() > 0)) {
            unreadState = 0x02;
        } else if ((HubApp.getInstance().getUnCallNum() > 0 && HubApp.getInstance().getUnReadSmsNum() > 0)) {
            unreadState = 0x03;
        }
        data[16] = ByteUtils.createIntToByteArray(unreadState, 1)[0];
        int sequence = markSequence(SET_NAVI_COMMAND);
        MsgFrame msgFrame = new MsgFrame(CONTROLL_APP2HUB_REQ, data, sequence);
        manager.write(msgFrame, false);

    }

    /**
     * 获取sequence并记录下与命令id对应表
     *
     * @param commandId
     * @return
     */
    private static int markSequence(int commandId) {
        HubApp hubApp = HubApp.getInstance();
        hubApp.generateSequence();
        int sequence = hubApp.getSequence();
        Map<Integer, Integer> sequence2CommmandMap = manager.getSequence2CommmandMap();
        sequence2CommmandMap.put(sequence, commandId);
        return sequence;
    }
}
