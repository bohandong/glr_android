package com.sheep.hub.bluetooth;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * 相关的byte工具类
 * @author chensf
 *
 */
public class ByteUtils
{
    /**
     * 数字2
     *
    public static final int NUM_2 = 2;
    
    /**
     * 数字4
     */
    public static final int NUM_4 = 4;
    
    /**
     * 数字8
     */
    public static final int NUM_8 = 8;
    
    /**
     * 数字255
     */
    public static final int HEX_255 = 0xff;
    
    /**
     * 将int转换成byte数组
     * 
     * @param value IntValue
     * @param length 长度
     * @return byte[] 字节数组
     */
    public static byte[] createIntToByteArray(int value, int length)
    {
        byte[] b = new byte[length];
        int tmp = 0;
        for (int i = 0; i < length; i++)
        {
            if (i == length - 1)
            {
                b[i] = (byte)(value & HEX_255);
            }
            else
            {
                tmp = value >> (NUM_8 * (length - 1 - i));
                b[i] = (byte)tmp;
                value -= tmp << (NUM_8 * (length - 1 - i));
            }
        }
        
        return b;
    }
    
    /**
     * 取data区域的补数
     * @param data
     * @return
     */
    public static byte[] complementArray(byte[] data) {
		byte[] retValue = new byte[data.length];
		for (int i = 0; i < data.length; i++) {
			int j = 0xff - data[i];
			if (0xff == j) { // 若此是0xff的
				retValue[i] = (byte) 0xff;
			} else {
				byte temp = (byte) (((j & 0xff) + 0x01) & 0xff); // 取补数是按反取反再加1
				retValue[i] = temp;
			}
		}
		return retValue;
	}
    
    /**
     * 将所有的byte字节 相加 保留一位byte，取补数
     * @param lists
     * @return
     */
    public static byte[] getBccComplement(List<byte[]> lists){
    	byte tempB = 0;
    	for(byte[] datas :lists){
    		byte tempA = 0 ;
    		for(byte b : datas){
    			tempA = (byte)((tempA + b)&0xff);
    		}
    		tempB = (byte)((tempB +tempA)&0xff);
    	}
    	//tempB  为所有的byte 相加的结果，再需要进行取补数，只需要一位即可 //bcc补数只有一位
    	return complementArray(new byte[]{tempB});
    }
    
    /**
     * 高低位取反
     * @param datas
     * @return
     */
    public static byte[] reverseSort(byte[] datas){
		byte[] retValues = new byte[datas.length];
		int j = 0 ;
		for (int i = datas.length -1 ; i >= 0; i--) {
			retValues[j] = datas[i];
			j++;
		}
		
		return retValues;
	}
    
    /**
     * 将byte数组转换成int
     * @param b byte数组
     * @return 转换后的整数
     */
    public static int parseByteToInt(byte[] b)
    {
        int length = b.length;
        int val = 0;
        for (int i = 0; i < length; i++)
        {
            if (i == length - 1)
            {
                val |= b[i] & HEX_255;
            }
            else
            {
                val |= (b[i] & HEX_255) << (length - 1 - i) * NUM_8;
            }
        }
        return val;
    }
    
    
    
    /**
     * 生成哈希值，取底8位转16进制   
     * @param data String
     * @return String
     */
    public static String toHA3(String data, String ha)
    {
        MessageDigest m = null;
        try
        {
            m = MessageDigest.getInstance(ha);
            m.update(data.getBytes("UTF-8"));
        }
        catch (NoSuchAlgorithmException ex1)
        {
        }
        catch (UnsupportedEncodingException ex)
        {
        }
        return toHex(m.digest()).toUpperCase();
    }
    
    /**
     * 把传来的字节数组取底8位转换到十六进制表示的字符串,并返回.
     * @param buffer byte[]
     * @return String
     */
    private static String toHex(byte[] buffer)
    {
        String result = "";
        for (int i = 0; i < buffer.length; i++)
        {
            result += Integer.toHexString((0x000000ff & buffer[i]) | 0xffffff00).substring(6);
        }
        return result;
    }
    
    /**
     * 将传过来的字节数组转成十六进制表示的字符串
     * @param datas
     * @param startIndex
     * @param length
     * @return
     */
    public static String toHexString(byte[] datas,int startIndex,int length){
    	byte[] newDatas = new byte[length];
    	System.arraycopy(datas, startIndex, newDatas, 0, length);
    	StringBuffer sb = new StringBuffer();
    	for (byte b : newDatas) {
    		sb.append(bytetoHexStr(b));
		}
    	return sb.toString();
    }
    
    /**
     * 
     * @param b
     * @return
     */
    public static String bytetoHexStr(byte b)
    {
        String s = Integer.toHexString(((int)b) & 0x000000FF).toUpperCase();
        if (s.length() == 1)
        {
            s = "0" + s;
        }
        return s;
    }
}
