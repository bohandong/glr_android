package com.sheep.hub.bluetooth;

import java.util.ArrayList;
import java.util.List;

import com.sheep.hub.HubApp;
/**
 * 命令数据包 封装类
 * @author chensf
 *
 */
public class MsgFrame {

	private static byte header = 0x02; //命令开始标志
	private static byte end = 0x03; //命令结束标志
	
	private int controlId ;  //控制位 
	private int commandId;  //命令字
	
	private byte[] data;
	private int dataLen; //data数据的大小
	private int sequence; //序列号
	
	public MsgFrame(int controlId,byte[] data,int sequence){
		this.controlId = controlId;
		this.data = data;
		this.sequence = sequence;
	}
	
	public static byte getHeader() {
		return header;
	}

	public static byte getEnd() {
		return end;
	}
	
	public byte[] getData() {
		return data;
	}

	public int getSequence() {
		return sequence;
	}

	/**
	 * 获取命令中 除开始及结束标志位外的数据
	 * @return
	 */
	private byte[] getContentMsg(){
		if(null == data){
			data = new byte[0];
		}
		this.dataLen = data.length;
		
		
		int intDateSize = 2 + 2 + 1 + dataLen + 1; //data size补数 + sequence号 + 控制ID + data数据大小 + BCC补数
		byte[] dataSizeBytes = ByteUtils.createIntToByteArray(intDateSize, 2);
		byte[] dataSizeComplementBytes = ByteUtils.complementArray(dataSizeBytes); //取datasize的补数
		dataSizeBytes = ByteUtils.reverseSort(dataSizeBytes); //高低位取反
		dataSizeComplementBytes = ByteUtils.reverseSort(dataSizeComplementBytes);
		byte[] sequenceBytes = ByteUtils.createIntToByteArray(sequence, 2); //sequence号从0x00 01 开始，，还需要高低位取反处理下
		sequenceBytes = ByteUtils.reverseSort(sequenceBytes);
		byte[] controlBytes = ByteUtils.createIntToByteArray(controlId, 1); //只有一位就不需要高低位取反了
		byte[] dataBytes = data; //data是除指令位外，已经进行高低位取反了
		List<byte[]> tempList = new ArrayList<byte[]>();
		tempList.add(dataSizeBytes);
		tempList.add(dataSizeComplementBytes);
		tempList.add(sequenceBytes);
		tempList.add(controlBytes);
		tempList.add(dataBytes);
		byte[] bccComplementBytes = ByteUtils.getBccComplement(tempList); //bcc补数只有一位，不需要高低位取反
		//除开始及结束标志 外数据的长度
		int contentLen = dataSizeBytes.length+dataSizeComplementBytes.length+sequenceBytes.length
				          +controlBytes.length+dataBytes.length+bccComplementBytes.length;
		
		byte[] retValue = new byte[contentLen];
		int position = 0;
		System.arraycopy(dataSizeBytes, 0, retValue, position, dataSizeBytes.length);
		position = position + dataSizeBytes.length;
		System.arraycopy(dataSizeComplementBytes, 0, retValue, position, dataSizeComplementBytes.length);
		position = position + dataSizeComplementBytes.length;
		System.arraycopy(sequenceBytes, 0, retValue, position, sequenceBytes.length);
		position = position + sequenceBytes.length;
		System.arraycopy(controlBytes, 0, retValue, position, controlBytes.length);
		position = position + controlBytes.length;
		System.arraycopy(dataBytes, 0, retValue, position, dataBytes.length);
		position = position + dataBytes.length;
		System.arraycopy(bccComplementBytes, 0, retValue, position, bccComplementBytes.length);
		return retValue;
	}
	
	/**
	 * 获取 命令指令数据
	 * @return
	 */
	public byte[] getCommandMsg(){
		byte[] startBytes = new byte[]{header};
		byte[] endBytes = new byte[]{end};
		byte[] contentBytes = getContentMsg();
		byte[] commandMsg = new byte[startBytes.length+endBytes.length+contentBytes.length];
		int position = 0;
		System.arraycopy(startBytes, 0, commandMsg, position, startBytes.length);
		position = position + startBytes.length;
		System.arraycopy(contentBytes, 0, commandMsg, position, contentBytes.length);
		position = position + contentBytes.length;
		System.arraycopy(endBytes, 0, commandMsg, position, endBytes.length);
		return commandMsg;
	}
}
