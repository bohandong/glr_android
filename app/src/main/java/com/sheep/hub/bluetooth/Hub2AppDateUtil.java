package com.sheep.hub.bluetooth;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.autonavi.tbt.l;
import com.sheep.framework.db.AfeiDb;
import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.HubApp;
import com.sheep.hub.R;
import com.sheep.hub.bean.DriveScore;
import com.sheep.hub.bean.Hitch;
import com.sheep.hub.bean.HitchSheet;

public class Hub2AppDateUtil {
	
	private static final int MAX_STROAGE_HITCH_NUM = 2000; //本地数据中，最多保留2000个故障
	private static final int MAX_STROAGE_DRIVESCORE_NUM = 100;  //最多保存2000次的驾驶评分
	
	private static final String DBFILENAME = "hitch.db";
	private static final String PACKAGENAME = "com.sheep.hub";
	private static String dbDir = "/data"+Environment.getDataDirectory().getAbsolutePath()+"/"+PACKAGENAME;
	private static String dbPath = dbDir+"/databases"+"/"+DBFILENAME;
	 
	public static SQLiteDatabase getHitchSheetDb(){
		InputStream is = HubApp.getInstance().getResources().openRawResource(R.raw.hub);
		File dbPathDir = new File(dbDir);
		if(!dbPathDir.exists()){
			dbPathDir.mkdirs();
		}
		File dbFile = new File(dbPath);
        FileOutputStream fos = null;
		try {
			if(!dbFile.exists()){
				fos = new FileOutputStream(dbFile);
				byte[] buf = new byte[2048];
				int len = -1;
				while((len=is.read(buf))!=-1){
					fos.write(buf, 0, len);
				}
                fos.flush();
				fos.close();
			}
			SQLiteDatabase sqliteDb = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
			return sqliteDb;
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
            try {
                if (null != is){
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return null;
	}
	
	public static ArrayList<HitchSheet> findHitch(SQLiteDatabase db, String hitchCode){
        try{
            if(db!=null&&!db.isOpen()){
                db = getHitchSheetDb();
            }
        }catch (Exception e){
            //L.e("--------->> db.isOpen() 打不开，，");
            db = getHitchSheetDb();
            e.printStackTrace();
        }
		Cursor cursor = db.query("com_sheep_hub_bean_HitchSheet",
								new String[]{"_id","type","code","isSerious","name","englishName","category","detail"}, " code = ? ", 
								new String[]{hitchCode}, null, null, null);
		ArrayList<HitchSheet> hss = new ArrayList<HitchSheet>();
		while(cursor.moveToNext()){
			String _id = cursor.getString(cursor.getColumnIndex("_id"));
			String type = cursor.getString(cursor.getColumnIndex("type"));
			String code = cursor.getString(cursor.getColumnIndex("code"));
			String isSerious = cursor.getString(cursor.getColumnIndex("isSerious"));
			String name = cursor.getString(cursor.getColumnIndex("name"));
			String englishName = cursor.getString(cursor.getColumnIndex("englishName"));
			String category = cursor.getString(cursor.getColumnIndex("category"));         
			String detail = cursor.getString(cursor.getColumnIndex("detail"));
			
			HitchSheet hs = new HitchSheet();
			hs.set_id(_id);
			hs.setType(type);
			hs.setCode(code);
			hs.setIsSerious(isSerious);
			hs.setName(name);
			hs.setEnglishName(englishName);
			hs.setCategory(category);
			hs.setDetail(detail);
			hss.add(hs);
		}
		cursor.close();
		db.close();
		return hss;
	}
	
	/**
	 * hub发给app的故障码数据
	 * @param msgFrame hub发过来的数据
	 */
	public static int  receiveHitch(MsgFrame msgFrame){
		int sequence = msgFrame.getSequence();
		byte[] dataBytes = msgFrame.getData();
		int hitchNum = ByteUtils.parseByteToInt(new byte[]{dataBytes[2]}); //第3位是此故障码的数量
		//从resouce里读取db
		
		//L.e("接收到的故障码的数量是："+hitchNum);
		if(hitchNum > 0){ //存在故障
			AfeiDb afeiDb = HubApp.getInstance().getAfeiDb();
			//ArrayList<Hitch> hitchs = afeiDb.findAllByWhereStr(Hitch.class, "");
			int dbHitchNum = afeiDb.calculateCountByWhereStr(Hitch.class, "");
			
			SQLiteDatabase hitchSheetDb = getHitchSheetDb();
            if(null == hitchSheetDb){
                //L.e("----->> 未返回一个正常的故障码数据库....");
                return 0;
            }

			//杜绝数据库中，已经存在的sequence，尽量已经存在此sequence的概述极小，也删除掉
			if(dbHitchNum>MAX_STROAGE_HITCH_NUM){
				int overNum = dbHitchNum - MAX_STROAGE_HITCH_NUM; //超过数据，要删除的条数
				ArrayList<Hitch> overHitchs = afeiDb.findAllByWhereStrWithOrderAndLimitStr(Hitch.class, "", " time asc " , overNum+"");
				if(null != overHitchs && overHitchs.size()>0){
					Hitch lastOverHitch = overHitchs.get(overHitchs.size()-1); //比这日期早的，全部删除掉。
					afeiDb.deleteByWhereStr(Hitch.class, " < "+lastOverHitch.getTime());
				}
			}
			afeiDb.deleteByWhereStr(Hitch.class, " sequence = '"+sequence+"'");

            String dataByteStr = ByteUtils.toHexString(dataBytes, 0, dataBytes.length);
            CommonUtils.writeLogDate("receiveHitch...dataByteStr:"+dataByteStr);
			for(int i=0;i<hitchNum;i++){
				byte byteType =  dataBytes[3+3*i];
				byte[] codeBytes = {dataBytes[3+3*i+1],dataBytes[3+3*i+2]};
				codeBytes = ByteUtils.reverseSort(codeBytes);//反序一下
				
				String type = hitchType(byteType);
				type = type.toUpperCase();
				String code = ByteUtils.toHexString(codeBytes, 0, codeBytes.length); //转成十六进制
				
				ArrayList<HitchSheet> hitchSheets = findHitch(hitchSheetDb,type+code);

                //每次体验会把前面的都清空的
				//ArrayList<HitchSheet> hitchSheets = afeiDb.findAllByWhereStr(HitchSheet.class, " code = '"+(type+code)+"'");
				Hitch hitch = new Hitch();
				hitch.setSequence(sequence+"");
				hitch.setTime(CommonUtils.date2StringTime(new Date())); //体验时间
				hitch.setCode(type+code);
				if(null != hitchSheets&&hitchSheets.size()>0){
					HitchSheet hs = hitchSheets.get(0);
					hitch.setType(hs.getType());
					hitch.setIsSerious(hs.getIsSerious());
					hitch.setName(hs.getName());
					hitch.setEnglishName(hs.getEnglishName());
					hitch.setCategory(hs.getCategory());
					hitch.setDetail(hs.getDetail());
				}else{
					hitch.setType(type);
				}
				// 判断 数据是否过大，过大就删除之前的数据
				afeiDb.save(hitch); //将数据存起来
                CommonUtils.writeLogDate("receiveHitch...hitch:"+hitch.toString());
			}
		}
        return hitchNum;
	}
	
	
	private static String hitchType(byte b){
		String retVal = null;
		switch (b) {
		case 0x01:
			retVal = "P";
			break;
		case 0x02:
			retVal = "B";
			break;
		case 0x03:
			retVal = "C";
			break;
		case 0x04:
			retVal = "U";
			break;
		default:
			break;
		}
		return retVal;
	}

	/**
	 * 解析 驾驶调整分的相关数据 是否ok
	 * @param msgFrame
	 */
	public static boolean receiveDriveScore(MsgFrame msgFrame) {
		boolean retValue = false;
		int sequence = msgFrame.getSequence();
		//add by lvmu 20150731 start
		int drivedistance = 0;
		int costoil = 0;
		byte[] drivedistanceBytes;
		byte[] costoilBytes;
		//add by lvmu 20150731 end

		byte[] dataBytes = msgFrame.getData();
		if(null != dataBytes && dataBytes.length > 24){ //有相关的数据了
			//先处理检查下，数据库的数据是否大于2000条了，有了，则删除部分
			AfeiDb afeiDb = HubApp.getInstance().getAfeiDb();
			int driveScoreNum = afeiDb.calculateCountByWhereStr(DriveScore.class, "");
			if(driveScoreNum > MAX_STROAGE_DRIVESCORE_NUM){
				int overTime = driveScoreNum - MAX_STROAGE_DRIVESCORE_NUM;
				ArrayList<DriveScore> driveScores = afeiDb.findAllByWhereStrWithOrderAndLimitStr(DriveScore.class, "", " time asc", overTime+"");
				DriveScore firstDelDriveScore = driveScores.get(driveScores.size()-1);
				afeiDb.deleteByWhereStr(DriveScore.class, " time < "+firstDelDriveScore.getTime());
			}

			//杜绝数据库中，已经存在的sequence，尽量已经存在此sequence的概述极小，也删除掉
			//afeiDb.deleteByWhereStr(DriveScore.class, " sequence = '"+sequence+"'");

            String dataByteStr = ByteUtils.toHexString(dataBytes, 0, dataBytes.length);
            //L.e("接收到的驾驶评分数据为receiveDriveScore："+dataByteStr);

			//第3位是驾驶得分，只占一位字符，不需要反序
			int driveScore = ByteUtils.parseByteToInt(new byte[]{dataBytes[2]});  //0~1是命令字的位置
			//开始驾驶时间占用4byte，从第4位开始，需要反序
			byte[] startTimeBytes = new byte[]{dataBytes[6],dataBytes[5],dataBytes[4],dataBytes[3]};
			int intStartTime = ByteUtils.parseByteToInt(startTimeBytes);  //然后秒级
            //L.e("---------->intStartTime::"+intStartTime+" startTimeBytes:"+ByteUtils.toHexString(startTimeBytes,0,startTimeBytes.length));
            String startTime = getDateTimeStr(intStartTime);

			byte[] endTimeBytes = new byte[]{dataBytes[10],dataBytes[9],dataBytes[8],dataBytes[7]};
			int intEndTime = ByteUtils.parseByteToInt(endTimeBytes);
			String endTime = getDateTimeStr(intEndTime);
					
			byte[] emergAddSpeedBytes = new byte[]{dataBytes[12],dataBytes[11]};
			int emergAddSpeed = ByteUtils.parseByteToInt(emergAddSpeedBytes);
			
			byte[] emergSubSpeedBytes = new byte[]{dataBytes[14],dataBytes[13]};
			int emergSubSpeed = ByteUtils.parseByteToInt(emergSubSpeedBytes);
			
			byte[] emptyRunTimeBytes = new byte[]{dataBytes[18],dataBytes[17],dataBytes[16],dataBytes[15]};
			int emptyRunTime = ByteUtils.parseByteToInt(emptyRunTimeBytes);
			
			byte[] overSpeedTimeBytes = new byte[]{dataBytes[22],dataBytes[21],dataBytes[20],dataBytes[19]};
			int overSpeedTime = ByteUtils.parseByteToInt(overSpeedTimeBytes);
            //L.e("------>overSpeedTime::"+overSpeedTime+" overSpeedTimeBytes:"+ByteUtils.toHexString(overSpeedTimeBytes,0,overSpeedTimeBytes.length));
			
			byte[] avgCostOilBytes = new byte[]{dataBytes[26],dataBytes[25],dataBytes[24],dataBytes[23]};
			int avgCostOil = ByteUtils.parseByteToInt(avgCostOilBytes);

			//add by lvmu 20150731 start
			if( dataBytes.length > 28 ) {
				drivedistanceBytes = new byte[]{dataBytes[30], dataBytes[29], dataBytes[28], dataBytes[27]};
				drivedistance = ByteUtils.parseByteToInt(drivedistanceBytes);
				costoilBytes = new byte[]{dataBytes[34], dataBytes[33], dataBytes[32], dataBytes[31]};
				costoil = ByteUtils.parseByteToInt(costoilBytes);
			}
			//add by lvmu 20150731 end
			
			DriveScore ds = new DriveScore();
			ds.setSequence(sequence + "");
			ds.setTime(CommonUtils.date2StringTime(new Date()));
			ds.setStartTime(startTime);
			ds.setEndTime(endTime);
			ds.setDriveScore(driveScore + "");
			ds.setEmerAddSpeedTime(emergAddSpeed + "");
			ds.setEmerSubSpeedTime(emergSubSpeed + "");
			ds.setEmptyRunningTime(emptyRunTime + "");
			ds.setOverSpeedTime(overSpeedTime + "");
			ds.setAvgCostOil(avgCostOil + "");

			//add by lvmu 20150731 start
			if( dataBytes.length > 28 ) {
				ds.setDrivedistance(drivedistance + "");
				ds.setCostoil(costoil + "");
				if( drivedistance!= 0 && ((costoil * 10000 / drivedistance) < 10000 ) ) {
					ds.setAvgCostOil(costoil * 10000 / drivedistance + "");
				}
			}
			//add by lvmu 20150731 end

			CommonUtils.writeLogDate(ds.getDrivedistance());

            ArrayList<DriveScore> tempDss = afeiDb.findAllByWhereStr(DriveScore.class, "startTime='" + ds.getStartTime() + "'");
            if(null == tempDss || tempDss.size()<=0){
                afeiDb.save(ds);
            }else{
                afeiDb.deleteByWhereStr(DriveScore.class, "startTime='" + ds.getStartTime() + "'");//将上次的数据删除掉
                afeiDb.save(ds);
            }

			retValue = true;
            //L.e("接收到的驾驶评分数据为ds.toString()："+ds.toString());
            CommonUtils.writeLogDate("收到的驾驶评分数据为"+dataByteStr+" ---解析后为："+ds.toString());
        }
		return retValue ;
	}
	
	
	public static void receiveDriveScoreOld(MsgFrame msgFrame) {
		int sequence = msgFrame.getSequence();
		byte[] dataBytes = msgFrame.getData();
		if(null != dataBytes && dataBytes.length > 24){ //有相关的数据了
			//先处理检查下，数据库的数据是否大于2000条了，有了，则删除部分
			AfeiDb afeiDb = HubApp.getInstance().getAfeiDb();
			int driveScoreNum = afeiDb.calculateCountByWhereStr(DriveScore.class, "");
			if(driveScoreNum > MAX_STROAGE_DRIVESCORE_NUM){
				int overTime = driveScoreNum - MAX_STROAGE_DRIVESCORE_NUM;
				ArrayList<DriveScore> driveScores = afeiDb.findAllByWhereStrWithOrderAndLimitStr(DriveScore.class, "", " time asc", overTime+"");
				DriveScore firstDelDriveScore = driveScores.get(driveScores.size()-1);
				afeiDb.deleteByWhereStr(DriveScore.class, " time < "+firstDelDriveScore.getTime());
			}
			
			//杜绝数据库中，已经存在的sequence，尽量已经存在此sequence的概述极小，也删除掉
			afeiDb.deleteByWhereStr(DriveScore.class, " sequence = '"+sequence+"'");
			//第3位是驾驶得分，只占一位字符，不需要反序
			int driveScore = ByteUtils.parseByteToInt(new byte[]{dataBytes[2]});
			//L.e("---->> driveScore::"+driveScore + " ...:"+ByteUtils.toHexString(new byte[]{dataBytes[2]}, 0, 1));
			//开始驾驶时间占用4byte，从第4位开始，需要反序
			byte[] startTimeBytes = new byte[]{dataBytes[6],dataBytes[5],dataBytes[4],dataBytes[3]};
			int intStartTime = ByteUtils.parseByteToInt(startTimeBytes);  //然后秒级
			String startTime = getDateTimeStr(intStartTime);
			
			byte[] endTimeBytes = new byte[]{dataBytes[10],dataBytes[9],dataBytes[8],dataBytes[7]};
			int intEndTime = ByteUtils.parseByteToInt(endTimeBytes);
			String endTime = getDateTimeStr(intEndTime);
					
			byte[] emergAddSpeedBytes = new byte[]{dataBytes[12],dataBytes[11]};
			int emergAddSpeed = ByteUtils.parseByteToInt(emergAddSpeedBytes);
			
			byte[] emergSubSpeedBytes = new byte[]{dataBytes[14],dataBytes[13]};
			int emergSubSpeed = ByteUtils.parseByteToInt(emergSubSpeedBytes);
			
			byte[] emptyRunTimeBytes = new byte[]{dataBytes[18],dataBytes[17],dataBytes[16],dataBytes[15]};
			int intEmptyRunTime = ByteUtils.parseByteToInt(emptyRunTimeBytes);
			String emptyRunTime = getDateTimeStr(intEmptyRunTime);
			
			byte[] overSpeedTimeBytes = new byte[]{dataBytes[22],dataBytes[21],dataBytes[20],dataBytes[19]};
			int intOverSpeedTime = ByteUtils.parseByteToInt(overSpeedTimeBytes);
			String overSpeedTime = getDateTimeStr(intOverSpeedTime);
			
			byte[] avgCostOilBytes = new byte[]{dataBytes[26],dataBytes[25],dataBytes[24],dataBytes[23]};
			int avgCostOil = ByteUtils.parseByteToInt(avgCostOilBytes);
			
			DriveScore ds = new DriveScore();
			ds.setSequence(sequence+"");
			ds.setTime(CommonUtils.date2StringTime(new Date()));
			ds.setStartTime(startTime);
			ds.setEndTime(endTime);
			ds.setDriveScore(driveScore+"");
			ds.setEmerAddSpeedTime(emergAddSpeed+"");
			ds.setEmerSubSpeedTime(emergSubSpeed+"");
			ds.setEmptyRunningTime(emptyRunTime);
			ds.setOverSpeedTime(overSpeedTime);
			ds.setAvgCostOil(avgCostOil+"");
			afeiDb.save(ds);
		}
		
		
	}


	/**
	 * time为从1970年1月1号0时0分0秒开始到现在的秒数
	 * @param time
	 * @return
	 */
	private static String getDateTimeStr(int time) {
        long ltime = time-8*60*60;
        String stime = String.valueOf(ltime)+"000";
		/*long milliseconds = time * 1000;
		Date date = new Date(milliseconds);*/
		String date2StringTime = CommonUtils.date2StringTime(stime);
		return date2StringTime;
	}
}
