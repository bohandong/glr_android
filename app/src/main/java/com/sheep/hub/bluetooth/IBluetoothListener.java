package com.sheep.hub.bluetooth;

/**
 * 蓝牙相关状态改变的监听器
 * @author chensf
 *
 */
public interface IBluetoothListener {

	/**
	 * 状态变更 的
	 * @param state
	 * @param what
	 */
	public void onStateChange(int state,int what);
	/**
	 * 扫描蓝牙设备中，出现的设备名称及蓝牙mac地址
	 * @param name
	 * @param address
	 * @param bondState 此adress终端的 绑定状态
	 */
	public void onScanningDevice(String name,String address,int bondState);
}
