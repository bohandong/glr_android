package com.sheep.hub.bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;

import com.sheep.framework.log.L;
import com.sheep.framework.util.CommonUtils;
import com.sheep.hub.HubApp;

/**
 * 蓝牙
 * @author chensf
 *
 */
public class BluetoothManager {

	//add by lvmu 20150610 start
	//public BluetoothDevice glrhud_device;
	//public boolean remove_glrhud = false;
	//add by lvmu 20150610 end


	public static final String BLUETOOTH_NAME = "GLR-HUD";

    private static UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private static final String PIN = "1234";
	private BluetoothAdapter bluetoothAdpater ;
	private Handler handler ;  //用于计时，超时发执行此某线程do something
	private BluetoothBroadcastreceiver receiver; //监听蓝牙相关状态的广播
	private int lastState ;  //上次蓝牙相关状态
	private List<IBluetoothListener> listenerList = new ArrayList<IBluetoothListener>();
	
	public static final int STATE_BLUETOOTH_NO_SUPPORT = 0; //手机不支持蓝牙功能
	public static final int STATE_BLUETOOTH_CANNOT_ENABLE = 1;//手机处于飞行模式等，开启enable不了蓝牙
	public static final int STATE_DEVICE_DISCOVERING = 2; //正在搜索中...
	public static final int STATE_DISCOVERED = 3; //蓝牙搜索完成
	public static final int STATE_SETPIN = 4; //
	public static final int STATE_BONDED = 5; //绑定中
	public static final int STATE_BLUETOOTH_PAIRE_FAILURE = 6; //蓝牙匹配失败
	//add by lvmu 20150620 start
	public static final int STATE_BLUETOOTH_SHUT_DOWN_HAND = 66; //手动关蓝牙||手动取消配对
	public static final int STATE_FIRST_KIDOU_BT_OPEN = 67;//第一次启动，开蓝牙
	//add by lvmu 20150620 end
	public static final int STATE_CONNECTED = 7; //连接管道成功
	public static final int STATE_CHECKTIMEING = 8; //连接成功后，发送对时中...
	public static final int STATE_NOTFOUND = 9; //未找到相应的终端设备
	
	public static final int STATE_BLUETOOTH_DISCONNECT = 12; //蓝牙连接断开
	public static final int STATE_BLUETOOTH_OFF = 13; //手机蓝牙功能关闭
	public static final int STATE_OVERTIME_RES = 14; //应答帧响应超时
	
	
	public static final int STATE_CHECKTIME_SUCCESS = 10; //对时成功 -成功响应
	public static final int STATE_CHECKTIME_FAILURE = 11; //对时失败 -异常响应
	
	public static final int STATE_BLUETOOTH_SETMESSAGE_SUCCESS = 15; //设置送信成功
	public static final int STATE_BLUETOOTH_SETMESSAGE_FAILURE = 16; //设置送信失败
    public static final int STATE_BLUETOOTH_SETMESSAGE_OBDCONING = 29; //设置信息,但OBD正在连接中
	
	public static final int STATE_IMMEDIA_EXPERIENCE_SUCCESS = 17; //立即体验 响应成功，等0x05故障码数据
	public static final int STATE_IMMEDIA_EXPERIENCE_FAILURE = 18; //立即体验 响应异常 不在怠速状态，无法体检
	public static final int STATE_IMMEDIA_EXPERIENCE_NOTCONT_OBD = 19; //立即体验 obd未连接
	
	public static final int STATE_HITCH_SUCCESS = 20; //hub送故障码成功
	public static final int STATE_HITCH_FAILURE = 21; //hub送故障码失败
	
	
	public static final int STATE_DRIVE_SCORE_SUCCESS = 24;
	public static final int STATE_DRIVE_SCORE_FAILURE = 25;

    public static final int STATE_FEQURENCYSENDMSG_SUCCESS = 26;
    public static final int STATE_FEQURENCYSENDMSG_FAILURE = 27;

    public static final int STATE_HITCH_SUCCESS_NOHITCH = 28;

	private static final long REQ_DELAYMILLIS = 20000; //20秒
	private static BluetoothManager bluetoothManager = null;
	private HubApp hubApp;
	private BluetoothDevice remoteDevice; //连接的hub蓝牙设备
	private int retryCount; //重试次数
	private BluetoothSocket socket; //蓝牙套接字流
	private InputStream inputStream; //输入管道流
	private OutputStream outputStream; //输出管道流
	
	private boolean bluetoothConn = false;  //蓝牙是否正常连接状态中
	
	private Map<Integer,Runnable> runnableMap = new HashMap<Integer,Runnable>(); 
	
	//记录 序列号sequence到命令ID commandId的表，用于hub应答时，是对应哪个指令的
	private Map<Integer,Integer> sequence2CommmandMap = new HashMap<Integer, Integer>();
	
	private BluetoothManager(){
		bluetoothAdpater = BluetoothAdapter.getDefaultAdapter();
		handler = new Handler();
		hubApp = HubApp.getInstance();
		registerBroadcast();
	}

	//add by lvmu 20150610 start
	public boolean ispaired_glrhud() throws Exception {

		Set<BluetoothDevice> devices = bluetoothAdpater.getBondedDevices();
		for (BluetoothDevice device : devices) {
			if( BLUETOOTH_NAME.equals(device.getName()) )
			{
				//BluetoothPairUtils.removeBond(device.getClass(), device);
				return true;
			}
		}
		return false;


		//if( glrhud_device.getName() == BLUETOOTH_NAME && remove_glrhud == false ) {
		//	BluetoothPairUtils.removeBond(glrhud_device.getClass(), glrhud_device);
		//	remove_glrhud = true;
		//}
	}
//add by lvmu 20150610 end
//add by lvmu 20150610 start
	public boolean remove_glrhud() throws Exception {

		Set<BluetoothDevice> devices = bluetoothAdpater.getBondedDevices();
		for (BluetoothDevice device : devices) {
			if( BLUETOOTH_NAME.equals(device.getName()) )
			{
				BluetoothPairUtils.removeBond(device.getClass(), device);
				return true;
			}
		}
		return false;
	}
	//add by lvmu 20150610 end
	
	
	/**
	 * 注册广播用于监听接收 来自蓝牙状态改变，绑定及解绑等等状态的监听
	 */
	public void registerBroadcast() {
		receiver = new BluetoothBroadcastreceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		filter.addAction(BluetoothDevice.ACTION_FOUND);
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
		filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
		hubApp.registerReceiver(receiver, filter);//注册广播 
	}
	
	/**
	 * 取消 注册/解绑 广播 
	 */
	public void unregisterReceiver(){
		if(null != receiver){
			hubApp.unregisterReceiver(receiver);
			receiver = null;
		}
	}

	/**
	 * 添加  相关的蓝牙监听
	 * @param listener
	 */
	public synchronized void addListener(IBluetoothListener listener){
		if(!listenerList.contains(listener)){
			listenerList.add(listener);
		}
	}
	
	/**
	 * 删除 相关的蓝牙监听
	 * @param listener
	 */
	public synchronized void removeListener(IBluetoothListener listener){
		listenerList.remove(listener);
	}
	
	/**
	 *  enable开启 手机蓝牙功能
	 */
	public void enableBluetooth(){
		new Thread(){
			@Override
			public void run() {
				if(bluetoothAdpater.isEnabled()){ //是否已经开启了，没有开启则开启下，开启了则搜索查找设备
					searchFindDevice();
				}else{
					boolean enable = bluetoothAdpater.enable(); //enable同时，会收到蓝牙功能被打开的广播
					if(!enable){
						onStateChange(STATE_BLUETOOTH_CANNOT_ENABLE,-1,true);
					}
				}
			}
		}.start();
	}
	
	public static synchronized BluetoothManager getInstance(){
		if(null == bluetoothManager){
			bluetoothManager = new BluetoothManager();
		}
		return bluetoothManager;
	}
	
	
	/**
	 * 蓝牙是否正常连接中..
	 * @return
	 */
	public boolean isBluetoothConn() {
		return bluetoothConn;
	}


	/**
	 * 查找蓝牙终端设备，若之前有记录则去连接。若没有，则用扫描设备去查找
	 */
	private void searchFindDevice() {

		/*if(!TextUtils.isEmpty(hubApp.getPairBluetoothAddress())){
			remoteDevice = bluetoothAdpater.getRemoteDevice(hubApp.getPairBluetoothAddress());
			connect(remoteDevice); //连接蓝牙终端设备
		}else{*/
			if(bluetoothAdpater.isDiscovering()){
				bluetoothAdpater.cancelDiscovery();
			}
			bluetoothAdpater.startDiscovery();
		//}
	}
	
	/**
	 * 将指定的蓝牙地址进行匹配
	 * @param bluetoothAddress
	 */
	//public void pairBluetoothDevice(String bluetoothAddress){
	public boolean pairBluetoothDevice(String bluetoothAddress){
		 if (bluetoothAdpater.isDiscovering())  //取消正在进行的start...
         {
			 bluetoothAdpater.cancelDiscovery();
         }
		 
		BluetoothDevice tempDevice = bluetoothAdpater.getRemoteDevice(bluetoothAddress);
		
		return BluetoothPairUtils.pair(tempDevice);
		//add by lvmu 20150610 start
		//this.glrhud_device = tempDevice;
		//remove_glrhud = false;
		//add by lvmu 20150610 end

	}

    public static boolean shouldUseSecure() {
        if (Build.MANUFACTURER.equals("Xiaomi")) {
            if (Build.MODEL.equals("2013022") && Build.VERSION.RELEASE.equals("4.2.1")) {
                return true;
            }
        }
        if (Build.MODEL.equals("Lenovo A820")) {
            return true;
        }
        return false;
    }

    public static boolean shouldUseFixChannel() {
        if (Build.VERSION.RELEASE.startsWith("4.0.")) {
            if (Build.MANUFACTURER.equals("samsung")) {
                return true;
            }
            if (Build.MANUFACTURER.equals("HTC")) {
                return true;
            }
        }
        if (Build.VERSION.RELEASE.startsWith("4.1.")) {
            if (Build.MANUFACTURER.equals("samsung")) {
                return true;
            }
        }
        if (Build.MANUFACTURER.equals("Xiaomi")) {
            if (Build.VERSION.RELEASE.equals("2.3.5")) {
                return true;
            }
        }
        return false;
    }

	public void connect(final BluetoothDevice device) {
		new Thread(){
			@TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
            public void run() {
				retryCount ++;
				remoteDevice  = device;
				try {
					releaseResource(); //先释放资源，保证连接

					//lvmu bind
					//if (shouldUseSecure()) {
					//	socket = device.createRfcommSocketToServiceRecord(MY_UUID);
					//}
					//else {
					//	socket = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
					//}

					if(shouldUseSecure()){
                        socket = device.createRfcommSocketToServiceRecord(MY_UUID);
                    }else {
                        if (shouldUseFixChannel()) {
                            Method m = device.getClass().getMethod("createInsecureRfcommSocket", new Class[]{int.class});
                            socket = (BluetoothSocket) m.invoke(device, 6);
                        } else {
                            int sdk = Build.VERSION.SDK_INT;
                            if (sdk >= 10) {
                                socket = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
                                //L.e("----...，使用createInsecureRfcommSocketToServiceRecord");
                            } else {
                                socket = device.createRfcommSocketToServiceRecord(MY_UUID);
                            }
                        }
                    }
					socket.connect();
					onStateChange(STATE_CONNECTED,-1, true);
					
					inputStream = socket.getInputStream();
					outputStream = socket.getOutputStream();
                    //发送对时的指令
                    BluetoothInstruct.checkTime();
                    onStateChange(STATE_CHECKTIMEING,-1, true);
                    //接收hub读取相关的数据
					receiveHubData();
					bluetoothConn = true; //蓝牙正常连接了
				} catch (Exception e) {
					e.printStackTrace();
					if(retryCount<1){ //小于2，再尝试重新连接一次
						connect(device);
					}else{
						onStateChange(STATE_NOTFOUND, -1, true);
						retryCount = 0 ;  //进行重置
						releaseResource(); //失败了，释放相应的资源
						//L.e("-->>connect  失败，，蓝牙设备不可连接");
					}
				}
			}
			
		}.start();
	}
	
	private void receiveHubData() {

		new Thread(){
			public void run() {
				if(null == inputStream){
					return ;
				}
				try {
					int temp = -1;
					byte b = 0; 
					boolean commandStart = false; //命令帧是否开始了
					boolean commandEnd = false;
					List<Byte> dataByteLists = new ArrayList<Byte>();
					
					byte[] dataSizeByte ;
					//add by lvmu 20150609 start
					byte[] dataSizeByteBuShu;
					//add by lvmu 20150609 end
					int dataLen = 0 ;//data数据的长度 只包括包括命令字及data数据
					MsgFrame msgFrame = null;
					//int count = 0 ; //目前已经读取命令的byte数
					while((temp = inputStream.read())!=-1){
						b = (byte)temp;
						if(!commandStart&&MsgFrame.getHeader() == b){ //若命令帧开始了
							//L.e("INDEX","受信帧开始");
							commandStart = true;
							commandEnd = false;
							dataByteLists.clear(); //新的一帧开始了，将容器清除，即去掉上一帧的数据
						}
						dataByteLists.add(b);
                        byte[] temppp = new byte[dataByteLists.size()];
                        for (int i=0;i<dataByteLists.size();i++){
                            temppp[i] = dataByteLists.get(i);
                        }
						//TODO 判断结束的帧，，比较麻烦，得计算当前的dataByteLists的长度是否OK了
//                        L.e("--->>收到hub发的数据000000000000000："+dataByteLists.size()+"  --"+ByteUtils.toHexString(temppp,0,temppp.length));
						//先获取dataSize数据，要求帧数据开始了但未结束，且数据必须等于3个byte，后面2个byte数据即是dataSize的数据
						//modify by lvmu 20150609 start
						//if(dataByteLists.size()==3&&commandStart&&!commandEnd){
						if(dataByteLists.size()==5&&commandStart&&!commandEnd){
						//modify by lvmu 20150609 end

							dataSizeByte = new byte[]{dataByteLists.get(1),dataByteLists.get(2)};
							dataSizeByte = ByteUtils.reverseSort(dataSizeByte);
							int byteNum = ByteUtils.parseByteToInt(dataSizeByte); //dataSize区域的byte的数值
							//add by lvmu 20150617 start
							if( byteNum < 6 )
							{
								dataByteLists.clear();
								commandStart = false;
								continue;
							}
							//add by lvmu 20150617 end

							dataLen = byteNum - 6; //dataSize包括2个dataSize补数、2个sequence、1个控制位、1个bcc补数，剩下就是data的数据了
							//add by lvmu 20150609 start
							dataSizeByteBuShu = new byte[]{dataByteLists.get(3),dataByteLists.get(4)};
							dataSizeByteBuShu = ByteUtils.reverseSort(dataSizeByteBuShu);
							int byteNumBuShu = ByteUtils.parseByteToInt(dataSizeByteBuShu);
							if( byteNumBuShu + byteNum != 0x10000 )
							{
								dataByteLists.clear();
								commandStart = false;
								continue;
							}
							//add by lvmu 20150609 end
						}
						int controllId = 0 ;
						byte[] dataBytes = null;
						int sequence = 0;

						if(commandStart&&!commandEnd&&(dataLen+10)==dataByteLists.size()){ //10个固定byte(包含开始标志结束标志、dataSize...)+dataLen
                            byte[] dataSizeBytes = new byte[]{dataByteLists.get(1),dataByteLists.get(2)};
                            byte[] dataSizeComplementBytes = ByteUtils.complementArray(dataSizeBytes);
                            //dataSizeComplementBytes = ByteUtils.reverseSort(dataSizeComplementBytes);
                            byte[] readDataSizeComplementBytes=  new byte[]{dataByteLists.get(3),dataByteLists.get(4)};
                            //L.e("--->收到的dataSizeBytes:"+ByteUtils.toHexString(dataSizeBytes,0,dataSizeBytes.length));
                            //L.e("--->求补得出dataSizeComplementBytes:"+ByteUtils.toHexString(dataSizeComplementBytes,0,dataSizeComplementBytes.length));
                            //L.e("--->而收到的补readDataSizeComplementBytes:"+ByteUtils.toHexString(readDataSizeComplementBytes,0,readDataSizeComplementBytes.length));
                            if(dataSizeComplementBytes[0]!=readDataSizeComplementBytes[0]||
                                    dataSizeComplementBytes[1]!=readDataSizeComplementBytes[1]){
                                commandStart = false;
                                commandEnd = false;
                                dataByteLists.clear();
                                BluetoothInstruct.repeatSendLastFrameDate();
                                //L.e("本条数据待丢掉，待另一开关数据来了直接丢掉--->收到的dataSizeBytes:"+ByteUtils.toHexString(dataSizeBytes,0,dataSizeBytes.length) +
                                //"--->求补得出dataSizeComplementBytes:"+ByteUtils.toHexString(dataSizeComplementBytes,0,dataSizeComplementBytes.length)+
                                //"--->而收到的补readDataSizeComplementBytes:"+ByteUtils.toHexString(readDataSizeComplementBytes,0,readDataSizeComplementBytes.length));
                                continue;
                            }

                            commandEnd = true;
							//L.e("INDEX","受信帧结束");
                            commandStart = false;

                            //第6个与第7个是sequence位数据

							//第6个与第7个是sequence位数据
							byte[] sequenceBytes = new byte[]{dataByteLists.get(5),dataByteLists.get(6)};
							sequenceBytes = ByteUtils.reverseSort(sequenceBytes);
							sequence = ByteUtils.parseByteToInt(sequenceBytes); //获取sequence数值
							//控制位是第8个byte
							controllId = dataByteLists.get(7); //获取控制位值
							
							dataBytes = new byte[dataLen];
							for(int i=0;i<dataLen;i++){
								dataBytes[i] = dataByteLists.get(8+i); //从第8个开始取数据
							}
						}
						
						if(commandEnd&&!commandStart){ //此帧数据刚结束的
							Runnable runnable = runnableMap.get(sequence);
							handler.removeCallbacks(runnable); //超时的取消执行
							msgFrame = new MsgFrame(controllId, dataBytes,sequence);
							byte[] commandMsg = msgFrame.getCommandMsg();
							//L.e("--->>收到hub发的数据："+ByteUtils.toHexString(commandMsg, 0, commandMsg.length) );
							Integer intCommandId = sequence2CommmandMap.get(sequence); //收到的sequence对应的是哪发的指令字
							int resCommandId = 0;
							if(null != intCommandId){  //乱响应时，有可能sequence响应的不对，就找不到commandID了为null了
								resCommandId = intCommandId;
							}
							
							//dataBytes响应回来的数据，若是大于等于2个byte的，则前两个byte是命令
							if(null != dataBytes && dataBytes.length>=2&&0x03==controllId){ //hub主动发的是控制位必须是3
								byte[] commandIdBytes = {dataBytes[1],dataBytes[0]}; //命令ID要反序
								resCommandId = ByteUtils.parseByteToInt(commandIdBytes);
								//L.e("--->>hub主动发出来的命令字："+resCommandId);

                                String dataByteStr = ByteUtils.toHexString(dataBytes, 0, dataBytes.length);
                                CommonUtils.writeLogDate("hub主动发出来的命令字:"+resCommandId+" 收到的dataByteStr:"+dataByteStr);
							}
							BluetoothInstruct.removeMsgFrameFromCache(sequence);
							//int resCommandId = sequence2CommmandMap.get(sequence);
							CommonUtils.writeLogDate("--->>收到hub发的数据："+ByteUtils.toHexString(commandMsg, 0, commandMsg.length)+" resCommandId:"+resCommandId );
							//L.e("--->>收到hub发的数据："+ByteUtils.toHexString(commandMsg, 0, commandMsg.length)+" resCommandId:"+resCommandId );
							switch (resCommandId) {
							case BluetoothInstruct.CHECK_TIME_COMMAND: //收到的对时指令应答
								L.e("INDEX","对时应答");
								int resInt = ByteUtils.parseByteToInt(dataBytes);//应答情况，若0代表是正常，1代表异常了
								if(0 == resInt){
									onStateChange(STATE_CHECKTIME_SUCCESS,sequence, true);
								}else if(1 == resInt){
									onStateChange(STATE_CHECKTIME_FAILURE,sequence, true);
									if(retryCount<2){ //小于2，再尝试重新连接一次
										connect(remoteDevice);
										L.e( "XXXXX_RECEIEVE_CONNECT" );
									}
								}
								break;
                            case BluetoothInstruct.SET_NAVI_COMMAND: //常时送信,,
								//L.e("INDEX","常时送信应答");
                                int resInt6 = ByteUtils.parseByteToInt(dataBytes);//应答情况，若0代表是正常，1代表异常了
                                if(0 == resInt6){
                                    onStateChange(STATE_FEQURENCYSENDMSG_SUCCESS,sequence, true);
                                }else if(1 == resInt6){
                                    onStateChange(STATE_FEQURENCYSENDMSG_FAILURE,sequence, true);
                                }
                                break;
							case BluetoothInstruct.SET_MESSAGE_COMMAND:
								int resInt2 = ByteUtils.parseByteToInt(dataBytes);//应答情况，若0代表是正常，1代表异常了
								if(0 == resInt2){
									onStateChange(STATE_BLUETOOTH_SETMESSAGE_SUCCESS,sequence, true);
								}else if(1 == resInt2){
									onStateChange(STATE_BLUETOOTH_SETMESSAGE_FAILURE,sequence, true);
								}else if(2 == resInt2){
                                    onStateChange(STATE_BLUETOOTH_SETMESSAGE_OBDCONING,sequence, true);
                                }
								break;
							case BluetoothInstruct.IMMEDIA_EXPERIENCE_COMMAND: //立即体验
								//L.e("INDEX","立即体检应答");
								int resInt3 = ByteUtils.parseByteToInt(dataBytes);//应答情况，若0代表是正常，1代表异常了
								if(0 == resInt3){
									onStateChange(STATE_IMMEDIA_EXPERIENCE_SUCCESS,sequence, true);
								}else if(1 == resInt3){
									onStateChange(STATE_IMMEDIA_EXPERIENCE_FAILURE,sequence, true);
								}else if(2 == resInt3){
									onStateChange(STATE_IMMEDIA_EXPERIENCE_NOTCONT_OBD,sequence, true);
								}
								break;
							case BluetoothInstruct.HITCH_COMMAND: //收到的故障的指令了，就是ok的
								// 等待解析故障码的命令，插入数据中，从那边读取出来
								//L.e("INDEX","故障码指令应答");
								CommonUtils.writeLogDate( "收到故障应答" );
								int hitNum = Hub2AppDateUtil.receiveHitch(msgFrame);
                                if(hitNum>0){
                                    onStateChange(STATE_HITCH_SUCCESS,sequence,true);
                                }else{
                                    onStateChange(STATE_HITCH_SUCCESS_NOHITCH,sequence,true);
                                }
                                BluetoothInstruct.responseSuccessCode(sequence);
                                //回一条数据给hub
								break;
							case BluetoothInstruct.DRIVE_SCORE_COMMAND: //直接回数据过来
								//L.e("INDEX","驾驶记录");
								CommonUtils.writeLogDate( "收到驾驶记录" );
								BluetoothInstruct.responseSuccessCode(sequence);
								if(Hub2AppDateUtil.receiveDriveScore(msgFrame)){
									onStateChange(STATE_DRIVE_SCORE_SUCCESS, sequence, true);
								}else{
									onStateChange(STATE_DRIVE_SCORE_FAILURE, sequence, true);
								}
                                //BluetoothInstruct.responseSuccessCode(sequence);
                                //L.e("--->驾驶评分 正式响应了...");
							 	/*int resInt4 = ByteUtils.parseByteToInt(dataBytes);//应答情况，若0代表是正常，1代表异常了
								if(0 == resInt4){
									onStateChange(STATE_GET_DRIVE_SCORE_SUCCESS,sequence, true);
								}else if(1 == resInt4){
									onStateChange(STATE_GET_DRIVE_SCORE_FAILURE,sequence, true);
								}*/

								break;
							default:
								break;
							}
						}
						
						//TODO 根据命令字进行处理，需要保证address至sp中
					}
				} catch (IOException e) {
					L.e("INDEX","受信处理异常");
					//L.e("INDEX","受信处理异常"+e.getCause().toString());
					e.printStackTrace();
				}
				
				//releaseResource();

				onStateChange(STATE_BLUETOOTH_SHUT_DOWN_HAND, -1, true); // 蓝牙被判断开了

			};
		}.start();

	}
	
	/**
	 * 释放相关的资源
	 */
	public void releaseResource() {
		bluetoothConn = false; //相关资源释放，则之前蓝牙肯定连接了
		
		if(bluetoothAdpater.isDiscovering()){
			bluetoothAdpater.cancelDiscovery();
		}
		
		try {
			if(null != socket){
				socket.close();
			}
			if(null != outputStream){
				outputStream.close();
			}
			if(null != inputStream){
				inputStream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	};
	
	/**
	 * 发送的数据给hub
	 * @param msgFrame  帧数据
	 * @param needRes  是否需要超时应答
	 */
	public void write(final MsgFrame msgFrame,final boolean needRes){

		new Thread(){
			public void run() {
				byte[] dataBytes = msgFrame.getCommandMsg();
				//L.e("-->待发送的数据："+ByteUtils.toHexString(dataBytes, 0, dataBytes.length));
				try{
					if(null != outputStream){
						outputStream.write(dataBytes);
						outputStream.flush();
					}
					if(needRes){
						Runnable r = new Runnable() {
							@Override
							public void run() {
								onStateChange(STATE_OVERTIME_RES,-1, true);
							}
						};
						runnableMap.put(msgFrame.getSequence(), r);
						handler.postDelayed(r, REQ_DELAYMILLIS);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			};
		}.start();

	}
	
	private void onStateChange(int state,int what, boolean b) {
		if(b){ //&&state!=lastState
			for(IBluetoothListener listener:listenerList){
				listener.onStateChange(state,what);
			}
		}
		lastState = state;
	}
	
	private void onScanningDevice(BluetoothDevice device) {
		for(IBluetoothListener listener:listenerList){
			listener.onScanningDevice(device.getName(), device.getAddress(), device.getBondState());
		}
	}
	
	
	class BluetoothBroadcastreceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {

			BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			String action = intent.getAction();
			if(BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)){
				switch (bluetoothAdpater.getState()) {
				case BluetoothAdapter.STATE_ON: //蓝牙开启了，则去尝试连接
					//判断之前有没有连接成功的设备地址，有则直接去连接了。没有则开启扫描模式，查找合适的蓝牙设备
					//add by lvmu 20150623 start
					onStateChange(STATE_FIRST_KIDOU_BT_OPEN, -1,true );
					//add by lvmu 20150623 end

					searchFindDevice();
					break;
				case BluetoothAdapter.STATE_TURNING_OFF: //正在关闭蓝牙，则去释放相关的资源
					releaseResource();
					break;
				case BluetoothAdapter.STATE_OFF: //蓝牙关闭了，通知相关监听者
					onStateChange(STATE_BLUETOOTH_OFF,-1, true);
					break;
				default:
					break;
				}
			}
			//add by lvmu 20150619 start
			else if( device != null && device.getName()!=null && !(device.getName().equals(BluetoothManager.BLUETOOTH_NAME) ) )
			{
				return;
			}
			//add by lvmu 20150619 end

			else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
				//  正在扫描蓝牙设备中...，通知相关监听者
				onStateChange(STATE_DEVICE_DISCOVERING, -1, true);
			}else if(BluetoothDevice.ACTION_FOUND.equals(action)){
				//  通知相关监听者，扫描到的蓝牙设备
				//L.e("-->found device ...:"+device.getAddress()+" "+device.getName());
				onScanningDevice(device);
			}else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
				//  扫描蓝牙设备结束...，通知相关监听者
				onStateChange(STATE_DISCOVERED, -1, true);
                //L.e("-->found device ...:--ACTION_DISCOVERY_FINISHED");
				
			}else if ("android.bluetooth.device.action.PAIRING_REQUEST".equals(action)){
				//进行PIN码 验证
				try {
					BluetoothPairUtils.setPin(device.getClass(), device, "0000");
					//onStateChange(STATE_SETPIN, -1, true);
					//BluetoothPairUtils.createBond(device.getClass(), device);
					BluetoothPairUtils.cancelPairingUserInput(device.getClass(), device);
					//L.e("---->setpin PAIRING_REQUEST----");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else if(BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)){
				switch (device.getBondState()) {
				case BluetoothDevice.BOND_BONDING: //正在匹对绑定中...
					//L.e("---->正在匹对绑定中...");

					break;
				case BluetoothDevice.BOND_BONDED:
					//L.e("---->匹对绑定完成，准备尝试进行连接 connect");
					try {
						Thread.sleep(100); //尝试 休眠100毫秒
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					// 准备进行连接
					L.e("XXXXXXX_CONNECT");
					connect(device);
					break;
				case BluetoothDevice.BOND_NONE:
					//L.e("---->匹对绑定失败...");
					// 通知 相关的监听者
					onStateChange(STATE_BLUETOOTH_PAIRE_FAILURE,-1, true);
					break;
				default:
					break;
				}
			}else if(BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)){
				//L.e("---->连接断开...");
				releaseResource();
				//通知 相关的监听者
				onStateChange(STATE_BLUETOOTH_DISCONNECT,-1, true);
			}
			//log lvmu
			L.e(action);
			//log lvmu
			
		}

	}

	public Map<Integer, Integer> getSequence2CommmandMap() {
		return sequence2CommmandMap;
	}


    /**
     * 蓝牙是否被打开了
     * @return
     */
    public boolean isBluetoothEnable(){
        return null!=bluetoothAdpater && bluetoothAdpater.isEnabled();
    }

    /**
     * 是否支持 蓝牙模块
     * @return
     */
    public boolean isSupportBluetooth(){
        return bluetoothAdpater==null ? false:true;
    }

}
