package com.sheep.framework.view;
/*
 */

import android.content.Context;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.MovementMethod;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class KeyOrHandSetEditText extends EditText {
    private boolean isHandSet;

    public KeyOrHandSetEditText(Context context) {
        super(context);
    }

    public KeyOrHandSetEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KeyOrHandSetEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public final void setHandText(CharSequence text) {
        isHandSet = true;
        setText(text);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        isHandSet = false;
        return super.onKeyDown(keyCode, event);
    }

    public boolean isHandSet() {
        return isHandSet;
    }
}
