package com.sheep.framework.view;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  xml view的相应的id及响应事件
 * @author chensf5
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ViewInject {

	public int id();
	
	public String onClick() default "";
	
	public String onLongClick() default "";
	
	public String onItemClick() default "";
	
	public String onItemLongClick() default "";
	
	//public String onItemSelected() default "";
	
	public Select select() default @Select(select="");
	
}
