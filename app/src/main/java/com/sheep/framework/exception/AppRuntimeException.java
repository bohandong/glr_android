package com.sheep.framework.exception;

public class AppRuntimeException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AppRuntimeException(String msg){
		super(msg);
	}

}
