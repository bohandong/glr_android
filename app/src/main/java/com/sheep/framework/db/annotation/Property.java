package com.sheep.framework.db.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * bean 中的属
 * 对应
 * sqlite的字段名及默认的
 *
 * @author chensf5
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME) 
public @interface Property {
	 public String column() default "";
	 public String defaultValue() default "";
}