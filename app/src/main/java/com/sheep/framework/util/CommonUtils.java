package com.sheep.framework.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.widget.Toast;

import com.sheep.framework.log.L;
import com.sheep.hub.HubApp;
import com.sheep.hub.dialog.CustomProgressDialog;

public class CommonUtils {

    /**
     * 从Assets资源文件中，获取文件流数据...
     *
     * @param context
     * @param fileName
     * @return
     */
    public static byte[] getAssetsFileDate(Context context, String fileName) {
        AssetManager am = context.getResources().getAssets();
        InputStream is = null;
        try {
            is = am.open(fileName);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int len = -1;
            while ((len = is.read(buf)) != -1) {
                bos.write(buf, 0, len);
            }
            is.close();
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 获得网络连接是否可用
     *
     * @param context
     * @return
     */
    public static boolean hasNetwork(Context context) {
        ConnectivityManager con = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo workinfo = con.getActiveNetworkInfo();
        if (workinfo == null || !workinfo.isAvailable()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 正则表达式 判断电话号是否正确
     */
    /*public static boolean isPhoneHomeNum(String num) {
        String regPhoneNum = VehicleApp.getInstance().getResources().getString(R.string.util_reg_phoneandtelphone_num);
		Pattern p = Pattern.compile(regPhoneNum);
		Matcher m = p.matcher(num);
		return m.matches();
	}*/

    /**
     * 距离m转化为km
     *
     * @param meterStr 距离(m)
     * @return
     */
    public static String getStringMeter2KM(int meterStr) {
        if (meterStr <= 1000) {
            return meterStr + "m";
        } else {
            int limit = 50 * 1000;
            if (meterStr <= limit) {
                DecimalFormat df = new DecimalFormat("0.0");
                Double floatDistance = (Double) (meterStr / 1000.00);
                return df.format(floatDistance) + "km";
            } else {
                return meterStr / 1000 + "km";
            }

        }
    }

    public static String date2StringTime(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public static String date2StringTime(String datetime) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Long.parseLong(datetime));
    }

    /**
     * 吐司对话框
     */
    public static void showShortToast(Context context, String str) {
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast(Context context, String str) {
        Toast.makeText(context, str, Toast.LENGTH_LONG).show();
    }

    public static String readInStream(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuffer sb = new StringBuffer();
        String line = null;
        try {
            while ((null != (line = reader.readLine()))) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static ProgressDialog showProgressDialog(Context context, int messageId) {
        ProgressDialog progressDialog = new CustomProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(true);
        progressDialog.setMessage(context.getString(messageId));
        progressDialog.show();
        return progressDialog;
    }

    public static ProgressDialog showHorizontalProgressDialog(Context context, int messageId, int titleId) {
        return showHorizontalProgressDialog(context, context.getString(messageId), context.getString(titleId));
    }

    public static ProgressDialog showHorizontalProgressDialog(Context context, String message, String title) {
        ProgressDialog progressDialog = new CustomProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(true);
        progressDialog.setMessage(message);
        progressDialog.setTitle(title);
        progressDialog.show();
        return progressDialog;
    }

    public static boolean isSDCardAvailable() {
        if (android.os.Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public static final int isPackageExists(Context context, String packageName) {
        if (packageName == null || "".equals(packageName))
            return 0;
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            //String pn = info.packageName;
            String strVersion = info.versionName;
            //L.e("------------ strVersion:" + strVersion);
            int versionNum = Integer.valueOf(strVersion.replace(".", ""));

            if (versionNum > 451) {
                return 2;
            } else {
                return 1;
            }
            //L.e("------------ pn:"+pn+"  info.versionName:"+info.versionName);
            //return true;
        } catch (NameNotFoundException e) {
            return 0;
        }
    }

    public static final boolean isGPSOPen(Context context) {
        LocationManager locationManager
                = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）
//        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//        if (gps || network) {
//            return true;
//        }
//        return false;
    }

    public static final boolean isWifiMode(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        if (null != info && info.getType() == ConnectivityManager.TYPE_WIFI) {
            return true;
        }
        return false;
    }



    public static final void writeLogDate(String data) {
        //HubApp.getInstance().getExternalCacheDir()
        if (!Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            return;
        }

        String dataTime = date2StringTime(new Date());
        data = dataTime + ":" + data + "\r\n";
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/hud/log/";
        File file = new File(path, "hudlog.txt");
        if (!file.exists()) {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //L.e("----->> hudlog:被创建");
        } else {
            //L.e("----->> hudlog:被创建");
        }
        //L.e("----->> hudlog:准备写入文本");
        BufferedWriter out = null;      try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true)));
            out.write(data);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
